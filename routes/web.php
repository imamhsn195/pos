
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/* Route::any('{url_param}', function() {
    abort(403, '404 Error. Page not found!');
})->where('url_param', '.*'); */

Route::post('pdfreport','SaleController@pdf')->name('pdfreport'); //PDF report test

Route::get('/reset',function ()
{
	return view('auth.passwords.reset');
});
Route::get('/user/registration', 'UserController@user_register')->name('user.registration');
Route::post('/user/registration', 'UserController@user_register_store')->name('user.registration');

// Auth::routes('register');

 // Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
// Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
// Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

Route::get('/laravel_chart', 'ChartDataController@getMonthlyPostData');

Route::group(['middleware'=>'auth'],function(){
	Route::get('/', 'HomeController@index')->name('/');
	Route::get('profile','UserController@profile')->name('profile');
	Route::post('profile.update','UserController@update_avatar')->name('profile.update');
	Route::resource('warranty_type','WarrantyTypeController');
	Route::resource('categories','CategoryController');
	
	Route::resource('branches','BranchController');
	
	Route::resource('suppliers','SupplierController');
	Route::post('supplier_beginning_balance','SupplierController@supplierBeginningBalance');
	Route::resource('sales_man','SalesManController');
	Route::get('show_invoice/{id}','SupplierController@show_invoice')->name('show_invoice');
	Route::get('monthly_status_of_supplier','SupplierController@monthlyStatusOfSupplier')->name('monthly_status_of_supplier');
	Route::resource('customer_groups','CustomerGroupController');
	Route::resource('customers','CustomerController');
	Route::get('monthly_status_of_customer','CustomerController@monthlyStatusOfCustomer')->name('monthly_status_of_customer');
	Route::post('customer_beginning_balance','CustomerController@customerBeginningBalance');
	Route::get('customer_invoice_show/{id}','CustomerController@customer_invoice_show')->name('customer_invoice_show');
	Route::resource('attribute_sets','AttributeSetController');
	Route::resource('attributes','AttributeController');
	Route::resource('vat','VatController');
	Route::resource('brand','BrandController');
	Route::resource('bundle_product','BundleProductController');
	Route::post('change_status/{id}','BundleProductController@change_status')->name('change_status');
	Route::get('getPrice/{id}','BundleProductController@get_price');
	//Route::resource('bundle_product_details','BundleProductDetailController');
	Route::resource('transactions','TransactionController');
	Route::get('transaction_status_show/{ridtype}','TransactionController@transaction_status_show')->name('transaction_status_show');
	
	Route::resource('products','ProductController');
	Route::get('barcode','ProductController@barcodeget')->name('barcode');
	Route::get('product_tracking','ProductController@productTracking')->name('product_tracking');
	Route::post('barcode','ProductController@barcodepost')->name('barcode');
	Route::resource('expire_products','ExpirableProductDetailController');
	Route::get('datatable','PurchaseController@getIndex');
	Route::get('datatabledata','PurchaseController@anyData');
	Route::get('show_invoice_details/{invoice_no}','PurchaseController@show_invoice_details')->name('show_invoice_details');
	Route::resource('purchases','PurchaseController');
	Route::get('purchase_history/{id}','BranchInventoryController@purchase_history')->name('purchase_history');
	Route::resource('trial_balance','AccountController');
	Route::get('income_report','AccountController@income_report');
	Route::resource('purchase_returns','PurchaseReturnController');
	Route::resource('journals','JournalController');
	Route::get('attributes_set_ajaxdata/{attr_set_id}','AttributeSetController@ajaxDataAttr');
	Route::get('warranty_type_data','WarrantyTypeController@warrenty_data');
	Route::resource('payment_method','PaymentMethodController');
	Route::post('add_payment_method_balance','PaymentMethodController@add_payment_method_balance');

	Route::resource('sales','SaleController');
	Route::resource('sales_return','SaleReturnController');
	Route::get('sales_report','SaleController@sales_report')->name('sales_report');
	Route::get('purchase_history_for_return','PurchaseReturnController@purchase_history_for_return');
	Route::get('sales_history_for_return','SaleReturnController@sales_history_for_return');
	Route::get('sales_details_report/{id}','SaleController@sales_details_report')->name('sales_details_report');
	Route::get('get_customer','SaleController@get_customer')->name('get_customer');
	Route::get('productSearch/{searchBy}','SaleController@productSearch')->name('productSearch');


	Route::get('add_to_cart/{productid}/{purchaseid}/{cust_group_id?}','SaleController@add_item_to_cart')->name('add_to_cart');
	Route::get('hold_customer_cart','SaleController@holdCustomerCart')->name('hold_customer_cart');
	Route::get('hold_back_customer_cart','SaleController@holdBackCustomerCart')->name('hold_back_customer_cart');
	Route::get('add_to_cart_bundle/{productid}','SaleController@add_bundle_item_to_cart')->name('add_to_cart_bundle');
	Route::get('update_cart_quentity/{rowid}/{qty}','SaleController@update_cart_quentity')->name('update_cart_quentity');
	Route::get('update_cart_discount/{coupon_no}','SaleController@update_cart_discount')->name('update_cart_discount');
	Route::get('set_customer_info/{customer_search_input}','SaleController@set_customer_info')->name('set_customer_info');
	Route::get('remove_item/{rowid}','SaleController@remove_item')->name('remove_item');
	Route::get('payment_model/{hello}','SaleController@returnCartTable')->name('payment_model');
	Route::get('view_product_details/{product_id}','SaleController@view_product_details')->name('view_product_details');
	Route::get('product_details/{product_id}','ProductController@get_product_details')->name('product_details');
	Route::get('invoice_details/{invoice}','PurchaseController@get_invoice_details')->name('invoice_details');
	Route::get('view_bundle_product_details/{product_id}','SaleController@view_bundle_product_details')->name('view_bundle_product_details');

	Route::resource('inventories','BranchInventoryController');
	Route::get('inventories_cart/{id}','BranchInventoryController@addToCart');

	Route::resource('discount_offers',"DiscountOfferController");
	Route::resource('customer_criterias',"CustomerCriteriaController");
	Route::resource('cheque_infos',"ChequeInfoController");
	Route::post('change_criteria_status/{id}','CustomerCriteriaController@criteria_status')->name('change_criteria_status');
	Route::post('change_discount_status/{id}','DiscountOfferController@discount_status')->name('change_discount_status');
	Route::get('find_purchase_id/{product_id}','DiscountOfferController@ajaxpurchaseid')->name('find_purchase_id');
	Route::get('find_product_for_discount/{product_id}/{invoice_no}','DiscountOfferController@find_product_for_discount')->name('find_product_for_discount');
	Route::post('change_user_status/{id}','HomeController@change_status')->name('change_user_status');
	Route::resource('warranty_product_detail','WarrantyProductDetailController');
	Route::get('expense_show','PrimarySettingsController@expenseShow')->name('expense_show');
	Route::get('payment_show','TransactionController@paymentShow')->name('payment_show');
	Route::get('record_expense','PrimarySettingsController@expenseCreate')->name('record_expense');
	Route::resource('primary_settings','PrimarySettingsController');
	Route::resource('invoice_setting','InvoiceSettingController');
	//Route::get('find_purchase_details/{product_purchase_id}','DiscountOfferController@ajaxpurchasedetails');
	//Route::get('sales/test/{sale}','SaleController@show')->name('sales.show');
	Route::get('/laravel_google_chart', 'HomeController@googlepiechart');
	Route::get('inventory_details/{id}','HomeController@show')->name('dashboard.branch');
});