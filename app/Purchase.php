<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    public function users() {
        return $this->belongsTo('App\User','created_by');
    }
    public function supplier() {
        return $this->belongsTo('App\Supplier','supplier_id');
    }
    public function product() {
        return $this->belongsTo('App\Product','product_id');
    }
    public function purchase_return() {
        return $this->hasMany('App\Purchase_return','purchase_id');
    }
    public function branch() {
        return $this->belongsTo('App\Branch','branch_id');
    }
    public function product_status() {
        return $this->hasOne('App\Branch_inventory','purchase_id');
    }
    public function bundle_product_details(){
        return $this->hasMany(Bundle_product_detail::class,'purchase_id');
    }
    public function product_warranty_types(){
        return $this->hasMany(Bundle_product_detail::class,'purchase_id');
    }
    public function warranty_types(){
        return $this->hasMany(Product_warranty_type::class);
    }
    public function inventories()
    {
        return $this->hasMany('App\Branch_inventory', 'purchase_id');
    }
    
    public function warranty_product_details()
    {
        return $this->hasMany('App\Warranty_product_detail', 'purchase_id');
    }
    public function sales_details()
    {
        return $this->hasMany('App\Sale_detail', 'purchase_id');
    }
    
    
}
