<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Account;
use DB;
use Artisan;
use Symfony\Component\Console\Helper\ProgressBar;

class InstallBridge extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bridge:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Initial setup bridge application, database migration:fresh with seed';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {


        $this->confirm('All previous data from database will be removed. Are you sure to run this command?');
        Artisan::call("migrate:fresh");
        DB::beginTransaction();
        $database_file = storage_path("/json/database.json"); // ie: app/storage/json/database.json
        $database_tables = json_decode(file_get_contents($database_file), true); 
        $progress = $this->output->createProgressBar(count($database_tables));
        $progress->setFormat("%current%/%max% [%bar%] %percent:3s%% - %message%");
        $progress->start();
        try {
            foreach($database_tables as $database_table){
                    sleep(2);
                    $progress->setMessage("Inserting ".$database_table['table']." table data.");
                    DB::table($database_table['table'])->insert($database_table['data']);
                    $progress->setMessage("Inserted ".$database_table['table']." table data.");
                    $progress->advance();
                }
            DB::commit();
            $progress->finish();
            $this->info("\nAll initial data seeded successfully!");
        } catch (\Exception $e) {
            DB::rollback();
            $this->error('\n'.$e->getMessage());
            $this->info('Rollbacked database');
        }
    
    }
        
}