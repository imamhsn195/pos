<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product_warranty_type extends Model
{
   public function purchase(){
      return $this->belongsTo(Purchase::class);
   }

   public function warranty_type(){
      return $this->belongsTo(Warranty_type::class,'warranty_type_id');
   }
}
