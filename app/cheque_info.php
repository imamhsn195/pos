<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cheque_info extends Model
{
    public function users() {
        return $this->belongsTo('App\User','created_by');
    }
    public function transaction() {
        return $this->belongsTo('App\Transaction','transaction_id');
    }
    public function account() {
        return $this->belongsTo('App\Account','accounts_id');
    }
    public function paymentmethod() {
        return $this->belongsTo('App\Payment_method','payment_method_id');
    }
    public function supplier() {
        return $this->belongsTo('App\Supplier','related_party_id');
    }
    public function customer() {
        return $this->belongsTo('App\Customer','related_party_id');
    }
    public function branch(){
        return $this->belongsTo('App\Branch','branch_id');
    }
}
