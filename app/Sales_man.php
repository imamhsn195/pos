<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sales_man extends Model
{
    function sales()
	{
		return $this->belongsToMany('App\Sale','sale_sales_man','sale_id','sales_man_id')->withTimestamps();
	}
}
