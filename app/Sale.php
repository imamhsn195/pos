<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    //A sale record belongs to a customer
    function customer()
    {
    	return $this->belongsTo('App\Customer','customer_id');
    }
    function branch()
    {
    	return $this->belongsTo('App\Branch','branch_id');
    }
    function user()
    {
    	return $this->belongsTo('App\User','created_by');
    }
    function discount_offer_details()
	{
		return $this->belongsTo('App\Discount_offer','discount_id');
	}
    function sale_details()
	{
		return $this->hasMany('App\Sale_detail','sales_id');
	}
    function transaction()
	{
		return $this->hasOne('App\Transaction','event_id')->where('event_type','=','sales');
	}
    function sales_men()
	{
		return $this->belongsToMany('App\Sales_man','sale_sales_man','sale_id','sales_man_id')->withTimestamps();
	}
}
