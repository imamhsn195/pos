<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branch_inventory extends Model
{
    
    public function user(){
        return $this->belongsTo('App\User','created_by');
    }

    public function purchase(){
        return $this->belongsTo('App\Purchase','purchase_id');
    }

    public function branch(){
        return $this->belongsTo('App\Branch','branch_id');
    }

    public function product(){
        return $this->belongsTo('App\Product','product_id');
    }
}
