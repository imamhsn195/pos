<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sale_return extends Model
{
    function sale_details()
	{
		return $this->belongsTo('App\Sale_detail','sale_details_id');
    }
    public function users() {
        return $this->belongsTo('App\User','created_by');
    }
}
