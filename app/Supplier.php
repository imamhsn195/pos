<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
     public function address() {
        return $this->belongsTo('App\Address_info','address_info_id');
    }
    public function journal() {
        return $this->hasMany('App\Journal','related_party_id');
    }
    public function purchases()
    {
        return $this->hasMany('App\Purchase','supplier_id');
    }
    
}
