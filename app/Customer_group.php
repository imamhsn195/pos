<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer_group extends Model
{
    public function users() {
		return $this->belongsTo('App\User','created_by');
	}


	public function discount_offers() {
		return $this->belongsToMany('App\Discount_offer')->withTimestamps();
	}

	public function bundle_products(){
		return $this->belongsToMany(Bundle_product::class)->withTimestamps();

	}
}
