<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment_method extends Model
{
    public function user(){
    	return $this->belongsTo(User::class,'created_by');
    }
    
    public function journals()
    {
        return $this->hasMany(Journal::class, 'payment_method_id');
    }
    
}
