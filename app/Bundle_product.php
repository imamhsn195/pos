<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bundle_product extends Model
{
    public function user(){
		return $this->belongsTo(User::class,'created_by');
	}

	public function bundle_product_details(){
		return $this->hasMany(Bundle_product_detail::class,'bundle_products_id');
	}
	//get 
	// public function bundle_product_stock(){
	// 	return $this->hasManyThrough(Branch_inventory::class,Bundle_product_detail::class,'bundle_products_id','purchase_id','purchase_id','purchase_id')->where('stock_quantity','>',0);
	// }

	public function customer_groups(){
		return $this->belongsToMany(Customer_group::class)->withTimestamps();
	}

	public function customer_criterias(){
		return $this->belongsToMany(CustomerCriteria::class)->withTimestamps();
	}
}
