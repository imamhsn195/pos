<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    function journals()
	{
		return $this->hasMany('App\Journal','transaction_id');
	}
	function user()
    {
    	return $this->belongsTo('App\User','created_by');
	}
	public function branch(){
        return $this->belongsTo('App\Branch','branch_id');
    }
	public function representative(){
        return $this->hasOne('App\Representative','transaction_id');
    }
}
