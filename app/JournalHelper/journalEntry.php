<?php

namespace App\JournalHelper {
    use App\Journal;
    use Illuminate\Support\Facades\Auth;
    class journalEntry {
        public function journal_entry($transaction_id,$related_party_id,$related_party_type,$journal_type,$accounts_id,$payment_method,$description,$amount,$branch_id)
        {
            $journal=new Journal;
            $journal->transaction_id=$transaction_id;
            $journal->created_by=Auth::id();
            $journal->branch_id=$branch_id;
            $journal->related_party_id=$related_party_id;
            $journal->related_party_type=$related_party_type;
            $journal->journal_type=$journal_type; //0->dr & 1->cr.
            $journal->accounts_id=$accounts_id;
            $journal->amount=$amount;
            $journal->payment_method_id=$payment_method;
            $journal->description =$description;
            $journal->save();
            return $journal->id;
        }
    }//endClass
}
?>