<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerCriteria extends Model
{

   public function discount_offers() {
		return $this->belongsToMany('App\Discount_offer')->withTimestamps();
	}

    public function bundle_products(){
        return $this->belongsToMany(Bundle_product::class)->withTimestamps();
    }

}
