<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    public function users() {
        return $this->belongsTo('App\User','created_by');
    }
	public function attributeSet() {
        return $this->belongsToMany('App\Attribute_set')->withTimestamps();
    }
    public function productvalue() {
        return $this->belongsToMany('App\Product')->withTimestamps();
    }
    public function attributeVal() {
        return $this->hasMany('App\Attribute','parent_id');
    }
}
