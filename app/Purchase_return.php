<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchase_return extends Model
{
    public function purchase() {
        return $this->belongsTo('App\Purchase','purchase_id');
    }  
    public function users() {
        return $this->belongsTo('App\User','created_by');
    }  
    
}
