<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bundle_product_detail extends Model
{
    public function product(){
    	return $this->belongsTo(Product::class);
    }

    public function purchase(){
    	return $this->belongsTo(Purchase::class,'purchase_id');
    }
    public function inventory(){
    	return $this->hasOne(Branch_inventory::class,'purchase_id','purchase_id');
    }

   
}
