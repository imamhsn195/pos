<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    // a user can have a Address
    public function address() {
        return $this->belongsTo('App\Address_info','address_info_id');
    }
    public function inventories() {
        return $this->hasMany('App\Branch_inventory');
    }
}
