<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sale_detail extends Model
{
    // A Sale details belongs to Sale
	function sale()
	{
		return $this->belongsTo('App\Sale','sales_id');
	}
	function sales_return()
	{
		return $this->hasMany('App\Sale_return','sale_details_id');
	}
    // A Sale details belongs Product
	function product()
	{
		return $this->belongsTo('App\Product','product_id');
	}
	function bundle()
	{
		return $this->belongsTo('App\Bundle_product','discount_id');
	}
	function serial_numbers()
	{
		return $this->hasMany('App\Warranty_product_detail','sale_details_id');
	}
    // Subtotal counting function
	function getSubtotal()
	{
		$qty = $this->sold_quantity;
		$price = $this->sold_unit_price;
		return $qty * $price;
	}
	

}