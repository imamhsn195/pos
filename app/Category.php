<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

	public function user()
	{
		return $this->belongsTo('App\User', 'created_by');
	}
	
	//Child Category
	public function childCategory()
	{
		return $this->hasMany('App\Category', 'parent_id');
	}
	//Parent Category
	public function parentCategory()
	{
		return $this->belongsTo('App\Category', 'parent_id');
	}
	public function products()
	{
		return $this->hasMany('App\Product', 'category_id');
	}
	

}
