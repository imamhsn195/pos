<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    public function user(){
		return $this->belongsTo(User::class,'created_by');
	}
	
	public function child_brands(){
		return $this->hasMany(Brand::class,'parent_id');
	}
}
