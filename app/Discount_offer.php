<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Discount_offer extends Model
{
    public function customer_groups(){
        return $this->belongsToMany(Customer_group::class)->withTimestamps();
    } 
	
	public function customer_criterias(){
        return $this->belongsToMany(CustomerCriteria::class)->withTimestamps();
    }
	
	public function product(){
        return $this->belongsTo('App\Product','product_id');
    }
}
