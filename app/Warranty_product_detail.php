<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Warranty_product_detail extends Model
{
    public function purchase(){
        return $this->belongsTo(Purchase::class,'purchase_id');
    }

    public function user(){
        return $this->belongsTo(User::class,'created_by');
    }
    public function sales_detail(){
        return $this->belongsTo(Sale_detail::class,'sale_details_id');
    } 
}
