<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attribute_set extends Model
{
    public function attribute() {
        return $this->belongsToMany('App\Attribute')->withTimestamps();
    }
	public function users() {
        return $this->belongsTo('App\User','created_by');
    }
}
