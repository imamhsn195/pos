<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Warranty_type extends Model
{
    //Warranty Created By User
	public function user()
	{
		return $this->belongsTo('App\user','created_by');
	}
	//Child Warranty
	public function childWarranty(){
		return $this->hasMany('App\Warranty_type','parent_id');
	}
	//Parent Warranty
	public function parentWarranty()
	{
		return $this->belongsTo('App\Warranty_type','parent_id');
	}
	//Warranty type has many categories
    public function categories()
    {
        return $this->hasMany('App\Category');
    }
	//Warranty type has many Product Type Warranty
	public function product_warranty_types()
    {
        return $this->hasMany('App\Product_warranty_type');
    }

}
