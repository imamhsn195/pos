<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    
    public function submittedattribute() {
        return $this->belongsToMany('App\Attribute')->withTimestamps();
    }
    public function brand() {
        return $this->belongsTo('App\Brand','brand_id');
    }
    public function category() {
        return $this->belongsTo('App\Category','category_id');
    }
    public function vat() {
        return $this->belongsTo('App\Vat','vat_id');
    }
    public function attributeset() {
        return $this->belongsTo('App\Attribute_set','attribute_set_id');
    }
    public function users() {
        return $this->belongsTo('App\User','created_by');
    }

    public function bundle_product_details(){
        return $this->hasMany(Bundle_product_detail::class,'product_id');
    }
    public function purchases(){
        return $this->hasMany(Purchase::class,'product_id');
    }

    public function sales_details(){
        return $this->hasMany(Sale_detail::class,'product_id');
    }
    public function inventory(){
        return $this->hasMany(Branch_inventory::class,'product_id');
    }
    public function discount(){
        return $this->hasMany(Discount_offer::class,'product_id');
    }
}
