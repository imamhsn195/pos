<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expirable_product_detail extends Model
{
    public function purchase(){
        return $this->belongsTo('App\Purchase','purchase_id');
    }
}
