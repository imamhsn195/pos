<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    public function journals(){
        return $this->hasMany(Journal::class,'accounts_id');
    }
}
