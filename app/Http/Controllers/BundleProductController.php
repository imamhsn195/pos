<?php

namespace App\Http\Controllers;

use App\Branch_inventory;
use App\Bundle_product;
use App\Bundle_product_detail;
use App\Product;
use App\Purchase;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Customer_group;
use App\CustomerCriteria;
use File;

class BundleProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $bundle_name = "";
        if(isset($_GET['bundle_name'])){
            $bundle_name = $_GET['bundle_name'];
        }
		$bundle_products = Bundle_product::where('bundle_name','LIKE',"%$bundle_name%")->latest()->paginate(5);
        return view('admin.bundle_product.index',compact('bundle_products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
        $products=Branch_inventory::where([['stock_quantity','>',2]])->orderBy('product_id')->get();
        $customer_groups = Customer_group::all();
        $customer_criterias = CustomerCriteria::all();
    
        return view('admin.bundle_product.create',compact('products','customer_groups','customer_criterias'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
			'bundle_price' => 'required',
            'bundle_name' => 'required',
		]);
        
        $image = $request->file('bundle_image');
        if($image){
            $path='upload/bundle_product/';
            $name=uniqid()."-".time().".".$image->getClientOriginalExtension();
            $image_url=$path.$name;
            $image->move($path,$image_url);
         }else{
            $image_url = 'upload/product_img/default.png';
         }
        

        $customerGroup = $request->customerGroup;
        $customerCriteria = $request->customerCriteria;
		$bundle_product = new Bundle_product;
        $bundle_product->bundle_name = $request->bundle_name;
        $bundle_product->bundle_image = $image_url;
        $bundle_product->bundle_discount_amount = $request->suggested_price -              $request->bundle_price;
		$bundle_product->bundle_price = $request->bundle_price;
		$bundle_product->created_by = Auth::id();
        $bundle_product->save();
        
        $bundle_id=$bundle_product->id;

        
        
        $qty = $request->qty;
        $product_id = $request->product_id;
        $purchase_id = $request->purchase_id;
        $a = 0;
        $b = 0;
        $count = count($qty);
        while($b < $count){
            if(!$qty[$b]==null){
                $bundle_product_detail = new Bundle_product_detail;
                $bundle_product_detail->created_by = Auth::id();
                $bundle_product_detail->bundle_products_id = $bundle_id;
                $bundle_product_detail->product_id =$product_id[$b];
                $bundle_product_detail->purchase_id =str_before($purchase_id[$b]," | ");
                $bundle_product_detail->quantity =$qty[$b];
                $bundle_product_detail->save();
            }
            $b++;

        }

        if($customerGroup){
            $bundles = Bundle_product::find($bundle_id);
            $bundles->customer_groups()->attach($customerGroup);
        }
        if($customerCriteria){
            $criteria = Bundle_product::find($bundle_id);
            $criteria->customer_criterias()->attach($customerCriteria);
        }
        


		return redirect()->route('bundle_product.index')->with('status','Bundle Product Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Bundle_product  $bundle_product
     * @return \Illuminate\Http\Response
     */
    public function show(Bundle_product $bundle_product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Bundle_product  $bundle_product
     * @return \Illuminate\Http\Response
     */
    public function edit(Bundle_product $bundle_product)
    {
        $products=Branch_inventory::where([['stock_quantity','>',2]])->orderBy('product_id')->get();
        $customer_groups = Customer_group::all();
        $customer_criterias = CustomerCriteria::all();   
        return view('admin.bundle_product.edit',compact('bundle_product','products','customer_groups','customer_criterias'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bundle_product  $bundle_product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Bundle_product $bundle_product)
    {
        return $bundle_product;
        $this->validate($request,[
            'bundle_price' => 'required',
            'bundle_name' => 'required',
        ]);
        $last_id = $bundle_product->id;
        $bundle_product_details = Bundle_product::find($last_id)->bundle_product_details;
        $bundle_product->bundle_price = $request->bundle_price;
        $bundle_product->bundle_name = $request->bundle_name;
        $bundle_product->update();

        
        $qty = $request->qty;
        $product_id = $request->product_id;
        $purchase_id = $request->purchase_id;
        $a = 0;
        $b = 0;
        $count = count($bundle_product_details);
        foreach($bundle_product_details as $bundle_product_detail){
            
                $bundle_product_detail->created_by = Auth::id();
                $bundle_product_detail->bundle_products_id = $last_id;
                $bundle_product_detail->product_id = $product_id[$b];
                $bundle_product_detail->purchase_id = $purchase_id[$b];
                $bundle_product_detail->quantity = $qty[$b];
                $bundle_product_detail->update();
            $b++;

        }
        
        return redirect()->route('bundle_product.index')->with('status','Bundle Product Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Bundle_product  $bundle_product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bundle_product $bundle_product)
    {
        $bundle_product->delete();
        return redirect()->route('bundle_product.index')->with('status','Bundle Product Deleted Successfully');
    }

    public function get_price($id){
        $products = Purchase::where('product_id','=',$id)->get();
        $a=0;
        $b=count($products);
        $hello=0;
        foreach($products as $pavg){
            $hello=$hello+$pavg->sell_unit_price;
            $a++;
        }
       return $hello/$a;
    }

    
    /**
     * active/inactive bundle product.
     * 
     */

    public function change_status(Request $request,$bundle_id){
       $status = Bundle_product::find($bundle_id);
       
       if($request->status){
           $status->status = 0;
       }else{
           $status->status = true;
       }
       
       $status->update();
       return redirect()->back();

    }
}
