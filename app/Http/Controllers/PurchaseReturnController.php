<?php

namespace App\Http\Controllers;

use App\Supplier;
use App\Payment_method;
use App\Purchase_return;
use App\Purchase;
use Validator;
use App\Branch_inventory;
use App\Branch;
use App\Transaction;
use App\Journal;
use App\JournalHelper\journalEntry;
use App\Warranty_product_detail;
use App\Product_warranty_type;
use App\Expirable_product_detail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PurchaseReturnController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(isset($_GET['searchbyfrom']) && isset($_GET['searchbyto'])){
            $from =$_GET['searchbyfrom'];
            $to = $_GET['searchbyto'];
        }else{
            $from =date('Y-m-01');
            $to =date('Y-m-t');
        }
        if(Auth::user()->branch_id!=null){
            $where=[['branch_id','=',Auth::user()->branch_id]];
        }else{
            $where=[['branch_id','like',"%''%"]];
        } 
        $purchase_returns=Purchase_return::whereHas('purchase',function($q)use($where){
            $q->where($where);
        })->whereBetween('created_at', [$from, $to])->latest()->paginate(10);
        return view('admin.purchase_return.index',compact('purchase_returns'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $suppliers=Supplier::all();
         $branches=Branch::all();
         $payment_methods=Payment_method::all();
        return view('admin.purchase_return.create',compact('suppliers','payment_methods','branches'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $journal_posted='';
        $remain_qty=$request->remain_qty;
        $payment_method_id=$request->payment_method_id;
        $branch_inv_id=$request->branch_inv_id;
        $branch_id=$request->branch_id;
        $amount_to_be_paid=$request->total_price;
       
        $this->validate($request,[
            'returned_qty' => "required|lte:$remain_qty",
            'product_id' => 'required'
            ]);

        
            
        $purchase_return=new Purchase_return;
        $purchase_return->purchase_id=$request->purchase_id;
        $purchase_return->created_by=Auth::id();
        $purchase_return->purchase_return_quantity=$request->returned_qty;
        $purchase_return->purchase_return_unit_price=$request->p_unit_price;
        $purchase_return->save();
        $purchase_r_id=$purchase_return->id;

        if($purchase_r_id){
            $branch=Branch_inventory::find($branch_inv_id);
            if($remain_qty >= $request->returned_qty){
                $branch->stock_quantity=($remain_qty - $request->returned_qty);
                $branch->update();


                $transaction=new Transaction;
                $transaction->event_id=$purchase_r_id;
                $transaction->event_type="purchase_return";
                $transaction->branch_id=$branch_id;
                $transaction->created_by=Auth::id();
                $transaction->save();
                $transaction_id=$transaction->id;

                //journal_Entry for purchase Return's Transaction
                if($transaction_id){
                    $accounts_title=['6','11','2','10'];  //   A/p,purchase_return,cash,bank,
                    //executing journal for purchase return;
                    for($i=0;$i<2;$i++){
                        $related_party_id=$request->supplier_id;
                        $related_party_type="supplier";
                        $journal_type=$i;
                        $accounts_id=$accounts_title[$i];
                        $payment_method=Null;
                        $description='Purchase Returned';
                        $amount=$amount_to_be_paid;
                        $journal_posted.=" | ";
                        $journalEntry=new journalEntry;
                            $journal_posted.=$journalEntry->journal_entry($transaction_id,$related_party_id,$related_party_type,$journal_type,$accounts_id,$payment_method,$description,$amount,$branch_id);
                    }
                }//transaction id

            }//qty if
        }//purchase_returned added
        
        
            return redirect()->route('purchase_returns.index')->with('status','Product '.$journal_posted.' Returned Successfully');



      
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Purchase_return  $purchase_return
     * @return \Illuminate\Http\Response
     */
    public function show(Purchase_return $purchase_return)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Purchase_return  $purchase_return
     * @return \Illuminate\Http\Response
     */
    public function edit(Purchase_return $purchase_return)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Purchase_return  $purchase_return
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Purchase_return $purchase_return)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Purchase_return  $purchase_return
     * @return \Illuminate\Http\Response
     */
    public function destroy(Purchase_return $purchase_return)
    {
        //
    }
    
    public function purchase_history_for_return()
    {
        if(isset($_GET['supplier_id'])){
            $supplier_id=$_GET['supplier_id'];
            $where_condition[]=['supplier_id','=',$supplier_id];
        }
        
        if(isset($_GET['condition'])){
        $hello=$_GET['condition'];
        $invoice_no=str_before($hello," | ");
        $supplier_id=str_after($hello," | ");
        $where_condition[0]=['invoice_no','=',$invoice_no];
        $where_condition[1]=['supplier_id','=',$supplier_id];
        }

        if(isset($_GET['histry'])){
        $hello=$_GET['histry'];
        $invoice_no=str_before($hello," | ");
        $product_id=str_after($hello," | ");
        $where_condition[0]=['invoice_no','=',$invoice_no];
        $where_condition[1]=['product_id','=',$product_id];
        }
        
        $returndara=Purchase::where($where_condition)->with('product_status','product')->get();
        return $returndara;
    }
   
}
