<?php

namespace App\Http\Controllers;

use App\Purchase;
use App\Branch_inventory;
use App\Transaction;
use App\Journal;
use App\Payment_method;
use App\Branch;
use App\Product;
use App\Supplier;
use App\cheque_info;
use App\Warranty_product_detail;
use App\Product_warranty_type;
use App\JournalHelper\journalEntry;
use App\Expirable_product_detail;
use Gate;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PurchaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(isset($_GET['searchbyfrom']) && isset($_GET['searchbyto'])){
            $from =$_GET['searchbyfrom'];
            $to = $_GET['searchbyto'];
        }else{
            $from =date('Y-m-01');
            $to =date('Y-m-t');
        }
        if(Auth::user()->branch_id!=null){
            $where=[['branch_id','=',Auth::user()->branch_id]];
        }else{
            $where=[];
        } 
        $purchases=Purchase::with('supplier','product','branch','users')->where($where)->whereBetween('created_at', [$from, $to])
        ->get();
        $purchases = $purchases->groupBy('invoice_no');
        return view('admin.purchase.report',compact('purchases'));
    }
    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function anyData()
    {  
       $purchase = Purchase::groupBy('invoice_no')
       ->selectRaw('*, sum(quantity) as Total')
       ->with('supplier','product','branch','users')
       ->get();
        return Datatables::of($purchase)->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
        if (Gate::allows('isSuperadmin') || Gate::allows('isAdmin') || Gate::allows('isManager')) {
            $suggested_price="";
            $suppliers=Supplier::all();
            $branches=Branch::all();
            $product_all=Product::all();
            //$payment_methods=Payment_method::all();
            $payment_methods=[];
            $user_type =  Auth::user()->user_type;
            
            if($user_type == 'superadmin'){
                 $payment_methods = Payment_method::with('journals')->latest()->get();
            }else{
                $payment_methods = Payment_method::with(['journals' =>function($query){
                    $query->where('branch_id','=',Auth::user()->branch_id);
                }])->latest()->get();
            }
            //end payment restriction
            if(Auth::user()->user_type!=="superadmin"){
                //$suggested_price=Purchase::where([['product_id','=',$product->id],['branch_id','=',Auth::user()->branch_id]])->latest()->first();
            }
            //dd($suggested_price);
            return view('admin/purchase/create_batch',compact('product_all','suppliers','branches','payment_methods'));
        }else {
            abort(403, Auth::user()->user_type." is not allowed to perform this action");
        }
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
public function store(Request $request){
    $total_price= $request->total_price;
    $request->validate([
        'purchase_qty'=>'required|numeric|min:1',
        'product_id'=>'required',
        'branch_id'=>'required',
        'supplier_id'=>'required',
    ]);
    if($request->paid_amount){
        $request->validate([
            'paid_amount'=>"required|lte:$total_price",
        ]);
    }
    if($request->is_warrantable==1){
        $request->validate([
        // 'product_sl_no.*'=>'required|unique_with:warranty_product_details,purchase_id',
        'product_sl_no.*'=>'required',
        'purchase_qty'=>'required|numeric|min:1|max:30',
        ]);
    }
    if($request->is_expirable==1){
        $request->validate([
        'expire_date'=>'required',
        ]);
    }
    
       $journal_posted="";
        $msg="";
        $product_id=$request->product_id;
        $branch_id=$request->branch_id;
        $purchase_qty=$request->purchase_qty;
        $supplier_id=$request->supplier_id;
        $p_unit_price=$request->p_unit_price;
        $payment_method_id=str_before($request->payment_method_id," | ");
        $payment_method_type=str_after($request->payment_method_id," | ");
        $paid_amount=$request->paid_amount;
        $product_sl_no=$request->product_sl_no;
        $amount_to_be_paid=($purchase_qty*$p_unit_price);
        $duration=$request->warranty_duration;
        $warranty_type_id=$request->warranty_type_id;
        $expire_date=$request->expire_date;
        
        //check transaction has any cheque info
        if($request->cheque_disposal_date){
            $cheque_info_status='1';
            $paid_amount=0;
            $payment_method_id=0;
        }else{
            $cheque_info_status="0";
        }
        if($request->check_no){
            $cheque_info=new cheque_info;
            $cheque_info->payment_method_id=str_before($request->payment_method_id," | ");
            $cheque_info->created_by=Auth::id();
            $cheque_info->branch_id=$branch_id;
            $cheque_info->related_party_id=$supplier_id;
            $cheque_info->related_party_type="supplier";
            $cheque_info->amount=$request->paid_amount;
            $cheque_info->cheque_sl_no =$request->check_no;
            $cheque_info->status=$cheque_info_status;
            $cheque_info->cheque_disposal_date=$request->cheque_disposal_date;
            $cheque_info->save();
    
            $msg.="Your Check has been Registered";
        }
        // $request->all();
        $purchase=new Purchase;
        $purchase->invoice_no =str_replace(' ', '_',$request->invoice_no);
        $purchase->quantity=$purchase_qty;
        $purchase->purchase_unit_price=$p_unit_price;
        $purchase->sell_unit_price =$request->sales_unit_price;
        $purchase->supplier_id=$request->supplier_id;
        $purchase->product_id=$product_id;
        $purchase->branch_id=$branch_id;
        $purchase->created_by=Auth::id();
        $purchase->save();
        $purchase_id=$purchase->id;

    
      
        if($purchase_id){
            if(isset($request->product_sl_no)){
            $sl=0;
            $pcounter=count($product_sl_no);
             //register product sl for warranty
            while($sl < $pcounter){
       
                $Warranty_product_detail=new Warranty_product_detail;
                $Warranty_product_detail->product_sl_no=$product_sl_no[$sl];
                $Warranty_product_detail->purchase_id=$purchase_id;
                $Warranty_product_detail->created_by=Auth::id();
                $Warranty_product_detail->save();
               
                 $sl++ ;       
            }
             //register Product_warranty_type
            $dl=0;
            $wt=0;
            $wtime=count($duration);
            while($dl < $wtime){
                if(!$duration[$dl]==null){
                    $Product_warranty_type=new Product_warranty_type;
                    $Product_warranty_type->warranty_type_id=$warranty_type_id[$wt++];
                    $Product_warranty_type->purchase_id=$purchase_id;
                    $Product_warranty_type->duration=$duration[$dl];
                    $Product_warranty_type->created_by=Auth::id();
                    $Product_warranty_type->save();
                }
                $dl++;
            }// End of while warranty duration
          
        }
          //input Expire date for Epirable product
          if($expire_date){ 

            $expirable_product_detail=new Expirable_product_detail;
            $expirable_product_detail->expire_date=$expire_date;
            $expirable_product_detail->purchase_id=$purchase_id;
            $expirable_product_detail->created_by=Auth::id();
            $expirable_product_detail->save();
            
         }
       
            //input in Branch or warehouse
            $branch_inv=new Branch_inventory;
            $branch_inv->purchase_id=$purchase_id;
            $branch_inv->product_id=$product_id;
            $branch_inv->branch_id=$branch_id;
            $branch_inv->stock_quantity=$purchase_qty;
            $branch_inv->created_by=Auth::id();
            $branch_inv->save();

            //inPut transaction Table
            $transaction=new Transaction;
            $transaction->event_id=$purchase_id;
            $transaction->event_type="purchase";
            $transaction->branch_id=$branch_id;
            $transaction->created_by=Auth::id();
            $transaction->save();
            $transaction_id=$transaction->id;
        // }
        if($transaction_id){
            $accounts_id=['9','6','2','10'];//purchase,A/p,cash,bank
            //executing journal for purchase;
            $related_party_type='supplier';
            $related_party_id=$supplier_id;
            for($i=0;$i<2;$i++){

                if(!$payment_method_id==0){
                    $payment_method=$payment_method_id;
                   // $payment_type=Payment_method::find($payment_method_id);
                   $payment_method_type ? $pmt='Bank' : $pmt='Cash';//0==cash & 1==Bank;
                    $description='Purchase On '.$pmt;
                    if($i==1){
                        $payment_method_type==0 ? $journal_account=$accounts_id[2]:$journal_account=$accounts_id[3];
                        $amount=$paid_amount;
                    }else{
                        $journal_account=$accounts_id[$i];
                        $amount=$amount_to_be_paid;
                    }

                }else{
                    $payment_method=null;
                    $amount=$amount_to_be_paid;
                    $journal_account=$accounts_id[$i];
                    $description='Purchase On account';
                }

                $journalEntry=new journalEntry;
                $journal_posted.=$journalEntry->journal_entry($transaction_id,$related_party_id,$related_party_type,$i,$journal_account,$payment_method,$description,$amount,$branch_id);
            }//end for-loop


            if(($paid_amount<$amount_to_be_paid) && ($paid_amount!=0)){
              
                $journal_account=$accounts_id[1];
                $description ='Purchase On account';
                $amount=($amount_to_be_paid-$paid_amount);
                
                $journalEntry=new journalEntry;
                $journal_posted.=$journalEntry->journal_entry($transaction_id,$related_party_id,$related_party_type,1,$journal_account,$payment_method,$description,$amount,$branch_id);
            }//due journal
        }//transaction journal
        $purchases=Purchase::where('invoice_no','=',str_replace(" ","_",$request->invoice_no))->get();
        $returndata="";
        $returndata.='<div class="table table-responsive"><caption>Purchased List</caption><table class="table table-bordered">
    <thead>
      <tr>
        <th>Sl.</th>
        <th>Product Sku</th>
        <th>Qty</th>
        <th>unit price</th>
        <th>SubTotal</th>
      </tr>
    </thead>
    <tbody>';
    $i =1;
    $total=0;
        if($purchases!==null){
            foreach($purchases as $cat){
                $returndata.='<tr>
                    <td>'.$i++.'</td>';
                    $time=strtotime($cat->created_at);
                    $date=date("d-M-Y",$time);
                    $timm=date("h:i:s",$time);
                // $returndata.='<td title="'.$timm.'">'.$date.'</td>';
                $returndata.='<td>'.str_before($cat->product->product_sku,"|").'</td>
                <td>'.$cat->quantity.'</td>
                <td>'.$cat->purchase_unit_price.'</td>';
                    $subtotal=$cat->quantity*$cat->purchase_unit_price;
                    $total=$total+$subtotal;
                $returndata.='<td>tk. '.$subtotal.'</td>
            </tr>';
                }
        }else{
            $returndata.='<tr>
          <td colspan="8">There is no records available</td>
      </tr>';
    }//end-else
    $returndata.='</tbody>
    <tfoot>
      <th colspan="4" class="text-right">Total Purchased Amount:</th>
      <th class="text-left">tk. '.$total.'</th>
      
    </tfoot>
  </table></div>';
  if($request->batch_purchase==1){
    return response()->json($returndata,200);
  }else{
    return redirect()->back()->with('status','Product Purchased successfully journal '.$journal_posted);
  }
    }else{
        return "Something Went Wrong";
    }//purchase id
        
        //return redirect()->back()->with('status','Product Purchased successfully journal '.$journal_posted);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function get_invoice_details($invoice)
    {
       
        $purchases=Purchase::where([['invoice_no','=',str_replace(' ','_',str_after($invoice,"|"))],['supplier_id','=',str_before($invoice,"|")]])->get();
        
        if(count($purchases)>0){
        $returndata="";
        $returndata.='<div class="table table-responsive"><caption>Invoice No.'.str_after($invoice,"|").' </caption><table class="table table-bordered">
            <thead>
                <tr>
                    <th>Sl.</th>
                    <th>Date</th>
                    <th>Product Sku</th>
                    <th>Qty</th>
                    <th>unit price</th>
                    <th>SubTotal</th>
                </tr>
            </thead>
        <tbody>';
    $i =1;
    $total=0;
        if($purchases!==null){
            foreach($purchases as $cat){
                $returndata.='<tr>
                    <td>'.$i++.'</td>';
                    $time=strtotime($cat->created_at);
                    $date=date("d-M-Y",$time);
                    $timm=date("h:i:s",$time);
                 $returndata.='<td title="'.$timm.'">'.$date.'</td>';
                $returndata.='<td>'.str_limit($cat->product->product_sku,20).'</td>
                <td>'.$cat->quantity.'</td>
                <td>'.$cat->purchase_unit_price.'</td>';
                    $subtotal=$cat->quantity*$cat->purchase_unit_price;
                    $total=$total+$subtotal;
                $returndata.='<td>tk. '.$subtotal.'</td>
            </tr>';
                }
        }else{
            $returndata.='<tr>
          <td colspan="8">There is no records available</td>
      </tr>';
    }//end-else
    $returndata.='</tbody>
    <tfoot>
      <th colspan="5" class="text-right">Total Purchased Amount:</th>
      <th class="text-left">tk. '.$total.'</th>
      
    </tfoot>
  </table></div>';

  return $returndata; 

}
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function edit(Purchase $purchase)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Purchase $purchase)
    {
        //return $request->all();
        $id=$request->purchase_id;
        $i=0;
         while($i<count($id)){
            $purchase = Purchase::find($id[$i]);
            $purchase->quantity=$request->quantity[$i];
            $purchase->purchase_unit_price=$request->purchase_unit_price[$i];
            $purchase->sell_unit_price=$request->sell_unit_price[$i];
            $purchase->update();
            
            if($request->quantity[$i]!==$request->old_quantity[$i]){
                $inventory=Branch_inventory::where('purchase_id',$id[$i])->first();
                $inventory->stock_quantity=$request->quantity[$i];
                $inventory->update();
            }
            if(($request->quantity[$i]!==$request->old_quantity[$i])||($request->purchase_unit_price[$i]!==$request->purchase_unit_price[$i])){
                $transaction=Transaction::where([['event_type','purchase'],['event_id',$id[$i]]])->first();
                $journals=Journal::where('transaction_id',$transaction->id)->get();
                $jrCounter=count($journals);
                $amount=($request->quantity[$i] * $request->purchase_unit_price[$i]);
                foreach($journals as $key=>$jr){                   
                    // if($jrCounter==2){
                        $jr->amount=($request->quantity[$i] * $request->purchase_unit_price[$i]);
                        $jr->update();
                    // }
                }//foreach journal
            }//if change qty

            $i++;
         }//while
         return redirect()->back()->with('status','Purchase Invoice successfully Adjusted');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function destroy(Purchase $purchase)
    {
        //
    }

    // Custom method for purchase invoice details
    public function show_invoice_details($invoice_no)
    {
        $purchase = Purchase::with('product')->where('invoice_no','=',str_replace(' ','_',$invoice_no))->get();
        if(request('iseditable')!=null){
            return view('admin/purchase/edit',compact('purchase'));
        }else{
            return  $purchase;
        }
        // return  $purchase = Purchase::with('product')->where('invoice_no','=',str_replace(' ','_',$invoice_no))->get();  
    }

    
}
