<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Auth;
use Image;
use App\Branch;
use App\User;
use Hash;
use Gate;
class UserController extends Controller
{
    public function profile(){
        $employee = User::find(Auth::id());
        return view('profile',compact('employee'));
    }

    public function update_avatar(Request $request){
        $new_password = null;
        $hashedpassword = Auth::user()->password;
        if($request->password){
            $this->validate($request,[
                'old_password' => 'required',
                'password' => 'required|confirmed',
            ]);

            if(Hash::check($request->old_password,$hashedpassword)){
            $new_password = $request->password;
            }else{
                return redirect()->back()->withErrors('Old password does not match');
            }
        }
        $employee = User::find(Auth::id());
        if($request->hasFile('avatar')){
            $image=$request->avatar;
            $path=$request->avatar->storeAs('public/avatar/',$employee->id.".".$image->getClientOriginalExtension());
            $employee->avatar = $employee->id.".".$image->getClientOriginalExtension();
        }
        
        $employee->name = $request->name;
        $employee->email = $request->email;
        if($new_password){
            $employee->password = $new_password;
        }
        $employee->update();

        return redirect()->route('profile')->with('status','Your profile has been updated successfully');
    }

    public function user_register(){
        if (Gate::allows('isSuperadmin')) {
            $branches = Branch::all();
            return view('auth.register',compact('branches'));
        }else{
            abort(403, Auth::user()->user_type."can not create user");
        }
        
    }
    public function user_register_store(Request $request)
    {
        // Only super admin can create user
        if (Gate::allows('isSuperadmin')) {
            $user = User::create(request()->validate([
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users',
                'password' => 'required|string|min:6|confirmed',
                'branch_id' => 'required|integer',
                'user_type' => 'required|string',
            ]));
            if ($user) {
                return redirect()->route('/')->with('status','User is created successfully');
            }else{
                return redirect()->back()->withInput();
            }            
        }else{
            abort(403, Auth::user()->user_type."can not create user");
        }

    }
}
