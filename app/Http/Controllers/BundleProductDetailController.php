<?php

namespace App\Http\Controllers;

use App\Bundle_product;
use App\Bundle_product_detail;
use App\Product;
use Illuminate\Http\Request;

class BundleProductDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $bundle_products = Bundle_product::all();
        $products = Product::all();
        return view('admin.bundle_product_details.create',compact('bundle_products','products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Bundle_product_detail  $bundle_product_detail
     * @return \Illuminate\Http\Response
     */
    public function show(Bundle_product_detail $bundle_product_detail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Bundle_product_detail  $bundle_product_detail
     * @return \Illuminate\Http\Response
     */
    public function edit(Bundle_product_detail $bundle_product_detail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bundle_product_detail  $bundle_product_detail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Bundle_product_detail $bundle_product_detail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Bundle_product_detail  $bundle_product_detail
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bundle_product_detail $bundle_product_detail)
    {
        //
    }
}
