<?php

namespace App\Http\Controllers;

use App\Customer_group;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Gate;
use App\User;

class CustomerGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$g="";
		if(isset($_GET['searchby'])){
			$g=$_GET['searchby'];
		}
        $user_type =  Auth::user()->user_type;
		if(!Gate::allows('isManager') || Gate::allows('branch_manager') || !Gate::allows('isSuperadmin')){
			$customergroups = Customer_group::where('customer_group_title','LIKE',"%$g%")->latest()->paginate(5);
        return view('admin/customer_group/index',compact('customergroups'));
		}else{
            abort(403,"$user_type is not permitted for this page.");
        }
		
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.customer_group.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$this->validate($request,[
		'customer_group_title'=>'required','customer_group_description'=>'required','discount_percentage'=>'required'
		]);
        $wp = new Customer_group;
	   $wp->customer_group_title = $request->customer_group_title;
	   $wp->customer_group_description = $request->customer_group_description;
	   $wp->discount_percentage = ($request->discount_percentage/100);
	   $wp->created_by = Auth::id();
	   $wp->save();
	   return redirect()->route('customer_groups.index')->with('status','Supplier data saved successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Customer_group  $customer_group
     * @return \Illuminate\Http\Response
     */
    public function show(Customer_group $customer_group)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Customer_group  $customer_group
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $editid = $id;
		$egroup = Customer_group::find($id);
        $customergroups = Customer_group::all();
        return view('admin/customer_group/edit',compact('editid' , 'egroup' , 'customergroups'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Customer_group  $customer_group
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$this->validate($request,[
		'customer_group_title'=>'required','customer_group_description'=>'required','discount_percentage'=>'required'
		]);
	   $u_customer_group = Customer_group::find($id);
	   $u_customer_group->customer_group_title = $request->get('customer_group_title');
	   $u_customer_group->customer_group_description = $request->get('customer_group_description');
	   $u_customer_group->discount_percentage = ($request->get('discount_percentage')/100);
	   $u_customer_group->created_by = Auth::id();
	   $u_customer_group->update();
	   return redirect()->route('customer_groups.index')->with('status','Customer group data updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Customer_group  $customer_group
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Customer_group::destroy($id);
		return redirect()->route('customer_groups.index')->with('status','Customer group data deleted successfully');
    }
}
