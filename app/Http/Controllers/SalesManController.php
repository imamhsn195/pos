<?php

namespace App\Http\Controllers;

use App\Sales_man;
use App\Branch;
use Illuminate\Http\Request;
use Auth;

class SalesManController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $a="";
		if(isset($_GET['searchby'])){
			$a=$_GET['searchby'];
        }
		$addresses = Sales_man::where('name','LIKE',"%$a%")->latest()->paginate(5);
        return view('admin.sales_men.index',compact('addresses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $branches=Branch::all();
        return view('admin.sales_men.create',compact('branches'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required|alpha',
            'address'=>'required',
            'phone'=>'required|unique:sales_men,phone',
            'branch_id'=>'required'
            ]);
            
            $sales_man = new Sales_man;
            $sales_man->name = $request->name;
            $sales_man->email = $request->email;
            $sales_man->address = $request->address;
            $sales_man->phone = $request->phone; 
            $sales_man->branch_id = $request->branch_id;
            $sales_man->save();
            
            return redirect()->route('sales_man.index')->with('status','Sales Person details saved successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sales_man  $sales_man
     * @return \Illuminate\Http\Response
     */
    public function show(Sales_man $sales_man)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sales_man  $sales_man
     * @return \Illuminate\Http\Response
     */
    public function edit(Sales_man $sales_man)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sales_man  $sales_man
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sales_man $sales_man)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sales_man  $sales_man
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sales_man $sales_man)
    {
        //
    }
}
