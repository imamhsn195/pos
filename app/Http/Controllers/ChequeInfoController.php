<?php

namespace App\Http\Controllers;

use App\Transaction;
use App\TransactionEvents;
use App\Supplier;
use App\Customer;
use App\Journal;
use App\Payment_method;
use App\cheque_info;
use Auth;
use Illuminate\Http\Request;

class ChequeInfoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$a="";
		if(isset($_GET['searchby'])){
			$a=$_GET['searchby'];
        }
        if(Auth::user()->user_type=="superadmin"){
        $cheque_index=cheque_info::where('cheque_sl_no','LIKE',"%$a%")->orderBy('status','DESC')->paginate(5);
        }else{
        $cheque_index=cheque_info::where('cheque_sl_no','LIKE',"%$a%")->where('branch_id','=',Auth::user()->branch_id)->orderBy('status','DESC')->paginate(5);
    }
		return view('admin.cheque_info.index',compact('cheque_index'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\cheque_info  $cheque_info
     * @return \Illuminate\Http\Response
     */
    public function show(cheque_info $cheque_info)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\cheque_info  $cheque_info
     * @return \Illuminate\Http\Response
     */
    public function edit(cheque_info $cheque_info)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\cheque_info  $cheque_info
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, cheque_info $cheque_info)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\cheque_info  $cheque_info
     * @return \Illuminate\Http\Response
     */
    public function destroy(cheque_info $cheque_info)
    {
        //
    }
}
