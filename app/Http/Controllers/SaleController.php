<?php

namespace App\Http\Controllers;
use Auth;
use Session;
use Gate;
use App\Sale;
use App\Sales_man;
use App\Customer;
use App\Journal;
use App\Customer_group;
use App\Sale_detail;
use App\Discount_offer;
use App\Payment_method;
use App\Vat;
use App\Category;
use App\CartData;
use Darryldecode\Cart\Cart;
use App\Product;
use App\InvoiceSetting;
use App\Transaction;
use App\Bundle_product;
use App\Bundle_product_detail;
use App\PrimarySettings;
use App\JournalHelper\journalEntry;
use App\Branch_inventory;
use App\cheque_info;
use App\Warranty_product_detail;
use App\Brand;
use Illuminate\Http\Request;
use DB;
use PdfReport;
use ExcelReport;
use CSVReport;
class SaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        //dd(session()->all());
        if(Gate::allows('isAdmin') || Gate::allows('isCashier') || Gate::allows('isManager')){

        $branch_id = Auth::user()->branch_id;
        $products = Branch_inventory::with('product','product.discount','product.brand','product.category')->where([['branch_id','like',"%$branch_id%"],['stock_quantity','>',0]])
                ->orderBy('product_id','ASC')->orderBy('purchase_id','ASC')
                ->get();
        if(request('categories')){
            $searchValue = request('categories');
            $products = Branch_inventory::with('product','product.discount','product.category')->whereHas('product.category', function ($query)use($searchValue){
                $query->whereIn('category_id',$searchValue);
            })
            ->where([['branch_id','like',"%$branch_id%"],['stock_quantity','>',0]])
            ->orderBy('product_id','ASC')->orderBy('purchase_id','ASC')
            ->get();
        }
        if(request('brands')){
            $searchValue = request('brands');
            $products = Branch_inventory::with('product','product.discount','product.brand')->whereHas('product.brand', function ($query)use($searchValue){
                $query->whereIn('brand_id',$searchValue);
            })
            ->where([['branch_id','like',"%$branch_id%"],['stock_quantity','>',0]])
            ->orderBy('product_id','ASC')->orderBy('purchase_id','ASC')
            ->get();
        }
       
        $payment_methods=Payment_method::all();
        $sales_persons=Sales_man::where('branch_id','=',$branch_id)->get();

        $bundle_product = Bundle_product::whereHas('bundle_product_details.inventory',function($q){
            $q->where('stock_quantity','>',0);
         })->get();
        //dd($bundle_product);
        
        //$discount_offers = Discount_offer::whereHas('product')->get();//to be checked duration

        $discount_offers_for_all = Discount_offer::where([['product_id','=',null],['promo_code','=',null],['status','=',1]])->first();

        $cusgroup = Customer_group::all();
        $brands = Brand::all();
        $categories = Category::all(); 
        // return $products;
        session('customer_id')?"":session(['customer_id'=>1 ,'customer_name'=>'Walking_Customer','customer_due'=>0,'sale_person_feature'=>0,'customer_credit_limit'=>0]);  //set default walking Customer 
        return view('admin.sale.index',compact('brands','products','categories','cusgroup','bundle_product','discount_offers','discount_offers_for_all','payment_methods','sales_persons'));  
            }else{
                abort(403, Auth::user()->user_type." is not allowed for this action");
            }
         }
   
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            DB::beginTransaction();
            
            
            //here fetch cart data//
            if($request->warranty_form==1){
                $product_sl=$request->product_sl;
                $bundle_product_sl=$request->bundle_product_sl;
                session(['product_sl'=>$product_sl]);
                session(['bundle_product_sl'=>$bundle_product_sl]);
                return response()->json(true,200);
            }
            if($request->sales_person_form==1){
                $sales_persons=$request->sales_persons;
                session(['sales_persons'=>$sales_persons]);
                return response()->json(true,200);
            }
            
            
            if(!\Cart::session(Auth::id())->isEmpty()){
                //intial variables
                $journal_posted='';
                $msg='';
                $branch_id=Auth::user()->branch_id;
                $user_id=Auth::id();
                $received_money=$request->paid_amount;
                $counter = 1;
                $intakePrice=0;
                $discount=0;
                $subTotal=0;
                $payment_method_id=str_before($request->payment_method_id," | ")==0?null:str_before($request->payment_method_id," | ");
                $payment_method_type=str_before($request->payment_method_id," | ")==0?null:str_after($request->payment_method_id," | ");
                $customer_id=session('customer_id')? session('customer_id'):1;//should be dynamic;

                //fetch cart data
                foreach(\Cart::session(Auth::id())->getContent() as $cart_items){
                    if($cart_items->attributes->type=='bundle'){
                        $bundle_details=Bundle_product::find($cart_items->attributes->purchase_id);
                        foreach($bundle_details->bundle_product_details as $key=>$bpd){
                            $cartData['product_id'][]=$bpd->product_id;
                            $cartData['quantity'][]=$bpd->quantity;
                            $cartData['sellUnitPrice'][]= $cart_items->price;
                            $key==0 ?$cartData['productSumPrice'][]= \Cart::get($cart_items->id)->getPriceSum():$cartData['productSumPrice'][]=0;//to be sum

                            $key==0 ? $cartData['productDiscountSum'][]=\Cart::get($cart_items->id)->getPriceSum()-$cart_items->getPriceSumWithConditions() :$cartData['productDiscountSum'][]=0;//to be sum
                            //$totalSaleValue=$totalSaleValue+\Cart::get($cart_items->id)->getPriceSum();
                            $key==0 ?$cartData['productSubTotal'][]= $cart_items->getPriceSumWithConditions():$cartData['productSubTotal'][]=0;//will be sum
                            $cartData['purchase_id'][]= $bpd->purchase_id;
                            $cartData['discount_type'][]= $cart_items->attributes->type;
                            $cartData['discount_id'][]= $cart_items->attributes->discount_id;
                        }
                    }else{
                        $cartData['product_id'][]=$cart_items->id;
                        $cartData['quantity'][]=$cart_items->quantity;
                        $cartData['sellUnitPrice'][]= $cart_items->price;
                        $cartData['productSumPrice'][]= \Cart::get($cart_items->id)->getPriceSum();//to be sum
                        $cartData['productDiscountSum'][]=\Cart::get($cart_items->id)->getPriceSum()-$cart_items->getPriceSumWithConditions();//to be sum
                        //$totalSaleValue=$totalSaleValue+\Cart::get($cart_items->id)->getPriceSum();
                        $cartData['productSubTotal'][]= $cart_items->getPriceSumWithConditions();//will be sum
                        $cartData['purchase_id'][]= $cart_items->attributes->purchase_id;
                        $cartData['discount_type'][]= $cart_items->attributes->type;
                        $cartData['discount_id'][]= $cart_items->attributes->discount_id;
                    }
                }
                //dd($cartData['productDiscountSum']);
                $individualProductDiscountedSum=array_sum( $cartData['productSubTotal']);
                
                // get coupon discount on Gross total and get tax from remainder
                if(\Cart::session(Auth::id())->getCondition('coupon@100')){
                $couponAmount=\Cart::session(Auth::id())->getCondition('coupon@100')->getCalculatedValue($individualProductDiscountedSum);
                $discountIdForTotal=str_after(\Cart::session(Auth::id())->getCondition('coupon@100')->getType(),'|');
                $discountType=str_before(\Cart::session(Auth::id())->getCondition('coupon@100')->getType(),'|');
                }else{
                    $couponAmount=0;
                    $discountIdForTotal=null;
                    $discountType=null;
                }
                $totalSaleValue=array_sum($cartData['productSumPrice']);
                $allProductDiscountTotal=array_sum( $cartData['productDiscountSum']);
                $totalDiscount=$allProductDiscountTotal+$couponAmount;
                $totalDiscountWithRegular=($totalDiscount+$request->regular_discount_total);
                $tax=\Cart::session(Auth::id())->getCondition('VAT') ? \Cart::session(Auth::id())->getCondition('VAT')->getCalculatedValue($individualProductDiscountedSum-$couponAmount) : 0;
                $totalSaleValue=round($totalSaleValue,2);
                $allProductDiscountTotal=round($allProductDiscountTotal,2);
                $totalDiscount=round($totalDiscount,2);
                $totalDiscountWithRegular=round($totalDiscountWithRegular,2);
                $tax=round($tax,2);
                $amount_to_be_received=($totalSaleValue-$totalDiscountWithRegular+$tax);

                //dd($totalDiscount);

                //check transaction has any cheque info;
                if(!$request->cheque_disposal_date==null){
                    $cheque_info_status='1';
                    $received_money=0;
                    $payment_method_id=null;
                    $payment_method_type=null;
                }else{
                    $cheque_info_status="0";
                }
    
                if(!$request->check_no==null){
                    $cheque_info=new cheque_info;
                    $cheque_info->payment_method_id=str_before($request->payment_method_id," | ");
                    $cheque_info->created_by=$user_id;
                    $cheque_info->branch_id=$branch_id;
                    $cheque_info->related_party_id=$customer_id;
                    $cheque_info->related_party_type="customer";
                    $cheque_info->amount=$request->paid_amount;
                    $cheque_info->cheque_sl_no =$request->check_no;
                    $cheque_info->status=$cheque_info_status;
                    $cheque_info->cheque_disposal_date=$request->cheque_disposal_date;
                    $cheque_info->save();

                    $msg.="Your Check has been Registered";
                }

            $sales=new Sale;
            $sales->created_by=$user_id;
            $sales->branch_id=$branch_id;
            $sales->customer_id=$customer_id;//should be dynamic
            $sales->invoice_no=time().'-'.$subTotal.'-'.$branch_id;//should be unique and auto generated
            $sales->total_amount=$totalSaleValue;
        
            if(\Cart::session(Auth::id())->getCondition('coupon@100')){
            $sales->discount_id=$discountIdForTotal;
            $sales->discount_type=$discountType;
            $sales->discount_total=$couponAmount;
            }
            $sales->regular_discount_total=$request->regular_discount_total?$request->regular_discount_total:0;
            $sales->total_tax=$tax;
            //dd( $sales);
            $sales->save();

            $sale_id=$sales->id;
            if($sale_id){
                if(session('sale_person_feature')!==null && session('sale_person_feature')==1){
                    $sales_person_attach=Sale::find($sale_id);
                    $sales_person_attach->sales_men()->attach(session('sales_persons'));
                }
                for($i=0; $i<count($cartData['product_id']); $i++){
                    $sale_detail=new Sale_detail;
                    $sale_detail->created_by=$user_id;
                    $sale_detail->product_id=$cartData['product_id'][$i];
                    $sale_detail->purchase_id=$cartData['purchase_id'][$i];
                    $sale_detail->branch_id=$branch_id;
                    $sale_detail->sales_id=$sale_id;
                    $sale_detail->sold_quantity=$cartData['quantity'][$i];
                    $sale_detail->sold_unit_price=$cartData['sellUnitPrice'][$i];
                    if($cartData['discount_id'][$i]!==null){
                        $sale_detail->discount_id=$cartData['discount_id'][$i];
                        $sale_detail->discount_type=$cartData['discount_type'][$i];
                        $sale_detail->discount_subtotal=$cartData['productDiscountSum'][$i];
                    }
                    $sale_detail->save();
                    $sale_detail=$sale_detail->id;

                //update in Branch or warehouse
                $inv_counter=0;
                while($inv_counter < $cartData['quantity'][$i]){
                $branch_inv=Branch_inventory::where([['product_id','=',$cartData['product_id'][$i]],['branch_id','=',$branch_id],['stock_quantity','>',0]])->orderBy('product_id')->first();
                $branch_inv->stock_quantity=$branch_inv->stock_quantity-1;
                $branch_inv->last_updated_by=$user_id;
                $branch_inv->update();
                $inv_counter++;
                }
                //Register Product SL
                if($cartData['discount_type'][$i]=='bundle'){
                    if(session('bundle_product_sl')!==null){
                        if(array_key_exists($cartData['purchase_id'][$i], session('bundle_product_sl'))){
                            foreach(session('bundle_product_sl')[$cartData['purchase_id'][$i]] as $wid){
                                $warrantyProduct=Warranty_product_detail::find($wid);
                                $warrantyProduct->sold_date =date('Y-m-d');
                                $warrantyProduct->sale_details_id =$sale_detail;
                                $warrantyProduct->update();
                            }   
                        }//key exist
                    }
                }else{
                    if(session('product_sl')!==null){
                        if(array_key_exists($cartData['product_id'][$i], session('product_sl'))){
                            foreach(session('product_sl')[$cartData['product_id'][$i]] as $wid){
                                $warrantyProduct=Warranty_product_detail::find($wid);
                                $warrantyProduct->sold_date =date('Y-m-d');
                                $warrantyProduct->sale_details_id =$sale_detail;
                                $warrantyProduct->update();
                            }
                        }//key exist
                    }
                }
            
                }//end loop;

                //inPut transaction Table
                $transaction=new Transaction;
                $transaction->event_id=$sale_id;
                $transaction->event_type="sales";
                $transaction->branch_id=$branch_id;
                $transaction->created_by=$user_id;
                $transaction->save();
                $transaction_id=$transaction->id;

                //journal entry;
                if($transaction_id){
                    $accounts_title=['5','2','10','12','13'];  //  5-A/R,2-cash,10-bank,12-Discount on Receive,13-Discount on Payment
                    //executing journal for account payment or collect.;
                    for($i=0;$i<5;$i++){
                        $related_party_type='customer';
                        $related_party_id=$customer_id;
                        // $payment_method_id=str_before($request->payment_method_id," | ");
                        // $payment_method_type=str_after($request->payment_method_id," | ");
                        $received_money ? ($payment_method_type ? $description='Sale on Bank':$description='Sale on Cash') :$description='Sale on Account';
                        if($i==0){
                            $accounts_id=5;
                            $journal_type=$i;
                        //change hereeee
                            if($amount_to_be_received > $received_money){
                                $amount_get=$amount_to_be_received-$received_money;//change hereeee
                                $journalEntry=new journalEntry;
                                $journal_posted.=$journalEntry->journal_entry($transaction_id,$related_party_id,$related_party_type,$journal_type,$accounts_id,$payment_method_id,$description,$amount_get,$branch_id);
                            }
                        }
                        if($i==1){
                            $payment_method_type ? $accounts_id=$accounts_title[2]:$accounts_id=$accounts_title[1];
                            
                            $journal_type=0;
                            if($received_money > $amount_to_be_received){//change hereeee
                                $amount_get=$amount_to_be_received;    //change hereeee
                            }else{//change hereeee
                                $amount_get=$received_money; 
                            }//change hereeee
                            
                            

                            if($received_money>0){
                                $journalEntry=new journalEntry;
                                $journal_posted.=$journalEntry->journal_entry($transaction_id,$related_party_id,$related_party_type,$journal_type,$accounts_id,$payment_method_id,$description,$amount_get,$branch_id);
                            }
                        
                        }
                        if($i==2){
                            $accounts_id=14;
                            $journal_type=0;
                            $amount_get=$totalDiscountWithRegular;
                            if($totalDiscountWithRegular>0){
                                $journalEntry=new journalEntry;
                                $journal_posted.=$journalEntry->journal_entry($transaction_id,$related_party_id,$related_party_type,$journal_type,$accounts_id,$payment_method_id,$description,$amount_get,$branch_id);
                            }
                        }
                        if($i==3){
                            $accounts_id=4;
                            $journal_type=1;
                            $amount_get=$totalSaleValue;
                            if($totalSaleValue>0){
                                $journalEntry=new journalEntry;
                                $journal_posted.=$journalEntry->journal_entry($transaction_id,$related_party_id,$related_party_type,$journal_type,$accounts_id,$payment_method_id,$description,$amount_get,$branch_id);
                            }
                        }
                        if($i==4 && $tax>0){
                            $accounts_id=16;
                            $journal_type=1;
                            $amount_get=$tax;
                            

                            if($totalSaleValue>0){
                                $journalEntry=new journalEntry;
                                $journal_posted.=$journalEntry->journal_entry($transaction_id,$related_party_id,$related_party_type,$journal_type,$accounts_id,$payment_method_id,$description,$amount_get,$branch_id);
                            }
                        }
                        
                        
                    }//for loop
                    
                }//transaction id
                
            }//sale_id

            //code for receive due payment from customer
            if($received_money > $amount_to_be_received){
            $str_data['customer_id']=$customer_id;
            $str_data['branch_id']=$branch_id;
            $str_data['amount']=($received_money-$amount_to_be_received);
            app('App\Http\Controllers\TransactionController')->store($request,$str_data);
            }
            ////code for receive due payment from customer

            \Cart::session($user_id)->clear();
            session('customer_id')?"":session(['customer_id'=>1 ,'customer_name'=>'Walking_Customer','customer_due'=>0,'customer_credit_limit'=>0]);
            
            DB::commit();
            return $this->sales_details_report($sale_id);
            }

           
        }catch(\Exception $exception){
            DB::rollback();
            return redirect()->back()->with('status',"No Transaction Has MADE");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function show(sale $sale)
    {
       //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function edit( $sale)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sale $sale)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sale $sale)
    {
        foreach($sale->sale_details as $sale_detail){
            $inventory=Branch_inventory::where([['purchase_id',$sale_detail->purchase_id],['branch_id',$sale_detail->branch_id]])->first();

            $inventory->stock_quantity=($inventory->stock_quantity+$sale_detail->sold_quantity);
            $inventory->update();
        }
        
        $transaction = Transaction::where([['event_type','=','sales'],['event_id','=',$sale->id],['branch_id','=',$sale->branch_id]])->first();
        
        if($transaction!=null){
            $journals=Journal::where('transaction_id',$transaction->id)->delete();
            $transaction->delete();
        }
        
        Sale_detail::where('sales_id',$sale->id)->delete();
        $sale->delete();
        
        return redirect()->route('sales_report')->with('status','Invoice And It\'s Releted Transaction Deleted Successfully');

    }
    public function holdCustomerCart()
    {
        $customer_id=session('customer_id');

        if(!\Cart::session(Auth::id())->isEmpty()){
            foreach(\Cart::session(Auth::id())->getContent() as $cart_items){
                $cart_data=new CartData;
                $cart_data->purchase_id=$cart_items->attributes->purchase_id;
                $cart_data->customer_id=$customer_id;
                $cart_data->qty=$cart_items->quantity;

                if($cart_items->attributes->type=="bundle"){
                    $cart_data->product_id=$cart_items->attributes->purchase_id;
                    $cart_data->type=1;
                }else{
                    $cart_data->product_id=$cart_items->id;
                    $cart_data->type=0;
                }
                $cart_data->save();                
            }
                session(['holded_customer_id'=>$customer_id]);
                $user_id=Auth::id();
                \Cart::session($user_id)->clear();

                session('customer_id')?"":session(['customer_id'=>1 ,'customer_name'=>'Walking_Customer','customer_due'=>0,'customer_credit_limit'=>0]);
        }
        return redirect()->route('sales.index');
    }
    public function holdBackCustomerCart()
    {
        $phoneNo=null;
        $cartData=CartData::latest()->first();
        $cartData!=null?$phoneNo=Customer::find($cartData->customer_id)->address->phone:"";
        ($phoneNo!=null)?$this->set_customer_info($phoneNo):'';
        $user_id=Auth::id();
        \Cart::session($user_id)->clear();
        $customer_id=(session('holded_customer_id')!=null)? session('holded_customer_id'):null;
                $cart_datas=CartData::all();
                if($cart_datas!=null){
                foreach($cart_datas as $cart_data){
                if($cart_data->type==1){//checkbunde
                    $this->add_bundle_item_to_cart($cart_data->purchase_id);
                    ($cart_data->qty>1)?$this->update_cart_quentity($cart_data->product_id,($cart_data->qty)):'';
                    ;
                }else{
                    $this->add_item_to_cart($cart_data->product_id,$cart_data->purchase_id);
                    ($cart_data->qty>1)?$this->update_cart_quentity($cart_data->product_id,($cart_data->qty)):'';
                }
                
                
                }
                session(['holded_customer_id'=>'']);
                CartData::truncate();

        }
        return redirect()->route('sales.index');
    }

     //Get all Customers
     public function get_customer()
     {  
        $customers['groups'] = Customer_group::all();
        $customers['customers'] = Customer::with('address')->where('status', 1)->get();
        $customers['customer_due'] = session('customer_due');
        return $customers;
     }

     public function set_customer_info($customer_search_input)
     {  
        \Cart::session(Auth::id())->clear();
        $customers = Customer::with('address')->where('status', 1)->whereHas('address', function($q) use($customer_search_input){
            $customerPhone = $customer_search_input;
            $q->where('phone','=',$customerPhone);
        })->first();

        if(!$customers==null){
            session(['cust_group_id'=>$customers->customer_group_id]);
            session(['customer_id'=>$customers->id]);
            session(['customer_phone'=>$customers->address->phone]);
            session(['customer_name'=>$customers->customer_name]);
            $customers->credit_limit?session(['customer_credit_limit'=>$customers->credit_limit]):'';

            //ALTER TABLE `journals` CHANGE `journal_type` `journal_type` TINYINT(1) NOT NULL;
            $debitSum=Journal::with('customer')->where([['journal_type','=',0],['accounts_id',5],['related_party_type','=','customer'],['related_party_id','=',$customers->id]])->sum('amount');
            
            $creditSum=Journal::with('customer')->where([['journal_type','=',1],['accounts_id',5],['related_party_type','=','customer'],['related_party_id','=',$customers->id]])->sum('amount');
            session(['customer_due'=>($debitSum-$creditSum)]);
        }else{
            session(['customer_id'=>1 ,'customer_name'=>'Walking_Customer','customer_due'=>0,'sale_person_feature'=>0,'customer_credit_limit'=>0]);
        }

     }
 
     //
     public function view_product_details($id)   
     {
         $product = Branch_inventory::where('product_id','=',$id)->with('purchase','branch','product','user')->get();
         return $product;
     }
 
 
     public function view_bundle_product_details($id)   
     {
         $product = Bundle_product::with('bundle_product_details.purchase.product')->where('id','=',$id)->get();
         return $product;
     }
     
 
 // Add Items To Cart
     public function add_item_to_cart($product_id,$purchase_id,$cust_group_id=0)
     {
        $userId = auth()->user()->id;   
        $branch_id = Auth::user()->branch_id;
        $product = Branch_inventory::where([['branch_id','=',$branch_id],['stock_quantity','>',0],['purchase_id','=',$purchase_id]])->first();
        //dd($product);

        //code for checking qty;
        if(!\Cart::session(Auth::id())->isEmpty()){
            foreach(\Cart::session(Auth::id())->getContent() as $key =>$cart_items){
                if($cart_items->id==$product->product_id){
                    if(!$product->stock_quantity>$cart_items->quantity){
                        $returndata='';
                    }
                }
            }

        }//end session
        //code for checking qty


         //Get Customer Group discount for Single product
        if($cust_group_id){
             $customer_group_id=$cust_group_id;
             $groupDiscountSingleProduct =Discount_offer::where([['purchase_id','=',$purchase_id],['promo_code','=',null],['status','=',1]])->with('customer_groups')->whereHas('customer_groups',function($q) use($customer_group_id){
                $q->where('id','=',$customer_group_id);
             })->first();//date should be check

              //Get Customer Group discount for single product
            if($groupDiscountSingleProduct==null){
                $groupDiscountSingleProduct =Discount_offer::where([['product_id','=',$product_id],['promo_code','=',null],['status','=',1]])->with('customer_groups')->whereHas('customer_groups',function($q) use($customer_group_id){
                    $q->where('id','=',$customer_group_id);
                })->first();//date should be chech
            }

            $groupDiscountAllProduct =Discount_offer::where([['product_id','=',null],['promo_code','=',null],['status','=',1]])->with('customer_groups')->whereHas('customer_groups',function($q) use($customer_group_id){
               $q->where('id','=',$customer_group_id);
            })->first();//date should be check

          
            $customerGroupDiscount=$this->identifyTheMaxDiscountValue($groupDiscountAllProduct,$groupDiscountSingleProduct,$product->purchase->sell_unit_price);
        }else{
            $customerGroupDiscount=null;
        }
  
        $discount = Discount_offer::doesntHave('customer_groups')->where('purchase_id','=',$purchase_id)->first();
 
        if($discount==null){
        $discount = Discount_offer::doesntHave('customer_groups')->where('product_id','=',$product_id)->first();
        }
 
        $all_product_discount = Discount_offer::doesntHave('customer_groups')->where([['product_id','=',null],['promo_code','=',null],['status','=',1]])->first();
     
        $generalDiscountOffer=$this->identifyTheMaxDiscountValue($all_product_discount, $discount,$product->purchase->sell_unit_price);

        $appliableDiscount=$this->identifyTheMaxDiscountValue($customerGroupDiscount, $generalDiscountOffer,$product->purchase->sell_unit_price);
     
        //add multiple conditions from different condition instances
    
        //applying Vat
        $appliedVat=Vat::where('status','=',1)->first();
        if(!$appliedVat==null){
            $vatValue=$appliedVat->vat_percentage*100;
            $vatValue.='%';
            $vatCondition = new \Darryldecode\Cart\CartCondition(array(
                'name' => 'VAT',
                'type' => $appliedVat->vat_title,
                'target' => 'subtotal', // this condition will be applied to cart's subtotal when getSubTotal() is called.
                'value' => $vatValue,
                'order' => 5
            ));

            \Cart::session(Auth::id())->condition($vatCondition);
 
        }else{
            \Cart::session(Auth::id())->clearCartConditions();
        }
     
        if($appliableDiscount==null) {
          $saleCondition='';
          $discount_type=null;
        }else{
            $discountValue='-'.$appliableDiscount->discount_value;
            $discount_title=$appliableDiscount->discount_title;
            $discount_type=$appliableDiscount->customer_groups->count()==0?'sale offer':'customer offer';
            $saleCondition = new \Darryldecode\Cart\CartCondition(array(
                'name' => $discount_title,
                'type' => $discount_type,
                'value' => $discountValue
            ));

            \Cart::session($userId)->condition($saleCondition);        
        }

    // Previous sold price of selected customer
    $customer_id = session('customer_id')?session('customer_id'):null;
    $previous_sold_price = Sale_detail::where('product_id','=',$product_id)->wherehas('sale',function($query) use($customer_id){
        $query->where('customer_id','=',$customer_id);
    })->latest()->first();
    $previous_sold_price=$previous_sold_price?$previous_sold_price->sold_unit_price:0;
// return $previous_sold_price;
 // now the product to be added on cart
     $product_con = array(
             'id' => $product->product_id,
             'name' => $product->product->product_sku,
             'price' => $product->purchase->sell_unit_price,
             'quantity' => 1,
             'attributes' => array(
                 'purchase_id' => $product->purchase_id,
                 'previous_sold_price' => $customer_id?$previous_sold_price:0,
                 'type' => $discount_type,
                 'discount_id' => $appliableDiscount!==null?$appliableDiscount->id:null,
                 'is_warrantable' => $product->product->category->is_warrantable,
                 'is_expirable' => $product->product->category->is_expirable,
             ),
             'conditions' => $saleCondition
         );
     
     \Cart::session($userId)->add($product_con);
        $this->update_cart_discount(session('coupon_no'));
     //dd(\Cart::getContent());
     //new Render data here//
     return $this->returnCartTable();
 
     }
     public function update_cart_quentity($product_id,$qty)
     {
         // updating a cart for a specific user
        $userId = auth()->user()->id; // or any string represents user identifier
        \Cart::session($userId)->update($product_id, array(
            'quantity' => array(
                'relative' => false,
                'value' => $qty
            ),
            'previous_sold_price' =>array(
                'value' => 10
            )
        ));
        return $this->returnCartTable();
     }


     public function identifyTheMaxDiscountValue($allDiscount,$singleDiscount,$salePrice)
     {   
         if(!$allDiscount==null){
            $valueA=$allDiscount->discount_percent ?($salePrice*$allDiscount->discount_percent) : $allDiscount->discount_amount;
            $allDiscount['discount_value']=$valueA;

         }else{
            $valueA=0;
         }   
         if(!$singleDiscount==null){
            $valueB= $singleDiscount->discount_percent ? ($salePrice*$singleDiscount->discount_percent) : $singleDiscount->discount_amount;
            $singleDiscount['discount_value']=$valueB;
         }else{
            $valueB=0;
         }

         if($valueA <= $valueB){
             return $singleDiscount;
         }else{
            return $allDiscount;
         }
        
     }


            // Add bundle Items To Cart****
     public function add_bundle_item_to_cart($bundle_id)
     {
     $userId = auth()->user()->id;   
     $branch_id = Auth::user()->branch_id;
     $is_warrentable=0;
     $warrantable_pro_purchase_id=[];
     $bundle_product_qty=[];
     $bundle_product_name=[];
    
     $product = Bundle_product::find($bundle_id);
     foreach($product->bundle_product_details as $dpd){
        if($dpd->product->category->is_warrantable==1){
            $warrantable_pro_purchase_id[]=$dpd->purchase_id;
            $bundle_product_qty[$dpd->purchase_id]=$dpd->quantity;
            $bundle_product_name[$dpd->purchase_id]=$dpd->product->product_sku;
            $is_warrentable=1;
        }
     }
     
   
     $discountValue="-".$product->bundle_discount_amount;
     
     $saleCondition = new \Darryldecode\Cart\CartCondition(array(
             'name' => 'bundle_discount',
             'type' => 'bundle offer',
             'value' => $discountValue
         ));
         \Cart::session($userId)->condition($saleCondition);
         
       
 // now the product to be added on cart
     $product_con = array(
             'id' => str_pad($product->id,5,'1'),
             'name' => $product->bundle_name,
             'price' => $product->bundle_price+$product->bundle_discount_amount,
             'quantity' => 1,
             'attributes' => array(
                 'purchase_id' => $product->id,
                 'type' => 'bundle',
                 'discount_id' => $product->id,
                 'is_warrantable' => $is_warrentable,
                 'is_expirable' => 0,
                 'warrantable_pro_purchase_id' => $warrantable_pro_purchase_id,
                 'bundle_product_name' => $bundle_product_name,
                 'bundle_product_qty' => $bundle_product_qty,
             ),
             'conditions' => $saleCondition
         );
         //dd($product_con);
     
     \Cart::session($userId)->add($product_con);
     return $this->returnCartTable();
    }


     public function update_cart_discount($couponNo)
     {
        $discount = Discount_offer::doesntHave('customer_groups')->where([['promo_code','=',$couponNo],['status','=',1]])->first();
        if(!$discount==null){
            $discountValue='-';
            if($discount->discount_percent==null){
                $discountValue.= $discount->discount_amount;
            }else{
                $discountValue.=($discount->discount_percent*100);
                $discountValue.='%';
            }
            
            //dd($discountValue);
        
//dd($discount->discount_percent.'%');
            $couponCondition = new \Darryldecode\Cart\CartCondition(array(
            'name' => 'coupon@100',
            'type' => 'coupon|'.$discount->id,
            'target' => 'total', // this condition will be applied to cart's subtotal when getSubTotal() is called.
            'value' => $discountValue,
            'order' => 1
            ));
        

            session(['coupon_no'=>$couponNo]);
            \Cart::session(Auth::id())->condition($couponCondition);
            //Cart::update($rowId, $qty);
        }else{
            session(['coupon_no'=>'']);
            \Cart::session(Auth::id())->removeCartCondition('coupon@100');
        }
        return $this->returnCartTable();
 
     }
 
     public function remove_item($rowId)
     {
         //$hahaha=str_split($rowId,1);
         \Cart::session(Auth::id())->remove($rowId);
         return $this->returnCartTable();
     }


    public function sales_report()
    {   
        $searchbyfrom = null;
        $searchbyto = null;

        if (isset($_GET['searchbyfrom'])) {
            $searchbyfrom = $_GET['searchbyfrom'];
            $searchbyto = $_GET['searchbyto'];
        }else{
            $searchbyfrom = date('Y-m-01');
            $searchbyto = date('Y-m-t');
        }
        if(Auth::user()->branch_id!=null){
            $where=[['branch_id','=',Auth::user()->branch_id]];
        }else{
            $where=[];
        } 
       
        $sales = Sale::where($where)->whereBetween('created_at', [$searchbyfrom, $searchbyto])->get();

        $total = $sales->sum('total_amount');
        
        return view('admin.sale.sales_report',compact('sales','total'));        
    }
     public function sales_details_report($id)
    {   
        $sales = Sale::find($id);
        $invoice_setting=InvoiceSetting::orderBy('id')->first();
        $company=PrimarySettings::orderBy('id')->first();
        $footer_contents=InvoiceSetting::where('id','!=',1)->orderBy('id')->get();
        
        //$view_page='admin.sale.sales_details_report';
        // $view_page='admin.sale.sales_invoice_layout2';
        
        $view_page=$invoice_setting->layouts;
        //$invoice_time=explode('-',$sales->invoice_no);
        $where=[['accounts_id','=',5],['related_party_type','=','customer'],['related_party_id','=',$sales->customer_id],['created_at','<',$sales->created_at],['branch_id','=',$sales->branch_id]];
        //$where=[['accounts_id','=',5],['related_party_type','=','customer'],['related_party_id','=',$sales->customer_id],['created_at','<',date('Y-m-d H:i:s',$invoice_time[0])],['branch_id','=',$sales->branch_id]];

        $debit_status=Journal::where($where)->whereJournalType(0)->sum('amount');
        $credit_status=Journal::where($where)->whereJournalType(1)->sum('amount');
        
        $previousBalance= ($debit_status-$credit_status);
        
        $Sale_detail = Sale_detail::where('sales_id','=',$id)->get();
        return view($view_page,compact('previousBalance','Sale_detail','sales','invoice_setting','footer_contents','company'));
    }


//pdf test//
public function pdf(Request $request)
    {
        // return  $request->all();
        $fromDate = $request->input('searchbyfrom');
        $toDate = $request->input('searchbyto');
        $format  = $request->input('format');
        $title = 'POINT OF SALE' ;// Report title
    
        $meta = [ // For displaying filters description on header
            'Sales Report of '.(!Auth::user()->user_type=null? Auth::user()->branch->name:"All Branches".' From') => $fromDate . ' To ' . $toDate
        ];
            
            if (Auth::user()->branch_id == null) {
                $values =  Sale::with('customer','user')
                ->whereBetween('created_at', [$fromDate, $toDate]);
            }else {
                // return
                $values =  Sale::with('customer','user')
                ->whereBetween('created_at', [$fromDate, $toDate])
                ->where('branch_id','=',Auth::user()->branch_id)
                // ->get()
                ;
            }        
    // return  $queryBuilder;
        $columns = [ // Set Column to be displayed
            'Date' => function($values){
                return $values->created_at;
            },
            'Invoice No' => function($values){
                return $values->invoice_no;
            },
            'Customer' => function($values){
                return $values->customer->first()->customer_name;
            },
            'Total' => function($values){
                return $values->total_amount;
            },
            'Discount' => function($values){
                return $values->regular_discount_total;
            },
            'Discounted-Total' => function($values){
                return $values->total_amount - $values->regular_discount_total;
            },
            'Created' => function($values){
                return $values->first()->user->user_type;
            },
        ];
    
        // Generate Report with flexibility to manipulate column class even manipulate column value (using Carbon, etc).
        return $format::of($title, $meta, $values, $columns)
            ->editColumn('Date', [ // Change column class or manipulate its data for displaying to report
                'displayAs' => function($result) {
                    return $result->created_at->format('d M Y');
                },
                'class' => 'left'
            ])
            ->editColumns(['Discount', 'Discounted-Total', 'Total','Created'], ['class' => 'right'])
            ->showTotal([ // Used to sum all value on specified column on the last table (except using groupBy method). 'point' is a type for displaying total with a thousand separator
                'Discount' => 'Tk', // if you want to show dollar sign ($) then use 'Total Balance' => '$'
                'Discounted-Total' => 'Tk', // if you want to show dollar sign ($) then use 'Total Balance' => '$'
                'Total' => 'Tk' // if you want to show dollar sign ($) then use 'Total Balance' => '$'
            ])
            ->limit(20) // Limit record to be showed
            ->setOrientation('landscape')
            ->download('Sales Report'); // other available method: download('filename') to download pdf / make() that will producing DomPDF / SnappyPdf instance so you could do any other DomPDF / snappyPdf method such as stream() or download() 
    }
 
    public function returnCartTable($hello=0)
    {
    $payment_methods=Payment_method::all();
    $returnTable="";
    $inputField=null;
    if(!\Cart::session(Auth::id())->isEmpty()){
        $returnTable="<table class='table table-bordered'  style='width:100%;'>
        <thead>
          <tr>
            <th>#</th>
            <th>SKU</th>";
            if($hello==1){
                $returnTable.="<th>Type</th>
                <th>Qty</th>
                <th>Price</th>
            <th>Total</th>
            <th>Discount</th>
            <th>SubTotal</th>";
            }
           
            if($hello==0){
                $returnTable.="<th>Qty</th>
                    <th>Price</th>
                    <th>Cost</th>
                    <th>Dis</th>
                    <th>Sub</th>
                    <th title='last sold price'>LP</th>";
            }
            $returnTable.="</tr>
        </thead>";
           $returnTable.="<tbody id='cart_table'>";
            
            $counter = 1;
            $intakePrice=0;
            $discount=0;
            $subTotal=0;
            $inputField=null;
           
             foreach(\Cart::session(Auth::id())->getContent() as $key =>$cart_items){
                
               $returnTable.="<tr>";
               if($hello==0){
                   $returnTable.="<td style='cursor:pointer' onclick='removeFromCart(".$cart_items->id.")'>X</td>";
               }else{
                $returnTable.="<td>".$counter++."</td>";
               }
                   if($hello==1){
                        $returnTable.="<td><b>".str_limit(str_before($cart_items->name,'|'),30)."</b><br>";
                        if($cart_items->attributes->type=="bundle"){
                            if(session('bundle_product_sl')!==null){
                                if(!$cart_items->attributes->warrantable_pro_purchase_id==null){
                                    foreach($cart_items->attributes->warrantable_pro_purchase_id as $wpid){
                                        $returnTable.='<i>&nbsp;&nbsp;'.str_limit($cart_items->attributes->bundle_product_name[$wpid],15).'</i><br>';
                                        if(array_key_exists($wpid, session('bundle_product_sl'))){
                                            $wps=Warranty_product_detail::whereIn('id',session('bundle_product_sl')[$wpid])->get();
                                            foreach($wps as $wp){
                                                $returnTable.="&nbsp;&nbsp;&nbsp;&nbsp;#".$wp->product_sl_no.'<br>';
                                            }
                                        }
                                    }// show bundle detailes
                                } //bundle product having warranty
                            }//session null ?

                        }else{
                            //check session and listed warrantable product sl no.
                            if(session('product_sl')!==null){
                                if(array_key_exists($cart_items->id, session('product_sl'))){
                                    $wps=Warranty_product_detail::whereIn('id',session('product_sl')[$cart_items->id])->get();
                                    foreach($wps as $wp){
                                        $returnTable.="&nbsp;&nbsp;#".$wp->product_sl_no.'<br>';
                                    }
                                }
                            }
                        }
                    $returnTable.="</td><td>".$cart_items->attributes->type."</td>";
                    //$returnTable.="<td>".$cart_items->attributes->is_warrantable."</td>";
                    $returnTable.="<td>".$cart_items->quantity."</td>";
                
                    //code for warrentable product
                    if($cart_items->attributes->is_warrantable==1){
                        //code to check bundle product
                        if($cart_items->attributes->type=="bundle"){
                            if(!$cart_items->attributes->warrantable_pro_purchase_id==null){
                            foreach($cart_items->attributes->warrantable_pro_purchase_id as $wpid){
                                $inputField.='<div class="form-group col-md-6"><label>Select <b class="text-danger"> ('.$cart_items->attributes->bundle_product_qty[$wpid].' item)</b>'.$cart_items->name.' Serial No for<br>'.str_limit($cart_items->attributes->bundle_product_name[$wpid],15).' </label><select multiple id="warranty'.$cart_items->id.$wpid.'" name="bundle_product_sl['.$wpid.'][]" selectable="3" class="form-control" onclick="restricSelectOption('.$cart_items->attributes->bundle_product_qty[$wpid].','.$cart_items->id.$wpid.')">';
                                $wps=Warranty_product_detail::where([['purchase_id','=',$wpid],['sold_date','=',null]])->get();
                                foreach($wps as $wp){
                                $inputField.='<option value="'.$wp->id.'">'.$wp->product_sl_no.'</option>';
                             }
                            $inputField.='</select></div>';

                            }//end foreach warrantable_pro_purchase_id
                        }
                        }else{
                            //code to check single product
                        $inputField.='<div class="form-group col-md-6"><label>Select <b class="text-danger"> ('.$cart_items->quantity.' item)</b>'.$cart_items->name. 'Serial No</label><select multiple id="warranty'.$cart_items->id.'" name="product_sl['.$cart_items->id.'][]" selectable="3" class="form-control" onclick="restricSelectOption('.$cart_items->quantity.','.$cart_items->id.')">';
                       
                            $wps=Warranty_product_detail::where([['purchase_id','=',$cart_items->attributes->purchase_id],['sold_date','=',null]])->get();
                            foreach($wps as $wp){
                            $inputField.='<option value="'.$wp->id.'">'.$wp->product_sl_no.'</option>';
                       
                         }
                        $inputField.='</select></div>';
                        }
                    }

                    //code for warrentable product





                }else{
                    $returnTable.="<td>".str_limit(str_before($cart_items->name,"|"),15)."</td>";
                    $returnTable.="<td><input type='number' style='max-width: 45px' value='".$cart_items->quantity."' onblur='update_cart_quentity(".$cart_items->id.",this.value,".$cart_items->quantity.")' size='1' id='pqty".$cart_items->id."'/></td>";
                }
                   
                   $returnTable.="<td>".$cart_items->price."</td>";
                   $intakePrice=$intakePrice+\Cart::get($cart_items->id)->getPriceSum();
                   $returnTable.="<td>".\Cart::get($cart_items->id)->getPriceSum()."</td>";
                   $returnTable.="<td>".(\Cart::get($cart_items->id)->getPriceSum()-$cart_items->getPriceSumWithConditions())."</td>";
                   
                   $discount+= \Cart::get($cart_items->id)->getPriceSum()-$cart_items->getPriceSumWithConditions();
                   $subTotal+= $cart_items->getPriceSumWithConditions();
   
                   $returnTable.="<td>".$cart_items->getPriceSumWithConditions()."</td>";
                   
                   if($hello==0){
                    $returnTable.="<td>".$cart_items->attributes->previous_sold_price."</td>";
                }
               $returnTable.="</tr>";
             } //endforeach
           $returnTable.="</tbody>";
           if($hello==1){
       $returnTable.="<tfoot>";
       $returnTable.="<tr>";

        $returnTable.="<th colspan='3' rowspan='4'>
        <div class='form-group'>
          <select required class='form-control' id='payment_method_id' name='payment_method_id'  onchange='addpaidvalue()'>
          <option value=''>Select Payment Method</option>";
         $remainingCredit=session('customer_due');
          (session('customer_id')!==1 && (session('customer_credit_limit') >= ($remainingCredit+$subTotal)))?
          $returnTable.="<option value='0 | 0'>Full Due</option>":"";
           foreach($payment_methods as $pm){
            $returnTable.="<option value='".$pm->id." | ".$pm->method_type."'>".$pm->method_title."</option>";
           }//endforeach
           $returnTable.="</select>
       </div>
       <!--form group for paid amount-->
        <div class='form-group' id='payment_tk'>
           
        </div>
        <div class='form-group' id='date_field'>
           
        </div>";
       $returnTable.="<th colspan='2'>GrandTotal : </th>
            <th>".$intakePrice."</th>
            <th>".$discount."</th>
            <th>".$subTotal."</th>
          </tr>
          <tr>";

        $returnTable.="<th colspan='2'>Discount :".(\Cart::session(Auth::id())->getCondition('coupon@100')?\Cart::session(Auth::id())->getCondition('coupon@100')->getType($subTotal):'0')."</th>
            <th colspan='2'>&nbsp;</th>";
            if(!$subTotal==0){
               \Cart::session(Auth::id())->getCondition('coupon@100') ? $coupon=\Cart::session(Auth::id())->getCondition('coupon@100')->getCalculatedValue($subTotal) : $coupon=0;
               $returnTable.="<th>".$coupon."</th>";
            }//endif
            $returnTable.="</tr>
           <tr>
            <th colspan='2'>TAX @ ".(\Cart::session(Auth::id())->getCondition('VAT') ? \Cart::session(Auth::id())->getCondition('VAT')->getValue():'0')." :</th>
            <th colspan='2'>&nbsp;</th>";
            if(!$subTotal==0){
               $returnTable.="<th>".(\Cart::session(Auth::id())->getCondition('VAT') ? $tax= \Cart::session(Auth::id())->getCondition('VAT')->getCalculatedValue($subTotal-$coupon):$tax='0')."</th>";
            }//endif
            $returnTable.="</tr>
          <tr>
            <th colspan='2'>Net Total :</th>
            <th colspan='2'>&nbsp;</th>
            <th id='netSalePrice'>".($subTotal-$coupon+$tax)."</th>
          </tr>
        </tfoot>";
    }
        $returnTable.="</table>";
           }//endif
           
   
          
        //new Return data here//
      
    return $returnTable."|,|".$inputField;
 }

}
