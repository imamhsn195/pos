<?php

namespace App\Http\Controllers;

use App\Brand;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brand = "";
        if(isset($_GET['brand_name'])){
            $brand = $_GET['brand_name'];
        }
		$parent_brands = Brand::with('child_brands','user')->where('parent_id',null)->where('brand_name','LIKE',"%$brand%")->paginate(6);
        $child_brands = Brand::all();
        return view('admin.brand.index',compact('parent_brands','child_brands'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$brands = Brand::where('parent_id',null)->get();
        return view('admin.brand.create',compact('brands'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
		
		$this->validate($request,[
	   'brand_name' => 'required|unique:brands'
	   ]);
	  
		$brand = new Brand;
		$brand->brand_name = $request->brand_name;
		$brand->created_by = Auth::id();
		$brand->save();
		$parent_id=$brand->id;
        $brand_child =[];
		if(isset($request->brand_child)){
            $brand_child=explode ( ',' ,$request->brand_child);
         
        $a = count($brand_child);
		if($a){
			$it=0;
			while ($it<$a) {
               
				$wp =new Brand;
				$wp->brand_name = $brand_child[$it];
				$wp->created_by = Auth::id();
				$wp->parent_id=$parent_id;
				$wp->save();
				$it++; 
            } 
        }
		}
		return redirect()->route('brand.index')->with('status','Successfully Inserted');
		
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function show(Brand $brand)
    {
        return $brand;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $brand=Brand::with('child_brands')->find($id);
		return $brand;
        //return view('admin.brand.edit',compact('brand','brands'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        /** brand active/inactive code start here */
        $brand = Brand::find($id);
        if(isset($request->status)){
        if($request->status){
            $brand->status = 0;
            $brand->update();
            return redirect()->back();
        }else{
            $brand->status = true;
            $brand->update();
            return redirect()->back();
        }
    }
         /** brand active/inactive code end here */

        $this->validate($request,[
	   'brand_name' => 'required'
	   ]);
	   
		
		$brand =Brand::find($id);
		$brand->brand_name = $request->brand_name;
		$brand->last_updated_by = Auth::id();
		$brand->update();
		
		
		
		if($request->brand_child){
			$brand_child=explode ( ',' ,$request->brand_child);
		$a=count($brand_child);
			$it=0;
			while ($it<$a) {
				$wp =new Brand;
				$wp->brand_name = $brand_child[$it];
				$wp->created_by = Auth::id();
				$wp->parent_id=$id;
				$wp->save();
				$it++; 
			} 
		}
		return redirect()->route('brand.index')->with('status','Successfully Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $brand = Brand::find($id);
	   $brand->delete();
	   return redirect()->route('brand.index')->with('status','Brand deleted successfully');
    }
}
