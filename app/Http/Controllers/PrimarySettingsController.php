<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Account;
use App\PrimarySettings;
use App\Branch;
use App\Transaction;
use App\Payment_method;
use App\Journal;
use App;
use Session;
use App\Address_info;
use Gate;
use Auth;

class PrimarySettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Gate::allows('isSuperadmin')){
            $PrimarySettings = PrimarySettings::first();
            return view('admin.primary_settings.create',compact('PrimarySettings'));
        }else{
            abort(403,auth()->user()->user_type." is not allowed for this action");
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       // 
    }

    public function expenseShow()
    {
        if (isset($_GET['searchbyfrom'])) {
            $searchbyfrom = $_GET['searchbyfrom'];
            $searchbyto = $_GET['searchbyto'];
        }else{
            $searchbyfrom =date('Y-m-01');
            $searchbyto =date('Y-m-t');
        }
        if(Auth::user()->branch_id!=null){
            $where=[['event_type','=','Expense paid'],['branch_id','=',Auth::user()->branch_id]];
        }else{
            $where=[['event_type','=','Expense paid']];
        } 
       
       $expenses= Transaction::where($where)->whereBetween('created_at', [$searchbyfrom, $searchbyto])->get();
     
       return view('admin.primary_settings.expense_report',compact('expenses'));
    }


    public function expenseCreate()
    {
        $payment_methods=[];
        $user_type =  Auth::user()->user_type;
        if($user_type == 'superadmin'){
             $payment_methods = Payment_method::with('journals')->latest()->get();
        }else{
            $payment_methods = Payment_method::with(['journals' =>function($query){
                $query->where('branch_id','=',Auth::user()->branch_id);
            }])->latest()->get();
        }
        //$payment_methods=Payment_method::all();
        $fixed_assets = Account::where([['type','=','Operating Expense'],['id','!=',12]])->orWhere('type','=','Non-Operating Expense')->get();
        $branches = Branch::where('status','=',1)->get();
        return view('admin.primary_settings.expense',compact('fixed_assets','branches','payment_methods'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        // return $request;
        if(!Gate::allows('isSuperadmin')){
            abort(403,auth()->user()->user_type." is not allowed for this action");
        }
        $request->validate([
            'company_name' => 'required',
            'company_logo' => 'mimes:jpeg,png|max:250',//|dimensions:width=190,height=37
            'prodct_placeholder' => 'mimes:png|max:250',//|dimensions:width=190,height=37
            'phone' => 'required|regex:/(01)[0-9]{9}/',
            'branch_can_purchase' => 'boolean',
            'email' => 'required|email',
            'address' => 'required',
            'thana' => 'required',
            'district' => 'required',
            'postal_code' => 'required',
            'product_barcode_type' => 'required',
            'bin_no' => 'numeric|nullable',
            'warranty_alert_time' => 'required|numeric',
            'expire_alert_time' => 'required|numeric',
            'check_alert_time' => 'required|numeric'
            ]);
            
        $primary_settings = PrimarySettings::latest()->first();
        $logo = $request->file('company_logo');
            if($logo){
                $logo_path = 'upload/logo/';
                    if (!file_exists($logo_path)) {
                        mkdir($logo_path, 666, true);
                    }
                $logo_name = 'logo.'.$logo->getClientOriginalExtension();
                $logo->move($logo_path,$logo_path.$logo_name);
                $primary_settings->company_logo = $logo_path.$logo_name;
            }
        
            $product_placeholder_image = $request->file('prodct_placeholder');

            if($product_placeholder_image){
                $placeholder_image_path = 'default/images/';
                    if (!file_exists($placeholder_image_path)) {
                        mkdir($placeholder_image_path, 666, true);
                    }
                $placeholder_image_name = 'default.'.$product_placeholder_image->getClientOriginalExtension();
                $product_placeholder_image->move($placeholder_image_path,$placeholder_image_path.$placeholder_image_name);
            }

        if($primary_settings == null)
        {
            $primary_settings = new PrimarySettings;
            $primary_settings->company_name = $request->company_name;
            $primary_settings->phone = $request->phone;
            $primary_settings->email = $request->email;
            $primary_settings->address = $request->address;
            $primary_settings->thana = $request->thana;
            $primary_settings->district = $request->district;
            $primary_settings->postal_code = $request->postal_code;
            $primary_settings->bin_no = $request->bin_no;
            $primary_settings->product_barcode_type = $request->product_barcode_type;

        if($request->salesperson_feature == 1){

            $primary_settings->salesperson_feature = $request->salesperson_feature;

        }else{

            $primary_settings->salesperson_feature = 0;

        }
        if($request->branch_can_purchase == 1){

            $primary_settings->branch_can_purchase = $request->branch_can_purchase;

        }else{

            $primary_settings->branch_can_purchase = 0;

        }

        $primary_settings->warranty_alert_time = $request->warranty_alert_time;
        $primary_settings->expire_alert_time = $request->expire_alert_time;
        $primary_settings->check_alert_time = $request->check_alert_time;
        $primary_settings->save();

        return redirect()->back()->with('status','Updated Primay Information successfully');

        }else{
            $primary_settings->company_name = $request->company_name;
            $primary_settings->phone = $request->phone;
            $primary_settings->email = $request->email;
            $primary_settings->address = $request->address;
            $primary_settings->thana = $request->thana;
            $primary_settings->district = $request->district;
            $primary_settings->postal_code = $request->postal_code;
            $primary_settings->bin_no = $request->bin_no;
            $primary_settings->product_barcode_type = $request->product_barcode_type;

        if($request->salesperson_feature == 1){

            $primary_settings->salesperson_feature = $request->salesperson_feature;

        }else{

            $primary_settings->salesperson_feature = 0;
        }
        if($request->branch_can_purchase == 1){

            $primary_settings->branch_can_purchase = $request->branch_can_purchase;

        }else{

            $primary_settings->branch_can_purchase = 0;

        }
            $primary_settings->warranty_alert_time = $request->warranty_alert_time;
            $primary_settings->expire_alert_time = $request->expire_alert_time;
            $primary_settings->check_alert_time = $request->check_alert_time;
            $primary_settings->update();
            return redirect()->back()->with('status','Updated Primay Information successfully');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($locale)
    {
        if($locale==1){
        App::setLocale('bn');
        Session::put('locale', 'bn');
        }else{
        App::setLocale('en');
        Session::put('locale', 'en');
        }
        return 1;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
