<?php

namespace App\Http\Controllers;

use App\Discount_offer;
use App\Branch_inventory;
use App\Customer_group;
use App\CustomerCriteria;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DiscountOfferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $search = request('search');
       $discount_index = Discount_offer::where('discount_title','like',"%$search%")
       ->orWhere('discount_amount','like',"%$search%")
       ->orWhere('promo_code','like',"%$search%")
       ->orWhere('discount_percent','like',"%$search%")
       ->latest()
       ->paginate(10);
		return view('admin.discount_offer.index',compact('discount_index'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = Branch_inventory::where('stock_quantity','>',0)->get();
        $groups = Customer_group::all();
        $criterias = CustomerCriteria::all();
        return view('admin.discount_offer.create',compact('products','groups','criterias'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,['discount_title'=>'required','discount_type'=>'required','discount_on'=>'required','offer_from'=>'before:offer_to']);

        $customerGroup=$request->customerGroup;
        $customerCriteria=$request->customerCriteria;
        if($request->discount_on==0){
            $store_discount = new Discount_offer;
            $store_discount->promo_code = $request->promo_code;
            $store_discount->discount_title = $request->discount_title; 
            $store_discount->offer_from = $request->offer_from;
            $store_discount->offer_to = $request->offer_to;
            $request->product_id==null? $store_discount->status = 0 : $store_discount->status = 1;
            $request->discount_type==0? $store_discount->discount_percent = ($request->discount_rate/100):$store_discount->discount_amount = $request->discount_rate;
            $store_discount->created_by = Auth::id();		
            $store_discount->save();

            if($customerGroup){
                $discountable = Discount_offer::find($store_discount->id);
                $discountable->customer_groups()->sync($customerGroup);
            }

        }else if(!$request->product_name){ 
            $store_discount = new Discount_offer;
            $store_discount->discount_title = $request->discount_title; 
            $store_discount->offer_from = $request->offer_from;
            $store_discount->offer_to = $request->offer_to;
            $request->product_id==null? $store_discount->status = 0 : $store_discount->status = 1;
            $request->discount_type==0? $store_discount->discount_percent = ($request->discount_rate/100):$store_discount->discount_amount = $request->discount_rate;
            $store_discount->created_by = Auth::id();

            $store_discount->save();
            
            if($customerGroup){
                $discountable = Discount_offer::find($store_discount->id);
                $discountable->customer_groups()->sync($customerGroup);
            }
        }else{
            for ($product_counter=0; $product_counter < count($request->product_name) ; $product_counter++) { 
                $store_discount = new Discount_offer;

                $existing_discount_offer = Discount_offer::whereStatus(1)->whereProductId($request->product_name[$product_counter])->get();
                if($existing_discount_offer){
                    foreach ($existing_discount_offer as $key => $offer) {
                        $offer->status = 0;
                        $offer->update();
                    }
                }

                $store_discount->product_id = $request->product_name[$product_counter];
                $store_discount->purchase_id = $request->purchase_id[$product_counter]; 
                $store_discount->discount_title = $request->discount_title; 
                $store_discount->offer_from = $request->offer_from;
                $store_discount->offer_to = $request->offer_to;
                $store_discount->status = 1;
                $request->discount_type==0? $store_discount->discount_percent = ($request->discount_rate/100):$store_discount->discount_amount = $request->discount_rate;
                $store_discount->created_by = Auth::id();		
                $store_discount->save();

                //  $store_discount->customer_groups()->attach($customerGroup);
                if($customerGroup){
                    $discountable = Discount_offer::find($store_discount->id);
                    $discountable->customer_groups()->sync($customerGroup);
                }
            }
            
        }

    return redirect()->route('discount_offers.index')->with('status','Discount Offer saved successfully');
}

    /**
     * Display the specified resource.
     *
     * @param  \App\Discount_offer  $discount_offer
     * @return \Illuminate\Http\Response
     */
    public function show(Discount_offer $discount_offer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Discount_offer  $discount_offer
     * @return \Illuminate\Http\Response
     */
    public function edit(Discount_offer $discount_offer)
    {
        // return $discount_offer;
        $purchases = Branch_inventory::with('purchase')->where('product_id','=',$discount_offer->product_id)->where('stock_quantity','>',0)->get();
        $groups = Customer_group::whereStatus(1)->get();
        // $criterias = CustomerCriteria::all();
        $products = Branch_inventory::where('stock_quantity','>',0)->get();
        
        return view('admin/discount_offer/edit',compact('purchases' , 'products' , 'discount_offer','groups'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Discount_offer  $discount_offer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Discount_offer $discount_offer)
    {
        
        //  return $discount_offer;
         $discount_offer->discount_title = $request->discount_title;
        if($request->discount_on==0){
            $discount_offer->promo_code = $request->promo_code;
            $discount_offer->product_id=null;
        }else if($request->discount_on==1){
            $discount_offer->product_id = $request->product_name;
            $discount_offer->purchase_id = $request->purchase_id;
            $discount_offer->promo_code=null;
        }
            $discount_offer->offer_from = $request->offer_from; 
            $discount_offer->offer_to = $request->offer_to; 
            
            if($request->discount_type==0){
            $discount_offer->discount_percent = $request->discount_rate/100; 
            $discount_offer->discount_amount=null;
        }
        
        else{
            $discount_offer->discount_amount = $request->discount_rate; 
            $discount_offer->discount_percent=null;
        }

        $discount_offer->created_by = Auth::id();

        $discount_offer->update();
        return redirect()->route('discount_offers.index')->with('status','Discount Offer updated successfully');
        }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Discount_offer  $discount_offer
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Discount_offer::destroy($id);
        return redirect()->route('discount_offers.index')->with('status','Discount data deleted successfully');
    }

    /**
     * ajax for showing purchase_id field while click on product_id
     */
    public function ajaxpurchaseid($product_id)
    {
        $purchases=Branch_inventory::where('product_id','=',$product_id)->where('stock_quantity','>',0)->get();
        $returndata="<br>";
        $returndata.="<label for='purchase_id'>Purchase Price / Invoice No.</label>";
        $returndata.="<select class='form-control' id='purchase_id' onchange='purchaseid()'>";
        $returndata.="<option value='' >Not Applicable</option>";        
            foreach($purchases as $product){
                $returndata.="<option value='".$product->purchase_id."'> Inv: ".$product->purchase->invoice_no." / Selling Price: ".$product->purchase->purchase_unit_price."</option>";
            }
        $returndata.='</select>';
        return $returndata;
    }
    
    /**
     * restricting active,inactive status . While a user will select 'All product or will use promo_code'.initially all status will be inactive. whenever a user will activate a discount offer for "all product" other "all product" status will automatically remain inactive.
     */
    public function discount_status(Request $request,$id){
      $status = Discount_offer::find($id);
      $status_remaider = Discount_offer::doesntHave('customer_groups')->where([['promo_code','=',null],['product_id','=',null]])->get();
    //  dd($status_remaider);
      if($request->status){
         $status->status =0;
         $status->update();
     }else{
         $status->status = 1;
         $status->update();
         $active_id=$status->id;
         //check updated status has product id or not
         $find_updated_status=Discount_offer::find($active_id);
         if($find_updated_status->product_id==null && $find_updated_status->promo_code==null){
         foreach($status_remaider as $sr){
          if(($sr->id!==$active_id) && ($sr->status!==0)){
              $update_status=Discount_offer::find($sr->id);
              $update_status->status=0;
              $update_status->update();
          }
      }
    }
      
  }

  return redirect()->back();
}
    public function find_product_for_discount($product_id,$purchase_id)
    {
         $product = Branch_inventory::where(function($query)use($product_id,$purchase_id){

            $query->whereHas('purchase',function($product_query)use($purchase_id){
                $product_query->where('id',$purchase_id);
            });

            $query->whereHas('product',function($product_query)use($product_id){
                $product_query->where('id',$product_id);
            });
        })
        ->where('stock_quantity','>',0)
        ->with('product','purchase')->first();  
        return $product;
    }
}
