<?php

namespace App\Http\Controllers;

use App\Vat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class VatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vat_title = "";
        if(isset($_GET['vat_title'])){
            $vat_title = $_GET['vat_title'];
        }
        $vats = Vat::where('vat_title','LIKE',"%$vat_title%")->latest()->paginate(5);
        return view('admin.vat.index',compact('vats'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.vat.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'vat_title' => 'required',
            'vat_description' => 'required',
            'vat_percentage' => 'required',

        ]);
        
        $vat = new Vat;
        $vat->vat_title = $request->vat_title;
        $vat->vat_description = $request->vat_description;
        $vat->vat_percentage = ($request->vat_percentage/100);
        $vat->created_by = Auth::id();
        $vat->save();
        return redirect()->route('vat.index')->with('status','Vat Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Vat  $vat
     * @return \Illuminate\Http\Response
     */
    public function show(Vat $vat)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Vat  $vat
     * @return \Illuminate\Http\Response
     */
    public function edit(Vat $vat)
    {
        return view('admin.vat.edit',compact('vat'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Vat  $vat
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vat $vat)
    {
        if($request->status){
            $vat->status = 0;
            $vat->update();
            return redirect()->back();
        }else{
            $vat->status = true;
            $cats = Vat::where('status',1)->get();
            foreach($cats as $cat){
                $cat->status = 0;
                $cat->update();
            }
            $vat->update();
            return redirect()->back();
        }
        $this->validate($request,[
            'vat_title' => 'required',
            'vat_percentage' => 'required',
            'vat_description' => 'required',
        ]);

        $vat->vat_title = $request->vat_title;
        $vat->vat_percentage = $request->vat_percentage;
        $vat->vat_description = $request->vat_description;
        $vat->save();
        return redirect()->route('vat.index')->with('status','Vat updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Vat  $vat
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $vat = Vat::find($id);
		$vat->delete();
		return redirect()->route('vat.index')->with('status','Vat Deleted Successfully');
    }
}
