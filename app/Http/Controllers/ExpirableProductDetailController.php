<?php

namespace App\Http\Controllers;

use App\Expirable_product_detail;
use Session;
use Illuminate\Http\Request;

class ExpirableProductDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $now=date('d-m-Y');
        $expirable_product_detail=Expirable_product_detail::where([['status','=',1],['expire_date','>',$now]])->get();
        return view('admin.expirable_product.index',compact('expirable_product_detail'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Expirable_product_detail  $expirable_product_detail
     * @return \Illuminate\Http\Response
     */
    public function show(Expirable_product_detail $expirable_product_detail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Expirable_product_detail  $expirable_product_detail
     * @return \Illuminate\Http\Response
     */
    public function edit(Expirable_product_detail $expirable_product_detail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Expirable_product_detail  $expirable_product_detail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Expirable_product_detail $expirable_product_detail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Expirable_product_detail  $expirable_product_detail
     * @return \Illuminate\Http\Response
     */
    public function destroy(Expirable_product_detail $expirable_product_detail)
    {
        //
    }
}
