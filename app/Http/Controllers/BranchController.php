<?php

namespace App\Http\Controllers;

use App\Branch;
use App\Address_info;
use App\Account;
use App\Transaction;
use App\Journal;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BranchController extends Controller
{
    /**
     * Display a listing of the resource.
     *  
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Only super admin , admin and manager can see the branches
        if (Gate::allows('isSuperadmin')){
           

            $branch_name_search="";
            if(isset($_GET['searchby'])){
                $branch_name_search=$_GET['searchby'];
            }
             $addresses = Branch::where('name','LIKE',"%$branch_name_search%")->paginate(10);
            return view('admin.branch.index',compact('addresses'));
        }else{
            abort(403,Auth::user()->user_type." is not permitted for this action.");
        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Only Super Admin and Admin can create branch
        if (Gate::allows('isSuperadmin')) {
            $fixed_assets = Account::where('type','=','Fixed Assets')->orWhere('type','=','Current Assets')->whereNotIn('id',[2,10,5])->get();
           return view('admin.branch.create',compact('fixed_assets'));
        }else{
            abort(403, Auth::user()->user_type." is not permitted for this action.");
        }
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $request->validate([
            'name'=>'required|unique:branches,name',
            'email'=>'required|email',
            'fulladdress'=>'required',
            'branch_type'=>'required'
        ]);
        if(isset($request->account_head)){
            $request->validate([
            'begining_balance'=>'required',
            'account_head.*'=>'unique:accounts,account_head'
        ]);
        }
        //return $request;
         // Only Super Admin and Admin can create branch
        if (Gate::allows('isSuperadmin')) {
            $journal_posted='';
            $account_id=['default'];
            if($request->asset!=null){
                $arrayId=$request->asset;
                $account_id=array_merge($account_id,$arrayId);
            }
             //return $account_id;
            // return 
            $begining_balance = $request->begining_balance;      
            $begining_balance = array_values(array_filter($begining_balance));
            $total=array_sum($begining_balance);
            
            $address = new Address_info;
            $address->email = $request->email;
            $address->full_address = $request->fulladdress;
            $address->phone = $request->phone; 
            $address->save();
            $address_id = $address->id;
    // Add new branch
            $branch = new Branch;
            $branch->name = $request->name;
            $branch->branch_type = $request->branch_type;
            $branch->address_info_id =$address_id;
            $branch->begining_balance = $total;
            $branch->save();
            $branch_id = $branch->id;


    if($request->account_head!=null){
        $account_head=$request->account_head;
        $type=$request->type;
        $counter=count($account_head);

        for($i=0;$i<$counter;$i++){
            $account = new Account;
            $account->account_head = $request->account_head[$i];
            $account->type = $request->type[$i];
            $account->status = 1;
            $account->save();
            $stringVar=(string) $account->id;      
            array_push($account_id,$stringVar);
        }
    } 
        //jaournal and transaction for primary 
        //dd($account_id);

        if($branch_id){
            $transaction=new Transaction;
            $transaction->event_id=$branch_id;
            $transaction->event_type="begining_balance";
            $transaction->branch_id=$branch_id;
            $transaction->created_by=Auth::id();
            $transaction->save();
            $transaction_id=$transaction->id;

            //journal entry;
            if($transaction_id){
               //  [6- A/p,5-A/R,2-cash,10-bank,12-Discount on Receive,13-Discount on Payment 8-Capital]
                //executing journal for account payment or collect.;
                $related_party_type='Owner';
                $related_party_id=8;
                $payment_method_id=null;
                $description='Initial Capital Invested';
                $idCounter=count($account_id);
                for($i=0;$i<($idCounter-1);$i++){
                    $accounts_id=$account_id[($i+1)];
                    $journal_type=0;
                    $amount_get=$begining_balance[$i];

                    $journal_posted.=$this->journal_entry($transaction_id,$related_party_id,$related_party_type,$journal_type,$accounts_id,$payment_method_id,$description,$amount_get,$branch_id);
                }//for loop
                        $accounts_id=8;
                        $journal_type=1;
                        $amount_get= $total;
                    $journal_posted.=$this->journal_entry($transaction_id,$related_party_id,$related_party_type,$journal_type,$accounts_id,$payment_method_id,$description,$amount_get,$branch_id);
            }//transaction id
        }//Branch ID
        //End jaournal and transaction for primary setting
    //end of account head insert 
    return redirect()->route('branches.index')->with('status','Branch has been created successfully!');
        }else{
            abort(403, Auth::user()->user_type." is not permitted for this action.");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function show(Branch $branch)
    {
        // When unauthoried users try to delete branch are redirect to error page but query string remain for deletion process and if the user refresh the page this show method is called.
        // To redirect the user we used the redirect method here.
        return redirect()->route('branches.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function edit(Branch $branch)
    {
        // Only Super Admin and Admin car edit branch
        if (Gate::allows('isSuperadmin')) {
            return view('admin.branch.edit',compact('branch'));
        }else{
            abort(403, Auth::user()->user_type." is not permitted for this action.");
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Branch $branch)
    {
        // Only Super Admin and Admin can update branch
        if(Gate::allows('isSuperadmin')) {
            $branch->name = $request->name;
            $branch->branch_type = $request->branch_type;
            $branch->update();
            
            $address = Address_info::find($branch->address_info_id);
            $address->email = $request->email;
            $address->full_address = $request->fulladdress;
            $address->phone = $request->phone; 
            $address->update();
            return redirect()->route('branches.index')->with('status','Branch has been updated successfully!');
        }else{
            abort(403, Auth::user()->user_type." is not permitted for this action.");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function destroy(Branch $branch)
    {
        // Only Super Admin Can Delete Branch
        if (Gate::allows('isSuperadmin')) {
            Address_info::destroy($branch->address_info_id);
            Branch::destroy($branch->id);
            return redirect()->route('branches.index')->with('status','Branch data has been deleted successfully');
        }else{
            abort(403, Auth::user()->user_type." is not permitted for this action.");
        }
        
    }
    public function journal_entry($transaction_id,$related_party_id,$related_party_type,$journal_type,$accounts_id,$payment_method,$description,$amount,$branch_id)
    {
        $journal=new Journal;
        $journal->transaction_id=$transaction_id;
        $journal->created_by=Auth::id();
        $journal->branch_id=$branch_id;
        $journal->related_party_id=$related_party_id;
        $journal->related_party_type=$related_party_type;
        $journal->journal_type=$journal_type; //0->dr & 1->cr.
        $journal->accounts_id=$accounts_id;
        $journal->amount=$amount;
        $journal->payment_method_id=$payment_method;
        $journal->description =$description;
        $journal->save();
        return $journal->id.", ";
    }
}
