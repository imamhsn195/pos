<?php

namespace App\Http\Controllers;
use Auth;
use App\Branch_inventory;
use Illuminate\Http\Request;
use App\Product;
use App\Supplier;
use App\Purchase;
use App\Category;

class BranchInventoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $branch_id= Auth::user()->branch_id;
        
      
       if ($branch_id == null) {
            $where[] = ['branch_id','like',"%$branch_id%"];
       }else{
            $where[] = ['branch_id','=',$branch_id];
       }
       // total stock of Inventory
       $total_products_in_stock = Branch_inventory::with('purchase','product')->where('stock_quantity' , '>' , 0)->where($where)->get();
       $total_purchase_price = 0;
       $total_selling_price = 0;

       // Add Total Purchase Price and Sale Price of Products
       foreach ($total_products_in_stock as $product_in_stock) {
           $total_purchase_price += $product_in_stock->purchase->purchase_unit_price*$product_in_stock->stock_quantity;
           $total_selling_price += $product_in_stock->purchase->sell_unit_price*$product_in_stock->stock_quantity;
       }


       $products = Branch_inventory::with('purchase.supplier','product')
        ->where(function($query){
            if(request('supplier_id') && request('supplier_id')!=='all'){
                $query->whereHas('purchase.supplier',function($q){
                    $q->where('id','=',request('supplier_id'));
                });
            }
            if(request('category_id') && request('category_id')!=='all'){
                $query->whereHas('product.category',function($q){
                    $q->where('id','=',request('category_id'));
                });
            }
            if(request('product_sku') && request('product_sku')!==''){
                $query->whereHas('product',function($q){
                    $q->where('product_sku','like',"%".request('product_sku')."%");
                });
            }
            if(request('type') && request('type') !=='all'){
                if(request('type') =='warrantiable'){
                    $query->whereHas('product.category',function($q){
                    $q->where('is_warrantable','=',true);
                    });
                }elseif(request('type') =='expirable'){
                    $query->whereHas('product.category',function($q){
                    $q->where('is_expirable','=',true);
                    });
                }
               
            }
            
        })
        ->where($where)
        ->orderBy('product_id','ASC')
        ->orderBy('purchase_id','ASC')
        ->get();

        /*variables for stock reports dropdawns*/
        $suppliers = Supplier::with('purchases.inventories')->wherehas('purchases')->get();
        $categories = Category::with('products.inventory')->wherehas('products')->get();
        $product_dropdown = Product::with('inventory')->get();
        /*variables for stock reports dropdawns*/
        return view('admin.product.purchase_list',compact('products','product_dropdown','suppliers','categories','total_purchase_price','total_selling_price'));
    }

    /**
     * return the purchase history for specfic product
     *
     * 
     */
    public function purchase_history($product_id)
    {
        $branch_id= Auth::user()->branch_id;
        if ($branch_id == null) {
                $where[] = ['branch_id','like',"%$branch_id%"];
        }else{
                $where[] = ['branch_id','=',$branch_id];
        }
        return branch_inventory::with(['purchase.supplier','product'])
        ->wherehas('product.purchases')
        ->where('product_id','=',$product_id)
        ->where($where)
        ->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Branch_inventory  $branch_inventory
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::find($id);
        return view('admin.sale.show',compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Branch_inventory  $branch_inventory
     * @return \Illuminate\Http\Response
     */
    public function edit(Branch_inventory $branch_inventory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Branch_inventory  $branch_inventory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Branch_inventory $branch_inventory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Branch_inventory  $branch_inventory
     * @return \Illuminate\Http\Response
     */
    public function destroy(Branch_inventory $branch_inventory)
    {
        //
    }
    public function addToCart($id)
    {
        $cp = Branch_inventory::find($id);
            $prepend_cart_tr = "<tr>";
            $prepend_cart_tr .= "<td>".$cp->products->id."</td>";
            $prepend_cart_tr .= "<td>".$cp->products->product_sku."</td>";
            $prepend_cart_tr .= "<td>1</td>";
            $prepend_cart_tr .= "<td>".$cp->purchase->sell_unit_price."</td>";
            $prepend_cart_tr .= "<td id='subtotal".$cp->products->id."'></td>";
            $prepend_cart_tr .= "<td>x</td>";
            $prepend_cart_tr .= "</tr>";
        return $prepend_cart_tr;

    }
}
