<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Gate;
use App\Product;
use App\Purchase;
use App\Attribute_set;
use App\Branch_inventory;
use App\Attribute;
use App\PrimarySettings;
use App\Category;
use App\Branch;
use App\Supplier;
use App\Vat;
use App\Brand;
use App\Payment_method;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Only Super Admin , Admin and Manager car see and register product 
      if (Gate::allows('isSuperadmin') || Gate::allows('isAdmin') || Gate::allows('isManager')) {
        $searchby="";
        if(isset($_GET['searchby'])){
            $searchby=$_GET['searchby'];
        }     
        //$searchby="Samsung";       
        //$products=Product::latest()->paginate(10);

               $products=Product::where('product_sku', 'LIKE', "%$searchby%")
               ->where('status','=',1)
               ->latest()->paginate(15);
                
                return view('admin/product/index',compact('products'));
      }else{
          abort(403, Auth::user()->user_type." is not allowed to perform this action");
      }
        
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         // Only Super Admin , Admin and Manager car see and register product 
        if (Gate::allows('isSuperadmin') || Gate::allows('isAdmin') || Gate::allows('isManager')) {
            $attribute_set = Attribute_set::all();
            $vat = Vat::all();
            $brands = Brand::all();
            $categories = Category::all();
            return view('admin/product/create',compact('attribute_set','categories','vat','brands'));
        } else {
            abort(403, Auth::user()->user_type." is not allowed to perform this action");
        }
        
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request;
        // Only Super Admin , Admin and Manager car see and register product 
        if (Gate::allows('isSuperadmin') || Gate::allows('isAdmin') || Gate::allows('isManager')) {
            $request->validate([
                'category'=>'required',
                'product_name'=>'required',
                'brand' => 'required',
            ]);
            //start here
            if($request->product_barcode){
                $request->validate([
                    'product_barcode'=>'unique:products'
                    ]);
                    $product_barcode=$request->product_barcode;
            }else{
                $product_barcode=0;
            }
            $brand=str_before($request->brand,"|");
            $brand_name=str_after($request->brand,"|");
            $image=$request->product_img;

            if ($request->attribute_set){
                $attr_number=Attribute_set::find($request->attribute_set)->attribute->count();
                $result_arry=$request->attribute_submited_value;
                $res= array_chunk($result_arry,$attr_number);
                $res_counter=count($res);
            }else{
                $res_counter=1;
            }
            $num=0;
            while($num < $res_counter){
                $uniq_name="";
                if ($request->attribute_set){
                    $uniq_name="";
                    foreach($res[$num] as $v){
                        $attribute=Attribute::find($v);
                        $uniq_name .=" ".$attribute->attribute_name;
                    }//endforeach
                }//endif
       
                $uniq_name_to_be_submited = $brand_name." ".$request->product_name." |".$uniq_name;
                $unique_product =  Product::where('product_sku',$uniq_name_to_be_submited)->get()->count();
                if ($unique_product) {
                    return back()->with('warning','The product name is already used!');
                }
                $product=new Product;
                $product->product_sku =  $uniq_name_to_be_submited;
                $product->reorder_limit=$request->reorder_limit;
                $product->product_barcode=$product_barcode?($product_barcode[$num]?$product_barcode[$num]:null):null;
                $product->short_description=$request->short_description;
                $product->long_description=$request->long_description;
                $product->brand_id=$brand;
                // $product->vat_id=$request->vat;
                $product->category_id=$request->category;
                $product->attribute_set_id=$request->attribute_set;
                $product->created_by=Auth::id();
                $product->save();
                $product_id=$product->id;

               if($request->attribute_set){
                    $product_find=Product::find($product_id);
                    $product_find->submittedattribute()->attach($res[$num]);
               }
                if($image){
                    $path='upload/product_img/';
                    $name=$product_id.".".$image->getClientOriginalExtension();
                    $image_url=$path.$name;
                    if($num==0){
                        $image->move($path,$name);
                        //$image_url=$path.$name;
                        $old_image_url=$path.$name;
                    }else{
                        \File::copy($old_image_url , $image_url);
                    }
                    $image_update=Product::find($product_id);
                    $image_update->image=$image_url;
                    $image_update->update();
                }
                $num++;
            }//endwhile
            return redirect()->route('products.index')->with('status', $num.' Product Added successfully');
        }else{
            abort(403, Auth::user()->user_type." is not allowed to perform this action");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
         // Only Super Admin , Admin and Manager car see and register product 
         if (Gate::allows('isSuperadmin') || Gate::allows('isAdmin') || Gate::allows('isManager')) {
            $suggested_price="";
            $suppliers=Supplier::all();
            $branches=Branch::all();
            $payment_methods=[];
            $user_type =  Auth::user()->user_type;
            if($user_type == 'superadmin'){
                 $payment_methods = Payment_method::with('journals')->latest()->get();
            }else{
                $payment_methods = Payment_method::with(['journals' =>function($query){
                    $query->where('branch_id','=',Auth::user()->branch_id);
                }])->latest()->get();
            }
            //end payment restriction
            if(Auth::user()->user_type!=="superadmin"){
                $suggested_price=Purchase::where([['product_id','=',$product->id],['branch_id','=',Auth::user()->branch_id]])->latest()->first();
            }
            //dd($suggested_price);
            return view('admin/purchase/create',compact('product','suppliers','branches','payment_methods','suggested_price'));
        }else {
            abort(403, Auth::user()->user_type." is not allowed to perform this action");
        }
 }

    public function get_product_details($product_id)
    {
        if(request('product_with_invoice')){
             $products=Purchase::with('product')->whereProductId($product_id)->get();
             return $products;
        }
        $product=Product::find($product_id);
        $retutndata='';
        
         $suggested_price=Purchase::where([['product_id','=',$product->id],['branch_id','=',Auth::user()->branch_id]])->latest()->first();
         //dd($suggested_price);
         $retutndata.='<h4 class="card-title">'.$product->product_sku.'</h4>
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <tr>
                                    <th>ID :</th>
                                    <td>'.$product->id.'</td>
                                </tr>
                                <tr>
                                    <th>Image :</th>
                                    <td><img src="'.url($product->image).'" width="100px"></td>
                                </tr>
                                <tr>
                                    <th>Product Name :</th>
                                    <td>'.str_limit($product->product_sku,20).'</td>
                                </tr>
                                <tr>
                                    <th>Category :</th>
                                    <td>'.$product->category->category_name.'</td>
                                </tr>
                                <tr>
                                    <th>Configuration :</th>
                                    <td>';
                                    $confiqr='';
                                    $a=0;
                                        if($product->attributeset){
                                        foreach($product->attributeset->attribute as $confiq){
                                        $confiqr.= $confiq->attribute_name.' : '.$product->submittedattribute[$a++]->attribute_name."<br>";
                                        }//endforeach
                                        }//endif
                                        $retutndata.=$confiqr.'</td>
                                </tr>

                                <tr>
                                    <th>Expirable ?</th>
                                    <td>';
                                    $retutndata.= $product->category->is_expirable ? 'Yes' :'No';
                                    $retutndata.='<input type="hidden" id="is_expirable" value="'.$product->category->is_expirable.'"></td>
                                </tr>
                                <tr>
                                    <th>warrantable ?</th>
                                    <td>';
                                    $retutndata.=$product->category->is_warrantable ? 'Yes' : 'No';
                                    $retutndata.='<input type="hidden" id="is_warrantable" value="'.$product->category->is_warrantable.'"></td>
                                </tr>
                                <tr class="table-active">
                                    <th>Last Sold unit Price:</th>
                                    <td>';
                                    $retutndata.=$suggested_price?$suggested_price->sell_unit_price:"Yet not purchased";
                                    $retutndata.='</td>
                                </tr>
                                <tr class="table-active">
                                    <th>Last Purchased unit Price:</th>
                                    <td>';
                                    $retutndata.=$suggested_price?$suggested_price->purchase_unit_price:"Yet not purchased";
                                    
                                    $retutndata.='</td>
                                </tr>
                            </table>
                        </div>';
                        return $retutndata;
 }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
    // Only Super Admin , Admin and Manager car see and register product 
    if (Gate::allows('isSuperadmin') || Gate::allows('isAdmin') || Gate::allows('isManager')) {
        $attribute_set = Attribute_set::all();
        $attribute = Attribute::all();
        $vat = Vat::all();
        $brands = Brand::all();
        $categories = Category::all();
        return view('admin/product/edit',compact('product','attribute','attribute_set','categories','vat','brands'));
        }else{
            abort(403, Auth::user()->user_type." is not allowed to perform this action");
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Product $product)
    {
        //$product=Product::find($id);
        // Only Super Admin , Admin and Manager car see and register product 
    if (Gate::allows('isSuperadmin') || Gate::allows('isAdmin') || Gate::allows('isManager')) {
        $uniq_name="";
        $image=$request->product_img;

        if($image){
            $path='upload/product_img/';
            $name=$product->id.".".$image->getClientOriginalExtension();
            $image_url=$path.$name;
            $image->move($path,$name);
            $product->image=$image_url;
        }

        $product_name=explode(" |",$request->product_name);
       
        if ($request->attribute_set){
          $res=$request->attribute_submited_value;
                foreach($res as $v){
                $attribute=Attribute::find($v);
                    $uniq_name .=" ".$attribute->attribute_name;
                }
                    $product->submittedattribute()->sync($res);     
            }
      //dd($uniq_name);
        $product->product_sku=$product_name[0]." |".$uniq_name;
        $product->reorder_limit=$request->reorder_limit;
        $product->short_description=$request->short_description;
        $product->long_description=$request->long_description;
        $product->product_barcode=$request->product_barcode;
        $product->brand_id=str_before($request->brand,'|');
        // $product->vat_id=$request->vat;
        $product->category_id=$request->category;
        $product->attribute_set_id=$request->attribute_set;
        $product->last_updated_by=Auth::id();
        //return $product;
        $product->update();
    
        return redirect()->route('products.index')->with('status','Product Updated successfully');
    
    }else{
        abort(403, Auth::user()->user_type." is not allowed to perform this action");
    }}

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->status = 0;
        $product->update();
        // \File::delete($product->image);
        // Product::destroy($product->id);
        return redirect()->back()->with('status','Product is deleted successfully!');
    }

// This function is used for barcode generate and show
    public function barcodeget(){
        $product = Product::where('product_barcode', '<>' , null)->get();
        return view('admin.product.barcode',compact('product'));
    }
    public function barcodepost(Request $request)
    {    
        $request->validate([
            'barcodeData'=>'required',
            'barcodeQty'=>'required|numeric|max:20|min:1',
        ]);

        // return $request; ¯\_(ツ)_/¯
        $product = Product::where('product_barcode', '<>' , null)->get();

        $product_searched = Product::where('product_barcode','=',trim(str_before($request->barcodeData,'--')))
        // ->orWhere('product_sku','=',str_after($request->barcodeData,'--'))
        ->first();
        $barcode = [];
        $barcode['productbarcode'] = trim(str_before($request->barcodeData,'--'));
        $barcode['productsku'] =$product_searched->product_sku;
        $barcode['productname'] = str_before($barcode['productsku'],'|');
        $barcode['productattribute'] = str_after($barcode['productsku'],'|');
        $barcode['barcode_column'] = $request->barcode_column;

        $product_price = Branch_inventory::with('purchase')
        ->wherehas('product',function($query)use($barcode){
            $query->where('product_barcode','=',$barcode['productbarcode']);
        })
        ->wherehas('purchase',function($query){
            $query->orderBy('created_at','ASC');
        })->first(); 

        if($product_price){
            $barcode['product_price'] = $product_price->purchase->sell_unit_price;
        }else{
            $barcode['product_price'] = "Not Available"; 
        }
        $productSearch = Product::where('product_barcode','=',$barcode['productbarcode'])->count();
            if($productSearch == 0){
                return redirect()->back()->withErrors('No such  product found!');
            }
         $barcode['qty'] = $request->barcodeQty;
         $barcode['show_product_name'] = $request->product_name=='on'?true:false;
         $barcode['show_product_attribute'] = $request->product_attribute=='on'?true:false;
         $barcode['show_product_price'] = $request->selling_price=='on'?true:false;
         $barcode['show_product_barcode'] = $request->barcode_number=='on'?true:false;
         $barcode['barcode_per_column'] = $request->barcode_column;

         $barcode['barcode_type'] = PrimarySettings::latest()->first()->product_barcode_type;
        //  $barcode;
         return view('admin.product.barcode',compact('product','barcode'));
    }
    public function productTracking()
    {
        $branch_id =  Auth::user()->branch_id;
        $where = [];
        if($branch_id !== null){
            $where[] = ['branch_id','=',$branch_id];
        }else{
             $where[] = ['branch_id','like',"%$branch_id%"];
        }
       $pid= str_before(request('product_id'),'--');
       $products=Product::all();
       $productTracks=[];
       if($pid!=null){
        //$productTracks=Product::find($pid);
        $productTracks=Product::whereHas('purchases', function ($query)use($where){
            $query->where($where);
        })->find($pid);
       }
       return view('admin.product.tracking',compact('products','productTracks'));
       
       
    }
}