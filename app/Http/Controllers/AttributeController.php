<?php

namespace App\Http\Controllers;

use App\Attribute;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Category;
use App\Warranty_type;
use Gate;
use App\User;
use DB;
use Validator;



class AttributeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $searchby="";
        if(isset($_GET['searchby'])){
            $searchby=$_GET['searchby'];
        }

        /**
         * restricting user permission
         */
        $user_type =  Auth::user()->user_type;
		if(Gate::allows('isManager') || Gate::allows('isAdmin') || Gate::allows('isSuperadmin')){
			$attributes=Attribute::where('attribute_name', 'LIKE', "%$searchby%")->latest()->where('parent_id',null)->whereStatus(1)->paginate(10);
            $sub_attr=Attribute::all();
            //dd($attributes);
            return view('admin/attribute/index',compact('attributes','sub_attr'));
		}else{
            abort(403,Auth::user()->user_type." is not permitted for this page.");
        }
		   }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   // Only Super Admin Can Create attributes
        if (Gate::allows('isSuperadmin')) {
            $attributes = Attribute::all();
            return view('admin/attribute/create',compact('attributes'));
        }else{
            abort(403, Auth::user()->user_type."is not permitted for this action");
        }
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Only Super Admin can store attributes
        // if (Gate::allows('isSuperadmin')) {

            $this->validate($request,['attribute_name' => 'required|unique:attributes','attribute_values' => 'required']);
          
            $attr_values=explode ( ',' ,$request->attribute_values);
            $wp =new Attribute;
            $wp->attribute_name = $request->attribute_name;
            $wp->created_by = Auth::id();
            $wp->save();
            $parent_id=$wp->id;
            
            if($a=count($attr_values)){
                
                $it=0;
                while ($it<$a) {
                    $wp =new Attribute;
                    $wp->attribute_name = $attr_values[$it];
                    $wp->created_by = Auth::id();
                    $wp->parent_id=$parent_id;
                    $wp->save();
                    $it++; 
                } 
            }
            return redirect()->route('attributes.index');
        
        // }else{
        //     abort(403, Auth::user()->user_type." is not permitted for this action");
        // }
       
		
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Attribute  $attribute
     * @return \Illuminate\Http\Response
     */
    public function show(Attribute $attribute)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Attribute  $attribute
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Only super admin car edit attributes
        if (Gate::allows('isSuperadmin')) {
            $attr=Attribute::with("attributeVal")->find($id);
            return $attr;            
        }else{
            abort(403, Auth::user()->user_type." is not permitted for this action");
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Attribute  $attribute
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Only Super Admin Can update Attributes
        if (Gate::allows('isSuperadmin')) {
        $attr=Attribute::find($id);
        $attr->attribute_name = $request->attribute_name;
        $attr->updated_by = Auth::id();
        $attr->update();
        if($request->attribute_values){
        $attr_values=explode ( ',' ,$request->attribute_values);
        $a=count($attr_values);
            
            $it=0;
            while ($it<$a) {
                $wp =new Attribute;
                $wp->attribute_name = $attr_values[$it];
                $wp->created_by = Auth::id();
                $wp->parent_id=$id;
                $wp->save();
                $it++; 
            } 
        }
        return redirect()->route('attributes.index')->with('status','Update Successfully');            
        } else {
            abort(403, Auth::user()->user_type." is not permitted for this action");
        }
        

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Attribute  $attribute
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Only Super Admin can delete attribute
        if (Gate::allows('isSuperadmin')) {
            Attribute::destroy($id);
            return redirect()->route('attributes.index')->with('status','Deleted Successfully');            
        } else {
            abort(403, Auth::user()->user_type);
        }
        

    }
}
