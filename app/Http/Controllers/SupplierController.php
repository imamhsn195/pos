<?php

namespace App\Http\Controllers;

use App\Supplier;
use App\Purchase;
use App\Branch;
use App\Address_info;
use App\Journal;
use App\Transaction;
use App\JournalHelper\journalEntry;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Gate;
use App\User;


class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         // add search option
        

        $supplier_name_search="";
		if(isset($_GET['searchby'])){
			$supplier_name_search=$_GET['searchby'];
        }

         // authenticate users

        $user_type =  Auth::user()->user_type;
		// if(Gate::allows('branch_manager') || Gate::allows('isAdmin')){
		// 	abort(403,"$user_type is not permitted for this page.");
        // }
        // pagination,search,return view
        $branches = Branch::where('status','=',1)->latest()->get();
		$addresses = Supplier::where('supplier_name','LIKE',"%$supplier_name_search%")->latest()->paginate(10);
        return view('admin.supplier.index',compact('addresses','branches'));
    }
    public function monthlyStatusOfSupplier()
    {
        // if(request('month')){
        //     $month=date("n",(strtotime(request('month'))));
        //     //str_after(request('month'),'-'));
        //     $year=date("Y",(strtotime(request('month'))));
        //    // dd($month ."|". $year);
        // }else{
        //     $month=date('n');
        //     $year=date('Y');
        // }
        
        $branch_id =  Auth::user()->branch_id;
        $where = [];
        if($branch_id !== null){
            $where=[['related_party_type','=','supplier'],['branch_id','=',$branch_id]];
        }else{
             $where= [['related_party_type','=','supplier'],['branch_id','like',"%$branch_id%"]];
        }
		// if(Gate::allows('branch_manager') || Gate::allows('isAdmin')){
		// 	abort(403,"$user_type is not permitted for this page.");
        // }
        // pagination,search,return view
        //$branches = Branch::where('status','=',1)->latest()->get();
		$suppliers = Supplier::whereHas('journal',function($q) use($where){
            $q->where($where);
            // ->whereYear('created_at', '=', $year)->whereMonth('created_at', '=', $month);
        })->latest()->paginate(10);
        return view('admin.supplier.supplier_month_wise_status_report',compact('suppliers'));
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response*/
     
    public function create()
    {
         return view('admin.supplier.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        /**
         * validation*/
        
         $this->validate($request,[
        'full_address'=>'required',
        'phone'=>'required|unique:address_infos,phone'
        ]);
        
          //save data in database table
         
		$address = new Address_info;
		$address->email = $request->email;
		$address->full_address = $request->full_address;
		$address->phone = $request->phone; 
		$address->save();
		$address_id = $address->id;
		
        $suppliers = new Supplier;
	    $suppliers->supplier_name = $request->supplier_name;
	    $suppliers->address_info_id = $address_id;
	    $suppliers->created_by = Auth::id();
	    $suppliers->save();
		
	    return redirect()->route('suppliers.index')->with('status','Supplier details saved successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        /**
         * show specific supplier's transaction
         */ 
        $searchbyfrom = null;
        $searchbyto = null;

        if (isset($_GET['searchbyfrom'])) {
            $searchbyfrom = $_GET['searchbyfrom'];
            $searchbyto = $_GET['searchbyto'];
        }else{
            $searchbyfrom =date('Y-m-01');
            $searchbyto =date('Y-m-t');
        }
        if(Auth::user()->branch_id!=null){
            $where=[['related_party_type','=','supplier'],['related_party_id','=',$id],['branch_id','=',Auth::user()->branch_id]];
        }else{
            $where=[['related_party_type','=','supplier'],['related_party_id','=',$id]];
        }
        if(isset($_GET['ledger']) && $_GET['ledger']==1){
            $supplier_status=Journal::with('supplier')->whereBetween('created_at', [$searchbyfrom, $searchbyto])->where('accounts_id',6)->where($where)->get();
        }else{
            $supplier_status=Journal::with('supplier')->whereBetween('created_at', [$searchbyfrom, $searchbyto])->where($where)->get();
        }
        
        
            $supplier=Supplier::find($id);
            return view('admin/supplier/report',compact('supplier_status','supplier'));
    }
    public function show_invoice($id)
    {
        /**
         * show specific supplier's invoice
         */ 
        $total=0;
        $searchbyfrom = null;
        $searchbyto = null;

        if (isset($_GET['searchbyfrom'])) {
            $searchbyfrom = $_GET['searchbyfrom'];
            $searchbyto = $_GET['searchbyto'];
        }else{
            $searchbyfrom =date('Y-m-01');
            $searchbyto =date('Y-m-t');
        }
        if(Auth::user()->branch_id!=null){
            $where=[['supplier_id','=',$id],['branch_id','=',Auth::user()->branch_id]];
        }else{
            $where=[['supplier_id','=',$id]];
        } 

      
        
        $supplier_status=Purchase::with('supplier')->where($where)->whereBetween('created_at', [$searchbyfrom, $searchbyto])->get();
        $supplier_invoice=$supplier_status->groupBy('invoice_no');
        $supplier=Supplier::find($id);
   
        return view('admin/supplier/invoice_report',compact('supplier_invoice','supplier'));
    // }else{
        // return redirect()->route('suppliers.index')->with('status','Supplier Yet not interrect with Transaction');
    // }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
	    $esupplier = Supplier::with('address')->where('id','=',$id)->get();
        //  $address_info = Address_info::find($id);
        return view('admin/supplier/edit',compact('esupplier'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$this->validate($request,[
            'email'=>'email|nullable',
            'full_address'=>'required',
            'phone'=>'required'
        ]);
            $update_supplier = Supplier::findOrFail($id);
            $update_supplier->supplier_name = $request->supplier_name;
            $update_supplier->last_updated_by = Auth::id();
            $update_supplier->update();

            $update_address = Address_info::findOrFail($update_supplier->address_info_id);
            $update_address->email=$request->email;
            $update_address->phone=$request->phone;
            $update_address->full_address=$request->full_address;
            $update_address->update();
        
        return redirect()->route('suppliers.index')->with('status','Supplier data updated successfully');
		
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $supplier= Supplier::find($id);
        // $supplier->status=0;
        // $supplier->update();

		 return redirect()->route('suppliers.index')->with('status','Supplier data softly deleted successfully');
    }
    public function supplierBeginningBalance(Request $request)
    {
        
        $this->validate($request,[
            'supplier_id'=>'required',
            'branch_id'=>'required',
            'paid_amount'=>'required|numeric'
            ]);

      
        $branch_id=$request->branch_id;
        $supplier_id=$request->supplier_id;
        $related_party_id=$supplier_id;
        $description='begining_balance';
        $journal_posted='';
        $amount_get=$request->paid_amount;
        $havePreviousJournal=Journal::where([['related_party_id','=',$supplier_id],['related_party_type','=','supplier'],['journal_type','=',1],['accounts_id','=',6],['branch_id','=',$branch_id],['description','=','begining_balance']])->latest()->first();

        if(($supplier_id!=null) && ($havePreviousJournal==null)){
            $transaction=new Transaction;
            $transaction->event_id=$branch_id;
            $transaction->event_type=$description;
            $transaction->branch_id=$branch_id;
            $transaction->created_by=Auth::id();
            $transaction->save();
            $transaction_id=$transaction->id;
    
            //journal entry;
            if($transaction_id){
               //  [6- A/p,5-A/R,2-cash,10-bank,12-Discount on Receive,13-Discount on Payment 8-Capital]
                //executing journal for account payment or collect.;
                    
                    $journalEntry=new journalEntry;
                    $journal_posted.=$journalEntry->journal_entry($transaction_id,$related_party_id,'supplier',0,8,null,$description,$amount_get,$branch_id);
                    $journalEntry=new journalEntry;
                    $journal_posted.=$journalEntry->journal_entry($transaction_id,$related_party_id,'supplier',1,6,null,$description,$amount_get,$branch_id);
                    
                // }//for loop
            }//transaction id
        }
        if(($havePreviousJournal==null) && ($journal_posted!=null)){
            return redirect()->route('suppliers.index')->with('status','Supplier Balance Added successfully');
        }else{
            return redirect()->route('suppliers.index')->with('warning','Supplier Already Having Previous Balance For this Branch');
        }
    }
}
