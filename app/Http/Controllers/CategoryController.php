<?php

namespace App\Http\Controllers;

use DB;
use App\Category;
use App\Warranty_type;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Gate;
use App\User;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $a="";
		if(isset($_GET['searchby'])){
			$a=$_GET['searchby'];
        }
        $categories = Category::all();
        $all_cat_users = Category::where('category_name','like',"%$a%")->latest()->paginate(10);
        return view('admin/category/index', compact('all_cat_users','categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $warranty_type = Warranty_type::all();
        $categories = Category::all();
        return view('admin/category/create', compact('warranty_type', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      
        $this->validate($request,['category_name' => 'required|unique:categories']);
        $wp = new Category;
        $wp->category_name = $request->category_name;
        $wp->is_expirable = $request->is_expirable;
        $wp->is_warrantable  = $request->is_warrantable;
        $wp->parent_id = $request->parent_id;
        $wp->created_by = Auth::id();
        $wp->save();
        return redirect()->route('categories.index')->with('status', 'Category Added Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $all_warranty = Warranty_type::all();
        $categories = Category::all();
        return view('admin/category/edit', compact('all_warranty', 'categories', 'category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        if (isset($request->status)) {
            $request->status?$category->status =0:$category->status = 1;
            $category->update();
            return redirect()->back();
        }else{
            $category->category_name = $request->category_name;
            $category->is_expirable = $request->is_expirable;
            $category->is_warrantable  = $request->is_warrantable;
            $category->parent_id = $request->parent_id;
            $category->last_updated_by = Auth::id();
            $category->update();
        }
        
        return redirect()->route('categories.index')->with('status', 'Category updated Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Category::destroy($id);
        return redirect()->route('categories.index')->with('status', 'Category deleded Successfully!');
    }
}
