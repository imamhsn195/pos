<?php

namespace App\Http\Controllers;

use App\Sale_return;
use App\Sale;
use App\Sale_detail;
use Gate;
use App\cheque_info;
use App\Customer;
use App\Branch_inventory;
use App\Transaction;
use App\JournalHelper\journalEntry;
use App\Payment_method;
use Auth;
use Illuminate\Http\Request;

class SaleReturnController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (isset($_GET['searchbyfrom'])) {
            $searchbyfrom = $_GET['searchbyfrom'];
            $searchbyto = $_GET['searchbyto'];
        }else{
            $searchbyfrom =date('Y-m-01');
            $searchbyto =date('Y-m-t');
        }
        if(Auth::user()->branch_id!=null){
            $where=[['branch_id','=',Auth::user()->branch_id]];
        }else{
            $where=[];
        } 
        $sales_returns=Sale_return::whereHas('sale_details',function($q)use($where){
            $q->where($where);
        })->whereBetween('created_at', [$searchbyfrom, $searchbyto])->latest()->get();
            return view('admin.sales_return.index',compact('sales_returns')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        if(Gate::allows('isAdmin') || Gate::allows('isCashier') || Gate::allows('isManager')){
            $customers=Customer::all();
            $payment_methods=Payment_method::all();
            return view('admin.sales_return.create',compact('customers','payment_methods'));
        }else{
            abort(403, Auth::user()->user_type." is not allowed for this action"); 
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Gate::allows('isSuperadmin')){
            abort(403, Auth::user()->user_type." is not allowed for this action"); 
        }
        $remain_qty=$request->remain_qty?$request->remain_qty:0;
        $this->validate($request,[
            'returned_qty' => "required|lte:$remain_qty",
            'product_id' => 'required'
            ]);
        
        //return $request;
        $msg="";
        $journal_posted='';
        $product_id=str_before($request->product_id,"|");
        $sales_detail_id=str_after($request->product_id,"|");
        $purchase_id=$request->purchase_id;
        $sales_id=$request->invoice_no;
        $branch_inv_id=$request->branch_inv_id;
        $returned_qty=$request->returned_qty;
        $discount_rate=$request->discount;
        $total_price=$request->total_price;
        $p_unit_price=$request->p_unit_price;
        $customer_id=$request->customer_id;
        $amount_to_be_paid=$request->total_price;
        $returnValue=$returned_qty*$p_unit_price;
        $discount=$returned_qty*$discount_rate;
        $paid_amount=$request->paid_amount;
        $payment_method=str_before($request->payment_method_id," | ");
        $payment_method_type=str_after($request->payment_method_id," | ");
        $branch_id=Auth::user()->branch_id;
        $branch_data=Branch_inventory::where([['branch_id','=',$branch_inv_id],['purchase_id','=',$purchase_id],['product_id','=',$product_id]])->first();
        

        //check transaction has any cheque info
        if($request->cheque_disposal_date){
            $cheque_info_status='1';
            $paid_amount=0;
            $payment_method=Null;
        }else{
            $cheque_info_status='0';
        }
        if($request->check_no){
            $cheque_info=new cheque_info;
            $cheque_info->payment_method_id=str_before($request->payment_method_id," | ");
            $cheque_info->created_by=Auth::id();
            $cheque_info->related_party_id=$customer_id;
            $cheque_info->branch_id=$branch_id;
            $cheque_info->related_party_type="customer";
            $cheque_info->amount=$request->paid_amount;
            $cheque_info->cheque_sl_no =$request->check_no;
            $cheque_info->status=$cheque_info_status;
            $cheque_info->cheque_disposal_date=$request->cheque_disposal_date;
            $cheque_info->save();
    
            $msg.="Your Check has been Registered &";
        }

        $sales_return=new Sale_return;
        $sales_return->sale_details_id=$sales_detail_id;
        $sales_return->sold_return_quantity=$returned_qty;
        $sales_return->sold_return_unit_price=$p_unit_price;
        $sales_return->returned_discount_rate=$discount_rate;
        $sales_return->created_by=Auth::id();
        $sales_return->save();
        $sales_return_id=$sales_return->id;

        if($sales_return_id){
            $branch_data=Branch_inventory::where([['branch_id','=',$branch_inv_id],['purchase_id','=',$purchase_id],['product_id','=',$product_id]])->first();
            if(!$branch_data==null){
                $branch=Branch_inventory::find($branch_data->id);
                $branch->stock_quantity=($branch_data->stock_quantity + $request->returned_qty);
                $branch->update();
            }

                $transaction=new Transaction;
                $transaction->event_id=$sales_return_id;
                $transaction->event_type="sales_return";
                $transaction->branch_id=$branch_id;
                $transaction->created_by=Auth::id();
                $transaction->save();
                $transaction_id=$transaction->id;
                //journal_Entry for purchase Return's Transaction
                if($transaction_id){
                    $accounts_title=['6','11','2','10','4'];  //   A/p,purchase_return,cash,bank,sales
                    //executing journal for purchase return;
                        $related_party_id=$customer_id;
                        $related_party_type="customer";
                        $description='Sales Returned';
                    for($i=0;$i<4;$i++){
                        if($i==0){
                            $journal_type=0;
                            $accounts_id=17;
                            $amount=$returnValue;
                        
                        $journal_posted.=" | ";
                        $journalEntry=new journalEntry;
                            $journal_posted.=$journalEntry->journal_entry($transaction_id,$related_party_id,$related_party_type,$journal_type,$accounts_id,$payment_method,$description,$amount,$branch_id);
                        }
                        if($i==1){
                            $journal_type=1;
                            $accounts_id=$payment_method>0?($payment_method_type==0? "2":"10"):"5";
                            if($paid_amount>0){
                            $amount=$paid_amount;
                        
                            $journal_posted.=" | ";
                            $journalEntry=new journalEntry;
                            $journal_posted.=$journalEntry->journal_entry($transaction_id,$related_party_id,$related_party_type,$journal_type,$accounts_id,$payment_method,$description,$amount,$branch_id);
                            }
                        }
                        if($i==2){
                            $journal_type=1;
                            $accounts_id=5;
                            if($paid_amount<$amount_to_be_paid){
                            $amount=$amount_to_be_paid-$paid_amount;
                        
                            $journal_posted.=" | ";
                            $journalEntry=new journalEntry;
                            $journal_posted.=$journalEntry->journal_entry($transaction_id,$related_party_id,$related_party_type,$journal_type,$accounts_id,$payment_method,$description,$amount,$branch_id);
                        }
                        }
                        if($i==3){
                            $journal_type=1;
                            $accounts_id=14;
                            if($discount>0){
                            $amount=$discount;
                        
                            $journal_posted.=" | ";
                            $journalEntry=new journalEntry;
                            $journal_posted.=$journalEntry->journal_entry($transaction_id,$related_party_id,$related_party_type,$journal_type,$accounts_id,$payment_method,$description,$amount,$branch_id);
                            }
                        }
                    }//for loop
                }//transaction id

            }//sales_return_id
            return redirect()->route('sales_return.index')->with('status',$msg.'Sale '.$journal_posted.' Returned Successfully');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sale_return  $sale_return
     * @return \Illuminate\Http\Response
     */
    public function show(Sale_return $sale_return)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sale_return  $sale_return
     * @return \Illuminate\Http\Response
     */
    public function edit(Sale_return $sale_return)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sale_return  $sale_return
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sale_return $sale_return)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sale_return  $sale_return
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sale_return $sale_return)
    {
        //
    }
    public function sales_history_for_return()
    {
        if(isset($_GET['customer_id'])){
            $customer_id=$_GET['customer_id'];
            $where_condition[]=['customer_id','=',$customer_id];
            $returndara=Sale::where($where_condition)->get();
            return $returndara;
        }
        
        if(isset($_GET['condition'])){
        $hello=$_GET['condition'];
        //$invoice_no=str_before($hello," | ");
        $sales_id=$hello;
        $where_condition[]=['sales_id','=',$sales_id];
        $returndara=Sale_detail::where($where_condition)->with('product')->get();
        return $returndara;
        }

        if(isset($_GET['histry'])){
        $hello=$_GET['histry'];
        $product_id=str_before($hello,"|");
        $id=str_after($hello,"|");
        $returndara=Sale_detail::with('sale')->find($id);
        return $returndara;
        }
        
       
    }
    
}
