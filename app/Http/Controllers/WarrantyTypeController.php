<?php

namespace App\Http\Controllers;

use App\user;
use App\Warranty_type;
use App\Product_warranty_type;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WarrantyTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $warranties = Warranty_type::paginate(10);
        if (request('searchby')) {
            $searchBy = request('searchby');
            $warranties = Warranty_type::where('warranty_name','like',"%$searchBy%")->paginate(10);
        }
        return view('admin.warranty.index',compact('warranties'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$warranty_type = Warranty_type::all();
        return view('admin.product.product_warranty_type',compact('warranty_type'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'warranty_name'=>'required|unique:warranty_types',
            'warranty_description'=>'required'
        ]);

       $wp = new Warranty_type;
	   $wp->warranty_name = $request->warranty_name;
	   $wp->warranty_description = $request->warranty_description;
	   $wp->parent_id = $request->parent_id;
	   $wp->created_by = Auth::id();
	   $wp->save();
       return redirect()->route('warranty_type.index')->with('status','Warranty Type is added');
	   //return redirect()->back()->with('status','Warranty Type is added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Warranty_type  $warranty_type
     * @return \Illuminate\Http\Response
     */
    public function show(Warranty_type $warranty_type)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Warranty_type  $warranty_type
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   $editid = $id;
        $warranty = Warranty_type::all();
        $parent = Warranty_type::find($id);
        return view('admin.warranty.edit',compact('parent','editid','warranty'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Warranty_type  $warranty_type
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $warranty_update = Warranty_type::find($id);
        $warranty_update->warranty_name = $request->warranty_name;
        $warranty_update->warranty_description = $request->warranty_description;
        $warranty_update->parent_id = $request->parent_id;
        $warranty_update->last_updated_by = Auth::id();
        $warranty_update->save();
        return redirect()->route('warranty_type.index')->with('status','Warranty Type is Updated!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Warranty_type  $warranty_type
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         Warranty_type::destroy($id);
         return redirect()->back()->with('status','Warranty is deleted successfully!');
    }
    public function warrenty_data()
    {
        $warranty_type = Warranty_type::all();
        // dd($warranty_type);
        $returnwarranty = "<label>Warranty Type <i class='text-danger'>( Type Duration in Month )</i></label>";
        if($warranty_type->count() != 0){
         foreach($warranty_type as $wt){
            $returnwarranty .= "<div class='form-group'>
                                <div class='form-check form-check-flat'>
                                    <label class='form-check-label'>
                                        <input type='checkbox' class='form-check-input' id='warranty_type_id".$wt->id."' name='warranty_type_id[]' value='".$wt->id."' onclick=showduration(".$wt->id.")>".$wt->warranty_name."<i class='input-helper'></i></label></div><div class='input-group mb-3'><input type='hidden' name='warranty_duration[]' id='warranty_duration".$wt->id."' placeholder='Set Warranty Duration In Month' aria-describedby='basic-addon".$wt->id."' class='form-control'><div class='input-group-append' id='add_on_haha".$wt->id."'></div></div></div>";
            }
        }else{
            $returnwarranty .= '<br><a class="btn btn-danger" href="'.url('warranty_type').'">Add Warranty Type First</a>';
        }
        return $returnwarranty;  
    }
  
}
