<?php

namespace App\Http\Controllers;

use App\Account;
use App\Branch_inventory;
use App\Journal;
use App\Transaction;
use App\TransactionEvents;
use App\Supplier;
use App\Customer;
use App\Branch;
use App\Payment_method;
use App\cheque_info;
use App\JournalHelper\journalEntry;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Gate;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Only Super Admin car see the trail Balance
        if (Gate::allows('isSuperadmin')) {
            $trial_balance= Account::with('journals')->orderBy('type')->get();
            if($trial_balance->count()){
                return view('admin/transactions/trial_balance',compact('trial_balance'));
            }
        }else{
                abort(403,Auth::user()->user_type." is not permitted for this action.");
            }
    }
    public function income_report()
    {
        // Only Super Admin car see the trail Balance
        if (Gate::allows('isSuperadmin')) {
            $incomes= Account::with('journals')->whereIn('id',[4,17,14,9,15,11,12,13])->orderByRaw('FIELD(id,4,17,14,9,15,11,12,13)')->get();
            $operatingExpenses= Account::with('journals')->where('type','=','Operating Expense')->get();
            $endingInventory=Branch_inventory::where('stock_quantity','>',0)->get();
            $einvAmount=0;
            foreach($endingInventory as $eInv){
                $einvAmount+=($eInv->purchase->purchase_unit_price*$eInv->stock_quantity);
            }
            $returndata['endingInventory']=$einvAmount;
            foreach($incomes as $tb){
                $amount=0;
                $camount=0;
                $returndata[$tb->account_head]['account_head']=$tb->account_head;
                $returndata[$tb->account_head]['type']=$tb->type;
                foreach($tb->journals as $jr){

                    if($jr->journal_type==0){
                    $amount=$amount+$jr->amount;
                    }//endif

                    if($jr->journal_type==1){
                    $camount=$camount+$jr->amount;
                    }//endif

                }//endforeach
                if($amount>$camount){  
                    $returndata[$tb->account_head]['amount']=($amount-$camount);
                }elseif($amount<$camount){
                    $returndata[$tb->account_head]['amount']=($camount-$amount);
                }else{
                    $returndata[$tb->account_head]['amount']=0;
                }
                }//foreach
            foreach($operatingExpenses as $opEx){
                $amount=0;
                $camount=0;
                $operatingExp[$opEx->id]['account_head']=$opEx->account_head;
                $operatingExp[$opEx->id]['type']=$opEx->type;
                foreach($opEx->journals as $jr){

                    if($jr->journal_type==0){
                    $amount=$amount+$jr->amount;
                    }//endif

                    if($jr->journal_type==1){
                    $camount=$camount+$jr->amount;
                    }//endif

                }//endforeach
                if($amount>$camount){  
                    $operatingExp[$opEx->id]['amount']=($amount-$camount);
                }elseif($amount<$camount){
                    $operatingExp[$opEx->id]['amount']=($camount-$amount);
                }else{
                    $operatingExp[$opEx->id]['amount']=0;
                }
                }//foreach
           
                //dd($operatingExp);
            return view('admin/income_statement/income_report',compact('returndata','operatingExp'));
        }else{
                abort(403,Auth::user()->user_type." is not permitted for this action.");
            }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {//store expense data
        // return $request;
        $total_price=$request->total_price;
        $request->validate([
            'branch_id'=>'required',
            'total_price'=>"required"
        ]);
        if(isset($request->account_head)){
            $request->validate([
            'expense_amount'=>'required',
            'account_head.*'=>'unique:accounts,account_head'
        ]);
        }
        $journal_posted='';
        $msg="";
        $payment_method_id=str_before($request->payment_method_id," | ");
        $payment_method_type=str_after($request->payment_method_id," | ");
        $transaction_title="Expense paid";
        $account_id=$request->expense_field;
        $branch_id=$request->branch_id;
        $expense_amount = $request->expense_amount;      
        $expense_amount = array_values(array_filter($expense_amount));
       $total=array_sum($expense_amount);
        
    //    //check transaction has any cheque info but Ismail vai let it undone
    //     if($request->cheque_disposal_date){
    //         $cheque_info_status='1';
    //         $paid_amount=0;
    //         $payment_method_id=0;
    //     }else{
    //         $cheque_info_status="0";
    //     }
    //     if($request->check_no){
    //         $cheque_info=new cheque_info;
    //         $cheque_info->payment_method_id=str_before($request->payment_method_id," | ");
    //         $cheque_info->created_by=Auth::id();
    //         $cheque_info->related_party_id=$supplier_id;
            //$cheque_info->branch_id=$branch_id;
    //         $cheque_info->related_party_type="supplier";
    //         $cheque_info->amount=$request->paid_amount;
    //         $cheque_info->cheque_sl_no =$request->check_no;
    //         $cheque_info->status=$cheque_info_status;
    //         $cheque_info->cheque_disposal_date=$request->cheque_disposal_date;
    //         $cheque_info->save();
    
    //         $msg.="Your Check has been Registered";
    //     }


        if(isset($request->account_head)){
            $account_head=$request->account_head;
            $type=$request->type;
            $counter=count($account_head);
    
            for($i=0;$i<$counter;$i++){
                $account = new Account;
                $account->account_head = $request->account_head[$i];
                $account->type = $request->type[$i];
                $account->status = 1;
                $account->save();
                $stringVar=(string) $account->id;
                array_push($account_id,$stringVar);
            }
        }
         //jaournal and transaction for primary setting
         $transaction_eve=new TransactionEvents;
         $transaction_eve->transaction_title=$transaction_title;
         $transaction_eve->branch_id=$branch_id;
         $transaction_eve->created_by=Auth::id();
         $transaction_eve->save();
         $transaction_eve_id=$transaction_eve->id;
         if($transaction_eve_id){
            $transaction=new Transaction;
            $transaction->event_id=$transaction_eve_id;
            $transaction->event_type=$transaction_title;
            $transaction->branch_id=$branch_id;
            $transaction->created_by=Auth::id();
            $transaction->save();
            $transaction_id=$transaction->id;

            //journal entry;
            if($transaction_id){
               //  [6- A/p,5-A/R,2-cash,10-bank,12-Discount on Receive,13-Discount on Payment 8-Capital]
                //executing journal for account payment or collect.;
                $related_party_type=null;
                $related_party_id=null;
                $description=$transaction_title;
                $idCounter=count($account_id);
                for($i=0;$i<$idCounter;$i++){
                    $accounts_id=$account_id[$i];
                    $journal_type=0;
                    $amount_get=$expense_amount[$i];
                    $journalEntry=new journalEntry;
                    $journal_posted.=$journalEntry->journal_entry($transaction_id,$related_party_id,$related_party_type,$journal_type,$accounts_id,$payment_method_id,$description,$amount_get,$branch_id);
                }//for loop
                        $payment_method_type ? $accounts_id=10:$accounts_id=2;
                        $journal_type=1;
                        $amount_get= $total;
                        $journalEntry=new journalEntry;
                            $journal_posted.=$journalEntry->journal_entry($transaction_id,$related_party_id,$related_party_type,$journal_type,$accounts_id,$payment_method_id,$description,$amount_get,$branch_id);
            } //transaction id
        } //transaction_eve_id
        //End jaournal and transaction for primary setting
    

        return redirect()->route('journals.index')->with('status',$msg."and journal id:".$journal_posted);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Account  $account
     * @return \Illuminate\Http\Response
     */
    public function show(Account $account)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Account  $account
     * @return \Illuminate\Http\Response
     */
    public function edit(Account $account)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Account  $account
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Account $account)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Account  $account
     * @return \Illuminate\Http\Response
     */
    public function destroy(Account $account)
    {
        //
    }
}
