<?php

namespace App\Http\Controllers;

use App\Product_warranty_type;
use Illuminate\Http\Request;

class ProductWarrantyTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product_warranty_type  $product_warranty_type
     * @return \Illuminate\Http\Response
     */
    public function show(Product_warranty_type $product_warranty_type)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product_warranty_type  $product_warranty_type
     * @return \Illuminate\Http\Response
     */
    public function edit(Product_warranty_type $product_warranty_type)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product_warranty_type  $product_warranty_type
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product_warranty_type $product_warranty_type)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product_warranty_type  $product_warranty_type
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product_warranty_type $product_warranty_type)
    {
        //
    }
}
