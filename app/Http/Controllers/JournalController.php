<?php

namespace App\Http\Controllers;

use App\Journal;
use App\Account;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class JournalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $searchby=Auth::user()->branch_id;
         //should be dynamic date
         if(isset($_GET['searchbyfrom']) && isset($_GET['searchbyto'])){
        $from =$_GET['searchbyfrom'];
        $to = $_GET['searchbyto'];
        }else{
            $from =date('Y-m-01');
            $to =date('Y-m-t');
        }
        //dd( $from."  ". $to);
        if(Auth::user()->user_type=="superadmin"){
        $journals=Journal::whereBetween('created_at', [$from, $to])->with('account')->orderBy('transaction_id','DESC')->orderBy('journal_type','ASC')->latest()->paginate(20);
    }else{
        $journals=Journal::whereBetween('created_at', [$from, $to])->where('branch_id',"=",$searchby)->with('account')->orderBy('transaction_id','DESC')->orderBy('journal_type','ASC')->latest()->paginate(20);
    }
        return view('admin.journal.report',compact('journals'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Journal  $journal
     * @return \Illuminate\Http\Response
     */
    public function show(Journal $journal)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Journal  $journal
     * @return \Illuminate\Http\Response
     */
    public function edit(Journal $journal)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Journal  $journal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Journal $journal)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Journal  $journal
     * @return \Illuminate\Http\Response
     */
    public function destroy(Journal $journal)
    {
        //
    }
   
}


