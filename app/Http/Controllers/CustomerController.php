<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Address_info;
use App\Customer_group;
use App\Journal;
use App\TransactionEvents;
use App\Transaction;
use App\JournalHelper\journalEntry;
use App\Sale;
use App\Branch;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Gate;
use App\User;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /**
         * search by cutomer_name
         * authentication
         */
		$customer_name_search="";
		if(isset($_GET['customer_name'])){

            $customer_name_search=$_GET['customer_name'];
            
        }
        
        $user_type =  Auth::user()->user_type;
		if(Gate::allows('isManager') || !Gate::allows('isAdmin') || !Gate::allows('isSuperadmin')){
			$cusgroup = Customer::where('customer_name','LIKE',"%$customer_name_search%")->latest()->paginate(10);
			$branches = Branch::where('status','=',1)->latest()->get();
            return view('admin.customer.index',compact('cusgroup','branches'));
		}else{
            abort(403,"$user_type is not permitted for this page.");
        }
		
    }
    public function monthlyStatusOfCustomer()
    {

        $branch_id =  Auth::user()->branch_id;
        $where = [];
        if($branch_id !== null){
            $where=[['related_party_type','=','customer'],['branch_id','=',$branch_id]];
        }else{
             $where= [['related_party_type','=','customer'],['branch_id','like',"%$branch_id%"]];
        }
		$customers = Customer::whereHas('journal',function($q) use($where){
            $q->where($where);
        })->latest()->paginate(10);
        return view('admin.customer.customer_month_wise_status_report',compact('customers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$cusgroup = Customer_group::all();
        return view('admin.customer.create',compact('cusgroup'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $branch_id= Auth::user()->branch_id;

		$this->validate($request,[
        'customer_name'=>'required',
        'email'=>'required|unique:address_infos,email|email',
        'credit_limit'=>'nullable|numeric',
        'phone'=>'required|min:11|regex:/(01)[0-9]{9}/'
        ]);
        // return $request;
        $address = new Address_info;
		$address->email = $request->email;
		$address->phone = $request->phone; 
		$address->full_address = ""; 
		$address->save();
        $address_id = $address->id;
        
		$newCustomer = new Customer;
		$newCustomer->customer_name = $request->customer_name;
		$newCustomer->customer_group_id = $request->customer_group_id; 
		$newCustomer->credit_limit = $request->credit_limit; 
		$newCustomer->address_info_id = $address_id;
		$newCustomer->created_by = Auth::id();		
		$newCustomer->save();
		if ($request->addCustomerFromSale == 'requestFromSales' && $newCustomer->id){
                \Cart::session(Auth::id())->clear();
                session(['cust_group_id'=>$request->customer_group_id]);
                session(['customer_id'=>$newCustomer->id]);
                session(['customer_phone'=>$request->phone]);
                session(['customer_name'=>$request->customer_name]);
                session(['customer_due'=>0]);

            $debitSum=Journal::with('customer')->where([['journal_type','=',0],['accounts_id',5],['related_party_type','=','customer'],['related_party_id','=',$newCustomer->id],['branch_id','=',$branch_id]])
            ->sum('amount');
                
            $creditSum=Journal::with('customer')->where([['journal_type','=',1],['accounts_id',5],['related_party_type','=','customer'],['related_party_id','=',$newCustomer->id],['branch_id','=',$branch_id]])
            ->sum('amount');
                session(['customer_due'=> number_format($debitSum-$creditSum,2)]);
                return back()->with('status','Customer data saved successfully');
        }
		return redirect()->route('customers.index')->with('status','Customer data saved successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $searchbyfrom = null;
        $searchbyto = null;

        if (isset($_GET['searchbyfrom'])) {
            $searchbyfrom = $_GET['searchbyfrom'];
            $searchbyto = $_GET['searchbyto'];
        }else{
            $searchbyfrom =date('Y-m-01');
            $searchbyto =date('Y-m-t');
        }
    

      
        if(Auth::user()->branch_id!=null){
            $where=[['accounts_id',5],['related_party_type','=','customer'],['related_party_id','=',$id],['branch_id','=',Auth::user()->branch_id]];
        }else{
            $where=[['accounts_id',5],['related_party_type','=','customer'],['related_party_id','=',$id]];
        } 

        $customer_status=Journal::with('customer')->whereBetween('created_at', [$searchbyfrom, $searchbyto])->where($where)->latest()->get();
        //task for current balance
        $debit_status=Journal::where($where)->whereJournalType(0)->sum('amount');
        $credit_status=Journal::where($where)->whereJournalType(1)->sum('amount');        
        $previousBalance= ($debit_status-$credit_status);
        
        $customer=Customer::find($id);
        return view('admin/customer/report',compact('customer_status','customer','previousBalance'));
    }
    public function customer_invoice_show($id)
    {
        /**
         * specific customer transaction details(Laiser)
         */
        $total=0;
        $searchbyfrom = null;
        $searchbyto = null;

        if (isset($_GET['searchbyfrom'])) {
            $searchbyfrom = $_GET['searchbyfrom'];
            $searchbyto = $_GET['searchbyto'];
        }else{
            $searchbyfrom =date('Y-m-01');
            $searchbyto =date('Y-m-t');
        }
        if(Auth::user()->branch_id!=null){
            $where=[['customer_id','=',$id],['branch_id','=',Auth::user()->branch_id]];
        }else{
            $where=[['customer_id','=',$id]];
        } 
        
            $sales = Sale::where($where)->whereBetween('created_at', [$searchbyfrom, $searchbyto])->get(); 
      
            $total = $sales->sum('total_amount');
            $customer=Customer::find($id);
            return view('admin.sale.customer_invoice_report',compact('sales','total','customer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $editid = $id;
		$cusgroup = Customer_group::all();
		$cust = Customer::find($id);
        $address_info = Address_info::find($id);
		
        return view('admin/customer/edit',compact('editid' , 'cust' , 'cusgroup','address_info'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'customer_name'=>'required',
            'email'=>'required|email',
            'credit_limit'=>'nullable|numeric',
            'phone'=>'required|numeric'
            ]);
		$u_customer = Customer::find($id);
		$address_id=$u_customer->address_info_id;
		$u_address = Address_info::find($address_id);
		
	   $u_customer->customer_name = $request->get('customer_name');
	   $u_customer->credit_limit = $request->get('credit_limit');
	   $u_address->email = $request->get('email');
	   $u_address->phone = $request->get('phone');
	   $u_customer->customer_group_id = $request->get('customer_group_id'); 
	   $u_customer->created_by = Auth::id();
	   $u_customer->update();
	   $u_address->update();
	   return redirect()->route('customers.index')->with('status','Customer data updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $customer=Customer::find($id);
         $customer->status=0;
         $customer->update();
		 return redirect()->route('customers.index')->with('status','Customer data deleted successfully');
    }
    public function customerBeginningBalance(Request $request)
    {
        $this->validate($request,[
            'customer_id'=>'required',
            'branch_id'=>'required',
            'paid_amount'=>'required|numeric'
            ]);

      
        $branch_id=$request->branch_id;
        $customer_id=$request->customer_id;
        $related_party_id=$customer_id;
        $description='begining_balance';
        $journal_posted='';
        $amount_get=$request->paid_amount;
        $havePreviousJournal=Journal::where([['related_party_id','=',$customer_id],['related_party_type','=','customer'],['accounts_id','=',5],['journal_type','=',0],['branch_id','=',$branch_id],['description','=','begining_balance']])->latest()->first();

        if(($customer_id!=null) && ($havePreviousJournal==null)){
            $transaction=new Transaction;
            $transaction->event_id=$branch_id;
            $transaction->event_type=$description;
            $transaction->branch_id=$branch_id;
            $transaction->created_by=Auth::id();
            $transaction->save();
            $transaction_id=$transaction->id;
    
            //journal entry;
            if($transaction_id){
               //  [6- A/p,5-A/R,2-cash,10-bank,12-Discount on Receive,13-Discount on Payment 8-Capital]
                //executing journal for account payment or collect.;
                    
                    $journalEntry=new journalEntry;
                    $journal_posted.=$journalEntry->journal_entry($transaction_id,$related_party_id,'customer',0,5,null,$description,$amount_get,$branch_id);
                    $journalEntry=new journalEntry;
                    $journal_posted.=$journalEntry->journal_entry($transaction_id,$related_party_id,'customer',1,8,null,$description,$amount_get,$branch_id);
                    
                // }//for loop
            }//transaction id
        }
        if(($havePreviousJournal==null) && ($journal_posted!=null)){
            return redirect()->route('customers.index')->with('status','Customer Balance Added successfully');
        }else{
            return redirect()->route('customers.index')->with('warning','Customer Already Having Previous Balance For this Branch');
        }
		 
    }
}
