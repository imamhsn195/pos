<?php

namespace App\Http\Controllers;

use App\Transaction;
use App\Representative;
use App\TransactionEvents;
use App\Supplier;
use App\Customer;
use App\Journal;
use App\Branch;
use App\Payment_method;
use App\InvoiceSetting;
use App\PrimarySettings;
use App\cheque_info;
use App\JournalHelper\journalEntry;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(request('month')){
            $month=date("n",(strtotime(request('month'))));
            //str_after(request('month'),'-'));
            $year=date("Y",(strtotime(request('month'))));
           // dd($month ."|". $year);
        }else{
            $month=date('n');
            $year=date('Y');
        }
        if(Auth::user()->branch_id!=null){
            $where=[['accounts_id','=','2'],['branch_id','=',Auth::user()->branch_id]];
        }else{
            $where=[['accounts_id','=','2']];
        } 
        //cash Flow should 
        $cash_flows=Journal::with('customer','supplier')->whereYear('created_at', '=', $year)->whereMonth('created_at', '=', $month)->where($where)->get();
        return view('admin/transactions/cash_flow',compact('cash_flows'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $suppliers=Supplier::all();
        $branches=Branch::all();
        $customers=Customer::all();
        $payment_methods=[];
        $user_type =  Auth::user()->user_type;
        if($user_type == 'superadmin'){
             $payment_methods = Payment_method::with('journals')->latest()->get();
        }else{
            $payment_methods = Payment_method::with(['journals' =>function($query){
                $query->where('branch_id','=',Auth::user()->branch_id);
            }])->latest()->get();
        }
        //$payment_methods=Payment_method::all();
        return view('admin.transactions.payment_transaction',compact('suppliers','customers','payment_methods','branches'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    //Due clearence with Customer or Supplier
    public function store(Request $request,$str_data=0)
    {
        $customer_id=null;
        if($str_data!==0){
            $branch_id=$str_data['branch_id'];
            $customer_id=$str_data['customer_id'];
            $amount=$str_data['amount'];
      
        }else{
            $request->validate([
                'paid_amount'=>'required',
                'branch_id'=>'required',
            ]);
            $branch_id=$request->branch_id;
            $customer_id=$request->customer_id;
            $amount=$request->paid_amount;
        }
        $payment_method_id=str_before($request->payment_method_id," | ");
        $payment_method_type=str_after($request->payment_method_id," | ");
        $cheque_disposal_date=null;
        $discount=0;
        $discount=$discount + $request->discount;
        $journal_posted='';
        $msg="";
            if($request->supplier_id){
                $related_party_id=$request->supplier_id;
                $related_party_type="supplier";
                $transaction_title="paid to Supplier";
            }else if($customer_id!==null){
                $related_party_id=$customer_id;
                $related_party_type="customer";
                $transaction_title="Receive from Customer";
            }
            if($request->cheque_disposal_date){
                $cheque_info_status='1';
            }else{
                $cheque_info_status="0";
            }
        if($request->check_no){
            $request->validate([
                'check_no'=>'required|unique:cheque_infos,cheque_sl_no',
            ]);
            $cheque_info=new cheque_info;
            $cheque_info->payment_method_id=$payment_method_id;
            $cheque_info->created_by=Auth::id();
            $cheque_info->branch_id=$branch_id;
            $cheque_info->related_party_id=$related_party_id;
            $cheque_info->related_party_type=$related_party_type;
            $cheque_info->amount=$amount;
            $cheque_info->cheque_sl_no =$request->check_no;
            $cheque_info->status=$cheque_info_status;
            $cheque_info->cheque_disposal_date=$request->cheque_disposal_date;
            $cheque_info->save();

            $msg.="Your Check has been Registered";
        }

        if(!$request->cheque_disposal_date){ //if check disposal date not set
            $transaction_eve=new TransactionEvents;
            $transaction_eve->transaction_title=$transaction_title;
            $transaction_eve->branch_id=$branch_id;
            $transaction_eve->created_by=Auth::id();
            $transaction_eve->save();
            $transaction_eve_id=$transaction_eve->id;
       

            if($transaction_eve_id){
                $transaction=new Transaction;
                $transaction->event_id=$transaction_eve_id;
                $transaction->event_type=$transaction_title;
                $transaction->branch_id=$branch_id;
                $transaction->created_by=Auth::id();
                $transaction->save();
                $transaction_id=$transaction->id;

                //journal entry;
                if($transaction_id){
                    if($request->representative){
                        $representative=new Representative;
                        $representative->transaction_id=$transaction_id;
                        $representative->name=$request->representative;
                        $representative->save();
                    }

                    $accounts_title=['6','5','2','10','12','13'];  //  6- A/p,5-A/R,2-cash,10-bank,12-Discount on Receive,13-Discount on Payment
                    //executing journal for account payment or collect.;
                    for($i=0;$i<3;$i++){
                        if($i==0){
                            if($related_party_type=='supplier'){
                                $accounts_id=$accounts_title[0];
                                $journal_type=$i;
                                $amount_get=$amount+$discount;
                                $description='Paid to Supplier';
                            }else{
                                $payment_method_type?$accounts_id=$accounts_title[3]:$accounts_id=$accounts_title[2];
                                $journal_type=$i;
                                $amount_get=$amount;
                                $description='received payment from customer';
                            }
                            $journalEntry=new journalEntry;
                            $journal_posted.=$journalEntry->journal_entry($transaction_id,$related_party_id,$related_party_type,$journal_type,$accounts_id,$payment_method_id,$description,$amount_get,$branch_id);
                        }
                        if($i==1){
                            if($related_party_type=='supplier'){
                                $accounts_id=$accounts_title[5];
                                $journal_type=$i;
                                $amount_get=$discount;
                                $description='Paid to Supplier';
                            }else{
                                $accounts_id=$accounts_title[4];
                                $journal_type=0;
                                $amount_get=$discount;
                                $description='received payment from customer';
                            }
                            if($discount>0){
                                $journalEntry=new journalEntry;
                                $journal_posted.=$journalEntry->journal_entry($transaction_id,$related_party_id,$related_party_type,$journal_type,$accounts_id,$payment_method_id,$description,$amount_get,$branch_id);
                            }
                        }
                        if($i==2){
                            if($related_party_type=='supplier'){
                                $payment_method_type ? $accounts_id=$accounts_title[3]:$accounts_id=$accounts_title[2];
                                $journal_type=1;
                                $amount_get=$amount;
                                $description='Paid to Supplier';
                            }else{
                                $accounts_id=$accounts_title[1];
                                $journal_type=1;
                                $amount_get=$amount+$discount;
                                $description='received payment from customer';
                            }
                            $journalEntry=new journalEntry;
                            $journal_posted.=$journalEntry->journal_entry($transaction_id,$related_party_id,$related_party_type,$journal_type,$accounts_id,$payment_method_id,$description,$amount_get,$branch_id);
                        }
                        
                        
                    }//for loop
                }//transaction id
            }//transactionEvent

        }//check not disposed
        if($str_data==0){
        return redirect()->route('journals.index')->with('status',$msg."and journal id:".$journal_posted);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction)
    {
        $invoice_setting=InvoiceSetting::orderBy('id')->first();
        $company=PrimarySettings::orderBy('id')->first();
        $footer_contents=InvoiceSetting::where('id','!=',1)->orderBy('id')->get();
        $payments=null;
        return view('admin.transactions.money_reciept',compact('company','payments','footer_contents','invoice_setting','transaction'));
    }
    public function paymentShow()
    {
        if(isset($_GET['search_view'])){
            $searchView=$_GET['search_view'];
        }else{
            $searchView=0;
        }
       
        if (isset($_GET['searchbyfrom'])) {
            $searchbyfrom = $_GET['searchbyfrom'];
            $searchbyto = $_GET['searchbyto'];
        }else{
            $searchbyfrom =date('Y-m-01');
            $searchbyto =date('Y-m-t');
        }
        if($searchView==1){
            $where=['event_type','like','%Receive from Customer%'];
        }else{
            $where=['event_type','=','paid to Supplier'];
        } 

        if(Auth::user()->branch_id!=null){
       $payments= Transaction::where([$where,['branch_id','=',Auth::user()->branch_id]])->whereBetween('created_at', [$searchbyfrom, $searchbyto])->get();
        }else{
       $payments= Transaction::where([$where])->whereBetween('created_at', [$searchbyfrom, $searchbyto])->get();
        }
     
       return view('admin.transactions.transaction_report',compact('payments'));
    }
    public function transaction_status_show($related_party_id_type)
    {
        $related_party_id=str_before($related_party_id_type,"|");
        $related_party_type=str_after($related_party_id_type,"|");
        if($related_party_type==1){
            $related_party_type="customer";
            $accounts_id=5;
       }elseif($related_party_type==0){
        $related_party_type="supplier";
        $accounts_id=6;

       }
       $debit=Journal::where([['accounts_id','=',$accounts_id],['journal_type','=',0],['related_party_type','=',$related_party_type],['related_party_id','=',$related_party_id]])->sum('amount');
       $credit=Journal::where([['accounts_id','=',$accounts_id],['journal_type','=',1],['related_party_type','=',$related_party_type],['related_party_id','=',$related_party_id]])->sum('amount');
       return $debit.'|'.$credit;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cheque=cheque_info::find($id);
        $cheque->status=0;
        $cheque->update();
        $journal_posted="";
        $branch_id=Auth::user()->branch_id;
        $related_party_id=$cheque->related_party_id;
        $related_party_type=$cheque->related_party_type;
        $amount_get=$cheque->amount;
        $payment_method_id=$cheque->payment_method_id;
        $transaction_title=($cheque->related_party_type=="supplier")?"Pay To Supplier by Bank":"Receive from Customer by Bank";
        $description=$transaction_title;
        // $transaction_title="Cheque ";
        $transaction_eve=new TransactionEvents;
        $transaction_eve->transaction_title=$transaction_title;
        $transaction_eve->branch_id=$branch_id;
        $transaction_eve->created_by=Auth::id();
        $transaction_eve->save();
        $transaction_eve_id=$transaction_eve->id;
        if($transaction_eve_id){
            $transaction=new Transaction;
            $transaction->event_id=$transaction_eve_id;
            $transaction->event_type=$transaction_title;
            $transaction->branch_id=$branch_id;
            $transaction->created_by=Auth::id();
            $transaction->save();
            $transaction_id=$transaction->id;

            //journal entry;
            if($transaction_id){
                $accounts_title=['6','5','2','10','12','13'];  //  6- A/p,5-A/R,2-cash,10-bank,12-Discount on Receive,13-Discount on Payment
                //executing journal for account payment or collect.;
                for($i=0;$i<2;$i++){
                    if($i==0){
                        if($related_party_type=='supplier'){
                            $accounts_id=6;
                            $journal_type=$i;
                        }else{
                            $accounts_id=10;
                            $journal_type=$i;
                        }
                        $journalEntry=new journalEntry;
                        $journal_posted.=$journalEntry->journal_entry($transaction_id,$related_party_id,$related_party_type,$journal_type,$accounts_id,$payment_method_id,$description,$amount_get,$branch_id);
                    }
                    if($i==1){
                        if($related_party_type=='supplier'){
                            $accounts_id=10;
                            $journal_type=1;
                        }else{
                            $accounts_id=5;
                            $journal_type=1;
                        }
                        $journalEntry=new journalEntry;
                        $journal_posted.=$journalEntry->journal_entry($transaction_id,$related_party_id,$related_party_type,$journal_type,$accounts_id,$payment_method_id,$description,$amount_get,$branch_id);
                    }
                }//for loop
            }//transaction id
        }//transactionEvent
    
        return redirect()->route('journals.index')->with('status',"journal id:".$journal_posted);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction $transaction)
    {
        //
    }
    
}
