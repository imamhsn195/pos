<?php

namespace App\Http\Controllers;

use App\CustomerCriteria;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CustomerCriteriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $criteria_index=CustomerCriteria::all();
		return view('admin.customer_criteria.index',compact('criteria_index'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.customer_criteria.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /**
         * validation
         */
		$this->validate($request,[
		'customer_criteria_title'=>'required','criteria_type'=>'required','criteria_value'=>'required','condition'=>'required'
		]);
       //return $request;

       /**
        * save data in database table
        */
        
		$customer_criteria = new CustomerCriteria;
		$customer_criteria->title = $request->customer_criteria_title;
		$customer_criteria->criteria_type = $request->criteria_type;
        
        /**
         * 'criteria_type=1' means membership duration 'criteria_type=2' means 'age'
         */
		if($request->criteria_type==1 || $request->criteria_type==2){
			$customer_criteria->criteria_value =$request->criteria_value;
		}
		else{
		$customer_criteria->criteria_value = $request->criteria_value;}
		$customer_criteria->condition = $request->condition;
		$customer_criteria->created_by = Auth::id();
		//return $customer_criteria;
		 $customer_criteria->save();
		 
		 return redirect()->route('customer_criterias.index')->with('status','Criteria saved successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CustomerCriteria  $customerCriteria
     * @return \Illuminate\Http\Response
     */
    public function show(CustomerCriteria $customerCriteria)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CustomerCriteria  $customerCriteria
     * @return \Illuminate\Http\Response
     */
    public function edit(CustomerCriteria $customerCriteria)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CustomerCriteria  $customerCriteria
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CustomerCriteria $customerCriteria)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CustomerCriteria  $customerCriteria
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        CustomerCriteria::destroy($id);
		return redirect()->route('customer_criterias.index')->with('status','Criteria data deleted successfully');
    }
    
    /**
     *criteria_status function is used for active,inactive status
    */
	public function criteria_status(Request $request,$id){
		$status = CustomerCriteria::find($id);
       
       if($request->status){
           $status->status = 0;
       }else{
           $status->status = true;
       }
       
       $status->update();
       return redirect()->back();
	}
}
