<?php

namespace App\Http\Controllers;

use App\Warranty_product_detail;
use Illuminate\Http\Request;
use App\Product_warranty_type;
use App\Branch_inventory;
use Auth;

class WarrantyProductDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $branch_id= Auth::user()->branch_id;
        if ($branch_id == null) {
            $where[] = ['branch_id','like',"%$branch_id%"];
       }else{
            $where[] = ['branch_id','=',$branch_id];
       }

        if(request('product_sl_no')){
            $product_sl_no = request('product_sl_no');
            $products = Warranty_product_detail::with('purchase','sales_detail.sale.customer','purchase.supplier')
            ->whereProductSlNo( $product_sl_no)
            ->paginate(5);
        }else{
            $products = Warranty_product_detail::with('sales_detail.sale','purchase.supplier')->paginate(5);
        }
        // return $products;
        return view('admin.warranty.warranty_product_details',compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Warranty_product_detail  $warranty_product_detail
     * @return \Illuminate\Http\Response
     */
    public function show(Warranty_product_detail $warranty_product_detail)
    {
    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Warranty_product_detail  $warranty_product_detail
     * @return \Illuminate\Http\Response
     */
    public function edit(Warranty_product_detail $warranty_product_detail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Warranty_product_detail  $warranty_product_detail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Warranty_product_detail $warranty_product_detail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Warranty_product_detail  $warranty_product_detail
     * @return \Illuminate\Http\Response
     */
    public function destroy(Warranty_product_detail $warranty_product_detail)
    {
        //
    }
}
