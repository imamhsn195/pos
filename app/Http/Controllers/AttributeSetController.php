<?php

namespace App\Http\Controllers;

use App\Attribute_set;
use App\Attribute;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use Validator;


class AttributeSetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $searchby="";
        if(isset($_GET['searchby'])){
            $searchby=$_GET['searchby'];
        } 
        $attribute_sets = Attribute_set::where('attribute_set_name', 'LIKE', "%$searchby%")->latest()->paginate(10);
        return view('admin/attribute_set/index',compact('attribute_sets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $attributes = Attribute::where('parent_id',null)->get();
        return view('admin/attribute_set/create',compact('attributes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$this->validate($request,[
	   'attribute_set_name' => 'required|unique:attribute_sets',
	   'choose_attribute' => 'required'
	   ]);
	   $res=$request->choose_attribute;
	   
	   $attr_set=new Attribute_set;
	   $attr_set->attribute_set_name=$request->attribute_set_name;
	   $attr_set->description=$request->description;
	   $attr_set->created_by = Auth::id();
	   $attr_set->save();
	   $attr_set_id=$attr_set->id;
       
	   $attr_setfind=Attribute_set::find($attr_set_id);
	   $attr_setfind->attribute()->attach($res);
	   
		/*if($a=count($res)){
			
			$it=0;
			while ($it<$a) {
				$wp =new Attribute_attribute_set;
				$wp->attribute_id = $res[$it];
				$wp->attribute_set_id = $attr_set_id;
				$wp->save();
				$it++; 
			} 
		} */
		return redirect()->route('attribute_sets.index')->with('status','Successfully Inserted!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Attribute_set  $attribute_set
     * @return \Illuminate\Http\Response
     */
    public function show(Attribute_set $attribute_set)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Attribute_set  $attribute_set
     * @return \Illuminate\Http\Response
     */
    public function edit(Attribute_set $attribute_set)
    {
        $atset=$attribute_set;
		$attributes = Attribute::where('parent_id',null)->get();
        return view('admin/attribute_set/edit',compact('attributes','atset'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Attribute_set  $attribute_set
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
	   $res=$request->choose_attribute;
	   
	   $attr_set=Attribute_set::find($id);
	   $attr_set->attribute_set_name=$request->attribute_set_name;
	   $attr_set->description=$request->description;
	   $attr_set->last_updated_by = Auth::id();
	   $attr_set->update();
	   
       
	   $attr_setfind=Attribute_set::find($id);
	   $attr_setfind->attribute()->sync($res);
	   return redirect()->route('attribute_sets.index')->with('status','Successfully Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Attribute_set  $attribute_set
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Attribute_set::destroy($id);
		return redirect()->route('attribute_sets.index')->with('status','Delete Successfully');
    }

    public function ajaxDataAttr($attr_set_id)
    {
        $attributes = Attribute_set::with('attribute')->find($attr_set_id);
        $attribute_value = Attribute::all();
        $returndata="<div class='row'>";
       
        foreach($attributes->attribute as $att){
            $returndata.="<div class='col-md-4'>";
            $returndata.="<label for=''>".$att->attribute_name."</label>";
            $returndata.="<select  class='form-control' name='attribute_submited_value[]'>";
            
               foreach($attribute_value as $ap){
                if($ap->parent_id==$att->id){
               $returndata.="<option value=".$ap->id.">".$ap->attribute_name."</option>";
               }
            }
            
            $returndata.="</select></div>";
        }
       
        $returndata.='<div class="col-md-4">
        <label>BarCode  <small class="text-danger"> **(Optional) </small></label>
        <input type="number" class="form-control" name="product_barcode[]" placeholder="Enter product Barcode">
      </div>';
      $returndata.="</div>";
        return $returndata;
     
    }
}
