<?php

namespace App\Http\Controllers;

use App\Discount_detail;
use Illuminate\Http\Request;

class DiscountDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Discount_detail  $discount_detail
     * @return \Illuminate\Http\Response
     */
    public function show(Discount_detail $discount_detail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Discount_detail  $discount_detail
     * @return \Illuminate\Http\Response
     */
    public function edit(Discount_detail $discount_detail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Discount_detail  $discount_detail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Discount_detail $discount_detail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Discount_detail  $discount_detail
     * @return \Illuminate\Http\Response
     */
    public function destroy(Discount_detail $discount_detail)
    {
        //
    }
}
