<?php

namespace App\Http\Controllers;
use App\User;
use App\Branch_inventory;
use App\Payment_method;
use App\Branch;
use App\Expirable_product_detail;
use App\Warranty_product_detail;
use App\cheque_info;
use App\Sale;
use App\Purchase;
use App\Sale_detail;
use Carbon\Carbon;  
use App\Journal;  
use Auth;
use Session;
use Charts;
use DB;
use App\Transaction;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $branch_id =  Auth::user()->branch_id;
        $where = [];
        if($branch_id !== null){
            $where[] = ['branch_id','=',$branch_id];
        }else{
             $where[] = ['branch_id','like',"%$branch_id%"];
        }
        $date = [];
        $dates = [];
        $a = 0;
        $amount = [];
        for ($i=7; $i > 0; $i--) { 
            $dates[] = date("Y-m-d",strtotime("-".$i." days"));
            $salesbarChart = Sale::whereDate('created_at',$dates[$a])
            ->get();
            $date[]=date("F j",strtotime($dates[$a++]));
            $amount[] = $salesbarChart->sum('total_amount');
        }
$TotalSaleschart = [];
        // $TotalSaleschart = Charts::create('bar', 'morris')
        // ->title('Last week Sales')
        // ->elementLabel('Sales in amount')
        // ->labels($date)
        // ->values($amount)
        // ->dimensions(1000,500)
        // ->responsive(true)
        // ->colors(['#355da1','#4e66ba']);


        $totalSales = Sale::where($where)
        ->sum('total_amount');
// Total receivable of current date
        $receivable = Journal::where('journal_type','=',0)
        ->where($where)
        ->whereIn('accounts_id',[5])
        ->get()
        ->sum('amount');
// Total recepts of current date
        $reciept = Journal::where('journal_type','=',1)
        ->where($where)
        ->whereIn('accounts_id',[5])
        ->get()
        ->sum('amount');
// Total due of current date
        $totalDue = $receivable - $reciept;
        $donut_chart = [];
        // $donut_chart = Charts::create('donut', 'morris')
        // ->title('Sales In Details')
        // ->labels(['Receivable', 'Received', 'Due'])
        // ->values([$receivable,$reciept,$totalDue])
        // ->dimensions(1000,500)
        // ->colors(['lightgreen','green','red'])
        // ->responsive(true);
        //Show Total sales of individual Branch or all branch if user is superadmin
        $totalsales = Sale::where($where)->get()->count();

        //Show total employee of individual Branch or all branch if user is superadmin
        $totalemployee = User::where($where)->get()->count();
        
        //show total revenue in dashboard
        $todaysReceipts = Journal::where('journal_type','=',0)
        ->whereDate('created_at', '>=', date('Y-m-d'))
        ->where($where)
        ->whereIn('accounts_id',[2,10])
        ->get()
        ->sum('amount');
        // Todays total Payment
        $todaysPayments = Journal::where('journal_type','=',1)
        ->whereDate('created_at', '>=', date('Y-m-d'))
        ->where($where)
        ->whereIn('accounts_id',[2,10])
        ->get()
        ->sum('amount');
        
        $expirable_product_detail=Expirable_product_detail::where([['status','=',1],['expire_date','>',date('d-m-Y')]])->get();

        session(['e_pro_no'=>$expirable_product_detail->count()]);

        $Warranty_product_detail = Warranty_product_detail::where('status','=',1)->get();
        session(['Warranty_product_detail'=>$Warranty_product_detail->count()]);

        $cheque_infos=cheque_info::where([['status','=',1],['cheque_disposal_date','>',date('d-m-Y')]])->get();
          session(['pending_check'=>$cheque_infos->count()]);
        //show branch details in dashboard

		  //show user details for super admin
        $users = User::all();
        $transaction = Transaction::latest()->first();
        
        //show best selling product in dashboard
        $branches = Branch::paginate(10);
        $users = User::all();

        $top_selling_products = Sale_detail::where($where)->orderBy('product_id')->get();
        $topproducts = [];
        //return $top_selling_products;
        $pid=0;
        foreach($top_selling_products as $key => $value){
           if($pid!==$value->product_id){
            $topproducts['id'] = $value->product_id;
            $topproducts[$value->product_id]['total_sold_qty']=0;
            $topproducts[$value->product_id]['sold_amount'] = 0;
            $topproducts[$value->product_id]['total_sold_qty']+= $value->sold_quantity;
            $topproducts[$value->product_id]['product_sku'] = $value->product->product_sku;
            $topproducts[$value->product_id]['product_img'] = $value->product->image;
            $topproducts[$value->product_id]['sold_amount'] += ($value->sold_unit_price*$value->sold_quantity);
           }else{
               $topproducts[$value->product_id]['sold_amount'] += ($value->sold_unit_price*$value->sold_quantity);
               $topproducts[$value->product_id]['total_sold_qty']+= $value->sold_quantity;
           }
           $pid=$value->product_id;
        }
        $qty_ary=[];
        if($topproducts !== null){
            foreach($topproducts as $k=>$v){
                if(isset($topproducts[$k]['total_sold_qty'])){
                $qty_ary[$k]=$v['total_sold_qty'];
                }
            }
        }
        $qty_ary=array_filter($qty_ary);
        arsort($qty_ary);
        // dd($qty_ary);
//code for payment Method todays status
        $payment_methods = Payment_method::with(['journals' =>function($query)use($where){
            $query->where($where);
        }])->latest()->get();
//code for payment Method todays status


        $inventories = Branch_inventory::groupBy('branch_id');
        return view('admin/dashboard',compact('users','inventories','branches','totalsales','totalemployee','todaysReceipts','todaysPayments','transaction','TotalSaleschart','donut_chart','topproducts','qty_ary','payment_methods'));
    }
	 public function googlepiechart(){
        //show graph chart for total employee number
        $data=DB::table('sales')
            ->select(
                DB::raw('created_at as created_at'),
                DB::raw('count(*) as number') )
                ->groupBy('branch_id')
            ->get();
        $array[] = ['created_at','Number'];
        foreach($data as $key=>$value)
        {
            $array[++$key] = [$value->created_at,$value->number];
        }
        return view('admin/google_pie_chart')->with('created_at',json_encode($array));
    }

    public function change_status(Request $request,$id){
        $status = User::find($id);
        
        if($request->status){
            $status->status = 0;
        }else{
            $status->status = true;
        }
        
        $status->update();
        return redirect()->back();
 
     }
     public function show($id){
            $branch = Branch_inventory::where('branch_id','=',$id)->orderBy('stock_quantity','DESC')->take(5)->get();

            $i =1;
            $returnData='';
            foreach($branch as $bnch){
                $returnData.='<tr>';
                $returnData.='<td>'.$i++.'</td>';
                $returnData.='<td>'.$bnch->product->product_sku.'</td>';
                $returnData.='<td>'.$bnch->purchase->invoice_no.'</td>';
                $returnData.='<td>'.$bnch->purchase->purchase_unit_price.'</td>';
                $returnData.='<td>'.$bnch->stock_quantity.'</td>';
                $returnData.='</tr>';
            }
            return $returnData;
     }
}
