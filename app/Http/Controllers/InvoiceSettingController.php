<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\InvoiceSetting;
use App\PrimarySettings;
use Auth;

class InvoiceSettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $company=PrimarySettings::orderBy('id')->first();
        $invoice_setting=InvoiceSetting::orderBy('id')->first();
        $footer_contents=InvoiceSetting::where('id','!=',1)->orderBy('id')->get();
        return view('admin.invoice_setting.index',compact('invoice_setting','footer_contents','company'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $footer_old_contents=$request->footer_content;
        if($footer_old_contents!=null){
        foreach($footer_old_contents as $key=>$foc){
            $newFooterContent=InvoiceSetting::find($key);
            $newFooterContent->footer_content=$foc;
            $newFooterContent->update();
        }
    }

        $invoice_setting=InvoiceSetting::orderBy('id')->first();
        $invoice_setting->layouts=$request->layouts;
        $invoice_setting->footer_content_no=$request->footer_content_no;
        $invoice_setting->footer_address=$request->footer_address;
        $invoice_setting->update();
        $newContent=$request->footer_content_new;
        if($newContent!=null){
            foreach($newContent as $nfc){
                $newFooterContent=new InvoiceSetting;
                $newFooterContent->footer_content=$nfc;
                $newFooterContent->created_by=Auth::user()->id;
                $newFooterContent->save();
            }

        }
        return redirect()->route('sales_report')->with('status','Invoice Layouts and Footer Content have Changed successfully');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //return $request;
        
        //dd($newContent);
        $requestData = $request->all();
        $requestData['invoice_logo']=$requestData['old_invoice_logo']?$requestData['old_invoice_logo']:null;
        unset($requestData['old_invoice_logo']);
        $request->logo_enable?$requestData['logo_enable']=1:$requestData['logo_enable']=0;
      
       // return $requestData;
        $image=$request->invoice_logo;
        if($image){
            $path='upload/inoice_logo/';
            $name='invoice_logo'.".".$image->getClientOriginalExtension();
            $image_url=$path.$name;
            $image->move($path,$name);
            $requestData['invoice_logo']=$image_url;
        }
        $invoice_setting=InvoiceSetting::orderBy('id')->first();
        $invoice_setting->fill($requestData)->save();
        return redirect()->back()->with('status','Invoice Titles have Changed successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
