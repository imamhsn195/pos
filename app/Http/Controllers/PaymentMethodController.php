<?php

namespace App\Http\Controllers;

use App\Payment_method;
use App\Journal;
use App\Transaction;
use App\Branch;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\JournalHelper\journalEntry;
use Illuminate\Support\Facades\Auth;

class PaymentMethodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   $payment_methods=[];
        $methods = Payment_method::with('journals')->whereStatus(1)->get();
        $user_type =  Auth::user()->user_type;
        $method_title = "";
        $branches=Branch::where('status',1)->get();
        if(isset($_GET['method_title'])){
            $method_title = $_GET['method_title'];
        }
        if($user_type == 'superadmin'){
             $payment_methods = Payment_method::with('journals')->where('method_title','LIKE',"%$method_title%")->latest()->paginate(5);
        }else{
            $payment_methods = Payment_method::with(['journals' =>function($query){
                $query->where('branch_id','=',Auth::user()->branch_id);
            }])->where('method_title','LIKE',"%$method_title%")->latest()->paginate(5);
        }
        return view('admin.payment_method.index',compact('methods','payment_methods','branches'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.payment_method.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'method_title' => 'required|unique:payment_methods',
            'method_type' => 'required',
            'description' => 'required',
        ]);

        $payment_method = new Payment_method;
        $payment_method->method_title = $request->method_title;
        $payment_method->created_by = Auth::id();
        $payment_method->method_type = $request->method_type;
        $payment_method->description = $request->description;
        $payment_method->save();
        return redirect()->route('payment_method.index')->with('status','Payment Method Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Payment_method  $payment_method
     * @return \Illuminate\Http\Response
     */
    public function show(Payment_method $payment_method)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Payment_method  $payment_method
     * @return \Illuminate\Http\Response
     */
    public function edit(Payment_method $payment_method)
    {
        return view('admin.payment_method.edit',compact('payment_method'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Payment_method  $payment_method
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Payment_method $payment_method)
    {
        /** active/inactive payment_method start here */
        if($request->status){
            $payment_method->status = 0;
            $payment_method->update();
            return redirect()->back();
        }else{
            $payment_method->status = true;
            $payment_method->update();
            return redirect()->back();
        }
        /** active/inactive payment_method end here */
        
        $this->validate($request,[
            'method_title' => 'required',
            'method_type' => 'required',
            'description' => 'required',
        ]);

        $payment_method->method_title = $request->method_title;
        $payment_method->method_type = $request->method_type;
        $payment_method->description = $request->description;
        $payment_method->save();
        return redirect()->route('payment_method.index')->with('status','Payment Method Updated Successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Payment_method  $payment_method
     * @return \Illuminate\Http\Response
     */
    public function destroy(Payment_method $payment_method)
    {
        
    }
    public function add_payment_method_balance(Request $request)
    {
        // return $request;
        if($request->transaction_type){
            $this->validate($request,[
                'payment_method_id' => 'required'
            ]);
        }
        if($request->payment_method_id){
            $payment_method_id=str_before($request->payment_method_id," | ")==0?null:str_before($request->payment_method_id," | ");
            $payment_method_type=str_before($request->payment_method_id," | ")==0?null:str_after($request->payment_method_id," | ");
        }else{
            $payment_method_id=null;
            $payment_method_type=null;
        }
        
        $branch_id=$request->branch_id;
        $journal_posted='';
        $payment_method_to_be_add_type=$request->payment_method_to_be_add_type;
        $payment_method_to_be_add_id=$request->payment_method_to_be_add_id;
        $amount_get=$request->paid_amount;

        if($payment_method_to_be_add_id){
        $transaction=new Transaction;
        $transaction->event_id=$branch_id;
        $transaction->event_type=($request->payment_method_id!==null)?'amount_transfer':'begining_balance';
        $transaction->branch_id=$branch_id;
        $transaction->created_by=Auth::id();
        $transaction->save();
        $transaction_id=$transaction->id;

        //journal entry;
        if($transaction_id){
           //  [6- A/p,5-A/R,2-cash,10-bank,12-Discount on Receive,13-Discount on Payment 8-Capital]
            //executing journal for account payment or collect.;
            $related_party_type='Owner';
            $related_party_id=8;
           
            $description=($request->payment_method_id!==null)?$description='Payment Method Amount Transfer':'initial Capital invested';

            
              
                $accounts_id=$payment_method_to_be_add_type?10:2;
                $journal_type=0;
                $pm_id=$payment_method_to_be_add_id;
                $journalEntry=new journalEntry;
                $journal_posted.=$journalEntry->journal_entry($transaction_id,$related_party_id,$related_party_type,$journal_type,$accounts_id,$pm_id,$description,$amount_get,$branch_id);
                
                $accounts_id=$payment_method_id?($payment_method_type?10:2):8;
                $journal_type=1;
                $pm_id=$payment_method_id?$payment_method_id:$payment_method_to_be_add_id;
                $journalEntry=new journalEntry;
                $journal_posted.=$journalEntry->journal_entry($transaction_id,$related_party_id,$related_party_type,$journal_type,$accounts_id,$pm_id,$description,$amount_get,$branch_id);
                
            // }//for loop
        }//transaction id
    }
    return redirect()->route('payment_method.index')->with('status','Payment Method Updated Successfully');
    }
}
