<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    public function address() {
        return $this->belongsTo('App\Address_info','address_info_id');
    }
	
	public function users() {
		return $this->belongsTo('App\User','created_by');
	}
	public function groups() {
		return $this->belongsTo('App\Customer_group','customer_group_id');
	}
	public function journal() {
        return $this->hasMany('App\Journal','related_party_id');
    }
}
