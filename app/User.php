<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','user_type','branch_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    // hashing new user password
    public function setPasswordAttribute($value){
        $this->attributes['password'] = bcrypt($value);
    }
    
	// a user can have a Address
    public function address() {
        return $this->hasOne('App\Address_info');
    }

    // a user can be a teacher
    public function attributes() {
        return $this->hasMany('App\Attribute');
    }

   public function branch(){
       return $this->belongsTo(Branch::class);
   }

    public function vats(){
        return $this->hasMany(Vat::class);
    }
	
	public function brands(){
		return $this->hasMany(Brand::class);
	}
	
	public function bundle_products(){
		return $this->hasMany(Bundle_product::class);
	}

    public function payment_methods(){
        return $this->hasMany(Payment_method::class);
    }
    public function journal(){
        return $this->hasMany(Journal::class);
    }
}
