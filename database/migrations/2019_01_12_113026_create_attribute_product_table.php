<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttributeProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attribute_product', function (Blueprint $table) {
            $table->integer('attribute_id')->unsigned()->index();
            $table->integer('product_id')->unsigned()->index();
            $table->foreign('attribute_id')->references('id')
                ->on('attributes')->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreign('product_id')->references('id')
                ->on('products')->onUpdate('cascade')
                ->onDelete('cascade');
            
            $table->primary(['attribute_id', 'product_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attribute_product');
    }
}
