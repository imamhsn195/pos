<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart_datas', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('product_id');
            $table->unsignedInteger('customer_id');
            $table->unsignedInteger('purchase_id');//bundle_id
            $table->integer('qty');
            $table->boolean('type')->default(0);//0 for single 1 for bundle
            //set_customer_info($customer_search_input)
            //add_item_to_cart($product_id,$purchase_id,$cust_group_id=0)
            //add_bundle_item_to_cart($bundle_id)
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart_datas');
    }
}
