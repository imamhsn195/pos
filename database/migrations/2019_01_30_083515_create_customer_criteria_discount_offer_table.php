<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerCriteriaDiscountOfferTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_criteria_discount_offer', function (Blueprint $table) {
           // $table->increments('id');
            $table->unsignedInteger('customer_criteria_id')->index();
            $table->unsignedInteger('discount_offer_id')->index();
            $table->timestamps();
            $table->foreign('customer_criteria_id')->references('id')->on('customer_criterias')->onDelete('cascade');
            $table->foreign('discount_offer_id')->references('id')->on('discount_offers')->onDelete('cascade');
            $table->primary(['customer_criteria_id','discount_offer_id'],'criteria_discount_key');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_criteria_discount_offer');
    }
}
