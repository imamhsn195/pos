<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWarrantyTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('warranty_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('warranty_name');
            $table->text('warranty_description');
            $table->unsignedInteger('parent_id')->nullable();
            $table->unsignedInteger('created_by');
            $table->integer('last_updated_by')->nullable();
            $table->boolean('status')->default(1);
            $table->timestamps();
			$table->foreign('parent_id')->references('id')->on('warranty_types')->onDelete('cascade');
			$table->foreign('created_by')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('warranty_types');
    }
}
