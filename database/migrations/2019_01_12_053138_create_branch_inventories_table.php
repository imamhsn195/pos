<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchInventoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branch_inventories', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('created_by');
            $table->unsignedInteger('purchase_id');
            $table->unsignedInteger('branch_id');
            $table->unsignedInteger('product_id');
            $table->integer('stock_quantity');
            $table->integer('last_updated_by')->nullable();
            $table->boolean('status')->default(1);
            $table->timestamps();
			$table->foreign('created_by')->references('id')->on('users')->onDelete('cascade');
			$table->foreign('purchase_id')->references('id')->on('purchases')->onDelete('cascade');
			$table->foreign('branch_id')->references('id')->on('branches')->onDelete('cascade');
			$table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branch_inventories');
    }
}
