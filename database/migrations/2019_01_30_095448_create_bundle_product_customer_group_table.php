<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBundleProductCustomerGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bundle_product_customer_group', function (Blueprint $table) {
             // $table->increments('id');
             $table->unsignedInteger('bundle_product_id')->index();
             $table->unsignedInteger('customer_group_id')->index();
             $table->timestamps();
             $table->foreign('bundle_product_id')->references('id')->on('bundle_products')->onDelete('cascade');
             $table->foreign('customer_group_id')->references('id')->on('customer_groups')->onDelete('cascade');
             $table->primary(['bundle_product_id','customer_group_id'],'bundle_customer_key');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bundle_product_customer_group');
    }
}
