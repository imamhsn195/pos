<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
			$table->unsignedInteger('address_info_id');
			$table->unsignedInteger('customer_group_id')->nullable();
			$table->unsignedInteger('created_by');
			$table->string('customer_name');
            $table->integer('last_updated_by')->nullable();
            $table->double('credit_limit',8,2)->nullable();
            $table->boolean('status')->default(1);
            $table->timestamps();
			$table->foreign('address_info_id')->references('id')->on('address_infos')->onDelete('cascade');
			$table->foreign('customer_group_id')->references('id')->on('customer_groups')->onDelete('cascade');
			$table->foreign('created_by')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
