<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSaleSalesManTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sale_sales_man', function (Blueprint $table) {
			$table->integer('sale_id')->unsigned()->index();
            $table->integer('sales_man_id')->unsigned()->index();
            $table->timestamps();

            $table->foreign('sale_id')->references('id')
                ->on('sales')->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreign('sales_man_id')->references('id')
                ->on('sales_men')->onUpdate('cascade')
                ->onDelete('cascade');
            
            $table->primary(['sale_id', 'sales_man_id']);
        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sale_sales_man');
    }
}
