<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJournalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('journals', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('created_by');
            $table->unsignedInteger('transaction_id');
            $table->unsignedInteger('accounts_id');
            $table->unsignedInteger('payment_method_id')->nullable();
            $table->unsignedInteger('branch_id');
            $table->unsignedInteger('related_party_id')->nullable();
            $table->string('related_party_type')->nullable();
            $table->double('amount');
            $table->text('description')->nullable();
            $table->boolean('journal_type');
            $table->integer('last_updated_by')->nullable();
            $table->boolean('status')->default(1);
            $table->timestamps();
			$table->foreign('created_by')->references('id')->on('users')->onDelete('cascade');
			$table->foreign('accounts_id')->references('id')->on('accounts')->onDelete('cascade');
			$table->foreign('payment_method_id')->references('id')->on('payment_methods')->onDelete('cascade');
			$table->foreign('branch_id')->references('id')->on('branches')->onDelete('cascade');
			$table->foreign('transaction_id')->references('id')->on('transactions')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('journals');
    }
}
