<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('invoice_title')->default('Sales Invoice');
            $table->string('invoice_no_title')->default('Invoice No');
            $table->string('customer_name')->default('Customer Name');
            $table->boolean('logo_enable')->default(0);
            $table->string('invoice_logo')->default('default/images/default.png');
            $table->string('logo_position')->default('left');
            $table->string('sl')->default('#SL');
            $table->string('product_name')->default('ProductName');
            $table->string('product_sl_no')->default('Product SL NO');
            $table->string('qty')->default('Qty');
            $table->string('unit_price')->default('Unit Price');
            $table->string('total')->default('Total');
            $table->string('discount_type')->default('Discount Type');
            $table->string('discount')->default('Discount');
            $table->string('subtotal')->default('SubTotal');
            $table->string('grand_total')->default('Grand Total');
            $table->string('sales_total')->default('Sales Total');
            $table->string('discount_total')->default('Discount Total');
            $table->string('discounted_total')->default('Discounted Total');
            $table->string('tax')->default('Tax');
            $table->string('single_discount')->default('Single Product Discount');
            $table->string('regular_discount')->default('Regular Discount');
            $table->string('net_sales_amount')->default('Net Sales Amount');
            $table->string('coupon')->default('Coupon');
            $table->string('paid_amount')->default('Amount Paid');
            $table->string('due_amount')->default('Amount Due');
            $table->string('paid_by')->default('Received in');
            $table->string('layouts')->default('admin.sale.sales_details_report');
            $table->string('footer_content')->default('nill');
            $table->integer('footer_content_no')->default('3');
            $table->string('footer_address')->default('Addressss');
            $table->boolean('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_settings');
    }
}
