<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrimarySettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('primary_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_name');
            $table->string('company_logo')->default('default/images/default.png');
            $table->char('phone',11);
            $table->string('email');
            $table->string('address');
            $table->string('thana');
            $table->string('district');
            $table->string('postal_code');
            $table->string('product_barcode_type')->default('TYPE_EAN_13');
            $table->boolean('salesperson_feature')->default(0);
            $table->boolean('bin_no')->nullable();
            $table->boolean('branch_can_purchase')->default(1);
            $table->integer('warranty_alert_time')->default(10);
            $table->integer('expire_alert_time')->default(10);
            $table->integer('check_alert_time')->default(10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('primary_settings');
    }
}
