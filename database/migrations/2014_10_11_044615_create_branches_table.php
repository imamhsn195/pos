<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branches', function (Blueprint $table) {
           $table->increments('id');
            $table->unsignedInteger('address_info_id');
            $table->string('name');
            $table->boolean('branch_type')->default(0); //warehouse = 0 , showroom = 1
            $table->integer('last_updated_by')->nullable();
            $table->decimal('begining_balance',12,2)->nullable();
            $table->boolean('status')->default(1);
            $table->timestamps();
			$table->foreign('address_info_id')->references('id')->on('address_infos')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branches');
    }
}
