<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
			$table->unsignedInteger('created_by');
			$table->unsignedInteger('attribute_set_id')->nullable();
			$table->unsignedInteger('category_id')->nullable();
			$table->unsignedInteger('vat_id')->nullable();
			$table->unsignedInteger('brand_id')->nullable();
			$table->string('image')->default('default/images/default.png');
			$table->integer('reorder_limit')->nullable();
			$table->string('product_sku')->unique();
			$table->string('product_barcode')->nullable();
			$table->text('short_description')->nullable();
			$table->text('long_description')->nullable();
			$table->integer('last_updated_by')->nullable();
            $table->boolean('status')->default(1);
            $table->timestamps();
			$table->foreign('created_by')->references('id')->on('users')->onDelete('cascade');
			$table->foreign('attribute_set_id')->references('id')->on('attribute_sets')->onDelete('cascade');
			$table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
			$table->foreign('vat_id')->references('id')->on('vats')->onDelete('cascade');
			$table->foreign('brand_id')->references('id')->on('brands')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
