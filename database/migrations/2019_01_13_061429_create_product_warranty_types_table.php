<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductWarrantyTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_warranty_types', function (Blueprint $table) {
            $table->increments('id');
			$table->unsignedInteger('warranty_type_id');
			$table->unsignedInteger('purchase_id');
			$table->unsignedInteger('created_by');
            $table->integer('last_updated_by')->nullable();
            $table->boolean('status')->default(1);
            $table->integer('duration');
            $table->timestamps();
            $table->foreign('warranty_type_id')->references('id')->on('warranty_types')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('created_by')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
			$table->foreign('purchase_id')->references('id')->on('purchases')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_warranty_types');
    }
}
