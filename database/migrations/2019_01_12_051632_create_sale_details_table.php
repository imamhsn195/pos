<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSaleDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sale_details', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('created_by');
            $table->unsignedInteger('product_id')->nullable();
            $table->unsignedInteger('sales_id');
            $table->unsignedInteger('purchase_id');
            $table->unsignedInteger('branch_id');
            $table->integer('last_updated_by')->nullable();
            $table->boolean('status')->default(1);
            $table->integer('sold_quantity');
            $table->double('sold_unit_price');
            $table->integer('discount_id')->nullable();
            $table->string('discount_type')->nullable();//bundle product and promo code
            $table->double('discount_subtotal')->default(0);
            $table->timestamps();
            $table->foreign('created_by')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->foreign('sales_id')->references('id')->on('sales')->onDelete('cascade');
            $table->foreign('purchase_id')->references('id')->on('purchases')->onDelete('cascade');
            $table->foreign('branch_id')->references('id')->on('branches')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sale_details');
    }
}
