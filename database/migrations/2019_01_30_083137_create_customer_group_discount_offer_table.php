<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerGroupDiscountOfferTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_group_discount_offer', function (Blueprint $table) {
           // $table->increments('id');
            $table->unsignedInteger('customer_group_id')->index();
            $table->unsignedInteger('discount_offer_id')->index();
            $table->timestamps();
            $table->foreign('customer_group_id')->references('id')->on('customer_groups')->onDelete('cascade');
            $table->foreign('discount_offer_id')->references('id')->on('discount_offers')->onDelete('cascade');
            $table->primary(['customer_group_id','discount_offer_id'],'customer_discount_key');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_group_discount_offer');
    }
}
