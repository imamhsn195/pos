<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('created_by');
            $table->unsignedInteger('branch_id');
            $table->unsignedInteger('customer_id');
            $table->integer('last_updated_by')->nullable();
            $table->boolean('status')->default(1);
            $table->string('invoice_no');
            $table->double('total_amount');
            $table->integer('discount_id')->nullable();//will check for relation with discount offer//
            $table->string('discount_type')->nullable();
            $table->double('discount_total')->default(0);
            $table->double('regular_discount_total')->default(0);
            $table->double('total_tax')->default(0);
            $table->timestamps();
            $table->foreign('created_by')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('branch_id')->references('id')->on('branches')->onDelete('cascade');
            $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales');
    }
}
