<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBundleProductCustomerCriteriaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bundle_product_customer_criteria', function (Blueprint $table) {
           // $table->increments('id');
           $table->unsignedInteger('bundle_product_id')->index();
           $table->unsignedInteger('customer_criteria_id')->index();
           $table->timestamps();
           $table->foreign('bundle_product_id')->references('id')->on('bundle_products')->onDelete('cascade');
           $table->foreign('customer_criteria_id')->references('id')->on('customer_criterias')->onDelete('cascade');
           $table->primary(['bundle_product_id','customer_criteria_id'],'bundle_criteria_key');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bundle_product_customer_criteria');
    }
}
