<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attributes', function (Blueprint $table) {
            $table->increments('id');
			$table->string('attribute_name');
			$table->unsignedInteger('parent_id')->nullable();
			$table->boolean('status')->default(1);
			$table->unsignedInteger('created_by');
			$table->integer('updated_by')->nullable();
			$table->foreign('created_by')->references('id')->on('users')->onDelete('cascade');
			$table->foreign('parent_id')->references('id')->on('attributes')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attributes');
    }
}
