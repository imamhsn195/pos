<?php

use Faker\Generator as Faker;

$factory->define(App\Warranty_type::class, function (Faker $faker) {
	$parents = App\Warranty_type::pluck('id')->toArray();
    return [
        'warranty_name' => $faker->name,
        'warranty_description' => $faker->name,
        'parent_id' => $faker->randomElement($parents),
        'created_by' => 1,
    ];
});
