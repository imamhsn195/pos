<?php

use Illuminate\Database\Seeder;

class BranchTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *`address_info_id`, `name`, `branch_type`, `last_updated_by`,
     * @return void
     */
    public function run()
    {
        DB::table('branches')->insert([
			'address_info_id'=>'1',
			'name'=>'Chawkbazar Showroom',
			'branch_type'=>'0',
		]);
		DB::table('branches')->insert([
			'address_info_id'=>'2',
			'name'=>'Chawkbazar Warehouse',
			'branch_type'=>'1',
		]);
        DB::table('branches')->insert([
			'address_info_id'=>'3',
			'name'=>'Agrabad Showroom',
			'branch_type'=>'0',
		]);
		DB::table('branches')->insert([
			'address_info_id'=>'4',
			'name'=>'Agrabad Warehouse',
			'branch_type'=>'1',
		]);
        DB::table('branches')->insert([
			'address_info_id'=>'5',
			'name'=>'Halishahar Showroom',
			'branch_type'=>'0',
		]);
		DB::table('branches')->insert([
			'address_info_id'=>'6',
			'name'=>'Halishahar Warehouse',
			'branch_type'=>'1',
		]);
    }
}
