<?php

use Illuminate\Database\Seeder;

class AttributesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('attributes')->insert([
        'attribute_name' => 'Storage', 
           'created_by' => 1,
        ]);
        DB::table('attributes')->insert([
            'attribute_name' => 'RAM',
            'created_by' => 1,
            ]);
        DB::table('attributes')->insert([
            'attribute_name' => 'Operating System',
            'created_by' => 1,
            ]);
        DB::table('attributes')->insert([
            'attribute_name' => 'Processor',
            'created_by' => 1,
                ]);
        //child attributes of storage
            DB::table('attributes')->insert([
                'attribute_name' => '32gb', 
                'parent_id' => 1,
                'created_by' => 1,
                ]);
            DB::table('attributes')->insert([
                'attribute_name' => '16gb', 
                    'parent_id' => 1,
                    'created_by' => 1,
                ]);
            DB::table('attributes')->insert([
                'attribute_name' => '8gb', 
                    'parent_id' => 1,
                    'created_by' => 1,
                ]);
        //child attributes of Ram
            DB::table('attributes')->insert([
                'attribute_name' => '8gb', 
                'parent_id' => 2,
                'created_by' => 1,
                ]);
            DB::table('attributes')->insert([
                'attribute_name' => '4gb', 
                    'parent_id' => 2,
                    'created_by' => 1,
                ]);
            DB::table('attributes')->insert([
                'attribute_name' => '2gb', 
                    'parent_id' => 2,
                    'created_by' => 1,
                ]);

        //child attributes of Operating System
             DB::table('attributes')->insert([
                'attribute_name' => 'Android', 
                'parent_id' => 3,
                'created_by' => 1,
                ]);
            DB::table('attributes')->insert([
                'attribute_name' => 'IOS', 
                    'parent_id' => 3,
                    'created_by' => 1,
                ]);
            DB::table('attributes')->insert([
                'attribute_name' => 'WindowsOS', 
                    'parent_id' => 3,
                    'created_by' => 1,
                ]);
        //child attributes of Processor
            DB::table('attributes')->insert([
            'attribute_name' => 'QuadCore', 
            'parent_id' => 4,
            'created_by' => 1,
            ]);
            DB::table('attributes')->insert([
                'attribute_name' => 'OctaCore', 
                    'parent_id' => 4,
                    'created_by' => 1,
                ]);
            DB::table('attributes')->insert([
                'attribute_name' => 'DualCore', 
                    'parent_id' => 4,
                    'created_by' => 1,
                ]);
    }
}
