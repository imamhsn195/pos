<?php

use Illuminate\Database\Seeder;

class BrandTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *`brands`(`id`, `created_by`, `parent_id`, `brand_name`,
     * @return void
     */
    public function run()
    {
        DB::table('brands')->insert([
		'brand_name'=>'HP',
		'created_by'=>1,
		]);
		DB::table('brands')->insert([
		'brand_name'=>'Lenovo',
		'created_by'=>1,
		]); 
		DB::table('brands')->insert([
		'brand_name'=>'LG',
		'created_by'=>1,
		]); 
		DB::table('brands')->insert([
		'brand_name'=>'MI',
		'created_by'=>1,
		]); 
		DB::table('brands')->insert([
		'brand_name'=>'Walton',
		'created_by'=>1,
		]); 
    }
}
