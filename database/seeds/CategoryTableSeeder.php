<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	//Parent category Mobiles id 1
        DB::table('categories')->insert([
			'category_name'=>'Mobiles',
			'is_expirable'=>'0',
			'is_warrantable'=>'1',
			'created_by'=>'1',
		]);
		//Parent category Electronics id 2
		DB::table('categories')->insert([
			'category_name'=>'Electronics',
			'is_expirable'=>'0',
			'is_warrantable'=>'1',
			'created_by'=>'1',
		]); 
		//Parent category Vehicle id 3
		DB::table('categories')->insert([
			'category_name'=>'Vehicles',
			'is_expirable'=>'0',
			'is_warrantable'=>'1',
			'created_by'=>'1',
		]); 
		//Parent category Fashion id 4
		DB::table('categories')->insert([
			'category_name'=>'Fashion',
			'is_expirable'=>'0',
			'is_warrantable'=>'0',
			'created_by'=>'1',
		]); 
		//Child category of Mobiles
		DB::table('categories')->insert([
			'category_name'=>'Mobile Phone',
			'is_expirable'=>'0',
			'is_warrantable'=>'1',
			'parent_id'=>'1',
			'created_by'=>'1',
		]); 
		//Child category of Mobiles
		DB::table('categories')->insert([
			'category_name'=>'Mobile Accessories',
			'is_expirable'=>'0',
			'is_warrantable'=>'1',
			'parent_id'=>'1',
			'created_by'=>'1',
		]); 
		
		//Child category of Electronics
		DB::table('categories')->insert([
			'category_name'=>'Computers',
			'is_expirable'=>'0',
			'is_warrantable'=>'1',
			'parent_id'=>'2',
			'created_by'=>'1',
		]); 
		//Child category of Electronics
		DB::table('categories')->insert([
			'category_name'=>'Laptops',
			'is_expirable'=>'0',
			'is_warrantable'=>'1',
			'parent_id'=>'2',
			'created_by'=>'1',
		]); 
		
		//Child category of Vehicle
		DB::table('categories')->insert([
			'category_name'=>'Car',
			'is_expirable'=>'0',
			'is_warrantable'=>'1',
			'parent_id'=>'3',
			'created_by'=>'1',
		]); 
		//Child category of Vehicle
		DB::table('categories')->insert([
			'category_name'=>'Motorbike',
			'is_expirable'=>'0',
			'is_warrantable'=>'1',
			'parent_id'=>'3',
			'created_by'=>'1',
		]); 
		
		 //Child category of Fashion
		DB::table('categories')->insert([
			'category_name'=>'Shirt',
			'is_expirable'=>'0',
			'is_warrantable'=>'0',
			'parent_id'=>'4',
			'created_by'=>'1',
		]); 
		 //Child category of Fashion
		DB::table('categories')->insert([
			'category_name'=>'pants',
			'is_expirable'=>'0',
			'is_warrantable'=>'0',
			'parent_id'=>'4',
			'created_by'=>'1',
		]); 

    }
}