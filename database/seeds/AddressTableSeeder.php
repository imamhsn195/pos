<?php

use Illuminate\Database\Seeder;

class AddressTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *`email`, `full_address`, `phone`, `nid`, `nationality`, `religion`, `address_infos` 
     * @return void
     */
    public function run()
    {
        DB::table('address_infos')->insert([
			'full_address'=>'Chawkbazar, Chittagong, Bangladesh',
			'phone'=>'1234501',
		]);
        DB::table('address_infos')->insert([
			'full_address'=>'Chawkbazar, Chittagong, Bangladesh',
			'phone'=>'1234501',
		]);
        DB::table('address_infos')->insert([
			'full_address'=>'Agrabad, Chittagong, Bangladesh',
			'phone'=>'1234502',
		]);
        DB::table('address_infos')->insert([
			'full_address'=>'Agrabad, Chittagong, Bangladesh',
			'phone'=>'1234502',
		]);
    	DB::table('address_infos')->insert([
			'full_address'=>'Andarkilla, Chittagong, Bangladesh',
			'phone'=>'1234503',
		]);
        DB::table('address_infos')->insert([
			'full_address'=>'Andarkilla, Chittagong, Bangladesh',
			'phone'=>'1234503',
		]);
        DB::table('address_infos')->insert([
        	'email'=>'azam@gmail.com',
			'full_address'=>'2 No gate Chittagong',
			'phone'=>'1234503',
		]);
		DB::table('address_infos')->insert([
        	'email'=>'jamal@gmail.com',
			'full_address'=>'Nasirabad housing society, chittagong',
			'phone'=>'01715285412',
		]);
		DB::table('address_infos')->insert([
			'full_address'=>'in everywhere, point of sale',
		]);
    }
}