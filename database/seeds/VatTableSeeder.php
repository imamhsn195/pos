<?php

use Illuminate\Database\Seeder;

class VatTableSeeder extends Seeder
{
    /** `id`, `created_by`, `vat_title`, `vat_description`, `vat_percentage`, `last_updated_by`, `status  vats
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vats')->insert([
			'vat_title'=>'Vat@15%',
			'vat_description'=>'15 Percent vat on sales',
			'vat_percentage'=>0.15,
			'created_by'=>1,
		]);
    }
}
