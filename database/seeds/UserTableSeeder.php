<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//Super Admin
        DB::table('users')->insert([
			'name'=>'superadmin',
			'email'=>'superadmin@pos.com',
			'password'=>bcrypt('123456'),
			'user_type'=>'superadmin'
		]);

		DB::table('users')->insert([
			'name'=>'admin_chawk',
			'branch_id'=>1,
			'email'=>'admin_chawk@pos.com',
			'password'=>bcrypt('123456'),
			'user_type'=>'admin'
		]);
		DB::table('users')->insert([
			'name'=>'cashier_chawk',
			'branch_id'=>1,
			'email'=>'cashier_chawk@pos.com',
			'password'=>bcrypt('123456'),
			'user_type'=>'cashier'
		]);
		DB::table('users')->insert([
			'name'=>'manager_chawk',
			'branch_id'=>1,
			'email'=>'manager_chawk@pos.com',
			'password'=>bcrypt('123456'),
			'user_type'=>'manager'
		]);

		DB::table('users')->insert([
			'name'=>'admin_agrabad',
			'branch_id'=>3,
			'email'=>'admin_agrabad@pos.com',
			'password'=>bcrypt('123456'),
			'user_type'=>'admin'
		]);
		DB::table('users')->insert([
			'name'=>'cashier_agrabad',
			'branch_id'=>3,
			'email'=>'cashier_agrabad@pos.com',
			'password'=>bcrypt('123456'),
			'user_type'=>'cashier'
		]);
		DB::table('users')->insert([
			'name'=>'manager_agrabad',
			'branch_id'=>3,
			'email'=>'manager_agrabad@pos.com',
			'password'=>bcrypt('123456'),
			'user_type'=>'manager'
		]);

		DB::table('users')->insert([
			'name'=>'admin_halishahar',
			'branch_id'=>5,
			'email'=>'admin_halishahar@pos.com',
			'password'=>bcrypt('123456'),
			'user_type'=>'admin'
		]);
		DB::table('users')->insert([
			'name'=>'cashier_halishahar',
			'branch_id'=>5,
			'email'=>'cashier_halishahar@pos.com',
			'password'=>bcrypt('123456'),
			'user_type'=>'cashier'
		]);
		DB::table('users')->insert([
			'name'=>'manager_halishahar',
			'branch_id'=>5,
			'email'=>'manager_halishahar@pos.com',
			'password'=>bcrypt('123456'),
			'user_type'=>'manager'
		]);
	}
}
