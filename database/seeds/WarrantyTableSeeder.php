<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;
class WarrantyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      //factory(App\Warranty_type::class,2)->create();
    	DB::table('warranty_types')->insert([
		 'warranty_name' => 'Product Warranty',
        'warranty_description' =>'Product Warranty Description',
        'created_by' => 1,
		]);
		DB::table('warranty_types')->insert([
		 'warranty_name' => 'Service Warranty',
        'warranty_description' =>'Service Warranty Description',
        'created_by' => 2,
		]);
		DB::table('warranty_types')->insert([
		 'warranty_name' => 'Motherboard Warranty',
        'warranty_description' =>'Motherboard Warranty Description',
        'parent_id' => 1,
        'created_by' => 1,
		]);
		DB::table('warranty_types')->insert([
		 'warranty_name' => 'Windows Setup Warranty',
        'warranty_description' =>'Windows Warranty Description',
        'parent_id' =>2,
        'created_by' => 2,
		]);
    }
}
