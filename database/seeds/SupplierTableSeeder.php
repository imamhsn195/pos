<?php

use Illuminate\Database\Seeder;

class SupplierTableSeeder extends Seeder
{
    /**`suppliers`(`id`, `supplier_name`, `address_info_id`, `created_by`, `last_updated_by`, `status`, `created_at`, `updated_at`)
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('suppliers')->insert([
			'supplier_name'=>'Azam Hossen',
			'address_info_id'=>7,
			'created_by'=>1,
        ]); 
        DB::table('suppliers')->insert([
			'supplier_name'=>'Jamal Hossen',
			'address_info_id'=>8,
			'created_by'=>1,
		]); 
    }
}