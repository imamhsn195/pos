<?php

use Illuminate\Database\Seeder;

class PaymentMethodTableSeeder extends Seeder
{
    /**SELECT `id`, `created_by`, `method_title`, `method_type`, `description`, `last_updated_by`, `status`, `created_at`, `updated_at` FROM `payment_methods` WHERE 1
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('payment_methods')->insert([
			'method_title'=>'Cash',
			'method_type'=>0,
			'description'=>'Cash on Hand',
			'created_by'=>1,
		]); 
        DB::table('payment_methods')->insert([
			'method_title'=>'bKash',
			'method_type'=>0,
			'description'=>'Cash related method',
			'created_by'=>1,
		]); 
		DB::table('payment_methods')->insert([
			'method_title'=>'DBBL',
			'method_type'=>1,
			'description'=>'Bank related method',
			'created_by'=>1,
		]); 

    }
}