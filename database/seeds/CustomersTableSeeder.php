<?php

use Illuminate\Database\Seeder;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customers')->insert([
			'customer_name'=>'Walking Customer',
			'address_info_id'=>9,
			'created_by'=>1,
        ]); 
    }
}
