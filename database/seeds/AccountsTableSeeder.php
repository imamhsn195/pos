<?php

use Illuminate\Database\Seeder;

class  AccountsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('accounts')->insert([
        	'account_head' => 'Land',
        	'type' => 'Fixed Assets',
        	'status' => '1',
        ]);
        DB::table('accounts')->insert([
        	'account_head' => 'Cash',
        	'type' => 'Current Assets',
        	'status' => '1',
        ]);
        DB::table('accounts')->insert([
        	'account_head' => 'Salary',
        	'type' => 'Operating Expense',
        	'status' => '1',
        ]);
        DB::table('accounts')->insert([
        	'account_head' => 'Sales',
        	'type' => 'Income',
        	'status' => '1',
		]);
		DB::table('accounts')->insert([
        	'account_head' => 'Account Receivable',
        	'type' => 'Current Assets',
        	'status' => '1',
        ]);
        DB::table('accounts')->insert([
        	'account_head' => 'Account Payable',
        	'type' => 'Current Liabilities',
        	'status' => '1',
        ]);
        DB::table('accounts')->insert([
        	'account_head' => 'Bank Loan',
        	'type' => 'Long Term Liabilities',
        	'status' => '1',
        ]);
        DB::table('accounts')->insert([
        	'account_head' => 'Capital',
        	'type' => 'Owners Equity',
        	'status' => '1',
        ]);
		DB::table('accounts')->insert([
        	'account_head' => 'Purchase',
        	'type' => 'Direct Expense',
        	'status' => '1',
		]);
		DB::table('accounts')->insert([
        	'account_head' => 'Bank',
        	'type' => 'Current Assets',
        	'status' => '1',
        ]);
		DB::table('accounts')->insert([
        	'account_head' => 'Purchase Return',
        	'type' => 'Direct Expense',
        	'status' => '1',
        ]);
		DB::table('accounts')->insert([
        	'account_head' => 'Discount on Receive',
        	'type' => 'Operating Expense',
        	'status' => '1',
        ]);
		DB::table('accounts')->insert([
        	'account_head' => 'Discount on Payment',
        	'type' => 'Income',
        	'status' => '1',
        ]);
		DB::table('accounts')->insert([
        	'account_head' => 'Discount on Sales',
        	'type' => 'Direct Expense',
        	'status' => '1',
        ]);
		DB::table('accounts')->insert([
        	'account_head' => 'Discount on Purchase',
        	'type' => 'Income',
        	'status' => '1',
        ]);
		DB::table('accounts')->insert([
        	'account_head' => 'Tax Payable',
        	'type' => 'Current liabilities',
        	'status' => '1',
        ]);
		DB::table('accounts')->insert([
        	'account_head' => 'Sales Return',
        	'type' => 'Income',
        	'status' => '1',
        ]);
    }
}
