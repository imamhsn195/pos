# Bridge ERP

> A ERP Application of Sales.
  - [Build Tools](#build-tools)
  - [Change Log](#change-log)
    - [Version 1.0](#version-10)
  - [Getting Started](#getting-started)
  - [Initial Configuration](#initial-configuration)
    - [Populate Initial Data](#Migration-widh-Data-Seeding)
  - [Copyright](#copyright)

## Build Tools
- Laravel 5.6.*
- bootstrap

## Change Log

### Version 1.0

Under development

## Getting Started

Run the following commands to clone this application:

```shell
git clone https://cgitpro@bitbucket.org/cgitpro/pointofsale.git
cd pointofsale
```
## Initial Configuration

Run the following commands to initiate the application with initial Data Seeding. Note that, each commands are compulsory to execute for the application to function properly.

```shell
composer install
cp .env.example .env
php artisan key:generate
php artisan bridge:install
```
## Copyright

 2019 CGIT.
