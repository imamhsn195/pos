<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */
	'dashboard'=>'ড্যাশবোর্ড',
	'products'=>'পণ্য',
	'registered_products'=>'নিবন্ধিত পণ্য',
	'bundle_product'=>'বান্ডিল পণ্য',
	'categories'=>'ক্যাটাগরি',
	'warranty'=>'ওয়ারেন্টি',
	'attributes'=>'এট্ট্রইবুটস',
	'attribute_sets'=>'এট্ট্রাইবুটস সেট্স',
	'brands'=>'ব্র্যান্ডস',
	'contacts'=>'যোগাযোগ',
	'suppliers'=>'সরবরাহকারী',
	'customers'=>'গ্রাহকরা',
	'customers_group'=>'গ্রাহকদল',
	'administration'=>'প্রশাসন',
	'manage_vat'=>'ভ্যাট প্রয়োগ',
	'manage_discount'=>'ডিসকাউন্ট প্রয়োগ',
	'manage_branches'=>'শাখা পরিচালনা',
	'payment_method'=>'মূল্যপরিশোধ পদ্ধতি',
	'transactions'=>'লেনদেন পরিচালনা',
	'purchase_returns'=>'ক্রয় ফেরত',
	'sales_return'=>'বিক্রয় ফেরত',
	'sales_point'=>'বিক্রয় কেন্দ্র',
	'reports'=>'প্রতিবেদন',
	'purchase'=>'ক্রয়',
	'sales'=>'বিক্রয়',
	'journals'=>'জাবেদা বিবরণী',
	'cashflow'=>'নগদ প্রবাহ বিবরণী',
	'trial_balance'=>'রেওয়ামিল বিবরণী',
	'bangla'=>'বাং',
	'inventories'=>'পণ্য তালিকা',
	'income_statement'=>'আয় বিবরণী',
	'record_expense'=>'ব্যয় নিবন্ধন',
	'expense_report'=>'ব্যয় বিবরণী',
	'payment_show'=>'রসিদ বিবরণী',
	'primary_settings'=>'প্রাথমিক সেটিংস',
	'invoice_settings'=>'চালান সেটিংস',
	'barcode'=>'বারকোড',
	'product_tracking'=>'পণ্য ট্র্যাকিং',
	'monthly_status_of_supplier'=>'সাপ্লাইয়েরদের মাসিক হিসাব',
	'monthly_status_of_customer'=>'গ্কাস্টোমারদের মাসিক হিসাব',
];