<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */
    'welcome'=>'স্বাগতম',
	'reports'=>'রিপোর্ট',
	'product_types'=>'পণ্যের ধরণ',
	'warrantable'=>'ওয়ারেন্টি পণ্য',
	'expirable'=>'মেয়াদী পণ্য',
	'cheques'=>'চেক',
];