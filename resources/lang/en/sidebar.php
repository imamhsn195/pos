<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */
	'dashboard'=>'Dashboard',
	'products'=>'Products',
	'registered_products'=>'Registered Products',
	'bundle_product'=>'Bundle Products',
	'categories'=>'Categories',
	'warranty'=>'Warranty',
	'attributes'=>'Attributes',
	'attribute_sets'=>'Attribute Sets',
	'brands'=>'Brands',
	'contacts'=>'Contacts',
	'suppliers'=>'Suppliers',
	'customers'=>'Customers',
	'customers_group'=>'Customers Group',
	'administration'=>'Administration',
	'manage_vat'=>'Manage VAT',
	'manage_discount'=>'Manage Discount',
	'manage_branches'=>'Manage Branches',
	'payment_method'=>'Payment Method',
	'transactions'=>'Transaction Here',
	'purchase_returns'=>'Purchase Returns',
	'sales_return'=>'Sales Returns',
	'sales_point'=>'Sales Point',
	'reports'=>'Reports',
	'purchase'=>'Purchase',
	'sales'=>'Sales',
	'journals'=>'Journal Statement',
	'cashflow'=>'Cash Flow Statement',
	'trial_balance'=>'Trial Balance Statement',
	'bangla'=>'বাং',
	'inventories'=>'Inventory Statement',
	'income_statement'=>'Income Statement',
	'record_expense'=>'Register Expenses',
	'expense_report'=>'Expense Report',
	'payment_show'=>'Money Receipts',
	'primary_settings'=>'Primary Settings',
	'invoice_settings'=>'Invoice Settings',
	'barcode'=>'Product Barcode',
	'product_tracking'=>'Product Tracking',
	'monthly_status_of_supplier'=>'Supplier\'s Monthy Status',
	'monthly_status_of_customer'=>'Customer\'s Monthy Status'
];