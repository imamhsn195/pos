<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>@yield('title')</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="{{asset('admin')}}/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="{{asset('admin')}}/vendors/css/vendor.bundle.base.css">
  <link rel="stylesheet" href="{{asset('admin')}}/vendors/css/vendor.bundle.addons.css">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="{{asset('admin')}}/css/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="{{asset('admin')}}/images/favicon.png" />
  @stack('css')
 <style>  
.table th, .table td {
    padding: 10px 15px;
    vertical-align: top;
    border-top: 1px solid #f2f2f2;
}
.row_table_responsive{
  width:100%;
}
.collapsing {
    -webkit-transition: none;
    transition: cubic-bezier(0.445, 0.05, 0.55, 0.95);
    display: none;
}

.navbar.default-layout{
  background: #0575E6;  
  background: -webkit-linear-gradient(to right, #112B8D, #4C7DAE);  
  background: linear-gradient(to right, #112B8D, #4C7DAE); 
  }
  .navbar.default-layout .navbar-menu-wrapper .navbar-nav.navbar-nav-right{
    margin-left: 10px;
  }
  .navbar.default-layout .navbar-brand-wrapper{
    background-color:transparent;
  }
  .navbar.default-layout .navbar-brand-wrapper .navbar-brand:active, .navbar.default-layout .navbar-brand-wrapper .navbar-brand:focus, .navbar.default-layout .navbar-brand-wrapper .navbar-brand:hover {
    color: #fff;
  }
  .main-panel dev{
    min-height: 522px;
  }
   
    @media print {
      .sidebar , .navbar,input,button,a {
        display:none !important;
      }
    }
</style>
</head>

<body>
  <div class="container-scroller">
    
    <!-- partial:partials/_navbar.html -->
    @include('layouts.partials.topmenu')

    <!-- partial -->
    <div class="container-fluid page-body-wrapper">

      <!-- partial:partials/_sidebar.html -->
      @if(!Request::is('sales'))
        @include('layouts.partials.sidebar')
      @endif
      <!-- partial -->
      <div class="main-panel"><br>
        @if (session('status'))
      <div class="alert alert-dismissible alert-success">
          <button type="button" class="close" data-dismiss="alert"><i class="icon-remove-sign"></i></button>
          {{ session('status') }}
      </div>
        @endif
        {{-- for error or warning notification --}}
        @if (session('warning'))
      <div class="alert alert-dismissible alert-danger">
          <button type="button" class="close" data-dismiss="alert"><i class="icon-remove-sign"></i></button>
          {{ session('warning') }}
      </div>
        @endif
        @if ($errors->any())
            @foreach($errors->all() as $error)
              <small class="alert alert-danger">**{{ $error }}</small>         
            @endforeach
        @endif
		  @yield('content')
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
       @include('layouts.partials.footer')
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- plugins:js -->
  <script src="{{asset('admin')}}/vendors/js/vendor.bundle.base.js"></script>
  <script src="{{asset('admin')}}/vendors/js/vendor.bundle.addons.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="{{asset('admin')}}/js/off-canvas.js"></script>
  <script src="{{asset('admin')}}/js/misc.js"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="{{asset('admin')}}/js/dashboard.js"></script>
  <script src="{{asset('admin')}}/js/select2.js"></script>
  <script>
    $(document).ready(function(){
    $(".alert-danger").delay(9000).slideUp(300);
});
  </script>
  
  <script>
    $(document).ready(function(){
    $(".alert-success").delay(5000).slideUp(300);
});
  </script>
  <script>
  $(function() {
    var activeUrl = window.location.origin+window.location.pathname;
	$('.nav li a').each(function(){
	    var $this = $(this);
      if($this.attr('href') !== activeUrl){
            $this.removeClass('active');
        }
	    if($this.attr('href') === activeUrl){
	        $this.addClass('active');
	    }
	})
})
</script>
@stack('js')  
  <!-- End custom js for this page-->
</body>

</html>