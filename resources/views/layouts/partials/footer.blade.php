 <footer class="footer">
     <div class="container-fluid clearfix">
         <span class="float-right text-info"> Copyright ©
             All rights reserved by<a href="https://webtech.app/" target="_blank"> Web Tecnologies BD</a>.
         </span>
     </div>
     <script>
         function lang_change(lan) {
             var route = "{{url('')}}/primary_settings/" + lan;
             $.get(route, function (data) {
                 location.reload();
             });
         }
     </script>
 </footer>