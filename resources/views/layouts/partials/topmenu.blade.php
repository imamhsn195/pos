<nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
          <a class="navbar-brand brand-logo" href="{{url('/')}}" style="padding-left:40px" >
            <!-- <img src="{{asset('upload/logo/logo.png')}}" style="height: 100%;width: 100%;"/> -->
            <b>POS System</b>
          </a>
          <a class="navbar-brand brand-logo-mini" href="index.html">
            <img src="{{asset('admin')}}/images/logo-mini.svg" alt="logo"  />
          </a>
          <button type="button" style="color: white;font-size: 2em;" class="btn btn-link btn-lg" data-toggle="collapse" data-target="#sidebar"><i class="icon mdi mdi-menu"></i></button>
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-center">
        <ul class="navbar-nav navbar-nav-right header-links d-none d-md-flex" style="margin: 0;"> 
         
			  <li class="nav-item dropdown">
            <a class="nav-link count-indicator dropdown-toggle" id="messageDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
            <h5>{{__('top_menu.reports')}}</h5></a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="messageDropdown">
               <a href="{{route('purchases.index')}}" class="dropdown-item mt-2">
                {{__('sidebar.purchase')}}
                </a>
              <a href="{{route('purchase_returns.index')}}" class="dropdown-item mt-2">
                {{__('sidebar.purchase_returns')}}{{-- Purchase Return --}}
                </a>
              <a href="{{route('journals.index')}}" class="dropdown-item mt-2">
                {{__('sidebar.journals')}}{{-- Journal Statement --}}
                </a>
              {{--Only super admin can see the trail balance --}}
              @if(Gate::allows('isSuperadmin'))
              <a href="{{route('trial_balance.index')}}" class="dropdown-item mt-2">
                {{__('sidebar.trial_balance')}}{{-- Trial Balance --}}
                </a>
              @endif
              <a href="{{route('transactions.index')}}" class="dropdown-item mt-2">
                {{__('sidebar.cashflow')}}{{-- Cash Flows --}}
                </a>
            </div>
          </li>

          <li class="nav-item dropdown">
            <a class="nav-link count-indicator dropdown-toggle" data-toggle="dropdown"  id="messageDropdown" href="#"  aria-expanded="false">
              <h5>{{__('top_menu.product_types')}}</h5>
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="messageDropdown">
                <a href="{{route('warranty_product_detail.index')}}" class="dropdown-item mt-2">
                  {{__('top_menu.warrantable')}}<span class="badge badge-danger"> {{ session('Warranty_product_detail')?session('Warranty_product_detail'):"(0)"}}</span>
                </a>
                <a class="dropdown-item mt-2" href="{{route("expire_products.index")}}" >
                  {{__('top_menu.expirable')}}<span class="badge badge-danger">{{ session('e_pro_no')?session('e_pro_no'):"(0)" }}</span>
                </a>
            </div>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link count-indicator" id="messageDropdown" href="{{route('cheque_infos.index')}}"  aria-expanded="false">
              <h5> {{__('top_menu.cheques')}}<span class="badge badge-danger"> {{ session('pending_check')?session('pending_check'):"(0)"}}</span></h5>
            </a>
          </li>
          @can('isAdmin','isManager','isCashier')
          <li class="nav-item dropdown" style="z-index:1">
            <a class="nav-link" href="{{route('sales.index')}}">
              <h5>  <span class="menu-title">{{__('sidebar.sales_point')}}</span></h5>
            </a>
          </li>
          @endcan
        </ul>
        <ul class="navbar-nav navbar-nav-right header-links d-none d-md-flex" style="position: absolute;right: 0;">

          <li class="nav-item dropdown d-none d-xl-inline-block" style="margin-left: 155px;">
              <a class="nav-link dropdown-toggle" id="UserDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
                <h5 class="profile-text">{{__('top_menu.welcome')}},  {{ Auth::user()->name }} !</h5>
                <img class="img-xs rounded-circle" src="{{Auth::user()->avatar=='default/images/default.png'?asset('default/images/default.png'):asset('storage/avatar/'.Auth::user()->avatar)}}" alt="Profile image">
              </a>
              <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">
                {{-- <a class="dropdown-item p-0">
                  <div class="d-flex border-bottom">
                    <div class="py-3 px-4 d-flex align-items-center justify-content-center">
                      <i class="mdi mdi-bookmark-plus-outline mr-0 text-gray"></i>
                    </div>
                    <div class="py-3 px-4 d-flex align-items-center justify-content-center border-left border-right">
                      <i class="mdi mdi-account-outline mr-0 text-gray"></i>
                    </div>
                    <div class="py-3 px-4 d-flex align-items-center justify-content-center">
                      <i class="mdi mdi-alarm-check mr-0 text-gray"></i>
                    </div>
                  </div>
                </a> --}}
                <a href="{{route('profile')}}" class="dropdown-item mt-2">
                  <i class="mdi mdi-account-outline mr-0 text-gray"></i>&nbsp;&nbsp; Manage Accounts
                </a>
                  {{--Only super admin can see the trail balance --}}
                @if(Gate::allows('isSuperadmin'))
                  <a href="{{ route('user.registration') }}" class="dropdown-item mt-2"><i class="mdi mdi-bookmark-plus-outline mr-0 text-gray"></i>&nbsp;&nbsp; Register New User</a>
                @endif
                
                <a class="dropdown-item" href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();">
                           <i class="mdi mdi-logout mr-0 text-gray"></i>&nbsp;&nbsp;
                  {{ __('Logout') }}
                </a>
  
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  @csrf
                </form>
                
              </div>
          </li>
          <li class="nav-item dropdown d-none d-xl-inline-block">
              {{-- ssssssssssss --}}
              <a class="nav-link" onclick="lang_change(0)" style="cursor:pointer;padding:16px 5px 16px 25px">
                <img title="English" class="img-xs rounded-circle" src="{{asset('admin/images/flags/eng.png')}}" alt="EN">
              </a>
              
              {{-- ssssssssssss --}}
           </li>
           {{-- <h5>|</h5> --}}
           <li class="nav-item dropdown d-none d-xl-inline-block">
              {{-- ssssssssssss --}}
              <a class="nav-link" onclick="lang_change(1)"  style="cursor:pointer;padding:16px 25px 16px 5px">
                <img title="{{__('sidebar.bangla')}}" class="img-xs rounded-circle" src="{{asset('admin/images/flags/bd.png')}}" alt="{{__('sidebar.bangla')}}">
                {{-- <h5>{{__('sidebar.bangla')}}</h5> --}}
              </a> 
              
              {{-- ssssssssssss --}}
           </li>
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
          <span class="mdi mdi-menu"></span>
        </button>
      </div>
    </nav>