<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        
        {{-- <li class="nav-item">
            <a class="nav-link" href="{{url('/')}}">
                <i class="menu-icon mdi mdi-television"></i>
                <span class="menu-title">{{__('sidebar.dashboard')}}</span>
            </a>
        </li> --}}

        <!-- Product menu start here -->
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-products" aria-expanded="false" aria-controls="ui-basic">
                <i class="menu-icon mdi mdi-content-copy"></i>
            <span class="menu-title">{{__('sidebar.products')}}</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-products">
                <ul class="nav flex-column sub-menu">
                    @if( Gate::allows('isAdmin') || Gate::allows('isSuperadmin') || Gate::allows('isManager') ||
                    Gate::allows('isCashier'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('products.index')}}">{{__('sidebar.registered_products')}}</a>
                    </li>
                    @endif
                    @if( Gate::allows('isAdmin') || Gate::allows('isSuperadmin') || Gate::allows('isManager') ||
                    Gate::allows('isCashier'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('barcode')}}">{{__('sidebar.barcode')}}</a>
                    </li>
                    @endif
                    @if( Gate::allows('isAdmin') || Gate::allows('isSuperadmin') || Gate::allows('isManager') ||
                    Gate::allows('isCashier'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('bundle_product.index')}}">{{__('sidebar.bundle_product')}}</a>
                    </li>
                    @endif
                    @if( Gate::allows('isAdmin') || Gate::allows('isSuperadmin') || Gate::allows('isManager') ||
                    Gate::allows('isCashier'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('categories.index')}}">{{__('sidebar.categories')}}</a>
                    </li>
                    @endif
                    @if( Gate::allows('isAdmin') || Gate::allows('isSuperadmin') || Gate::allows('isManager') ||
                    Gate::allows('isCashier'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('warranty_type.index')}}">{{__('sidebar.warranty')}}</a>
                    </li>
                    @endif
                    @if( Gate::allows('isAdmin') || Gate::allows('isSuperadmin') || Gate::allows('isManager') ||
                    Gate::allows('isCashier'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('attributes.index')}}">{{__('sidebar.attributes')}}</a>
                    </li>
                    @endif
                    @if( Gate::allows('isAdmin') || Gate::allows('isSuperadmin') || Gate::allows('isManager') ||
                    Gate::allows('isCashier'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('attribute_sets.index')}}">{{__('sidebar.attribute_sets')}}</a>
                    </li>
                    @endif
                    @if( Gate::allows('isAdmin') || Gate::allows('isSuperadmin') || Gate::allows('isManager') ||
                    Gate::allows('isCashier'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('brand.index')}}">{{__('sidebar.brands')}}</a>
                    </li>
                    @endif
                </ul>
            </div>
        </li>
        <!-- Product menu end here -->
        <!--supplier menu starts here -->
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-contact" aria-expanded="false" aria-controls="ui-basic">
                <i class="menu-icon mdi mdi-content-copy"></i>
                <span class="menu-title">{{__('sidebar.contacts')}}</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-contact">
                <ul class="nav flex-column sub-menu">
                    @if( Gate::allows('isAdmin') || Gate::allows('isSuperadmin') || Gate::allows('isManager') ||
                    Gate::allows('isCashier'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('suppliers.index')}}">
                            <i class="menu-icon mdi mdi-backup-restore"></i>
                            <span class="menu-title">{{__('sidebar.suppliers')}}</span>
                        </a>
                    </li>
                    @endif
                    <!--supplier menu ends here -->
                    <!-- customer menu start here -->
                    @if( Gate::allows('isAdmin') || Gate::allows('isSuperadmin') || Gate::allows('isManager') ||
                    Gate::allows('isCashier'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('customers.index')}}">
                            <i class="menu-icon mdi mdi-backup-restore"></i>
                            <span class="menu-title">{{__('sidebar.customers')}}</span>
                        </a>
                    </li>
                    @endif
                    <!--customer menu ends here -->
                    <!-- Customers Group menu start here -->
                    @if( Gate::allows('isAdmin') || Gate::allows('isSuperadmin') || Gate::allows('isManager') ||
                    Gate::allows('isCashier'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('customer_groups.index')}}">
                            <i class="menu-icon mdi mdi-backup-restore"></i>
                            <span class="menu-title">{{__('sidebar.customers_group')}}</span>
                        </a>
                    </li>
                    @endif
                    <!--Customers Group menu ends here -->
                </ul>
            </div>
        </li>
        <!--Administration menu starts here -->
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-admin" aria-expanded="ture" aria-controls="ui-basic">
                <i class="menu-icon mdi mdi-content-copy"></i>
                <span class="menu-title">{{__('sidebar.administration')}}</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-admin">
                <ul class="nav flex-column sub-menu">
                    @if( Gate::allows('isAdmin') || Gate::allows('isSuperadmin') || Gate::allows('isManager') ||
                    Gate::allows('isCashier'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('vat.index')}}">
                            <i class="menu-icon mdi mdi-backup-restore"></i>
                            <span class="menu-title">{{__('sidebar.manage_vat')}}</span>
                        </a>
                    </li>
                    @endif

                    @if( Gate::allows('isAdmin') || Gate::allows('isSuperadmin') || Gate::allows('isManager') ||
                    Gate::allows('isCashier'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('discount_offers.index')}}">
                            <i class="menu-icon mdi mdi-backup-restore"></i>
                            <span class="menu-title">{{__('sidebar.manage_discount')}}</span>
                        </a>
                    </li>
                    @endif
                    
                    @if(Gate::allows('isSuperadmin'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('primary_settings.index')}}">
                            <i class="menu-icon mdi mdi-backup-restore"></i>
                            <span class="menu-title">{{__('sidebar.primary_settings')}}</span>
                        </a>
                    </li>
                    @endif
                     @if(Gate::allows('isSuperadmin')|| Gate::allows('isAdmin'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('invoice_setting')}}">
                            <i class="menu-icon mdi mdi-backup-restore"></i>
                            <span class="menu-title">{{__('sidebar.invoice_settings')}}</span>
                        </a>
                    </li>
                    @endif
                    <!-- customer menu start here -->
                    @if( Gate::allows('isAdmin') || Gate::allows('isSuperadmin') || Gate::allows('isManager') ||
                    Gate::allows('isCashier'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('branches.index')}}">
                            <i class="menu-icon mdi mdi-backup-restore"></i>
                            <span class="menu-title">{{__('sidebar.manage_branches')}}</span>
                        </a>
                    </li>
                    @endif
                    <!--customer menu ends here -->
                    <!-- Customers Group menu start here -->
                    @if( Gate::allows('isAdmin') || Gate::allows('isSuperadmin') || Gate::allows('isManager') ||
                    Gate::allows('isCashier'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('payment_method.index')}}">
                            <i class="menu-icon mdi mdi-backup-restore"></i>
                            <span class="menu-title">{{__('sidebar.payment_method')}}</span>
                        </a>
                    </li>
                    @endif
                    @if( Gate::allows('isAdmin') || Gate::allows('isSuperadmin') || Gate::allows('isManager'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('record_expense')}}">
                            <i class="menu-icon mdi mdi-backup-restore"></i>
                            <span class="menu-title">{{__('sidebar.record_expense')}}</span>
                        </a>
                    </li>
                    @endif
                    <!--Customers Group menu ends here -->
                    <!-- Transaction menu start here -->
                    @if( Gate::allows('isAdmin') || Gate::allows('isSuperadmin') || Gate::allows('isManager') ||
                    Gate::allows('isCashier'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('transactions.create')}}">
                            <i class="menu-icon mdi mdi-backup-restore"></i>
                            <span class="menu-title">{{__('sidebar.transactions')}}</span>
                        </a>
                    </li>
                    @endif
                    <!--Transaction menu ends here -->
                    <!-- Purchase Return menu start here -->
                    
                    @if( Gate::allows('isAdmin') || Gate::allows('isSuperadmin') || Gate::allows('isManager') ||
                    Gate::allows('isCashier'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('purchase_returns.create')}}">
                            <i class="menu-icon mdi mdi-backup-restore"></i>
                            <span class="menu-title">{{__('sidebar.purchase_returns')}}</span>
                        </a>
                    </li>
                    @endif
                    <!-- Sales Return menu start here -->
                    @if( Gate::allows('isAdmin')  || Gate::allows('isManager') ||
                    Gate::allows('isCashier'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('sales_return.create')}}">
                            <i class="menu-icon mdi mdi-backup-restore"></i>
                            <span class="menu-title">{{__('sidebar.sales_return')}}</span>
                        </a>
                    </li>
                    @endif
                    <!--Transaction menu ends here -->
                </ul>
            </div>
        </li>
        @can('isAdmin','isManager','isCashier')
        <li class="nav-item">
            <a class="nav-link" href="{{route('purchases.create')}}">
                <i class="menu-icon mdi mdi-backup-restore"></i>
                <span class="menu-title">{{__('sidebar.purchase')}}</span>
            </a>
        </li>
        @endcan
        @can('isAdmin','isManager','isCashier')
        <li class="nav-item">
            <a class="nav-link" href="{{route('sales.index')}}">
                <i class="menu-icon mdi mdi-backup-restore"></i>
                <span class="menu-title">{{__('sidebar.sales_point')}}</span>
            </a>
        </li>
        @endcan
        <!-- Payment method menu end here -->
        <!-- Reports menu start here -->
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-report" aria-expanded="false" aria-controls="ui-basic">
                <i class="menu-icon mdi mdi-content-copy"></i>
                <span class="menu-title">{{__('sidebar.reports')}}</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-report">
                <ul class="nav flex-column sub-menu">
                    @if( Gate::allows('isAdmin') || Gate::allows('isSuperadmin') || Gate::allows('isManager') ||
                    Gate::allows('isCashier'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('inventories.index')}}">{{__('sidebar.inventories')}}</a>
                    </li>
                    @endif
                    @if( Gate::allows('isAdmin') || Gate::allows('isSuperadmin') || Gate::allows('isManager') ||
                    Gate::allows('isCashier'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('purchases.index')}}">{{__('sidebar.purchase')}} {{__('sidebar.reports')}}</a>
                    </li>
                    @endif
                    @if( Gate::allows('isAdmin') || Gate::allows('isSuperadmin') || Gate::allows('isManager') ||
                    Gate::allows('isCashier'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('purchase_returns.index')}}">{{__('sidebar.purchase_returns')}}</a>
                    </li>
                    @endif
                    @if( Gate::allows('isAdmin') || Gate::allows('isSuperadmin') || Gate::allows('isManager') ||
                    Gate::allows('isCashier'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('payment_show')}}">{{__('sidebar.payment_show')}}</a>
                    </li>
                    @endif
                    @if( Gate::allows('isAdmin') || Gate::allows('isSuperadmin') || Gate::allows('isManager') ||
                    Gate::allows('isCashier'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('expense_show')}}">{{__('sidebar.expense_report')}}</a>
                    </li>
                    @endif
                    @if( Gate::allows('isAdmin') || Gate::allows('isSuperadmin') || Gate::allows('isManager') ||
                    Gate::allows('isCashier'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('sales_report')}}">{{__('sidebar.sales')}}</a>
                    </li>
                    @endif

                    @if( Gate::allows('isAdmin') || Gate::allows('isSuperadmin') || Gate::allows('isManager') ||
                    Gate::allows('isCashier'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('sales_return.index')}}">{{__('sidebar.sales_return')}}</a>
                    </li>
                    @endif

                    @if( Gate::allows('isAdmin') || Gate::allows('isSuperadmin') || Gate::allows('isManager') ||
                    Gate::allows('isCashier'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('journals.index')}}">{{__('sidebar.journals')}}</a>
                    </li>
                    @endif
                    @if( Gate::allows('isAdmin') || Gate::allows('isSuperadmin') || Gate::allows('isManager') ||
                    Gate::allows('isCashier'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('transactions.index')}}">{{__('sidebar.cashflow')}}</a>
                    </li>
                    @endif
                    @if( Gate::allows('isSuperadmin'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('trial_balance.index')}}">{{__('sidebar.trial_balance')}}</a>
                    </li>
                    @endif
                    @if( Gate::allows('isAdmin') || Gate::allows('isSuperadmin') || Gate::allows('isManager') ||
                    Gate::allows('isCashier'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('income_report')}}">{{__('sidebar.income_statement')}}</a>
                    </li>
                    @endif
                    @if( Gate::allows('isAdmin') || Gate::allows('isSuperadmin') || Gate::allows('isManager') ||
                    Gate::allows('isCashier'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('product_tracking')}}">{{__('sidebar.product_tracking')}}</a>
                    </li>
                    @endif
                    @if( Gate::allows('isAdmin') || Gate::allows('isSuperadmin') || Gate::allows('isManager'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('monthly_status_of_supplier')}}">{{__('sidebar.monthly_status_of_supplier')}}</a>
                    </li>
                    @endif
                    @if( Gate::allows('isAdmin') || Gate::allows('isSuperadmin') || Gate::allows('isManager'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('monthly_status_of_customer')}}">{{__('sidebar.monthly_status_of_customer')}}</a>
                    </li>
                    @endif
                </ul>
            </div>
        </li>
        <!-- Reports menu Ends here -->
    </ul>
</nav>