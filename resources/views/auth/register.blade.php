@extends('layouts.admin')

@section('content')
<div class="container-scroller">
        <div class="container-fluid page-body-wrapper full-page-wrapper auth-page">
          <div class="content-wrapper d-flex align-items-center auth register-bg-1 theme-one">
            <div class="row w-100">
              <div class="col-lg-4 mx-auto">
                <div class="auto-form-wrapper">
                <h2 class="text-center mb-4">{{ __('Register') }}</h2>
                    <form method="POST" action="{{ route('user.registration') }}" aria-label="{{ __('Register') }}">
                        @csrf
                        <div class="form-group">
                            <label for="name" class="label">{{ __('Name') }}</label>
                            <div class="input-group">
                                    <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
                                <div class="input-group-append">
                                <span class="input-group-text">
                                    <i class="mdi mdi-check-circle-outline"></i>
                                </span>
                                </div>
                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    <div class="form-group">
                        <label for="email" class="label">{{ __('E-Mail Address') }}</label>
                        <div class="input-group">
                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                        <div class="input-group-append">
                            <span class="input-group-text">
                            <i class="mdi mdi-check-circle-outline"></i>
                            </span>
                        </div>
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="user_type" class="label">{{ __('User Type') }}</label>
                        <div class="input-group">
                            <select id="user_type" class="form-control{{ $errors->has('user_type') ? ' is-invalid' : '' }}" name="user_type" value="{{ old('user_type') }}" required>
                                <option value="">Select User Type</option>
                                <option value="admin">Admin</option>
                                <option value="branch_manager">Branch Manager</option>
                                <option value="cashier">Cashier</option>
                            </select>
                            @if ($errors->has('user_type'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('user_type') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="branch_id" class="label">{{ __('Branch Name') }}</label>
                        <div class="input-group">
                                <select id="branch_id" class="form-control{{ $errors->has('branch_id') ? ' is-invalid' : '' }}" name="branch_id" value="{{ old('branch_id') }}" required>
                                    <option value="">Select Branch Or Warehouse</option>
                                    @forelse($branches as $branch)
                                        <option value="{{$branch->id}}">{{$branch->name}}</option>
                                    @empty
                                        <option value="">{{__('There is no Branch')}}</option>
                                    @endforelse
                                </select>
                                @if ($errors->has('branch_id'))
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('branch_id') }}</strong>
                                    </span>
                                @endif
                        </div>
                    </div>
                    <div class="form-group">
                      <label for="password" class="label">{{ __('Password') }}</label>
                      <div class="input-group">
                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password"  placeholder="*********" required>
                        <div class="input-group-append">
                          <span class="input-group-text">
                            <i class="mdi mdi-check-circle-outline"></i>
                          </span>
                          @if ($errors->has('password'))
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $errors->first('password') }}</strong>
                          </span>
                         @endif
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                            <label for="password-confirm" class="label">{{ __('Confirm Password') }}</label>
                      <div class="input-group">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                        <div class="input-group-append" placeholder="*********">
                          <span class="input-group-text">
                            <i class="mdi mdi-check-circle-outline"></i>
                          </span>
                        </div>
                      </div>
                    </div>
                    {{--  <div class="form-group d-flex justify-content-center">
                      <div class="form-check form-check-flat mt-0">
                        <label class="form-check-label">
                          <input type="checkbox" class="form-check-input" checked=""> I agree to the terms
                        <i class="input-helper"></i></label>
                      </div>
                    </div>  --}}
                    <div class="form-group">
                      <button type="submit" class="btn btn-primary submit-btn btn-block"> {{ __('Register') }}</button>
                    </div>
                     {{-- <div class="text-block text-center my-3">
                      <span class="text-small font-weight-semibold">Already have and account ?</span>
                    <a href="{{route('login')}}" class="text-black text-small">Login</a>
                    </div>  --}}
                  </form>
                </div>
              </div>
            </div>
          </div>
          <!-- content-wrapper ends -->
        </div>
        <!-- page-body-wrapper ends -->
      </div>

{{-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

						<div class="form-group row">
                            <label for="user_type" class="col-md-4 col-form-label text-md-right">{{ __('User Type') }}</label>

                            <div class="col-md-6">
                                <select id="user_type" class="form-control{{ $errors->has('user_type') ? ' is-invalid' : '' }}" name="user_type" value="{{ old('user_type') }}" required>
									<option value="">Select User Type</option>
									<option value="admin">Admin</option>
									<option value="branch_manager">Branch Manager</option>
									<option value="cashier">Cashier</option>
								</select>
                                @if ($errors->has('user_type'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('user_type') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="branch_id" class="col-md-4 col-form-label text-md-right">{{ __('Branch Name') }}</label>

                            <div class="col-md-6">
                                <select id="branch_id" class="form-control{{ $errors->has('branch_id') ? ' is-invalid' : '' }}" name="branch_id" value="{{ old('branch_id') }}" required>
									<option value="">Select Branch Or Warehouse</option>
                                    @forelse($branches as $branch)
                                        <option value="{{$branch->id}}">{{$branch->name}}</option>
                                    @empty
                                        <option value="">{{__('There is no Branch')}}</option>
                                    @endforelse
								</select>
                                @if ($errors->has('branch_id'))
                                    <span class="invalid-feedback" role="alert">
                                         <strong>{{ $errors->first('branch_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> --}}
@endsection
