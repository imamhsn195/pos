@extends('layouts.admin')

@section('title','Inventory')

@section('content')

<div class="row">
    <div class="col-md-12 d-flex align-items-stretch grid-margin">
        <div class="row flex-grow">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Inventory Information</h4>
                        
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>SL</th>
                                        <th>Product Name</th>
                                        <th>Invoice No</th>
                                        <th>Purchase Price</th>
                                        <th>Stock Quantity</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i =1; ?>
                                    @forelse($branch->inventories as $bnch)
                                    <tr>
                                        <td>{{$i++}}</td>
                                        <td>{{$bnch->product->product_sku}}</td>
                                        <td>{{$bnch->purchase->invoice_no}}</td>
                                        <td>{{$bnch->purchase->purchase_unit_price}}</td>
                                        <td>{{$bnch->stock_quantity}}</td>
                                        
                                        
                                    </tr>
                                    @empty
                                    <tr>
                                        <td colspan="8">There is no records available</td>
                                    </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection