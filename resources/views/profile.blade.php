@extends('layouts.admin')
@section('title','profile')

@section('content')
<div class="row">
<div class="col-md-1"></div>
<div class="col-md-10  grid-margin stretch-card">
<div class="card">
<div class="card-body">
<img src="{{asset('storage/avatar/'.$employee->avatar)}}" alt="" style="width:150px; height:150px; margin-right:25px; float:right; border-radius:50%"><hr />
<h1>{{$employee->name}}'s Profile</h1><hr />
<form action="{{route('profile.update')}}" enctype="multipart/form-data" method="post">
    @csrf
    <div class="form-group">
    <label for="">Upload Image</label>
    <input type="file" name="avatar" class="form-control file-upload-info">
    </div>
    
    <div class="form-group">
        <label for="name">Name</label>
        <input type="text" class="form-control" value="{{$employee->name}}" name="name">
    </div>
    <div class="form-group">
        <label for="email">Email</label>
        <input type="text" class="form-control" value="{{$employee->email}}" name="email">
    </div>
    @if(Auth::user()->branch_id)
    <div class="form-group">
        <label for="branch">Branch</label>
        <input type="text" class="form-control" value="{{$employee->branch->name}}" name="branch" readonly>
    </div>
    @endif
    <div class="form-group">
        <label for="designation">Designation</label>
        <input type="text" class="form-control" value="{{$employee->user_type}}" name="branch" readonly>
    </div>
    <div class="form-group">
        <label for="old_password">Old Password</label>
        <input type="password" class="form-control"  name="old_password">
    </div>
    <div class="form-group">
        <label for="password">New Password</label>
        <input type="password" class="form-control"  name="password">
    </div>
    <div class="form-group">
        <label for="c_password">Confirm Password</label>
        <input type="password" class="form-control"  name="password_confirmation">
    </div>
    <input type="submit" value="Update" class="btn btn-success btn-sm">
</form>
</div>
</div>
</div>
</div>
@endsection