@extends('layouts.admin')

@section('content')

<div class="row">
<div class="col-md-12 d-flex align-items-stretch grid-margin">
  <div class="row flex-grow row_table_responsive">
	<div class="col-md-12">
	  <div class="card">
		<div class="card-body">
		  <h4 class="card-title">Payment Method Information</h4>
		  <div class="table-responsive">
                    <table class="table table-hover">
                      <thead>
                        <tr>
                          <th>ID</th>
                          <th>Account Title</th>
                          <th>Debit</th>
                          <th>Cretit</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($accounts as $ac)
                        <tr>
                           <td>ID</td>
                           <td>{{$ac->account->account_head}}</td>
                           <td>
                            @foreach($debit as $d)
                              @if($d->accounts_id == $ac->accounts_id)
                                {{$d->Total}}
                              @endif
                            @endforeach
                            </td>
                           <td>
                             @foreach($credit as $c)
                              @if($c->accounts_id == $ac->accounts_id)
                                {{$c->Total}}
                              @endif
                            @endforeach
                           </td>
                        </tr>
                       @endforeach
                      </tbody>
                    </table>
                  </div>
		</div>
	  </div>
	</div>
  </div>
</div>

</div>

@endsection