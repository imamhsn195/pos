@extends('layouts.admin')

@section('content')

<div class="row">
<div class="col-md-12 d-flex align-items-stretch grid-margin">
  <div class="row flex-grow row_table_responsive">
	<div class="col-md-12">
	  <div class="card">
		<div class="card-body">
        <h2 class="text-center">Cash Flow Statement</h2>
        @if(Auth::user()->user_type=="superadmin")
        <h4 class="text-center">Showing All Branches</h4>
        @else
        <h4 class="text-center">Showing {{Auth::user()->branch->name}} Branch</h4>
        @endif
        <p class="card-description text-center">
            For the month of {{request('month')? date('F-Y', strtotime(request('month'))):date('F-Y') }}
            <form method="get" action="{{url('').'/transactions'}}">
                <div class="input-group col-md-6 float-right">
                    <label for="example-search-input2">Month :</label>
                    <input class="form-control py-2" name="month" type="month" id="example-search-input2" value="{{request('month')? date('Y-m', strtotime(request('month'))):date('Y-m') }}">
                    <span class="input-group-append">
                        <button class="btn btn-primary" type="submit">
                            <i class="mdi mdi-magnify"></i>
                        </button>
                        {{-- <a href="{{route('record_expense')}}" class="btn btn-primary">Add New</a > --}}
                    </span>
                </div>
            </form>
      <div class="table-responsive">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>Transaction ID</th>
                          <th>Date</th>
                          <th>Account Title</th>
                          <th>description</th>
                          <th>Debit</th>
                          <th>Credit</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php
            
                        $amount=0;
                        $damount=0;
                        //$tspan=2;
                        ?>
					  @forelse($cash_flows as $jr)
                        <tr>
                        
                       
                            <td>{{$jr->transaction_id}}</td>
                           <?php $time=strtotime($jr->created_at);
                            $date=date("d-M-Y",$time);?>
                            <td>{{$date}}</td>
                          
                            
                            <td>{{$jr->transaction->event_type}}</td>   
                            <td>{{$jr->description}}</td>                          
                       
                         
                          @if($jr->journal_type==0)
                          <td>{{number_format($jr->amount,2)}}</td>
                         <?php $damount=$damount+$jr->amount;?>
                          @else
                          <td></td>
                          @endif
                          @if($jr->journal_type==1)
                          <td>{{number_format($jr->amount,2)}}</td>
                          <?php $amount=$amount+$jr->amount;?>
                          @else
                          <td></td>
                          @endif
                          
                        </tr>
					@empty
						<tr>
							<td colspan="8">There is no records available</td>
						</tr>
					@endforelse
                      </tbody>
                      <tfoot>
                        <th colspan="4" class="text-right">Total Transaction :</th>
                        <th>
                            @if($damount>$amount)
                            {{number_format($damount-$amount,2)}}
                            @endif</th>
                        <th>
                                @if($amount>$damount)
                                {{number_format($amount-$damount,2)}} 
                                @endif</th>
                       
                      </tfoot>
                    </table>
                  </div>
		</div>
	  </div>
	</div>
  </div>
</div>

</div>

@endsection