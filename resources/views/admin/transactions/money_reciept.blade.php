<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
    <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
    <meta name="author" content="cgit" />

    <title>Printing - Money Receipt</title>
</head>

<body onload="window.print()" onclick="window.close()">
    @php 
        $f = new \NumberFormatter("en", \NumberFormatter::SPELLOUT);//write in php.init ;extension=php_intl.dll
    @endphp
    @if($invoice_setting->logo_enable)
    <h2 align="{{$invoice_setting->logo_position?$invoice_setting->logo_position:'center'}}"> <img src="{{$invoice_setting->invoice_logo?asset($invoice_setting->invoice_logo):asset($company->company_logo)}}" id="imageView" style="width:250px;"/></h2>	
    @endif
    <h4 align="center" style="-webkit-margin-after: 0.01em;-webkit-margin-before: 0.1em;">Money Receipt</h4>
    <h2 align="center" style="-webkit-margin-after: 0em; -webkit-margin-before: 0.2em;">{{$company->company_name}}</h2>
    <h3 align="center" style="-webkit-margin-after: 0em; -webkit-margin-before: 0.2em;">{{$transaction->branch->name}} Branch</h3>
    <h4 align="center" style="-webkit-margin-before: 0.33em;">{{$transaction->branch->address!=null?$transaction->branch->address->email:""}}
        <br />{{$transaction->branch->address!=null?$transaction->branch->address->full_address:''}}
        {{-- <br />Dealer:Kiam,Nasir,Monno, Farr,Sharif,Kwality,Olila, Winner.
        <br />Rahaman Plaza,Elenga Bus Station,Tangail
        <br />Mob:01975553300,01730916292 --}}
    </h4>

    <table style="border-style: ridge;" align="center" width="500px">
        <tbody>
            <tr>
                <td>
                    <table width="495px">
                        <tr>
                            <td align="left">Receipt No. {{$transaction->id}}</td>
                            <td align="right">Date: {{date("d-F-Y",strtotime($transaction->created_at))}}</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table align="left">
                        <tbody>
                            <tr>
                                <td width="120px" align="left"><strong>{{$transaction->journals->first()->related_party_type=='supplier'?'Paid To':'Recieved From'}}</strong></td>
                                <td>{{$transaction->journals->first()->related_party_type=='supplier'?$transaction->journals->first()->supplier->supplier_name:$transaction->journals->first()->customer->customer_name}}</td>
                            </tr>
                            <tr>
                                <td align="left"><strong>Amount</strong></td>
                                @php
            
                                $saleAmt=0;
                                $expenseAmt=0;
                                $paymentMethod='';
                                @endphp
                            @foreach($transaction->journals as $jr)
                                @if($jr->related_party_type=='supplier' && $jr->journal_type==0)
                                @php 
                                $expenseAmt=$expenseAmt+$jr->amount;
                                @endphp
                                @endif
                                @if($jr->related_party_type=='customer' && $jr->journal_type==1)
                                @php 
                                $expenseAmt=$expenseAmt+$jr->amount;
                                @endphp
                                @endif            
                            @endforeach
                                <td align="right" style="border-style: solid;">{{number_format($expenseAmt,2)}}/=</td>
                            </tr>
                            <tr>
                                <td align="left"><strong>In Words</strong></td>
                                <td>{{ucwords($f->format($expenseAmt).' Taka Only')}}</td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tbody>
                            <tr>
                                <td width="120px"><strong>{{$transaction->journals->first()->related_party_type=='supplier'?'Paid Via':'Recieved Via'}}</strong>  </td><td> {{$transaction->journals->first()->paymentmethod->method_type?'Bank':'Cash'}}</td>
                            </tr>
                            {{-- <tr>
                                <td><strong>Cheque No.</strong> - 11180903002370</td>
                            </tr> --}}
                            <tr>
                                <td align="left"><strong>In {{$transaction->journals->first()->paymentmethod->method_type?'Bank':'Cash'}}</strong>  </td><td> {{$transaction->journals->first()->paymentmethod->method_title}}</td>
                            </tr>
                            {{-- <tr>Cheque Done.</tr> --}}
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="495px">
                        <tbody>
                            @if($transaction->representative!=null)
                            <tr>
                                <td width="100px"><strong>Represented By</strong></td>
                                <td align="left" width="300px">{{$transaction->representative->name}}</td>
                            </tr>
                            @endif
                            <tr>
                                <td width="100px"><strong>{{$transaction->journals->first()->related_party_type=='supplier'?'Paid By':'Recieved By'}}</strong></td>
                                <td align="left" width="300px">{{$transaction->user->name}} ({{$transaction->user->user_type}})</td>
                            </tr>
                            <tr height="30px" style="vertical-align: bottom;">
                                <td width="100px">....................</td>
                                <td align="right" width="300px">........................</td>
                            </tr>
                            <tr>
                                <td width="100px">Payee Sign</td>
                                <td align="right" width="300px">Receiver Sign</td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <h4 align="center">Powered By www.cgit.pro</h4>



</body>

</html>