@extends('layouts.admin') 
@section('content')

<div class="row">
  <div class="col-md-6 d-flex align-items-stretch grid-margin">
    <div class="row flex-grow">
      <div class="col-12">
        <div class="card">
          <div class="card-body">
          <form action="{{route('transactions.store')}}" method="post">
            @csrf
            @if(Gate::allows('isSuperadmin'))
            <div class="form-group">
                <label for="branch_id">Branch</label>
                <select required class="form-control" id="branch_id" name="branch_id">
                    <option value="">Select branch</option>
                    @forelse($branches as $bp)
                    <option value="{{$bp->id}}">{{$bp->name}}</option>
                    @empty
                    <option>{{'There is no branch'}}</option>
                    @endforelse
                </select>
            </div>
            @else
            <input type="hidden" name="branch_id" id="branch_id" value="{{ Auth::user()->branch_id }}">
            @endif

            
            
            
            <div class="form-group">
                {{-- <label for="transaction_type">Transaction Type</label> --}}
                <select required class="form-control" id="transaction_type" name="transaction_type" onchange="get_form_element()">
                 <option value="0" selected>Pay To Supplier</option>
                 <option value="1">Receive From Customer</option>
               </select>
            </div>




            <div id="payable_field" style="display:block">   
                <div class="form-group">
                    <label for="supplier_id">Supplier</label>
                    <select  class="form-control" id="supplier_id" name="supplier_id" onchange="getRelatedPartyInfo(this.value)">
                            <option value="">Select Supplier</option>
                        @forelse($suppliers as $sp)
                            <option value="{{$sp->id}}">{{$sp->supplier_name}}</option>
                        @empty
                            <option>{{'There is no supplier'}}</option>
                        @endforelse
                    </select>
                </div>
            </div>
            
            <div id="receivable_field" style="display:none">   
                <div class="form-group">
                    <label for="customer_id">Customer</label>
                    <select  class="form-control" id="customer_id" name="customer_id" onchange="getRelatedPartyInfo(this.value)">
                            <option value="">Select Customer</option>
                        @forelse($customers as $customer)
                            <option value="{{$customer->id}}">{{$customer->customer_name}}</option>
                        @empty
                            <option>{{'There is no Customer'}}</option>
                        @endforelse
                    </select>
                </div>
            </div>
            
            <div class="form-group">
                <label for="payment_method_id">Payment Method</label>
                <select required class="form-control" id="payment_method_id" name="payment_method_id"
                    onchange="addpaidvalue()">
                    <option value="">Select Payment Method</option>
                    @foreach($payment_methods as $pm)
                    {{-- gggg --}}
                    @php
                    $cashIn = 0; 
                    $cashOut = 0;
                    $payment_method =  $pm->method_type==1?10:2;
                    @endphp
                
                @foreach ($pm->journals as $js)
                    @if ($js->journal_type == 0 && $js->accounts_id == $payment_method)
                       @php
                           $cashIn = $cashIn + $js->amount;
                       @endphp
                    @endif
                @endforeach
               
                @foreach ($pm->journals as $js)
                    @if ($js->journal_type == 1 && $js->accounts_id == $payment_method)
                       @php
                           $cashOut = $cashOut + $js->amount;
                       @endphp
                    @endif
                @endforeach
               
                    {{-- hhhhh --}}
                    <option value="{{$pm->id." | ".$pm->method_type }}" @if($pm->method_type==0 && ($cashIn-$cashOut)<=0){{"disabled"}} class="text-danger disable_option" @endif>{{$pm->method_title}} &nbsp;&nbsp;&nbsp;&nbsp; balance:  {{$cashIn-$cashOut}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group" id="payment_tk">
        
            </div>
            <div class="form-group" id="date_field">
        
            </div>
            <div class="form-group">
                <label for="discount">Discount</label>
                <input type="number" class="form-control" name="discount" id="discount">
            </div>
            <div class="form-group">
                <label for="representative">Representative 
                    {{-- <span class="text-danger">(Name of represented person)</span> --}}
                </label>
                <input type="text" class="form-control" name="representative" id="representative">
            </div>
            <div class="form-group" id="pro_details"></div> 
               
          <button type="submit" class="btn btn-success mr-2">Submit</button>
          <a href="{{route('journals.index')}}" class="btn btn-danger">Cancel</a>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
</div>


<script>

function get_form_element(){
    var ttype=document.getElementById('transaction_type').value;
    if(ttype==1){
        $('#payable_field').css('display','none');
        $('#receivable_field').css('display','block');
        $("#customer_id").select2( {
            placeholder: "Select Customer",
            allowClear: true
        } );
        $('.disable_option').each(function(){
            $(this).attr('disabled',false);
        });
    }else if(ttype==0){
        $('#customer_id').val(null);
        $('#payable_field').css('display','block');
        $('#receivable_field').css('display','none');
        $("#supplier_id").select2( {
            placeholder: "Select Supplier",
            allowClear: true
        } );
        $('.disable_option').each(function(){
            $(this).attr('disabled',true);
        });
    }
}
function getRelatedPartyInfo(id){
    var ttype=document.getElementById('transaction_type').value;
    var route = "{{url('')}}/transaction_status_show/"+id+'|'+ttype;
    var returndata="";
        $.get(route, function(data) {
            console.log(data);
            if(ttype==1){
                var url_route="{{url('')}}/customers/"+id;
                returndata="<a href='"+url_route+"' title='Click To Get Details' class='btn btn-block btn-primary'>Receivable Amount : <b> "+(data.split('|')[0]-data.split('|')[1]).toFixed(2)+"<b></a>";
                $('#pro_details').empty();
                $('#pro_details').html(returndata);
            }else if(ttype==0){
                var url_route="{{url('')}}/suppliers/"+id;
                returndata="<a href='"+url_route+"' title='Click To Get Details' class='btn btn-block btn-danger'>Payable Amount : <b> "+(data.split('|')[1]-data.split('|')[0]).toFixed(2)+"<b></a>";
                $('#pro_details').empty();
                $('#pro_details').html(returndata);

            }
        });
}

//input field for paid Amount
function addpaidvalue(){
    
    var payment_method_id = document.getElementById("payment_method_id").value;
    var customer_id = document.getElementById("customer_id").value;
    $('#payment_tk').empty(paid_amount);
    payment_method_id=payment_method_id.split(" | ");
    console.log(payment_method_id[0]);
    if(payment_method_id[0]){
        var paid_amount="<label for='paid_amount'>Amount</label>";
        paid_amount+="<input type='number' class='form-control' name='paid_amount' id='paid_amount' step='.01'>";
        if(payment_method_id[1]==1){
        paid_amount+="<label for='check_no'>Check No</label><input type='text' class='form-control' name='check_no' id='check_no' onblur='check_disposal_field()'>";
        }
        $('#payment_tk').prepend(paid_amount);
    }
 }
//extra field for cheque disposal date 
function check_disposal_field(){
    var payment_method_id = document.getElementById("payment_method_id").value;
    $('#date_field').empty(paid_amount);
    payment_method_id=payment_method_id.split(" | ");
    if(payment_method_id[1]==1){
        var paid_amount="<label for='cheque_disposal_date'>Cheque Disposal Date</label>";
        paid_amount+="<input type='date' class='form-control' name='cheque_disposal_date' id='cheque_disposal_date'>";
        $('#date_field').prepend(paid_amount);
    }
 }

</script>
@endsection
@push('js')
<script>
    $(document).ready(function() {
        $("#supplier_id").select2( {
            placeholder: "Select Supplier",
            allowClear: true
        } );
       
    });
</script>
<script type="text/javascript" src="{{asset('admin/js/select-togglebutton.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#transaction_type').togglebutton();
        })
    </script>    
@endpush