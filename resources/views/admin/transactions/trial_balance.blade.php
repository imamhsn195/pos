@extends('layouts.admin')

@section('content')

<div class="row">
<div class="col-md-12 d-flex align-items-stretch grid-margin">
  <div class="row flex-grow row_table_responsive">
	<div class="col-md-12">
	  <div class="card">
		<div class="card-body">
		  <h2 class="text-center">Trial Balance</h2>
      <h4 class="text-center">
      Upto <?php echo date('F, Y ') ?></h4>
      <div class="table-responsive">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>SL. No.</th>
                          <th>Account Title</th>
                          <th>Type</th>
                          <th>Debit</th>
                          <th>Credit</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php
            
                        $amount=0;
                        $camount=0;
                        $i=1;
                        $dsum=0;
                        $csum=0;
                        //$tspan=2;
                        ?>
                      @forelse($trial_balance as $tb)
                      
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$tb->account_head}}</td>
                            <td>{{$tb->type}}</td>
                            @foreach($tb->journals as $jr)

                                @if($jr->journal_type==0)
                                <?php $amount=$amount+$jr->amount;?>
                                @endif

                                @if($jr->journal_type==1)
                                <?php $camount=$camount+$jr->amount;?>
                                @endif

                           @endforeach
                          <td>
                            @if($amount>$camount)  
                            {{$amount-$camount}}
                           <?php $dsum=$dsum+$amount-$camount ?>
                            @endif</td>
                          <td>
                                @if($amount<$camount)
                                {{$camount-$amount}}
                                <?php $csum=$csum+$camount-$amount ?>
                                @endif
                            </td>
                        
                        
                        <?php $amount=0;
                         $camount=0;?>  
                        </tr>
                        @empty
						<tr>
							<td colspan="8">There is no records available</td>
						</tr>
                    @endforelse
                    <tfoot>
                            <th colspan="3" class="text-right">Total:</th>
                    <th class="text-left">{{$csum}}</th>
                    <th class="text-left">{{$dsum}}</th>
                            
                          </tfoot>
                    </table>
                  </div>
		</div>
	  </div>
	</div>
  </div>
</div>

</div>

@endsection