@extends('layouts.admin')

@section('content')

<div class="row">
    <div class="col-md-10 d-flex align-items-stretch grid-margin">
        <div class="row flex-grow">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h2 class="text-center"> {{(isset($_GET['search_view']) && $_GET['search_view']==1)?'Receipts ':'Payments '}}Reports</h2>
                        @if(Auth::user()->user_type=="superadmin")
                        <h4 class="text-center">Showing All Branches</h4>
                        @else
                        <h4 class="text-center">Showing {{Auth::user()->branch->name}} Branch</h4>
                        @endif
                        <p class="card-description text-center">
							<form method="get" action="{{route('payment_show')}}">
								<div class="form-group">
									<select required class="form-control" id="view_type" name="search_view"  onchange="this.form.submit()">
									 <option value="0" {{isset($_GET['search_view'])?'':'selected'}}>Payments View</option>
									 <option value="1" {{(isset($_GET['search_view']) && $_GET['search_view']==1)?'selected':''}}>Receipts View</option>
								   </select>
								</div>
                                <div class="input-group col-md-6 float-right">
                                    <label for="example-search-input1">From :</label>
                                    <input class="form-control py-2" @if(isset($_GET['searchbyfrom']))
                                        value="{{$_GET['searchbyfrom']}}" @else value="{{date('Y-m-01')}}" @endif
                                        name="searchbyfrom" type="date" id="example-search-input1">
                                    <label for="example-search-input2">To :</label>
                                    <input class="form-control py-2" placeholder="To.." name="searchbyto" type="date"
                                        id="example-search-input2" @if(isset($_GET['searchbyto']))
                                        value="{{$_GET['searchbyto']}}" @else value="{{date('Y-m-t')}}" @endif>
                                    <span class="input-group-append">
                                        <button class="btn btn-primary" type="submit">
                                            <i class="mdi mdi-magnify"></i>
                                        </button>
                                    </span>

                                </div>
                            </form>
                        </p>
                        <div class="table-responsive">
                            <table class="table table-bordered" id="accordion" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Transaction No</th>
                                        <th>Date</th>
                                        <th>{{(isset($_GET['search_view'])&& $_GET['search_view']==1)?'Received':'Paid'}} Via</th>
                                        <th>Amount</th>
                                        <th>{{(isset($_GET['search_view'])&& $_GET['search_view']==1)?'Received From':'Paid To'}}</th>
                                        <th>{{(isset($_GET['search_view'])&& $_GET['search_view']==1)?'Received ':'Paid '}}By</th>
                                    </tr>
                                </thead>
                                <tbody>
                                   
                        @forelse($payments as $key=>$payments_status)
								<tr> 
								{{-- class="collapsed" data-toggle="collapse" href="#collapseOne{{$key}}" aria-expanded="true" aria-controls="collapseOne" style="cursor: pointer;" --}}
								<th><a class="btn link" target="_blank" href="{{route('transactions.show',$payments_status->id)}}"> {{$payments_status->id}}</a></th>
                            @php $time=strtotime($payments_status->created_at);
                            $date=date("d-M-Y",$time); @endphp
                                        <td>{{$date}}</td>
                                        @php
            
                                        $saleAmt=0;
                                        $expenseAmt=0;
                                        $paymentMethod='';
                                        @endphp
                                    @foreach($payments_status->journals as $jr)
                                        @if($jr->related_party_type=='supplier' && $jr->journal_type==0)
                                        @php 
                                        $expenseAmt=$expenseAmt+$jr->amount;
                                        @endphp
										@endif
										@if($jr->related_party_type=='customer' && $jr->journal_type==1)
                                        @php 
                                        $expenseAmt=$expenseAmt+$jr->amount;
                                        @endphp
                                        @endif            
                                    @endforeach
                                    <td>{{$payments_status->journals->first()->paymentmethod!=null?$payments_status->journals->first()->paymentmethod->method_title:''}}</td>
                                    <td>{{number_format($expenseAmt,2)}}</td>
                                    <td>{{isset($_GET['search_view'])?$payments_status->journals->first()->customer->customer_name:$payments_status->journals->first()->supplier->supplier_name}}</td>
                                    <td title="{{$payments_status->branch->name}}">{{$payments_status->user->name}}</td>
                        </tr>
                                    @empty
                                    <tr>
                                        <td colspan="8">There is no records available</td>
                                    </tr>
                                    @endforelse
                                </tbody>
                               
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection
@push('js')
<script type="text/javascript" src="{{asset('admin/js/select-togglebutton.js')}}"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $('#view_type').togglebutton();
            })
        </script>    
@endpush
