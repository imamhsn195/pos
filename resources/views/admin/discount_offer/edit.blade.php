@extends('layouts.admin')
@section('content')
<div class="row">
    <div class="col-md-12 d-flex align-items-stretch grid-margin">
        <div class="row flex-grow">
            <div class="col-md-1"></div>
            <div class="col-md-10 bg-secondary">
                <div class="card bg-secondary">
                    <div class="card-body">
                        <h4 class="text-center">Discount Form</h4>
                        <form class="forms-sample" method="post" action="{{  route('discount_offers.update', $discount_offer->id)}}">
                            @csrf
                            @method('put')
                            <div class="form-group row">
                                <label for="discount_title" class="col-sm-2">Discount Title</label>
                                <div class="col-sm-10">
                                    <input type="text" value="{{ $discount_offer->discount_title }}" name="discount_title" id="discount_title" class="form-control">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="offer_from" class="col-sm-2">Offer From</label>
                                <div class="col-sm-10">
                                    <input type="date" value="{{ $discount_offer->offer_from }}" name="offer_from" id="offer_from" class="form-control">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="offer_to" class="col-sm-2">Offer To</label>
                                <div class="col-sm-10">
                                    <input type="date" value="{{ $discount_offer->offer_to }}" name="offer_to" id="offer_to" class="form-control">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Discount Type</label>
                                <div class="col-sm-2">
                                    <div class="form-radio">
                                        <label class="form-check-label">
                                            <input type="radio" class="form-check-input" value="0" name="discount_type"
                                                id="discount_percent" {{ $discount_offer->discount_percent?'checked':'' }} onclick="changeAddon(0)">
                                            <i class="input-helper">Percent</i></label>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-radio">
                                        <label class="form-check-label">
                                            <input type="radio" class="form-check-input" value="1" name="discount_type"
                                                id="discount_amount" {{ $discount_offer->discount_amount?'checked':'' }}  onclick="changeAddon(1)">
                                            <i class="input-helper">Amount</i></label>
                                    </div>
                                </div>
                                <div class="input-group col-sm-6">
                                    <input type="number" class="form-control" name="discount_rate" id="hidden_1" value="{{ $discount_offer->discount_percent?$discount_offer->discount_percent*100:$discount_offer->discount_amount }}" aria-describedby="basic-addon-percent" step=".01">
                                    <div class="input-group-append">
                                    <span class="input-group-text text-info" id="basic-addon-percent">%</span></div>
                                </div>
                            </div>
                            {{--  <div class="form-group row">
                                <label for="customer_group" class="col-sm-2">Select Customer Group</label>
                                <div class="col-sm-10">
                                    <select name="customerGroup[]" class="form-control " data-live-search="true" id="customer_group"
                                        multiple>
                                        @foreach($groups as $grp)
                                        <option value="{{ $grp->id}}">{{ $grp->customer_group_title }}</option>
                                        @endforeach
                                    </select>
                                    <p><strong class="text-danger">'Ctrl+click'</strong> for multiple select</p>                                    
                                </div>
                            </div>  --}}
                            <br>
                            {{--  
                                <div class="form-group row">
                                    <label for="customer_criteria" class="col-sm-2">Select Customer Criteria</label>
                                    <div class="col-sm-10">
                                        <select name="customerCriteria[]" class="form-control " data-live-search="true" id="customer_criteria" multiple>
                                            @foreach($criterias as $crt)
                                                @if($crt->status==1)
                                                    <option value="{{$crt->id}}">{{$crt->title}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                        <p><strong class="text-danger">'Ctrl+click'</strong> for multiple select</p>
                                    </div>
                                </div>  
                            --}}
                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="form-group row">
                                            <div class="form-radio">
                                                <label class="form-check-label">
                                                    {{--onclick="hidden3()" is used for show-hide the parameterwised fields --}}
                                                    <input type="radio" class="form-check-input" value="0" name="discount_on" id="promo_code" onclick="hidden3('hidden_3','product_name','purchase_field')" {{ $discount_offer->promo_code !== null?'checked':'' }}>Promo Code <i class="input-helper"></i>
                                                </label>
                                            </div>
                                            <input type="text" style="display:{{ $discount_offer->promo_code !== null?'block':'none' }}" class="form-control" name="promo_code" id="hidden_3"   value="{{ $discount_offer->promo_code }}">
                                        </div>
                                </div>
                                <div class="col-sm-1"><label class="form-check-label">Or</label></div>
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="form-radio">
                                            <label class="form-check-label" id="discount_for_product_label">
                                                <input type="radio" class="form-check-input" value="1" name="discount_on" id="promo_code1" {{ ($discount_offer->product_id !== null && $discount_offer->purchase_id !== null)?'checked':'' }}  onclick="productname1('product_name','hidden_3','purchase_field')"/>Products Discount
                                                <i class="input-helper text-sm text-danger">
                                            </label>
                                        </div>
                                    </div>
                                    {{--productname() method used to retrive product_id when click on "choose product" field.this method is in script tag below.--}}
                                    <div class="form-control" style="display:{{ ($discount_offer->product_id !== null && $discount_offer->purchase_id !== null)?'block':'none' }}" id="product_name_div">
                                        <select class="form-control" name="product_name" id="product_name" value="" onchange="productname()">
                                            <option value="">Select Product</option>
                                            @php $pro=0; @endphp
                                                @foreach($products as $product)
                                                    @if($pro!==$product->product_id)
                                                    <option value="{{$product->product_id}}"
                                                        {{ $product->product_id==$discount_offer->product_id?'selected':''}}>{{$product->product->product_sku}}</option>
                                                    <?php $pro = $product->product_id ?>
                                                    @endif
                                                @endforeach
                                        </select>
                                    </div>
                                    <div style="display:{{ ($discount_offer->product_id !== null && $discount_offer->purchase_id !== null)?'block':'none' }}" class="form-group" id="purchase_field">
                                        <br>
                                            <label for="purchase_id">Purchase Price / Invoice No.</label>
                                            <select class="form-control" id="purchase_id" onchange="purchaseid()">
                                                <option value=''>Not Applicable</option>
                                                @forelse ($purchases as $purchase)
                                                     <option {{ $purchase->purchase->id === $discount_offer->purchase_id?'selected':''}} value="{{ $purchase->purchase->id }}"> {{ 'Inv: '.$purchase->purchase->invoice_no .' / Selling Price: '.$purchase->purchase->sell_unit_price }}</option>
                                                @empty
                                                    
                                                @endforelse
                                               
                                            </select>
                                    </div>  
                                </div>
                            </div>
                            <button type="submit" class="btn btn-success mr-2">Submit</button>
                            <a class="btn btn-danger" href="{{route('discount_offers.index')}}">Cancel</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function productname() {
        var purchase = document.getElementById("product_name").value;

        $('#purchase_field').empty();
        if (purchase) {
            $.ajax({
                type: 'GET',
                url: "<?=url('find_purchase_id')?>"+"/"+ purchase,
                success: function (html) {
                    $('#purchase_field').prepend(html);

                }
            });
        }
    }

    function hidden3(hidden_3, product_name, purchase_field) {
        $(document).ready(function () {
            $('#hidden_3').show();
            $('#product_name').hide();
            $('#product_name_div').hide();
            $('#purchase_field').hide();
        });
    }

    function productname1(product_name, hidden_3, purchase_field) {

        $(document).ready(function () {
            $('#product_name').show();
            $('#hidden_3').hide();
            $('#purchase_field').show();
            $('#product_name_div').show();
        });

    }

</script>
@endsection
