@extends('layouts.admin')
@push('css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<style>
    body
    {
        counter-reset: Serial;           /* Set the Serial counter to 0 */
    }
        
    table
    {
        border-collapse: separate;
    }

    tr td:first-child:before
    {
        counter-increment: Serial;      /* Increment the Serial counter */
        content: counter(Serial); /* Display the counter */
    }
    .auto-index td:first-child:before
    {
        counter-increment: Serial;      /* Increment the Serial counter */
        content: counter(Serial); /* Display the counter */
    }
</style>
@endpush
@section('content')
<div class="row">
    <div class="col-md-12 d-flex align-items-stretch grid-margin">
        <div class="row flex-grow">
            <div class="col-md-1"></div>
            <div class="col-md-10 bg-secondary">
                <div class="card bg-secondary">
                    <div class="card-body">
                        <h4 class="text-center">Discount Form</h4>
                        <form class="forms-sample" method="post" action="{{route('discount_offers.store')}}">
                            @csrf
                            <div class="form-group row">
                                <label for="discount_title" class="col-sm-2">Discount Title</label>
                                <div class="col-sm-10">
                                    <input type="text" value="{{old('discount_title')}}" name="discount_title" id="discount_title" class="form-control">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="offer_from" class="col-sm-2">Offer From</label>
                                <div class="col-sm-10">
                                    <input type="date" value="{{old('offer_from')}}" name="offer_from" id="offer_from" class="form-control">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="offer_to" class="col-sm-2">Offer To</label>
                                <div class="col-sm-10">
                                    <input type="date" value="{{ old('offer_to') }}" name="offer_to" id="offer_to" class="form-control">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Discount Type</label>
                                <div class="col-sm-2">
                                    <div class="form-radio">
                                        <label class="form-check-label">
                                            <input type="radio" class="form-check-input" value="0" name="discount_type"
                                                id="discount_percent" onclick="changeAddon(0)">
                                            <i class="input-helper">Percent</i></label>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-radio">
                                        <label class="form-check-label">
                                            <input type="radio" class="form-check-input" value="1" name="discount_type"
                                                id="discount_amount"  onclick="changeAddon(1)">
                                            <i class="input-helper">Amount</i></label>
                                    </div>
                                </div>
                                <div class="input-group col-sm-6">
                                    <input type="number" class="form-control" name="discount_rate" id="hidden_1" aria-describedby="basic-addon-percent" step=".01">
                                    <div class="input-group-append">
                                    <span class="input-group-text text-info" id="basic-addon-percent">%</span></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="customer_group" class="col-sm-2">Select Customer Group</label>
                                <div class="col-sm-10">
                                    <select name="customerGroup[]" class="form-control " data-live-search="true" id="customer_group"
                                        multiple>
                                        @foreach($groups as $grp)
                                        <option value="{{$grp->id}}">{{$grp->customer_group_title}}</option>
                                        @endforeach
                                    </select>
                                    <p><strong class="text-danger">'Ctrl+click'</strong> for multiple select</p>                                    
                                </div>
                            </div>
                            <br>
                            <div class="form-group row">
                                <label for="customer_criteria" class="col-sm-2">Select Customer Criteria</label>
                                <div class="col-sm-10">
                                    <select name="customerCriteria[]" class="form-control " data-live-search="true" id="customer_criteria" multiple>
                                        @foreach($criterias as $crt)
                                            @if($crt->status==1)
                                                <option value="{{$crt->id}}">{{$crt->title}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                    <p><strong class="text-danger">'Ctrl+click'</strong> for multiple select</p>
                                </div>

                                
                            </div>
                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="form-group row">
                                            <div class="form-radio">
                                                <label class="form-check-label">
                                                    {{--onclick="hidden3()" is used for show-hide the parameterwised fields --}}
                                                    <input type="radio" class="form-check-input" value="0" name="discount_on" id="promo_code" onclick="hidden3('hidden_3','product_name','purchase_field')">Promo Code <i class="input-helper"></i>
                                                </label>
                                            </div>
                                            <input type="text" style="display:none" class="form-control" name="promo_code" id="hidden_3">
                                        </div>
                                </div>
                                <div class="col-sm-1"><label class="form-check-label">Or</label></div>
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <div class="form-radio">
                                            <label class="form-check-label" id="discount_for_product_label">
                                                <input type="radio" class="form-check-input" value="1" name="discount_on" id="promo_code1" onclick="productname1('product_name','hidden_3','purchase_field')"/>Products Discount<i class="input-helper text-sm text-danger"><br>** If Product and Invoice below are not added discount will be applied for all Products</i>
                                            </label>
                                        </div>
                                        {{--productname() method used to retrive product_id when click on "choose product" field.this method is in script tag below.--}}
                                        <div class="form-control" style="display:none" id="product_name_div">
                                            <select class="form-control" id="product_name" onchange="productname()">
                                                <option value="" >Select Product</option>
                                                    @php $pro=0; @endphp
                                                    @foreach($products as $product)
                                                        @if($pro!==$product->product_id)
                                                            <option value="{{$product->product_id}}">{{$product->product->product_sku}}</option>
                                                            @php $pro = $product->product_id @endphp
                                                        @endif
                                                    @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div style="display:block" class="form-group row" id="purchase_field"></div>                                    
                                    <button type="button" class="btn btn-info btn-sm float-right" disabled="true" id="addProductToRowButton" onclick="addproductInfoToTable(product_name.value,purchase_id.value)">Add</button><br>
                                </div>
                            </div>
                            <div class="row">
                                <table class="table table-border auto-index">
                                    <thead id="discountedProductListHead" style="visibility:hidden">
                                        <tr>
                                            <th>SL</th>
                                            <th>Name</th>
                                            <th>Purchase/Invoice</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody id="discountedProductList">
                                     
                                    </tbody>
                                </table>
                            </div>
                            <button type="submit" class="btn btn-success mr-2">Submit</button>
                            <a class="btn btn-danger" href="{{route('discount_offers.index')}}">Cancel</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('js')

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script>

    //ajax for getting productwise purchase_id

    function productname() {
        var purchase = $("#product_name").val();
        $('#purchase_field').empty();
        if (purchase) {
            $.ajax({
                type: 'GET',
                url: "<?=url('find_purchase_id')?>"+"/"+ purchase,
                success: function (html) {
                    $('#purchase_field').prepend(html);
                }
            });
        } else {}
    }

//show promo code field while hiding product_name & purchase_field

    function hidden3(hidden_3, product_name, purchase_field) {
        
        $(document).ready(function () {
            $('#hidden_3').show();
            $('#product_name_div').css('display','none');
            $('#purchase_field').hide();
        });
    }

    function changeAddon(entity) {
        
        if(entity==1){
            $('#basic-addon-percent').text('Amount');
        }else{
            $('#basic-addon-percent').text('%');
        }
    }

    function productname1(product_name, hidden_3, purchase_field) {
         //show product name & purchase field while hiding promo_code field
         //cdn has been used to create the search box for searching "product name"
         //select2 method is used to get cdn facilities
        
        $(document).ready(function () {
            $('#product_name_div').show();
            $("#product_name").select2({
                placeholder: "Select individual Product",
                allowClear: true,
                tags: true,
                tokenSeparators: [',', ' ']
            });

            $('#hidden_3').hide();
            $('#purchase_field').show();
        });

    }

// Add button enable to add Product to Discount Product Table.
function purchaseid(){
    var product_id = $("#product_name").val();
    var invoice = $('#purchase_id').val();

    if(product_id !== '' && invoice !== ''){
       $('#addProductToRowButton').attr('disabled',false);
    }    
}
// Add Product info to Discount Product Table
function addproductInfoToTable(product_id,invoice_no){

    if(product_id && invoice_no){

       /// ajax request for search product for discount.
       $.ajax({
        type: 'GET',
        url: "<?=url('find_product_for_discount')?>" + "/" + product_id + "/" +invoice_no,
        success: function (data) {
            var discounted_product ='<tr>';
                    discounted_product += '<td></td>';
                    discounted_product += '<td>'+ data.product.product_sku + '<input type="hidden" name="product_name[]" value="'+ data.product_id +'"/><input type="hidden" name="purchase_id[]" value="'+ data.purchase_id +'"/></td>';
                    discounted_product += '<td>'+ data.purchase.invoice_no + '</td>';
                    discounted_product += '<td><span id="removeProduct" class="btn btn-link text-danger">X</span></td></tr>';
                $('#discountedProductList').append(discounted_product);
                if($('#discountedProductList tr').length !== 0){
                    $('#discountedProductListHead').css('visibility','visible');
                    $('#discount_for_product_label span').text('Discount For Selected Products');
                }
                $('#addProductToRowButton').attr('disabled',true); // Add button disable to add Product to Discount Product Table.
            }
        });
    }else{
        alert('Please select Product and Invoice No.');
    }  
}
// remove product from discounted Product List
$('#discountedProductList').on('click','tr span',function(e){
    $(this).parents('tr').remove();
    if($('#discountedProductList tr').length == 0){
        $('#discountedProductListHead').css('visibility','hidden');
    }
});

{{--  $( "#offer_from" ).datepicker({ minDate: 0});  --}}
{{--  $(document).ready(function(){
    alert('ok');
    $("#offer_from").datepicker({ minDate:0});
});  --}}
</script>
@endpush
