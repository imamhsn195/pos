@extends('layouts.admin') @section('content')

<div class="row">
    <div class="col-md-12 d-flex align-items-stretch grid-margin">
        <div class="row flex-grow row_table_responsive">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h2 class="text-center">Discount Offers</h2>
                            <div class="row">
                                {{-- <div class="col-md-6"></div> --}}
                                <div class="col-md-12">
                                    <form action="{{route('discount_offers.index')}}" method="GET">
                                      <div class="input-group float-right">
                                          <input class="form-control py-2 border-primary" placeholder="Search here.." value="{{ request('search')}}" name="search" type="text" id="example-search-input" required="">
                                          <span class="input-group-append">
                                            <button class="btn btn-primary" type="submit">
                                                <i class="mdi mdi-magnify"></i>
                                            </button>
                                            <a class="btn btn-primary" title="Register New Offers" href="{{route('discount_offers.create')}}"><i class="mdi mdi-playlist-plus"></i></a>  
                                            @if (request()->query())
                                            <a class="btn btn-primary" title="Show All Offers" href="{{route('discount_offers.index')}}"><i class="mdi mdi-format-list-bulleted"></i></a> 
                                            @endif
                                          </span>
                                      </div>
                                    </form>
                                </div>
                              </div>
                          <br>
                       

                        <div class="table-responsive">
                            <table class="table table-striped table-bordered border-primary">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Title</th>
                                        <th>Product Or Promo</th>
                                        <th>Duration</th>
                                        <th>Discount</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse($discount_index as $key=>$discount)
                                    <tr>
                                        <td>{{(($discount_index->currentPage() - 1) * $discount_index->perPage() + $key+1)}}</td>
                                        <td>{{$discount->discount_title}}</td>
                                        <td>
                                            @if($discount->product==null)
                                             {{$discount->promo_code or "All Product"}}
                                            @elseif(!$discount->product==null)
                                            {{str_before($discount->product->product_sku," | ")}}
                                            <br>{{str_after($discount->product->product_sku," | ")}} @endif
                                        </td>

                                        <td>{{date('d-M-Y',strtotime($discount->offer_from))}}
                                            <br>
                                            <br> {{date('d-M-Y',strtotime($discount->offer_to))}}
                                        </td>

                                        <td>{{$discount->discount_percent ? ($discount->discount_percent*100).' %' : 'tk.
                                            '.$discount->discount_amount}} </td>

                                        <td>
                                       
                                            <form action="{{route('change_discount_status',$discount->id)}}" method="post">
                                                @csrf
                                                <input type="hidden" name="status" value="{{$discount->status}}">
                                                <button type="submit" onclick="return confirm('Are you sure to change Status??')"
                                                    class="btn btn-link">
                                                    @if($discount->status == 1)
                                                    <span class="badge badge-success"><i class="mdi mdi-thumb-up"></i></span> @else
                                                    <span class="badge badge-danger"><i class="mdi mdi-thumb-down"></i></span> @endif
                                                </button>
                                            </form>
                                        </td>
                                        <td>
                                        <span class="input-group-append">
                                            <form action="{{route('discount_offers.destroy',$discount->id)}}" method="post">
                                                @csrf @method('delete')
                                                <a href="{{route('discount_offers.edit',$discount->id)}}" class="btn btn-dark btn-sm"><i class="mdi mdi-table-edit"></i></a>
                                                <button type="submit"  onclick="return confirm('Are you sure to delete this??')"
                                                    class="btn btn-danger btn-sm"><i class="mdi mdi-delete"></i></button>

                                            </form>
                                        </span>

                                        </td>
                                    </tr>
                                    @empty
                                    <tr>
                                        <td colspan="8">There is no records available</td>
                                    </tr>
                                    @endforelse
                                </tbody>
                            </table>
                    
                            {{--pagination--}}
                            <span class="float-right">{{$discount_index->links()}}</span>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>

</div>

@endsection
