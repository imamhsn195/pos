@extends('layouts.admin')

@section('content')

<div class="row">
            <div class="col-md-6 d-flex align-items-stretch grid-margin">
              <div class="row flex-grow">
                <div class="col-12">
                  <div class="card">
                    <div class="card-body">
                      <h4 class="card-title">Warranty Type form</h4>
                      <p class="card-description">
                      <form class="forms-sample" method="post" action="{{route('warranty_type.update',$editid)}}">
					              @csrf
                        @method('put')
                        <div class="form-group">
                          <label for="name">Warranty Type Name</label>
                          <input type="text" value="{{$parent->warranty_name}}" class="form-control" name="warranty_name" id="name" placeholder="Enter Warranty Type Name">
                        </div> 
						            <div class="form-group">
                          <label for="description">Description</label>
                          <textarea  class="form-control" id="description" name="warranty_description" placeholder="Enter Warranty Type description">{{$parent->warranty_description}}"</textarea>
                        </div>
                        <div class="form-group">
                         
                          <label for="parent_warranty_type">Root Warranty Type</label>
                          <select  class="form-control" id="parent_warranty_type" name="parent_id">
                            <option value="">Root Warranty</option>
                  						@forelse($warranty as $wr)
                                  @if($wr->id !== $parent->id)
                                  <option {{  $parent->parent_id == $wr->id?"selected":'' }} value="{{$wr->id}}">{{ $wr->warranty_name }}</option>
                                  @endif
                              @empty
                                <p>There are no Root Warranty!</p>
                              @endforelse
          						  </select>
                        </div>
                        <button type="submit" class="btn btn-success mr-2">Submit</button>
                        <a  href="{{route('warranty_type.index')}}" class="btn btn-danger" >Cancel</a>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
          </div>

@endsection