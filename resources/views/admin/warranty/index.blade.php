@extends('layouts.admin')

@section('content')
<div class="row">
  <div class="col-md-12 d-flex align-items-stretch grid-margin">
    <div class="row flex-grow row_table_responsive">
     <div class="col-md-12">
       <div class="card">
        <div class="card-body">
          <h2 class="text-center" >All Warranties</h2>
          <div class="row">
              {{-- <div class="col-md-8"></div> --}}
              <div class="col-md-12">
                  <form action="{{route('warranty_type.index')}}" method="GET">
                    <div class="input-group float-right">
                        <input class="form-control py-2 border-primary" placeholder="Search here.." name="searchby" type="text" id="example-search-input" required value="{{request('searchby')?request('searchby'):''}}">
                        <span class="input-group-append">
                          <button class="btn btn-primary" type="submit">
                              <i class="mdi mdi-magnify"></i>
                          </button>
                            <a class="btn btn-primary" href="{{route('warranty_type.create')}}"data-toggle="modal" data-target="#exampleModal"><i class="mdi mdi-plus"></i></a>
                            @if (request()->query())
                            <a class="btn btn-primary" href="{{route('warranty_type.index')}}"><i class="mdi mdi-format-list-bulleted"></i></a>
                            @endif
                          {{--  <button type="button" class="btn btn-primary"><i class="mdi mdi-plus"></i></button>  --}}
                        </span>
                    </div>
                  </form>
              </div>
              </div>
<br>
          <div class="table-responsive">
            <table class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Warranty Name</th>
                  <th>Description</th>
                   <th>Parent Warranty</th> 
                  <th>Created by</th>
                  <th class="text-center">Actions</th>
                </tr>
              </thead>
              <tbody>
               <?php $i =1; ?>
               @forelse($warranties as $wr)
               <tr>
                <td>{{$i++}}</td>
                <td>{{$wr->warranty_name}}</td>
                <td>{{$wr->warranty_description}}</td>
                   @if($wr->parentWarranty !== null)
                    <td>{{$wr->parentWarranty->warranty_name}}</td>
                  @elseif($wr->parent_id == null)
                    <td>This is root Warranty</td>
                  @endif 
                <td>{{$wr->user->user_type}}</td>
                <td class="text-center">
                  <a href="{{route('warranty_type.edit',$wr->id)}}" class="btn btn-dark btn-sm float-left"><i class="mdi mdi-table-edit"></i></a>
                  <form action="{{route('warranty_type.destroy',$wr->id)}}" method="post">
                  
                  {{--  <a href="{{route('warranty_type.edit',$wr->id)}}" class="btn btn-link">Edit</a>  --}}
                   @csrf
                   @method('delete')
                   <button type="submit" value="Delete" onclick="return confirm('Are you sure to delete this??')" class="btn btn-danger btn-sm">
                     <i class="mdi mdi-delete"></i>
                   </button>
                 </form>
               </td>
             </tr>
             @empty
             <tr>
               <td colspan="8">There is no records available</td>
             </tr>
             @endforelse
           </tbody>
         </table> 
         <span class="float-right">{{$warranties->appends(request()->query())->links()}}</span>
       </div>
     </div>
   </div>
 </div>
 <!-- Modal -->
 <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form class="forms-sample" method="post" action="{{route('warranty_type.store')}}">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Warranty Type Register</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          @csrf
          <div class="form-group">
            <label for="name">Warranty Type Name</label>
            <input type="text" class="form-control" name="warranty_name" id="name" placeholder="Enter Warranty Type Name">
          </div> 
          <div class="form-group">
            <label for="description">Description</label>
            <textarea  class="form-control" id="description" name="warranty_description" placeholder="Enter Warranty Type description"></textarea>
          </div>
          <div class="form-group">
            <label for="parent_warranty_type">Root Warranty Type</label>
            <select  class="form-control" id="parent_warranty_type" name="parent_id">
              <option value="">Select Root Warranty</option>
              @forelse($warranties as $wp)
              <option value="{{$wp->id}}">{{$wp->warranty_name}}</option>
              @empty
              <option>{{'There is no warranty type'}}</option>
              @endforelse
            </select>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-success mr-2">Submit</button>
          <a  href="{{route('warranty_type.index')}}" class="btn btn-danger" data-dismiss="modal" >Cancel</a>
        </div>
      </form>
    </div>
  </div>
</div>
</div>
</div>
</div>
@endsection
