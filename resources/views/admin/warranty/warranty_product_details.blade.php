@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-md-12 d-flex align-items-stretch grid-margin">
        <div class="row flex-grow row_table_responsive">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h1 class="text-center">All Warranty Products</h1>
                        <hr>
                        <form action="{{route('warranty_product_detail.index')}}" method="get">
                            <!-- Search Product Form -->
                            <div class="row float-right">
                                <div class="form-group col-sm-6">
                                    <input type="test" name="product_sl_no" class="form-control border-info"
                                        placeholder="Product Serial No">
                                </div>
                                <div class="form-group col-sm-3">
                                    <input type="submit" value="Search" class="form-control btn btn-info">
                                </div>
                                <div class="form-group col-sm-3">
                                    <a href="{{route('warranty_product_detail.index')}}"
                                        class="form-control btn btn-info "><i class="menu-icon mdi mdi-eye"></i>All</a>
                                </div>
                            </div>
                        </form>

                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Product Name</th>
                                        <th>Product Serial</th>
                                        <th>Sold At</th>
                                        <th>Sold To</th>
                                        <th>Invoice No (Sales)</th>
                                        <th>Purchased From</th>
                                        <th>Invoice No (Purchase)</th>
                                        <th>Warranty Details</th>
                                        <th>Created By</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php $sl_no = $products->currentPage()*$products->perPage()-$products->perPage()+1;
                                    @endphp
                                    @forelse($products as $product)
                                    <tr>
                                        <td>{{$sl_no++}}</td>
                                        <td>
                                            {{str_before($product->purchase->product->product_sku,"|")}}<br><br>
                                            {{str_after($product->purchase->product->product_sku,"|")}}
                                        </td>
                                        <td>
                                            @foreach ($product->purchase->warranty_product_details as
                                            $warranty_product_details)
                                            <label>
                                                <h6>{{$warranty_product_details->product_sl_no}}</h6>
                                            </label><br>
                                            @endforeach
                                        </td>
                                        <td>
                                            @foreach ($product->purchase->warranty_product_details as
                                            $warranty_product_details_sold_date)
                                            @if($warranty_product_details_sold_date->sold_date == null)
                                            <p><label class="badge badge-success">Unsold</label></p>
                                            @endif
                                            @if($warranty_product_details_sold_date->sold_date !== null)
                                            <p><label
                                                    class="badge badge-danger">{{$warranty_product_details_sold_date->sold_date}}</label>
                                            </p>
                                            @endif
                                            @endforeach
                                        </td>
                                        <td class="text-center">
                                            {{$product->sales_detail !== null?$product->sales_detail->sale->customer->customer_name:'NA'}}
                                        </td>
                                        <td class="text-center"><a
                                                href="{{$product->sales_detail !== null?route('sales_details_report',$product->sales_detail->sale->id):''}}">{{$product->sales_detail !== null?$product->sales_detail->sale->invoice_no:''}}</a>{{$product->sales_detail === null?'NA':''}}
                                        </td>
                                        <td>{{$product->purchase->supplier->supplier_name}}<br>{{$product->purchase->supplier->address->phone}}
                                        </td>
                                        <td>
                                            <a href="{{route('invoice_details',$product->purchase->invoice_no)}}"
                                                id="invoice_details_route" onclick="PurchaseInvoice(this)"
                                                class="badge badge-info" data-toggle="modal"
                                                data-target="#PurchaseInvoice">
                                                {{$product->purchase->invoice_no}}
                                            </a>
                                        </td>
                                        {{-- <td>{{date("d-M-Y",strtotime($product->created_at))}}</td> --}}
                                        <td>
                                            @foreach($product->purchase->warranty_types as $pt_type)
                                            <b
                                                class="text-left">{{$pt_type->warranty_type?$pt_type->warranty_type->warranty_name." : ":""}}</b>
                                            <b class="text-danger">{{$pt_type->warranty_type?$pt_type->duration." months":""}}
                                            </b><br><br>
                                            @endforeach
                                        </td>
                                        <td>
                                            {{$product->user->name}}
                                            | {{$product->user->user_type}}
                                        </td>


                                    </tr>
                                    @empty
                                    <tr>
                                        <td colspan="8">There is no records available</td>
                                    </tr>
                                    @endforelse

                                </tbody>
                            </table>
                            {{$products->appends(request()->query())->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal For Purchase Invoice View -->
<div class="modal fade" id="PurchaseInvoice" tabindex="-1" role="dialog" aria-labelledby="PurchaseInvoiceLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content" style="width: -moz-max-content; min-width:700px">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="PurchaseInvoiceModalBody">
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')

<script>
    function PurchaseInvoice(route) {
        $.get(route, function (data) {
            if (data) {
                $('#PurchaseInvoiceModalBody').html(data);
            } else {
                $('#PurchaseInvoiceModalBody').html('no data found!');
            }
        });
    }
    $("#PurchaseInvoice").on('hidden.bs.modal', function () {
        $('#PurchaseInvoiceModalBody').html();
    });
</script>

@endpush
