@extends('layouts.admin')
@section('content')
<div class="row">
    <div class="col-md-12 d-flex align-items-stretch grid-margin">
        <div class="row flex-grow row_table_responsive">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                            <h2 class="text-center">All Customer Groups</h2>
                            <div class="row">
                                {{-- <div class="col-md-6"></div> --}}
                                <div class="col-md-12">
                                    <form action="{{route('customer_groups.index')}}" method="GET">
                                      <div class="input-group float-right">
                                          <input class="form-control py-2 border-primary" placeholder="Search here.." name="searchby" type="text" id="example-search-input" required="">
                                          <span class="input-group-append">
                                            <button class="btn btn-primary" type="submit">
                                                <i class="mdi mdi-magnify"></i>
                                            </button>
                                            <a class="btn btn-primary" title="Register New Customer Group" href="{{route('customer_groups.create')}}"><i class="mdi mdi-playlist-plus"></i></a>  
                                            @if (request()->query())
                                            <a class="btn btn-primary" title="Show All Customer Group" href="{{route('customer_groups.index')}}"><i class="mdi mdi-format-list-bulleted"></i></a> 
                                            @endif
                                          </span>
                                      </div>
                                    </form>
                                </div>
                              </div>
                          <br>
                      
                     
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered border-primary">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Customer Group Title</th>
                                        <th>Group Description</th>
                                        <th>Discount %</th>
                                        <th>Created By</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i =1; ?>
                                    @forelse($customergroups as $cusg)
                                    <tr>
                                        <td>{{$i++}}</td>
                                        <td>{{$cusg->customer_group_title}}</td>
                                        <td>{{$cusg->customer_group_description}}</td>
                                        <td>{{$cusg->discount_percentage*100}}</td>
                                        <td>{{$cusg->users->name}}</td>
                                        <td>
                                        <span class="input-group-append">
                                            <a href="{{route('customer_groups.edit',$cusg->id)}}" class="btn btn-dark btn-sm"><i class="mdi mdi-table-edit"></i></a>
                                        
                                            <form action="{{route('customer_groups.destroy',$cusg->id)}}" method="post">
                                                @csrf
                                                @method('delete')
                                                <button type="submit" onclick="return confirm('Are you sure to delete this??')"
                                                    class="btn btn-danger btn-sm"><i class="mdi mdi-delete"></i></button>
                                            </form>
                                        </span>
                                        </td>
                                    </tr>
                                    @empty
                                    <tr>
                                        <td colspan="8">There is no records available</td>
                                    </tr>
                                    @endforelse
                                </tbody>
                            </table>
                            <span class="float-right">  {{$customergroups->links()}}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
