@extends('layouts.admin')

@section('content')

<div class="row">
    <div class="col-md-6 d-flex align-items-stretch grid-margin">
        <div class="row flex-grow">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title" align="center">Customer Group Form</h4>
                        <br>
                        <form class="forms-sample" method="post" action="{{route('customer_groups.store')}}">
                            @csrf
                            <div class="form-group">
                                <label for="Customer_group_title">Customer Group Name</label>
                                <input type="text" class="form-control" name="customer_group_title" id="customer_group_title"
                                    placeholder="Enter Group Name">
                            </div>

                            <br>

                            <div class="form-group">
                                <label for="customer_group_description">Customer Group Description</label>
                                <input type="text" class="form-control" name="customer_group_description" id="customer_group_description"
                                    placeholder="Enter Group Description">
                            </div>

                            <br>

                            <div class="form-group">
                                <label for="discount_percentage">Discount Percentage</label>
                                <div class="input-group mb-3">
                                    <input type="number" class="form-control" name="discount_percentage" id="discount_percentage"
                                    placeholder="Enter Discount Amount in percentage">
                                    <div class="input-group-append">
                                        <span class="input-group-text">%</span>
                                    </div> 
                                </div>
                                
                            </div>

                            <br>

                            <button type="submit" class="btn btn-success mr-2">Submit</button>
                            <a href="{{route('customer_groups.index')}}" class="btn btn-danger">Cancel</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection
