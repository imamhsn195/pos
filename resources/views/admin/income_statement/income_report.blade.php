@extends('layouts.admin')

@section('title','Income Statement')

@section('content')

<div class="row">
<div class="col-md-12 d-flex align-items-stretch grid-margin">
  <div class="row flex-grow row_table_responsive">
	<div class="col-md-12">
	  <div class="card">
		<div class="card-body">
		  <h2 class="text-center">Income Statement</h2>
      <p class="card-description text-center">
      For the end of <?php echo date('F Y') ?></p>
      <div class="table-responsive">
                    <table class="table table-bordered">
                      <thead>
                        <tr class="table-success">
                        
                          <th>Account Title</th>
                          <th>Notes</th>
                          <th>Amount</th>
                          <th>Amount</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <th><u>Sales Revenue:</u></th>
                          <th></th>
                          <th></th>
                          <th></th>
                        </tr>
                        <tr>
                          <td>&nbsp;&nbsp;&nbsp;&nbsp;{{$returndata['Sales']['account_head']}}</td>
                          <th></th>
                          <th>{{$sale=$returndata['Sales']['amount']}}</th>
                          <th></th>
                        </tr>
                        <tr>
                          <th>&nbsp;&nbsp;&nbsp;&nbspLess:Sales Return & allowances:</th>
                          <th></th>
                          <th></th>
                          <th></th>
                        </tr>
                        <tr>
                          <td>&nbsp;&nbsp;&nbsp;&nbsp;{{$returndata['Sales Return']['account_head']}}</td>
                          <td>{{$returndata['Sales Return']['amount']}}</td>
                          <th></th>
                          <th></th>
                        </tr>
                        <tr>
                          <td>&nbsp;&nbsp;&nbsp;&nbsp;{{$returndata['Discount on Sales']['account_head']}}:</td>
                          <td style="border-bottom: black 2px solid;">{{$returndata['Discount on Sales']['amount']}}</td>
                          <td style="border-bottom: black 2px solid;">{{$saleAllowence=($returndata['Discount on Sales']['amount']+$returndata['Sales Return']['amount'])}}</td>
                          <th></th>
                        </tr>
                        <tr>
                          <th class="text-right">Net Sales</th>
                          <th></th>
                          <th></th>
                          <th>{{$netSale=$sale-$saleAllowence}}</th>
                        </tr>
                        <tr>
                          <th><u>Less: Cost of Goods Sold(COGS)</u></th>
                          <th></th>
                          <th></th>
                          <th></th>
                        </tr>
                        <tr>
                          <td>&nbsp;&nbsp;&nbsp;&nbsp;Beginning Inventory</td>
                          <th></th>
                          <td>{{$bInventory=0}}</td>
                          <th></th>
                        </tr>
                        <tr>
                          <td>&nbsp;&nbsp;&nbsp;&nbsp;{{$returndata['Purchase']['account_head']}}</td>
                          <th>{{$purchase=$returndata['Purchase']['amount']}}</th>
                          <td></td>
                          <th></th>
                        </tr>
                        <tr>
                          <th>&nbsp;&nbsp;&nbsp;&nbsp;Less:Purchase Return & allowances:</th>
                          <th></th>
                          <th></th>
                          <th></th>
                        </tr>
                        <tr>
                          <td>&nbsp;&nbsp;&nbsp;&nbsp;{{$returndata['Purchase Return']['account_head']}}:</td>
                          <td>({{$returndata['Purchase Return']['amount']}})</td>
                          <th></th>
                          <th></th>
                        </tr>
                        <tr>
                          <td>&nbsp;&nbsp;&nbsp;&nbsp;{{$returndata['Discount on Purchase']['account_head']}} :</td>
                          <td style="border-bottom: black 2px solid;">({{$returndata['Discount on Purchase']['amount']}})</td>
                          <td></td>
                          <th></th>
                        </tr>
                        <tr>
                          <th class="text-right">Net Purchases</th>
                          <th></th>
                          
                          <th>{{$netPurchase=$purchase-($returndata['Purchase Return']['amount']+$returndata['Discount on Purchase']['amount'])}}</th>
                          <th></th>
                        </tr>
                        <tr>
                          <td>&nbsp;&nbsp;&nbsp;&nbsp;Ending Inventory</td>
                          <th></th>
                          <td style="border-bottom: black 2px solid;">({{$eInventory=$returndata['endingInventory']}})</td>
                          <th></th>
                        </tr>
                        <tr>
                          <td>Total Cost of Goods Sold</td>
                          <th></th>
                          <td></td>
                          <td style="border-bottom: black 2px solid;">({{$cogs=($bInventory+$netPurchase-$eInventory)}})</td>
                         
                        </tr>
                        <tr class="table-info">
                          <th><u>Gross Income:</u></th>
                          <th></th>
                          <th></th>
                          <th>@php
                              $grossProfit=$netSale-$cogs;
                              @endphp
                              {{number_format($grossProfit,2)}}</th>
                        </tr>
                        {{-- operating Expenses --}}
                        <tr>
                          <th><u>Less:Operating Expenses:</u></th>
                          <th></th>
                          <th></th>
                          <th></th>
                        </tr>
                        @php  $sumAmount=0; @endphp
                        @forelse ($operatingExp as $opEx)
                        <tr>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;{{$opEx['account_head']}}</td>
                          <td></td>
                          @php  $sumAmount+=$opEx['amount']; @endphp
                        <td>{{$opEx['amount']}}</td>
                          <td></td>
                        </tr> 
                        @empty
                            
                        @endforelse
                        <tr>
                          <td>Total Operating Expenses</td>
                          <th></th>
                          <td style="border-top: black 2px solid;"></td>
                          <td style="border-bottom: black 2px solid;">({{$sumAmount}})</td>
                        
                        </tr>
                        <tr class="table-info">
                          <th><u>Income Before Tax:</u></th>
                          <th></th>
                          <th></th>
                          <th>{{number_format(($grossProfit-$sumAmount),2)}}</th>
                        </tr>
                      </tbody>
                      
                    <tfoot>
                           
                          </tfoot>
                    </table>
                  </div>
		</div>
	  </div>
	</div>
  </div>
</div>

</div>
@endsection