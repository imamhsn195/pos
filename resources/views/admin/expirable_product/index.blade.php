@extends('layouts.admin')

@section('content')

<div class="row">
<div class="col-md-12 d-flex align-items-stretch grid-margin">
  <div class="row flex-grow row_table_responsive">
	<div class="col-md-12">
	  <div class="card">
		<div class="card-body">
		  <h4 class="text-center">Expirable Product Details</h4>
		  <div class="table-responsive">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>ID</th>
                          <th>Created at</th>
                          <th>Product Name</th>
                          <th>Status</th>
                          <th>Amount</th>
                        </tr>
                      </thead>
                      <tbody>
					  
						
                        <?php $i =1; ?>
                        
					  @forelse($expirable_product_detail as $ep)
                        <tr>
                          <td>{{$i++}}</td>  
                          <td>{{date('d-M-Y',strtotime($ep->created_at))}}</td>
                          <td>{{$ep->purchase->product->product_sku}}</td>
                          <td>{{$ep->expire_date}}</td>
                          <td>
                             
                            @if($ep->expire_date)
                            <span class="badge badge-warning">Pending</span>
						              {{-- @elseif($ep->expire_date == 'supplier')
                          <span class="badge badge-danger">Paid</span>
                          @else
                        <span class="badge badge-success">Received</span> --}}
                        @endif
                        <?=$date_select = date("Y-m-d", strtotime('10 days') ); ?>
                          </td>
					@empty
						<tr>
							<td colspan="8">There is no records available</td>
						</tr>
					@endforelse
                      </tbody>
                    </table>
                  </div>
		</div>
	  </div>
	</div>
  </div>
</div>

</div>

@endsection