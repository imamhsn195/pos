@extends('layouts.admin')

@section('content')

<div class="row">
    <div class="col-md-6 d-flex align-items-stretch grid-margin">
        <div class="row flex-grow">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title text-center">Edit Customer</h4>
                        <br>
                        <form class="forms-sample" method="post" action="{{route('customers.update',$editid)}}">
                            @csrf
                            @method('put')
                            <div class="form-group">
                                <label for="customer_name">Customer Name</label>
                                <input type="text" class="form-control" name="customer_name" id="customer_name" value=" {{$cust->customer_name}}"
                                    placeholder="Enter Name">
                            </div>

                            <br>

                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="text" class="form-control" name="email" id="email" value="{{$cust->address->email}}"
                                    placeholder="Enter Email Address">
                            </div>

                            <br>

                            <div class="form-group">
                                <label for="phone">Phone</label>
                                <input type="number" class="form-control" name="phone" id="phone" value="{{$cust->address->phone}}"
                                    placeholder="Enter Phone Number">
                            </div>

                            <br>

                            <div class="form-group">
                                <label for="credit_limit">Credit Limit</label>
                                <input type="number" class="form-control" name="credit_limit" id="credit_limit" value="{{$cust->credit_limit?$cust->credit_limit:''}}"
                                    placeholder="Enter Credit Limit">
                            </div>

                            <br>

                            <div class="form-group">
                                <label for="customer_group">If Any Group Available!</label>
                                <select name="customer_group_id" class="form-control">
                                    <option value="">Select Group</option>
                                    @forelse($cusgroup as $cg)
                                    <option value="{{$cg->id}}" {{$cg->id==$cust->customer_group_id?'selected':''}}>{{$cg->customer_group_title}}</option>
                                    @empty
                                    <option>{{'No group available'}}</option>
                                    @endforelse
                                </select>
                            </div>

                            <br>

                            <button type="submit" class="btn btn-success mr-2">Submit</button>
                            <a href="{{route('customers.index')}}" class="btn btn-danger">Cancel</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection
