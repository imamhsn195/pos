@extends('layouts.admin')

@section('content')

<div class="row">
    <div class="col-md-10 d-flex align-items-stretch grid-margin">
        <div class="row flex-grow">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">

                        <h1 class="text-center">{{$customer->customer_name}}'s Invoice Information</h1>
                        <h3 class="text-center">
                            @if (Auth::user()->user_type == 'superadmin')
                            All Branches
                            @else
                            {{Auth::user()->branch->name}}
                            @endif
                        </h3>
                        <h3 class="text-center">
                            @php
                            $searchbyfrom = date('Y-m-01');
                            $searchbyto = date('Y-m-t');
                            @endphp
                            @if(isset($_GET['searchbyfrom']) && $_GET['searchbyfrom']!='')
                            @php
                            $searchbyfrom = $_GET['searchbyfrom'];
                            $searchbyto = $_GET['searchbyto'];
                            @endphp
                            @endif
                            <form method="post" action="{{route('pdfreport')}}" id="downloadpdf">
                                @csrf
                                <input type="hidden" value="{{$searchbyfrom}}" name="searchbyfrom"
                                    id="example-search-input1">
                                <input type="hidden" value="{{$searchbyto}}" name="searchbyto"
                                    id="example-search-input2">
                                <input type='hidden' id='format' name='format' />
                            </form>
                        </h3>

                        <h4 class="text-center">
                            {{"Showing Reports From "}}<b>{{$searchbyfrom}}</b>{{" To "}}<b>{{$searchbyto}}</b>
                        </h4>

                        <form method="get" action="{{url('').'/customers/'.Request::segment(2)}}">
                            <div class="input-group col-md-6 float-right">
                                <label for="example-search-input1">From :</label>
                                <input class="form-control py-2" value="{{$searchbyfrom}}" name="searchbyfrom"
                                    type="date" id="example-search-input1">
                                <label for="example-search-input2">To :</label>
                                <input class="form-control py-2" value="{{$searchbyto}}" name="searchbyto" type="date"
                                    id="example-search-input2">
                                <span class="input-group-append">
                                    <button class="btn btn-primary" type="submit">
                                        <i class="mdi mdi-magnify"></i>
                                    </button>
                                </span>
                            </div>
                        </form>
                        <div class="table-responsive">
                            <table class="table table-bordered table-responsive">
                                <thead>
                                    <tr>
                                        <th>Transaction ID</th>
                                        <th>Date</th>
                                        <th>Account Title</th>
                                        <th>Event</th>
                                        <th>description</th>
                                        <th>Debit</th>
                                        <th>Credit</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
            
                                      $amount=0;
                                      $damount=0;
                                      //$tspan=2;
                                    ?>
                                    @forelse($customer_status as $jr)
                                    <tr>


                                        <td>{{$jr->transaction_id}}</td>
                                          <?php $time=strtotime($jr->created_at);
                                              $date=date("d-M-Y",$time);
                                          ?>
                                        <td>{{$date}}</td>


                                        <td>{{$jr->account->account_head}}</td>
                                        <td>{{$jr->transaction->event_type}}</td>
                                        <td>{{$jr->description}}</td>


                                        @if($jr->journal_type==0)
                                        <td>{{number_format($jr->amount,2)}}</td>
                                        <?php $damount=$damount+$jr->amount;?>
                                        @else
                                        <td></td>
                                        @endif
                                        @if($jr->journal_type==1)
                                        <td>{{number_format($jr->amount,2)}}</td>
                                        <?php $amount=$amount+$jr->amount;?>
                                        @else
                                        <td></td>
                                        @endif

                                    </tr>
                                    @empty
                                    <tr>
                                        <td colspan="8">There is no records available</td>
                                    </tr>
                                    @endforelse
                                </tbody>
                                <tfoot>
                                    @php $getable=0;
                                    $got=0; @endphp
                                    <tr>
                                        <th colspan="5" class="text-right">Total Transaction :</th>
                                        <th>{{number_format(($damount),2)}}</th>
                                        <th>{{number_format(($amount),2)}}</th>
                                    </tr>
                                    @php $getable+=$damount;
                                    $got+=$amount;
                                    @endphp
                                    <tr class="table-active">
                                        <th colspan="5" class="text-right">DateWise Balance :</th>
                                        <th colspan="2" class="text-right">{{number_format(($damount-$amount),2)}}</th>
                                    </tr>
                                    {{-- @endif
                                    @if($getable<$got) <tr class="table-danger">
                                        <th colspan="5" class="text-right">Amount To Be Paid :</th>
                                        <th colspan="2" class="text-right">{{number_format(($amount-$damount),2)}}</th>
                                        </tr>
                                        @endif --}}
                                   
                                    <tr class="{{$previousBalance>0 ? 'table-success' : 'table-danger'}}">
                                        <th colspan="5" class="text-right">{{$previousBalance>0?'Amount To Be Received :' : 'Amount To Be Paid '}}</th>
                                        <th colspan="2" class="text-right">{{number_format(($previousBalance>0?$previousBalance:($previousBalance*-1)),2)}}</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection