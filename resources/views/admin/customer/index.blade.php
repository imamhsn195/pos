@extends('layouts.admin')

@section('content')

<div class="row">
    <div class="col-md-12 d-flex align-items-stretch grid-margin">
        <div class="row flex-grow row_table_responsive">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                            <h2 class="text-center">All Customers</h2>
                            <div class="row">
                                {{-- <div class="col-md-6"></div> --}}
                                <div class="col-md-12">
                                    <form action="{{route('customers.index')}}" method="GET">
                                      <div class="input-group float-right">
                                          <input class="form-control py-2 border-primary" placeholder="Search here.." name="customer_name" type="text" id="example-search-input" required="">
                                          <span class="input-group-append">
                                            <button class="btn btn-primary" type="submit">
                                                <i class="mdi mdi-magnify"></i>
                                            </button>
                                            <a class="btn btn-primary" title="Register New Customers" href="{{route('customers.create')}}"><i class="mdi mdi-playlist-plus"></i></a>  
                                            @if (request()->query())
                                            <a class="btn btn-primary" title="Show All Customers" href="{{route('customers.index')}}"><i class="mdi mdi-format-list-bulleted"></i></a> 
                                            @endif
                                          </span>
                                      </div>
                                    </form>
                                </div>
                              </div>
                          <br>
                      
                      
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered border-primary">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Phone</th>
                                        <th>Email</th>
                                        {{-- <th>Group</th>
                                         <th>Discount(%)</th> --}}
                                         <th>Credit Limit</th>
                                        {{-- <th>Created By</th> --}}
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse($cusgroup as $key => $customer)
                                    <tr>
                                        <td>{{(($cusgroup->currentPage() - 1) * $cusgroup->perPage() + $key+1)}}</td>
                                        <td>{{$customer->customer_name}}</td>
                                        <td>{{$customer->address->phone or "-"}}</td>
                                        <td>{{$customer->address->email or "-"}}</td>
                                        {{-- <td>{{$customer->groups->customer_group_title or "-"}}</td>
                                        <td>{{$customer->groups ? $customer->groups->discount_percentage*100 : "-"}}</td> --}}
                                        <td>{{$customer->credit_limit}}</td>
                                        {{-- <td>{{$customer->users->name}}</td> --}}
                                        <td>
                                        <span class="input-group-append">
                                            <a href="{{route('customers.show',$customer->id)}}" class="btn btn-dark btn-sm" title="Account Status"> <i class="mdi mdi-book-open"></i></a>
                                            <a href="{{route('customer_invoice_show',$customer->id)}}" class="btn btn-info btn-sm" title="Invoice Status">
                                                    <i class="mdi mdi-newspaper"></i>
                                                </a>
                                            <a href="{{route('customers.edit',$customer->id)}}" class="btn btn-dark btn-sm" title="Edit Customer"><i class="mdi mdi-table-edit"></i></a>
                                           
                                            <button type="button" 
                                            @if($customer->id==1) {{'disabled'}} @endif
                                            class="btn btn-primary"  data-toggle="modal" data-target="#myModal" title="Add Previous Customer Amount" onclick='addId({{$customer->id}})'><i class="mdi mdi-bank"></i></button>
                                        </span>
                                        </td>
                                        <td>
                                            {{-- delete should be active/inactive --}}
                                            {{-- <form action="{{route('customers.destroy',$customer->id)}}" method="post">
                                                @csrf
                                                @method('delete')
                                                <input type="submit" value="Delete" onclick="return confirm('Are you sure to delete this??')"
                                                    class="btn btn-link" />
                                            </form> --}}
                                        </td>
                                    </tr>
                                    @empty
                                    <tr>
                                        <td colspan="8">There is no records available</td>
                                    </tr>
                                    @endforelse
                                </tbody>
                            </table>
                            
                            <span class="float-right"> {{$cusgroup->appends(request()->query())->links()}}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
{{-- model for update or increase amount --}}
<div class="modal fade" id="myModal">
    <div class="modal-dialog">
		<div class="modal-content">
      
        <!-- Modal Header -->
				<div class="col-12">
                  <div class="card">
                    <div class="card-body">
                      <h4 class="card-title">Add Beginning Balance<button type="button" class="close" data-dismiss="modal">&times;</button></h4>
                      <p class="card-description">
                      </p>
                      <form class="forms-sample" method="POST" action="{{url('customer_beginning_balance')}}">
                      @csrf
                      @if(Gate::allows('isSuperadmin'))
                      <div class="form-group">
                        <label for="branch_id">Branch</label>
                        <select required class="form-control" id="branch_id" name="branch_id">
                            <option value="">Select branch</option>
                            @forelse($branches as $bp)
                            <option value="{{$bp->id}}">{{$bp->name}}</option>
                            @empty
                            <option>{{'There is no branch'}}</option>
                            @endforelse
                        </select>
                    </div>
                    @else
                    <input type="hidden" name="branch_id" id="branch_id" value="{{ Auth::user()->branch_id }}">
                    @endif
        
                    
                    <input type="hidden" name="customer_id" id="customer_id" value="">
						
                        
                        <div class="form-group">
                            <label for="paid_amount">Amount</label>
                            <input type="number" class="form-control" name="paid_amount" id="paid_amount" value="{{ old('paid_amount') }}" step=".01">
                              @if ($errors->any())
                                      @foreach($errors->all() as $error)
                                      <span class="alert text-danger">**{{ $error }}
                                      </span>
                                      @endforeach
                                  @endif
                          </div>
						  <button type="submit" class="btn btn-success mr-2">Submit</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                      </form>
                    </div>
						
                  </div>
                </div>
        
        
		</div>
    </div>
  </div>
{{-- model for update or increase amount --}}
@endsection
@push('js')
        <script>
        function addId(id){
            $('#customer_id').val(id);
        }
        </script>    
@endpush