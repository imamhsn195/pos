@extends('layouts.admin')
    @push('css')
        <style>
            .table td.address{
                vertical-align: middle;
                font-size: 13px;
                line-height: 1.5; 
                white-space: normal;
               
            }
        </style>
    @endpush
@section('content')

<div class="row">
    <div class="col-md-12 d-flex align-items-stretch grid-margin">
        <div class="row flex-grow row_table_responsive">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h2 class="text-center"> Customer Month-wise Reports</h2>
                        @if(Auth::user()->user_type=="superadmin")
                        <h4 class="text-center">Showing All Branches</h4>
                        @else
                        <h4 class="text-center">Showing {{Auth::user()->branch->name}} Branch</h4>
                        @endif
                        <p class="card-description text-center">
                            For the month of {{request('month')? date('F-Y', strtotime(request('month'))):date('F-Y') }}
                            <form method="get" action="{{url('').'/monthly_status_of_customer'}}">
                                <div class="input-group col-md-6 float-right">
                                    <label for="example-search-input2">Month :</label>
                                    <input class="form-control py-2" name="month" type="month" id="example-search-input2" value="{{request('month')? date('Y-m', strtotime(request('month'))):date('Y-m') }}">
                                    <span class="input-group-append">
                                        <button class="btn btn-primary" type="submit">
                                            <i class="mdi mdi-magnify"></i>
                                        </button>
                                        {{-- <a href="{{route('record_expense')}}" class="btn btn-primary">Add New</a > --}}
                                    </span>
                                </div>
                            </form>
                        </p>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>SL</th>
                                        <th>Name</th>
                                        <th>Sales</th>
                                        <th>Received</th>
                                        <th class="address">Current Debit</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i =1; ?>
                                    @forelse($customers as $customer)
                                    <tr>
                                        <td>{{$i++}}</td>
                                        <td>{{$customer->customer_name}}</td>
                                       
                                            @php
                                                $sales = 0; 
                                                $returned = 0; 
                                                $received = 0;
                                                $current = 0;
                                                $fdate=request('month')?strtotime(request('month')):strtotime(date('Y-m').'-01');
                                                $tdate=request('month')?strtotime(request('month')):strtotime(date('Y-m-t'));
                                                $fdate=date("Y-m-d",$fdate);
                                                $tdate=date("Y-m-t",$tdate);
                                            @endphp
                                            {{-- <td>{{$fdate}}-{{$tdate}}</td> --}}
                                            @foreach ($customer->journal as $js)
                                                @if($js->created_at >= $fdate &&  $js->created_at <= $tdate)
                                                    @if ($js->accounts_id ==4 && $js->journal_type == 1)
                                                        @php
                                                            $sales = $sales + $js->amount;
                                                        @endphp
                                                    @elseif($js->accounts_id ==17 && $js->journal_type == 0)
                                                        @php
                                                            $returned = $returned + $js->amount;
                                                        @endphp
                                                    {{-- @elseif($js->accounts_id ==6)
                                                        @php
                                                            $paid = $paid + $js->amount;
                                                        @endphp --}}
                                                    @elseif($js->journal_type == 0 && ($js->accounts_id ==2||$js->accounts_id ==10) )
                                                    @php
                                                        $received = $received + $js->amount;
                                                    @endphp
                                                    
                                                    @endif
                                                @endif
                                                
                                            @endforeach
                                        <td class="text-success">
                                            {{$sales-$returned}}
        
                                        </td>
                                        <td class="text-danger">{{$received}}</td>
                                        <td class="text-danger">
                                            @foreach ($customer->journal as $js)
                                                @if ($js->journal_type == 0 && $js->accounts_id==5)
                                                   @php
                                                       $current = $current + $js->amount;
                                                   @endphp
                                                @elseif ($js->journal_type == 1 && $js->accounts_id==5)
                                                   @php
                                                       $current = $current - $js->amount;
                                                   @endphp
                                                @endif
                                            @endforeach 
                                            {{number_format($current,2)}}
                                        </td>
                                        {{-- <td class="{{$debited-$cashOut>1?"text-info":"text-danger"}}"><b>{{$debited - $cashOut}}</b></td> --}}
                                        <td>
                                           
                                            <a href="{{route('customers.show',$customer->id).'?searchbyfrom='.(request('month')!=null?date('Y-m-01',strtotime(request('month'))):date('Y-m-01')).'&searchbyto='.(request('month')!=null?date('Y-m-t',strtotime(request('month'))):date('Y-m-t'))}}" class="btn btn-success btn-sm" title="Account Status">
                                                <i class="mdi mdi-book-open"></i>
                                            </a>
                                            <a href="{{route('customer_invoice_show',$customer->id).'?searchbyfrom='.(request('month')!=null?date('Y-m-01',strtotime(request('month'))):date('Y-m-01')).'&searchbyto='.(request('month')!=null?date('Y-m-t',strtotime(request('month'))):date('Y-m-t'))}}" class="btn btn-info btn-sm" title="Invoice Status"> <i class="mdi mdi-newspaper"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    @empty
                                    <tr>
                                        <td colspan="8">There is no records available</td>
                                    </tr>
                                    @endforelse
                                </tbody>
                            </table>
                            {{$customers->appends(request()->query())->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection
@push('js')
        <script>
        function addId(id){
            $('#supplier_id').val(id);
        }
        </script>    
@endpush
