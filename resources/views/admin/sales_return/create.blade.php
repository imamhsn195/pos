@extends('layouts.admin')

@section('content')
<div class="row">
<div class="col-md-12 d-flex align-items-stretch grid-margin">
  <div class="row flex-grow">
	<div class="col-md-7">
	  <div class="card">
		<div class="card-body">
            <h4 class="card-title">Sales Return from Here</h4>
            <form class="forms-sample" method="post" action="{{route('sales_return.store')}}" enctype="multipart/form-data">
              @csrf
           
              <div class="form-group">
                <label for="supplier_id">Customer</label>
                <select  class="form-control" id="customer_id" name="customer_id" onchange="get_invoice()">
                <option value="">Select Customer</option>
               @forelse($customers as $sp)
               <option value="{{$sp->id}}">{{$sp->customer_name}}</option>
               @empty
               <option>{{'There is no Customer'}}</option>
               @endforelse
               </select>
             </div>

            <div class="form-group" id="invoice_no_select">
            </div>
            
            
          
             <div class="form-group" id="product_select">
               
                
                
              </div>
              <input type="hidden" class="form-control" name="purchase_id" id="purchase_id" value="">
                {{-- <input type="hidden" class="form-control" name="payment_method_id" id="payment_method_id" value="0"> --}}
                <input type="hidden" class="form-control" name="branch_inv_id" id="branch_id" value="">
                <input type="hidden" class="form-control" name="discount_rate" id="discount_rate" value="">

              <div class="form-group">
                <label for="returned_qty">Sold Quantity</label>
                <input type="number" class="form-control" name="returned_qty" id="returned_qty" onchange="getprice()">
                <input type="hidden" class="form-control" name="remain_qty" id="remain_qty">
              </div>
              <div class="form-group">
                <label for="discount">Discount</label>
                <input type="number" class="form-control" name="discount" value="0" id="discount" onchange="getprice()">
              </div>
              <div class="form-group">
                <label for="p_unit_price">Sold per Unit Price</label>
                <input readonly type="number" class="form-control" name="p_unit_price" id="p_unit_price" onchange="getprice()">
              </div>
            
           <div class="form-group">
                <label for="total_price">Amount to be paid</label>
                <input type="number" class="form-control" name="total_price" id="total_price" value="" readonly>
            </div>
            <div class="form-group">
              <label for="payment_method_id">Payment Method</label>
              <select required class="form-control" id="payment_method_id" name="payment_method_id"  onchange="addpaidvalue()">
              <option value="">Select Payment Method</option>
              <option value="0 | 0">Due</option>
               @foreach($payment_methods as $pm)
               <option value="{{$pm->id." | ".$pm->method_type}}">{{$pm->method_title}}</option>
               @endforeach
             </select>
           </div>
           <!--form group for paid amount-->
            <div class="form-group" id="payment_tk">
               
            </div>
            <div class="form-group" id="date_field">
               
            </div>
            
          
           <button type="submit" class="btn btn-success mr-2">Submit</button>
           <a class="btn btn-light" href="{{route('purchase_returns.index')}}">Cancel</a>
         </form>
       </div>
	  </div>
	</div>
 
  </div>
</div>

</div>


<script>
//multiply unit price n qty
 function getprice(){
 var p_unit_price = document.getElementById("p_unit_price").value;
 var discount = document.getElementById("discount").value;
 var returned_qty = document.getElementById("returned_qty").value;
 var total_price =returned_qty*(p_unit_price-discount);
  
    $('#total_price').val(total_price);
 }




 //check for invoice and supplier basis purchased history.
 function get_sales_history(){
  
var sales_id= document.getElementById("invoice_no").value;
var customer_id = document.getElementById("customer_id").value;
var product_id = document.getElementById("product_id").value;
var histry=product_id;
//check for warranty
 if(sales_id && customer_id){
          $.ajax({
                type:'GET',
                url:"<?=url('sales_history_for_return')?>",
                data:"histry="+histry,
                success:function(html){
                  var discountOnTotal=(html.sale.discount_total+html.sale.regular_discount_total)/html.sale.total_amount;
                  var discountPerQty=Math.round((html.sold_unit_price*discountOnTotal)+(html.discount_subtotal/html.sold_quantity));

                    $('#purchase_id').attr('value',html.purchase_id);
                    $('#returned_qty').attr('value',html.sold_quantity);
                    $('#remain_qty').attr('value',html.sold_quantity);
                    $('#p_unit_price').attr('value',html.sold_unit_price);
                    $('#discount').attr('value',discountPerQty);
                    $('#total_price').attr('value',(html.sold_unit_price-discountPerQty)*html.sold_quantity);
                    $('#branch_id').attr('value',html.branch_id);
                    
                    $('#discount_rate').attr('value',discountOnTotal);
                    
                  }
            });
        }else{
          alert("select Customer & invoice No First");
        }
 }


 //input field for invoice
 function get_invoice(){
  $('#invoice_no_select').empty();
    var customer_id = document.getElementById("customer_id").value;
    $.ajax({
            type:'GET',
            url:"<?=url('sales_history_for_return')?>",
            data:"customer_id="+customer_id,
            success:function(html){
              var showProduct="<label for='invoice_no'>Invoice</label><select name='invoice_no' id='invoice_no' class='form-control' onchange='get_product()'><option value=''>Select Invoice No.</option>";
                    var i=0;
                    var getLen=html.length;
                    while(i<getLen){
                      showProduct+="<option value='"+html[i].id+"'>"+html[i].invoice_no+"</option>";
                      i++;
                    }
                    showProduct+="</select>";
                    $('#invoice_no_select').append(showProduct);
                    
                }
    });
    }

 function get_product(){
  $('#product_select').empty();
    var sales_id= document.getElementById("invoice_no").value;
    var histry=sales_id;
    $.ajax({
            type:'GET',
            url:"<?=url('sales_history_for_return')?>",
            data:"condition="+histry,
            success:function(html){
              console.log(html.length);
              var showProduct="<label for='product_id'>Product</label><select name='product_id' id='product_id' class='form-control' onchange='get_sales_history()'><option value=''>Select Product</option>";
                    var i=0;
                    var getLen=html.length;
                    while(i<getLen){
                      showProduct+="<option value='"+html[i].product.id+'|'+html[i].id+"'>"+html[i].product.product_sku+"</option>";
                      i++;
                    }
                    showProduct+="</select>";
                    $('#product_select').append(showProduct);
                }
    });
    }
    function addpaidvalue() {
      var payment_method_id = document.getElementById("payment_method_id").value;
      $('#payment_tk').empty();
      payment_method_id = payment_method_id.split(" | ");
      if (payment_method_id[0]>0) {
          var paid_amount = "<label for='paid_amount'>Paid Amount</label>";
          paid_amount += "<input type='number' class='form-control' name='paid_amount' id='paid_amount'>";
          if (payment_method_id[1] == 1) {
              paid_amount +=
                  "<label for='check_no'>Check No <i class='text-danger'>( Optional )</i></label><input type='text' class='form-control' name='check_no' id='check_no' onchange='check_disposal_field()'>";
          }
          $('#payment_tk').prepend(paid_amount);
        }
}
//extra field for cheque disposal date 
function check_disposal_field() {
    var payment_method_id = document.getElementById("payment_method_id").value;
    $('#date_field').empty(paid_amount);
    payment_method_id = payment_method_id.split(" | ");
    if (payment_method_id[1] == 1) {
        var paid_amount =
            "<label for='cheque_disposal_date'>Cheque Disposal Date <i class='text-danger'>( Set if not Today )</i></label>";
        paid_amount +=
            "<input type='date' class='form-control' name='cheque_disposal_date' id='cheque_disposal_date'>";
        $('#date_field').prepend(paid_amount);
    }
  }
      
</script>
@endsection