@extends('layouts.admin')

@section('content')

<div class="row">
<div class="col-md-12 d-flex align-items-stretch grid-margin">
  <div class="row flex-grow row_table_responsive">
	<div class="col-md-12">
	  <div class="card">
		<div class="card-body">
      <h2 class="text-center">Sales Return History</h2>
      @if(Auth::user()->user_type=="superadmin")
            <h4 class="text-center">Showing All Branches</h4>
            @else
            <h4 class="text-center">Showing {{Auth::user()->branch->name}} Branch</h4>
            @endif
            <p class="card-description text-center">
                <form method="get" action="{{route('sales_return.index')}}">
                    <div class="input-group col-md-6 float-right">
                        <label for="example-search-input1">From :</label>
                        <input class="form-control py-2" @if(isset($_GET['searchbyfrom']))
                            value="{{$_GET['searchbyfrom']}}" @else value="{{date('Y-m-01')}}" @endif
                            name="searchbyfrom" type="date" id="example-search-input1">
                        <label for="example-search-input2">To :</label>
                        <input class="form-control py-2" placeholder="To.." name="searchbyto" type="date"
                            id="example-search-input2" @if(isset($_GET['searchbyto']))
                            value="{{$_GET['searchbyto']}}" @else value="{{date('Y-m-t')}}" @endif>
                        <span class="input-group-append">
                            <button class="btn btn-primary" type="submit">
                                <i class="mdi mdi-magnify"></i>
                            </button>
                        </span>

                    </div>
                </form>
            </p>
		  <div class="table-responsive">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>ID</th>
                          <th>Date</th>
                          <th>Product Sku</th>
                          <th class="text-right">Qty</th>
                          <th class="text-right">Unit price</th>
                          <th class="text-right">Discount</th>
                          <th class="text-right">SubTotal</th>
                          <th>Customer</th>
                          <th>Accepted by</th>
                        </tr>
                      </thead>
                      <tbody>
            <?php $i =1;
            $total=0; ?>
					  @forelse($sales_returns as $pr)
              <tr>
                <td>{{$i++}}</td>
                  <?php $time=strtotime($pr->created_at);
                  $date=date("d-M-Y",$time);
                  $timm=date("h:i:s",$time);?>
                <td title="{{$timm}}">{{$date}}</td>
                <td>{{ rtrim($pr->sale_details->product->product_sku,'|') }}</td>
                <td class="text-right">{{$pr->sold_return_quantity}}</td>
                <td class="text-right">{{$pr->sold_return_unit_price}}</td>
                <td class="text-right">{{$pr->returned_discount_rate}}</td>
                <?php
                $subtotal=$pr->sold_return_quantity*($pr->sold_return_unit_price-$pr->returned_discount_rate);
                $total=$total+$subtotal;?>
                <td  class="text-right">{{number_format($subtotal,2)}}</td>
                <td >{{$pr->sale_details->sale->customer->customer_name}}</td>
                <td title="{{$pr->users->name}}">{{$pr->users->user_type}}</td>
              </tr>
					@empty
						<tr>
							<td colspan="8">There is no records available</td>
						</tr>
					@endforelse
            </tbody>
            <tfoot>
              <th colspan="6" class="text-center">Total sales Return Amount:</th>
              <th class="text-right">Tk. {{number_format($total,2)}}</th>
              <th></th>
              <th></th>
            </tfoot>
          </table>
        </div>
		</div>
	  </div>
	</div>
  </div>
</div>

</div>

@endsection