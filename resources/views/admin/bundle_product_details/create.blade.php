@extends('layouts.admin')

@section('content')



<div class="row">
    <div class="col-md-6 d-flex align-items-stretch grid-margin">
        <div class="row flex-grow">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Add New Bundle Product</h4>
                        <form class="forms-sample" method="post" action="{{route('bundle_product_details.store')}}">
                            @csrf

                            <div class="form-group">
                                <label for="choose_attribute">Choose Bundle Name : </label>
                                <select class="form-control" name="bundle_products_id">
                                    <option value="">Select Bundle Name</option>
                                    @forelse($bundle_products as $bp)
                                    <option value="{{ $bp->id }}">{{ $bp->bundle_name }}</option>
                                    @empty
                                    <a href="{{route('bundle_product.create')}}" class="link link-info">Add Bundle
                                        Product First</a>
                                    @endforelse
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="choose_attribute">Select Bundle Products</label>
                                <select multiple class="form-control" name="choose_attribute[]">
                                    @forelse($products as $product)
                                    <option value="{{ $product->id }}">{{ $product->product_sku }}<img src="{{asset('upload/product_img/'.$product->image)}}"
                                            width="20px"></option>
                                    @empty
                                    <a href="{{route('products.create')}}" class="link link-info">Add Product First</a>
                                    @endforelse
                                </select>
                                <small> <i class="alert text-danger">"Ctrl+Click"</i> to Select Multiple Atrribute</small>
                            </div>

                            <button type="submit" class="btn btn-success mr-2">Submit</button>
                            <a href="{{route('bundle_product.index')}}" class="btn btn-primary mr-2">Back</a>
                            <button type="reset" class="btn btn-light">Cancel</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection