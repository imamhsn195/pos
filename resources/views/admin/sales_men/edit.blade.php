@extends('layouts.admin')

@section('content')

<div class="row">
    <div class="col-md-6 d-flex align-items-stretch grid-margin">
        <div class="row flex-grow">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title" align="center"> Edit Supplier Form</h4>
                        <br>
                        <form class="forms-sample" method="post" action="{{route('suppliers.update',$esupplier[0]->id)}}">
                            @csrf
                            @method('put')
                            <div class="form-group">
                                <label for="supplier_name">Supplier</label>
                                <input type="text" class="form-control" name="supplier_name" id="supplier_name" value=" {{$esupplier[0]->supplier_name}}"
                                    placeholder="Enter Supplier Name">
                            </div>

                            <div class="form-group">
                                <label for="email">Email Address</label>
                                <input type="email" class="form-control" name="email" id="email" value=" {{$esupplier[0]->address->email}}"
                                    placeholder="input a valid email id">
                            </div>

                            <div class="form-group">
                                <label for="phone">Contact No</label>
                                <input type="text" class="form-control" name="phone" id="phone" value=" {{$esupplier[0]->address->phone}}"
                                    placeholder="e.g. 018XX-XXXXXX">
                            </div>

                            <div class="form-group">
                                <label for="fulladdress">Address Information</label>
                                <textarea class="form-control" name="full_address" id="full_address" placeholder="Enter fulladdress">{{$esupplier[0]->address->full_address}}</textarea>
                            </div>

                            <button type="submit" class="btn btn-success mr-2">Submit</button>
                            <a href="{{route('suppliers.index')}}" class="btn btn-danger">Cancel</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection
