@extends('layouts.admin')

@section('content')

<div class="row">
    <div class="col-md-6 d-flex align-items-stretch grid-margin">
        <div class="row flex-grow">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title" align="center"> Create new Sales Person form</h4>

                        <br>

                        <form class="forms-sample" method="post" action="{{route('sales_man.store')}}">
                            @csrf
                            <div class="form-group">
                                <label for="name">SalesPerson</label>
                                <input type="text" class="form-control" name="name" id="name"
                                    placeholder="Enter Sales Person Name">
                            </div>

                            <div class="form-group">
                                <label for="email">Email Address</label>
                                <input type="email" class="form-control" name="email" id="email" placeholder="input a valid email id">
                            </div>

                            <div class="form-group">
                                <label for="phone">Contact No</label>
                                <input type="text" class="form-control" name="phone" id="phone" placeholder="e.g. 018XX-XXXXXX">
                            </div>

                            <div class="form-group">
                                <label for="address">Address Information</label>
                                <textarea class="form-control" name="address" id="address" placeholder="Enter fulladdress"></textarea>
                            </div>
                            @if(Gate::allows('isSuperadmin'))
                            <div class="form-group">
                                <label for="branch_id">Branch</label>
                                <select required class="form-control" id="branch_id" name="branch_id">
                                    <option value="">Select branch</option>
                                    @forelse($branches as $bp)
                                    <option value="{{$bp->id}}">{{$bp->name}}</option>
                                    @empty
                                    <option>{{'There is no branch'}}</option>
                                    @endforelse
                                </select>
                            </div>
                            @else 
                                <input type="hidden" name="branch_id" id="branch_id" value="{{ Auth::user()->branch_id }}">
                            @endif

                            <button type="submit" class="btn btn-success mr-2">Submit</button>
                            <a href="{{route('suppliers.index')}}" class="btn btn-danger">Cancel</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection
