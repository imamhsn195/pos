@extends('layouts.admin')
    @push('css')
        <style>
            .table td.address{
                vertical-align: middle;
                font-size: 13px;
                line-height: 1.5; 
                white-space: normal;
               
            }
        </style>
    @endpush
@section('content')

<div class="row">
    <div class="col-md-12 d-flex align-items-stretch grid-margin">
        <div class="row flex-grow row_table_responsive">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Sales Person List</h4>
                        <a class="btn btn-info pull-right" href="{{route('sales_man.create')}}">Create Sales Person</a>
                        <form method="get" action="{{route('sales_man.index')}}">
                            <div class="input-group col-md-4 float-right">
                                <input class="form-control py-2" placeholder="Search here.." name="searchby" type="text"
                                    id="example-search-input">
                                <span class="input-group-append">
                                    <button class="btn btn-primary" type="submit">
                                        <i class="mdi mdi-magnify"></i>
                                    </button>
                                </span>

                            </div>
                        </form>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>SL</th>
                                        <th>SalesPerson</th>
                                        <th>Email</th>
                                        <th>Contact No.</th>
                                        <th class="address">Full Address</th>
                                        <th class="text-center">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i =1; ?>
                                    @forelse($addresses as $add)
                                    <tr>
                                        <td>{{$i++}}</td>
                                        <td>{{$add->name}}</td>
                                        <td>{{$add->email}}</td>
                                        <td>{{$add->phone}}</td>
                                        <td class="address">{{$add->address}}</td>
                                        <td>
                                           
                                            <form action="{{route('sales_man.destroy',$add->id)}}" method="post"> 
                                            <a href="{{route('sales_man.show',$add->id)}}" class="btn btn-success btn-sm" title="Account Status">
                                                <i class="mdi mdi-eye"></i>
                                            </a>
                                            {{-- <a href="{{route('show_invoice',$add->id)}}" class="btn btn-info btn-sm" title="Invoice Status"> <i class="mdi mdi-format-list-bulleted"></i>
                                            </a> --}}
                                            <a href="{{route('sales_man.edit',$add->id)}}" class="btn btn-dark btn-sm" title="Edit Information">
                                                <i class="mdi mdi-table-edit"></i>
                                            </a>
                                                @csrf
                                                @method('delete')
                                                <button type="submit" onclick="return confirm('Are you sure to delete this??')"
                                                    class="btn btn-danger btn-sm"><i class="mdi mdi-delete"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                    @empty
                                    <tr>
                                        <td colspan="8">There is no records available</td>
                                    </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection
