@extends('layouts.admin')

@section('content')

<div class="row">
    <div class="col-md-10 d-flex align-items-stretch grid-margin">
        <div class="row flex-grow">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h2 class="text-center">{{$supplier_status[0]->supplier->supplier_name}}'s Accounts Status</h2>
                        <p class="card-description text-center">
                            For the month of January</p>
                            <form method="get" action="{{url('').'/suppliers/'.Request::segment(2)}}">
                                    <div class="form-group">
                                        <select required class="form-control" id="view_type" name="ledger"  onchange="this.form.submit()">
                                         <option value="0" selected>Transaction View</option>
                                         <option value="1">Ledger View</option>
                                       </select>
                                    </div>          
                                {{-- <button class="btn btn-primary" type="submit">
                                              <i class="mdi mdi-magnify"></i>
                                          </button> --}}
                                  </form>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Transaction ID</th>
                                        <th>Date</th>
                                        <th>Account Title</th>
                                        <th>Event</th>
                                        <th>description</th>
                                        <th>Debit</th>
                                        <th>Credit</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
            
                        $amount=0;
                        $damount=0;
                        //$tspan=2;
                        ?>
                                    @forelse($supplier_status as $jr)
                                    <tr>


                                        <td>{{$jr->transaction_id}}</td>
                                        <?php $time=strtotime($jr->created_at);
                            $date=date("d-M-Y",$time);?>
                                        <td>{{$date}}</td>

                                        <td>{{$jr->account->account_head}}</td> 
                                        <td>{{$jr->transaction->event_type}}</td>
                                        <td>{{$jr->description}}</td>


                                        @if($jr->journal_type==0)
                                        <td>{{number_format($jr->amount,2)}}</td>
                                        <?php $damount=$damount+$jr->amount;?>
                                        @else
                                        <td></td>
                                        @endif
                                        @if($jr->journal_type==1)
                                        <td>{{number_format($jr->amount,2)}}</td>
                                        <?php $amount=$amount+$jr->amount;?>
                                        @else
                                        <td></td>
                                        @endif

                                    </tr>
                                    @empty
                                    <tr>
                                        <td colspan="8">There is no records available</td>
                                    </tr>
                                    @endforelse
                                </tbody>
                                <tfoot>
                                        @php
                                        $got=0;
                                        $payable=0;
                                        @endphp
                                        <tr class="table-default">
                                        <th colspan="5" class="text-right">Total Transaction :</th>
                                        <th>{{$getable=number_format($damount,2)}}</th>
                                        <th>{{$payable=number_format($amount,2)}}</th>
                                        </tr>
                                        @php
                                        $got=$damount;
                                        $payable=$amount;
                                        @endphp
                                        @if($got<$payable)
                                       
                                        <tr class="table-danger">
                                        <th colspan="5" class="text-right">Amount To Be Paid :</th>
                                        <th colspan="2" class="text-right">{{number_format(($amount-$damount),2)}}</th>
                                        </tr>
                                        @endif
                                        @if($got>$payable)
                                        <tr class="table-success">
                                        <th colspan="5" class="text-right">Amount To Be Received :</th>
                                        <th colspan="2" class="text-right">{{number_format(($damount-$amount),2)}}</th>
                                        </tr>
                                        @endif
                                       
                                      </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection
@push('js')
<script type="text/javascript" src="{{asset('admin/js/select-togglebutton.js')}}"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $('#view_type').togglebutton();
            })
        </script>    
@endpush
