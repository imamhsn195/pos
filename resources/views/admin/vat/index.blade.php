@extends('layouts.admin')

@section('title','Vat')

@section('content')

<div class="row">
    <div class="col-md-12 d-flex align-items-stretch grid-margin">
        <div class="row flex-grow row_table_responsive">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                            <h2 class="text-center">All VAT</h2>
                            <div class="row">
                                {{-- <div class="col-md-6"></div> --}}
                                <div class="col-md-12">
                                    <form action="{{route('vat.index')}}" method="GET">
                                      <div class="input-group float-right">
                                          <input class="form-control py-2 border-primary" placeholder="Search here.." name="vat_title" type="text" id="example-search-input" required="">
                                          <span class="input-group-append">
                                            <button class="btn btn-primary" type="submit">
                                                <i class="mdi mdi-magnify"></i>
                                            </button>
                                            <a class="btn btn-primary" title="Register New Vat" href="{{route('vat.create')}}"><i class="mdi mdi-playlist-plus"></i></a>  
                                            @if (request()->query())
                                            <a class="btn btn-primary" title="Show All Vat" href="{{route('vat.index')}}"><i class="mdi mdi-format-list-bulleted"></i></a> 
                                            @endif
                                          </span>
                                      </div>
                                    </form>
                                </div>
                              </div>
                          <br>
                    
                        
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered border-primary">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Title</th>
                                        <th>Percentage(%)</th>
                                        <th>Status</th>
                                        <th>Created By</th>
                                        {{-- <th colspan="2">Actions</th> --}}
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i =1; ?>
                                    @forelse($vats as $vat)
                                    <tr>
                                        <td>{{$i++}}</td>
                                        <td>{{$vat->vat_title}}</td>
                                        <td>{{$vat->vat_percentage*100}}%</td>
                                        <td>
                                            <form action="{{route('vat.update',$vat->id)}}" method="post">
                                                @csrf
                                                @method('put')
                                                <input type="hidden" name="status" value="{{$vat->status}}">
                                                <button type="submit" onclick="return confirm('Are you sure to change status??')"
                                                    class="btn btn-link">
                                                    @if($vat->status == 1)
                                                    <span class="badge badge-success"><i class="mdi mdi-thumb-up"></i></span>
                                                    @else
                                                    <span class="badge badge-danger"><i class="mdi mdi-thumb-down"></i></span>
                                                    @endif
                                                </button>
                                            </form>
                                        </td>
                                        <td>{{$vat->user->user_type}}</td>
                                        {{-- <td>
                                            <a href="{{route('vat.edit',$vat->id)}}" class="btn btn-link">
                                                <i class="mdi  mdi-table-edit text-info icon-md">
                                                </i></a>
                                        </td> --}}
                                        {{-- <td>
                                             <form action="{{route('vat.destroy',$vat->id)}}" method="post">
                                                @csrf
                                                @method('delete')
                                                <input type="submit" value="" onclick="return confirm('Are you sure to delete this??')"
                                                    class="btn btn-link" />
                                                <i class="mdi  mdi-delete text-info icon-md">
                                                </i>
                                            </form> 

                                        </td> --}}
                                    </tr>
                                    @empty
                                    <tr>
                                        <td colspan="8">There is no records available</td>
                                    </tr>
                                    @endforelse
                                </tbody>
                            </table>
                           <span class="float-right"> {{$vats->links()}}</span>
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>
    </div>

</div>

@endsection