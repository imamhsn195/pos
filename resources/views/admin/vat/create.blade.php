@extends('layouts.admin')

@section('title','Vat')

@section('content')



<div class="row">
    <div class="col-md-6 d-flex align-items-stretch grid-margin">
        <div class="row flex-grow">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Add New Vat</h4>
                        <form class="forms-sample" method="post" action="{{route('vat.store')}}">
                            @csrf
                            <div class="form-group">
                                <label for="vat_title">Vat Title</label>
                                <input type="text" class="form-control" name="vat_title" id="vat_title" placeholder="Enter Vat Title">
                            </div>
                            <div class="form-group">
                                <label for="vat_percentage">Vat Percentage</label>
                                <div class="input-group mb-3">
                                    <input type="number" class="form-control" name="vat_percentage" id="vat_percentage"
                                    placeholder="Enter Vat Percentage" step=".01">
                                    <div class="input-group-append">
                                        <span class="input-group-text">%</span>
                                    </div> 
                                </div>
                            </div>
                           
                            
                            <div class="form-group">
                                <label for="vat_description">Vat Description</label>
                                <textarea class="form-control" name="vat_description" id="vat_description" placeholder="Enter Vat Description"></textarea>
                            </div>
                            <button type="submit" class="btn btn-success mr-2">Submit</button>
                            <a href="{{route('vat.index')}}" class="btn btn-primary mr-2">Back</a>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection