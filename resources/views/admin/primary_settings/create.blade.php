@extends('layouts.admin')

@section('title','Primary Settings')

@push('css')

@endpush

@section('content')

<div class="row">
<div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h4 class="text-center">Basic Company Information</h4><br>
        <form class="forms-sample row" method="post" action="{{route('primary_settings.store')}}" enctype="multipart/form-data">
          @csrf
          
          <div class="col-md-6">
            <div class="form-group row">
              <label for="" class="col-sm-5">Company Name</label>
              <input type="text" name="company_name" value="{{$PrimarySettings?$PrimarySettings->company_name:''}}" class="form-control col-sm-7" id="" placeholder="Company Name">
            </div>
          <div class="form-group row">
            <label class="col-sm-5" for="">Phone</label>
            <input type="text" name="phone" value="{{$PrimarySettings?$PrimarySettings->phone:''}}" class="form-control col-sm-7" id="" placeholder="Company Phone">
            </div>
          <div class="form-group row">
            <label class="col-sm-5" for="">Email</label>
            <input type="email" name="email" value="{{$PrimarySettings?$PrimarySettings->email:''}}" class="form-control col-sm-7" id="" placeholder="Company Email">
            </div>
            <div class="form-group row">
            <label class="col-sm-5" for="exampleTextarea1">Address</label>
            <textarea class="form-control col-sm-7" name="address" id="exampleTextarea1" rows="2" placeholder="Address">{{$PrimarySettings?$PrimarySettings->address:''}}</textarea>
          </div>
          <div class="form-group row">
                <label class="col-sm-5" for="">Thana</label>
                <input type="text" name="thana" value="{{$PrimarySettings?$PrimarySettings->thana:''}}" class="form-control col-sm-7" id="" placeholder="Thana" />
          </div>
              
          <div class="form-group row">
            <label class="col-sm-5" for="">District</label>
            <input type="text" name="district" value="{{$PrimarySettings?$PrimarySettings->district:''}}" class="form-control col-sm-7" id="" placeholder="District">
          </div>
          
          <div class="form-group row" >
                <label class="col-sm-5" for="">Postal Code</label>
                <input type="text" name="postal_code" value="{{$PrimarySettings?$PrimarySettings->postal_code:''}}" class="form-control col-sm-7" id="" placeholder="Postal Code">
           </div>
          <div class="form-group row" >
                <label class="col-sm-5" for="">BIN / eBIN No</label>
                <input type="number" name="bin_no" value="{{$PrimarySettings?$PrimarySettings->bin_no:''}}" class="form-control col-sm-7" id="" placeholder="eBIN No or BIN No">
           </div>
          </div>
          <div class="col-sm-6">
           <div class="form-group row">
                <label class="col-sm-5" for="product_barcode_type">Barcode Type Code</label>
                <select name="product_barcode_type" id="product_barcode_type" class="form-control col-sm-7">
                  <option {{$PrimarySettings?$PrimarySettings->product_barcode_type=='TYPE_EAN_13'?'selected':'':''}} value="TYPE_EAN_13">TYPE_EAN_13</option>
                  <option {{$PrimarySettings?$PrimarySettings->product_barcode_type=='TYPE_EAN_8'?'selected':'':''}} value="TYPE_EAN_8">TYPE_EAN_8</option>
                  <option {{$PrimarySettings?$PrimarySettings->product_barcode_type=='TYPE_CODE_128'?'selected':'':''}} value="TYPE_CODE_128">TYPE_CODE_128</option>
                </select>
            </div>
            <div class="form-group row">
                <label class="col-sm-5" for="salesperson_feature">Has Sales Person Feature ?</label>
                <input type="checkbox" name="salesperson_feature" {{$PrimarySettings?$PrimarySettings->salesperson_feature==1?'checked':'':''}} value="1" class="form-control-checkbox col-sm-1" id="salesperson_feature">
           </div>
            <div class="form-group row">
                <label class="col-sm-5" for="branch_can_purchase">Can Branch Purchase ?</label>
                <input type="checkbox" name="branch_can_purchase" {{$PrimarySettings?$PrimarySettings->branch_can_purchase==1?'checked':'':''}} value="1" class="form-control-checkbox col-sm-1" id="branch_can_purchase">
           </div>
           <div class="form-group row">
                <label class="col-sm-5" for="warranty_alert_time">Warranty Alert Days</label>
                <input type="text" name="warranty_alert_time" value="{{$PrimarySettings?$PrimarySettings->warranty_alert_time:''}}" class="form-control col-sm-7" id="warranty_alert_time" placeholder="Warranty Alert Days">
           </div>
           <div class="form-group row">
                <label class="col-sm-5" for="expire_alert_time">Expirable Alert Days</label>
                <input type="text" name="expire_alert_time" value="{{$PrimarySettings?$PrimarySettings->expire_alert_time:''}}" class="form-control col-sm-7" id="expire_alert_time" placeholder="Expirable Alert Days">
           </div>
           <div class="form-group row">
                <label class="col-sm-5" for="check_alert_time">Check Disposal Alert Days</label>
                <input type="text" name="check_alert_time" value="{{$PrimarySettings?$PrimarySettings->check_alert_time:''}}" class="form-control col-sm-7" id="check_alert_time" placeholder="Check Disposal Alert Days"/>
           </div>
           <div class="form-group row">
            <label class="col-sm-5">Company Logo <br><span class="small text-danger ">Best logo size 190 X 37</span></label>
            <input type="file" name="company_logo" class="file-upload-info form-control col-sm-7">
            
          </div>
           <div class="form-group row">
            <label class="col-sm-5">Placeholder Image<br><span class="small text-danger">Default Product Image</span></label>
            <input type="file" name="prodct_placeholder" class="file-upload-info form-control col-sm-7">
          </div>
           
          </div>
          <div class="col-sm-12 text-right">
              <button type="submit" class="btn btn-success mr-2">Update</button>
              <a href="{{url('')}}" class="btn btn-danger">Cancel</a>
          </div>
        </form>
      </div>
    </div>
  </div>

</div>
@endsection

@push('js')
<script>

</script>
@endpush