@extends('layouts.admin')

@section('title','Expense Record')

@push('css')

@endpush

@section('content')

<div class="row">
  <div class="col-md-6 d-flex align-items-stretch grid-margin">
      <div class="row flex-grow">
          <div class="col-12">
              <div class="card">
                  <div class="card-body">
                      <form class="forms-sample" method="post" action="{{route('trial_balance.store')}}">
                          @csrf
                          @if(Gate::allows('isSuperadmin'))
                          <div class="form-group">
                              <label for="branch_id">Branch</label>
                              <select required class="form-control" id="branch_id" name="branch_id">
                                  <option value="">Select branch</option>
                                  @forelse($branches as $bp)
                                  <option value="{{$bp->id}}">{{$bp->name}}</option>
                                  @empty
                                  <option>{{'There is no branch'}}</option>
                                  @endforelse
                              </select>
                          </div>
                          @else
                          <input type="hidden" name="branch_id" id="branch_id" value="{{ Auth::user()->branch_id }}">
                          @endif
                          
                          <div class="form-group">
                              <label for="exampleInputCity1">Expense Register in Details</label>
                              @foreach($fixed_assets as $asset)

                              <hr>
                              <div class="form-check">
                                  <label class="form-check-label"><span class="float-right badge badge-info">{{$asset->type}}</span>
                                      <input type="checkbox" name="expense_field[]" onclick="showDiv({{$asset->id}})" class="form-check-input"
                                          value="{{$asset->id}}">
                                      {{$asset->account_head}}
                                      <i class="input-helper"></i></label>
                              </div>
                              <div class="form-group" style="display:none" id="{{$asset->id}}">
                                  <label for="exampleInputCity1">Amount</label>
                                  <input type="number" class="form-control amount" name="expense_amount[]" id="exampleInputCity1"
                                      placeholder="Amount" onblur="getprice()">
                              </div>
                              <hr>
                              @endforeach
                              <div id="addDiv">

                              </div>
                              <button type="button" class="btn btn-xs btn-success  form-control col-sm-3" onclick="newDiv()">+more
                                  Expenses</button>

                                  <div class="form-group">
                                    <label for="total_price">Amount to be paid</label>
                                    <input type="number" class="form-control" name="total_price" id="total_price" value=""
                                        readonly>
                                </div>
                                <div class="form-group">
                                    <label for="payment_method_id">Payment Method</label>
                                    <select required class="form-control" id="payment_method_id" name="payment_method_id"
                                        onchange="addpaidvalue()">
                                        <option value="">Select Payment Method</option>
                                        @foreach($payment_methods as $pm)
                                        {{-- gggg --}}
                                        @php
                                        $cashIn = 0; 
                                        $cashOut = 0;
                                        $payment_method =  $pm->method_type==1?10:2;
                                        @endphp
                                    
                                    @foreach ($pm->journals as $js)
                                        @if ($js->journal_type == 0 && $js->accounts_id == $payment_method)
                                           @php
                                               $cashIn = $cashIn + $js->amount;
                                           @endphp
                                        @endif
                                    @endforeach
                                   
                                    @foreach ($pm->journals as $js)
                                        @if ($js->journal_type == 1 && $js->accounts_id == $payment_method)
                                           @php
                                               $cashOut = $cashOut + $js->amount;
                                           @endphp
                                        @endif
                                    @endforeach
                                   
                                        {{-- hhhhh --}}
                                        <option value="{{$pm->id." | ".$pm->method_type }}" @if($pm->method_type==0 && ($cashIn-$cashOut)<=0) {{"disabled"}} class="text-danger" @endif>{{$pm->method_title}} &nbsp;&nbsp;&nbsp;&nbsp; balance:  {{$cashIn-$cashOut}}</option>
                                        @endforeach
                                    </select>
                                </div>
                               
                                
                              
                                <!--form group for paid amount-->
                                <div class="form-group" id="payment_tk">
    
                                </div>
                                <div class="form-group" id="date_field">
    
                                </div>
                          </div>
                          <button type="submit" class="btn btn-success mr-2">Submit</button>
                          <a href="{{route('trial_balance.index')}}" class="btn btn-danger">Cancel</a>
                      </form>
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>
<script>
  function showDiv(id){
      attType = $('#'+id).css('display');
      if(attType=='none'){
          $('#'+id).css('display','block');
      }else{
           $('#'+id).css('display','none');
      }
  }

  function newDiv(){
      var newPre = '<div class="newAsset"><label for="exampleInputCity1">Title</label>';
      newPre += '<span style="cursor:pointer" class="badge badge-danger assetClose float-right" onclick="hideDiv()">remove</span>';
      newPre +='<input type="text" name="account_head[]" class="form-control" placeholder="Title"/>';
      newPre +='<label for="exampleInputCity1">Select Type</label>';
      newPre +='<select name="type[]" class="form-control">';
      newPre +='<option value="Operating Expense">Operating Expense</option>';
      newPre +='<option value="Non-Operating Expense">Non-Operating Expense</option>';
      newPre +='</select>';
      newPre +='<label for="exampleInputCity1">Amount</label>';
      newPre +='<input type="number" class="form-control amount" name="expense_amount[]"    placeholder="Amount" onblur="getprice()"/><br><hr></div>';
      $('#addDiv').append(newPre);
  }
 function hideDiv(){
  $('.assetClose').click(function(){
 $(this).parent().remove();
});
 }
 function addpaidvalue() {
var payment_method_id = document.getElementById("payment_method_id").value;

// $('#payment_tk').empty(paid_amount);
// payment_method_id = payment_method_id.split(" | ");
// console.log(payment_method_id[0]);
// if (payment_method_id[0]>0) {
//     var paid_amount = "<label for='paid_amount'>Paid Amount</label>";
//     paid_amount += "<input type='number' class='form-control' name='paid_amount' id='paid_amount' step='.01'>";
    // if (payment_method_id[1] == 1) {
    //     paid_amount +=
    //         "<label for='check_no'>Check No <i class='text-danger'>( Optional )</i></label><input type='text' class='form-control' name='check_no' id='check_no' onblur='check_disposal_field()'>";
    // }
    //$('#payment_tk').prepend(paid_amount);
//}
}
// //extra field for cheque disposal date 
// function check_disposal_field() {
// var payment_method_id = document.getElementById("payment_method_id").value;
// $('#date_field').empty(paid_amount);
// payment_method_id = payment_method_id.split(" | ");
// if (payment_method_id[1] == 1) {
//     var paid_amount =
//         "<label for='cheque_disposal_date'>Cheque Disposal Date <i class='text-danger'>( Set if not Today )</i></label>";
//     paid_amount +=
//         "<input type='date' class='form-control' name='cheque_disposal_date' id='cheque_disposal_date'>";
//     $('#date_field').prepend(paid_amount);
// }
// }
function getprice(){
  var field_amount=[];
  $('.amount').each(function(){
    if($(this).val()!==""){
     field_amount.push(parseInt($(this).val()));
    }
  });
  var calculated_amount=field_amount.reduce(function(acc, val){ return acc+val;});
        // var p_unit_price = document.getElementById("p_unit_price").value;
        // var purchase_qty = document.getElementById("purchase_qty").value;
        // var total_price = (purchase_qty * p_unit_price);

        $('#total_price').val(calculated_amount);
    }
</script>
@endsection


@push('js')

@endpush