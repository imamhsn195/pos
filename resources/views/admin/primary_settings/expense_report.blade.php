@extends('layouts.admin')

@section('content')

<div class="row">
    <div class="col-md-10 d-flex align-items-stretch grid-margin">
        <div class="row flex-grow">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h2 class="text-center"> Expense Reports</h2>
                        @if(Auth::user()->user_type=="superadmin")
                        <h4 class="text-center">Showing All Branches</h4>
                        @else
                        <h4 class="text-center">Showing {{Auth::user()->branch->name}} Branch</h4>
                        @endif
                        <p class="card-description text-center">
                            <form method="get" action="{{route('expense_show')}}">
                                <div class="input-group col-md-6 float-right">
                                    <label for="example-search-input1">From :</label>
                                    <input class="form-control py-2" @if(isset($_GET['searchbyfrom']))
                                        value="{{$_GET['searchbyfrom']}}" @else value="{{date('Y-m-01')}}" @endif
                                        name="searchbyfrom" type="date" id="example-search-input1">
                                    <label for="example-search-input2">To :</label>
                                    <input class="form-control py-2" placeholder="To.." name="searchbyto" type="date"
                                        id="example-search-input2" @if(isset($_GET['searchbyto']))
                                        value="{{$_GET['searchbyto']}}" @else value="{{date('Y-m-t')}}" @endif>
                                    <span class="input-group-append">
                                        <button class="btn btn-primary" type="submit">
                                            <i class="mdi mdi-magnify"></i>
                                        </button>
                                    </span>

                                </div>
                            </form>
                        </p>
                        <div class="table-responsive">
                            <table class="table table-bordered" id="accordion" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Transaction No</th>
                                        <th>Date</th>
                                        <th>Payment Method</th>
                                        <th>Total Expense</th>
                                        <th>ExpensedBy</th>
                                    </tr>
                                </thead>
                                <tbody>
                                   
                        @forelse($expenses as $key=>$expenses_status)
                                <tr class="collapsed" data-toggle="collapse" href="#collapseOne{{$key}}" aria-expanded="true" aria-controls="collapseOne" style="cursor: pointer;">
                            <th>{{$expenses_status->id}}</th>
                            @php $time=strtotime($expenses_status->created_at);
                            $date=date("d-M-Y",$time); @endphp
                                        <td>{{$date}}</td>
                                        @php
            
                                        $saleAmt=0;
                                        $expenseAmt=0;
                                        $paymentMethod='';
                                        @endphp
                                    @foreach($expenses_status->journals as $jr)
                                        @if($jr->journal_type==0)
                                        @php 
                                        $expenseAmt=$expenseAmt+$jr->amount;
                                        @endphp
                                        @endif           
                                    @endforeach
                                    <td>{{$expenses_status->journals[0]->paymentmethod->method_title}}</td>
                                    <td>{{number_format($expenseAmt,2)}}</td>
                                    <td title="{{$expenses_status->branch->name}}">{{$expenses_status->user->name}}</td>
                                   
                        </tr>
                    <tbody id="collapseOne{{$key}}" class="table table-success collapse" data-parent="#accordion" style="border:black 2px solid">
                            <tr class="table-info">
                            <th colspan="5" class="text-center">Transaction No:{{$expenses_status->id}}</th>
                            </tr>
                                <tr class="table-success">
                                <th>SL</th>
                                <th>ExpenseTitle</th>
                                <th>ExpenseType</th>
                                <th>Amount</th>
                                <th>By</th>
                                </tr>
                                @php 
                                $subTotal=0;
                                @endphp 
                                    @foreach($expenses_status->journals as $subkey=>$jr)
                                    @if($jr->journal_type==0)
                                             
                                    <tr>
                                        <td>{{$subkey+1}}</td>
                                        <td>{{$jr->account->account_head}}</td>
                                        <td>{{$jr->account->type}}</td>
                                        <td>{{$jr->amount}}</td>
                                        @php $subTotal=$subTotal+$jr->amount
                                         @endphp
                                        <td>{{($jr->users->name)}}</td>
                                    </tr>
                                    @endif
                                @endforeach
                           
                                <tr class="table-info">
                                    <th colspan="3" class="text-right">Expense Total</th>
                                    <th colspan="2">{{$subTotal}}</th>
                                </tr>
                    </tbody>
                                    @empty
                                    <tr>
                                        <td colspan="8">There is no records available</td>
                                    </tr>
                                    @endforelse
                                </tbody>
                               
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection
