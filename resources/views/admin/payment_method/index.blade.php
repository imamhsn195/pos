@extends('layouts.admin')

@section('content')

<div class="row">
    <div class="col-md-12 d-flex align-items-stretch grid-margin">
        <div class="row flex-grow row_table_responsive">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                            <h2 class="text-center">Payment Method Information</h2>
                            <div class="row">
                                {{-- <div class="col-md-6"></div> --}}
                                <div class="col-md-12">
                                    <form action="{{route('payment_method.index')}}" method="GET">
                                      <div class="input-group float-right">
                                          <input class="form-control py-2 border-primary" placeholder="Search here.." name="method_title" type="text" id="example-search-input" required="">
                                          <span class="input-group-append">
                                            <button class="btn btn-primary" type="submit">
                                                <i class="mdi mdi-magnify"></i>
                                            </button>
                                            <a class="btn btn-primary" title="Register New Payment Method" href="{{route('payment_method.create')}}"><i class="mdi mdi-playlist-plus"></i></a>  
                                            @if (request()->query())
                                            <a class="btn btn-primary" title="Show All Payment Methods" href="{{route('payment_method.index')}}"><i class="mdi mdi-format-list-bulleted"></i></a> 
                                            @endif
                                          </span>
                                      </div>
                                    </form>
                                </div>
                              </div>
                          <br>
                        {{-- <h4 class="card-title"></h4>
                        <a href="{{route('payment_method.create')}}" class="btn btn-info">Add New</a>
                        <form action="{{route('payment_method.index')}}" method="get">
                            <div class="input-group col-md-4 float-right">
                                <input type="text" name="method_title" class="form-control" placeholder="Search">

                                <input type="submit" value="Search" class="btn btn-info">
                            </div>
                        </form> --}}
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered border-info">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Title</th>
                                        <th>Type</th>
                                        <th>B.Balance</th>
                                        <th>In</th>
                                        <th>Out</th>
                                        <th>E.Balance</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                     $i =1;
                                     $date=date('Y-m-d'); 
                                    @endphp
                                    @forelse($payment_methods as $pm)
                                    <tr>
                                        <td rowspan="2">{{$i++}}</td>
                                        <th rowspan="2">{{$pm->method_title}}</th>
                                        <td>
                                            {{$pm->method_type==1?'Bank':'Cash'}}
                                        </td>
                                        <td>
                                        </td>
                                        <td class="text-success">
                                            @php
                                                $cashIn = 0; 
                                                $cashOut = 0;
                                                $payment_method =  $pm->method_type==1?10:2;
                                            @endphp
                                            
                                            @foreach ($pm->journals as $js)
                                                @if ($js->journal_type == 0 && $js->accounts_id == $payment_method)
                                                   @php
                                                       $cashIn = $cashIn + $js->amount;
                                                   @endphp
                                                @endif
                                            @endforeach
                                            {{$cashIn}}
        
                                        </td>
                                        <td class="text-danger">
                                            @foreach ($pm->journals as $js)
                                                @if ($js->journal_type == 1 && $js->accounts_id == $payment_method)
                                                   @php
                                                       $cashOut = $cashOut + $js->amount;
                                                   @endphp
                                                @endif
                                            @endforeach
                                            {{$cashOut}}
                                        </td>
                                        <td class="{{$cashIn-$cashOut>1?"text-info":"text-danger"}}"><b>{{$cashIn - $cashOut}}</b></td>
                                        <!-- <td rowspan="2">{{$pm->user->user_type}}</td> -->
                                        <td rowspan="2">
                                            <form action="{{route('payment_method.update',$pm->id)}}" method="post">
                                                @csrf
                                                @method('put')
                                                <input type="hidden" name="status" value="{{$pm->status}}">
                                                <button type="submit" onclick="return confirm('Are you sure to change status??')"
                                                    class="btn btn-link">
                                                    @if($pm->status == 1)
                                                    <span class="badge badge-success">Active</span>
                                                    @else
                                                    <span class="badge badge-danger">Inactive</span>
                                                    @endif
                                                </button>
                                            </form>
                                        </td>
                                    <td rowspan="2"><button type="button" class="btn btn-primary"  data-toggle="modal" data-target="#myModal" onclick='addId({{$pm->id}},{{$pm->method_type}})'>Add Balance</button></td>
                                    </tr>
                                    <tr class="table-primary">
                                    <th>Today's Staus:</th>
                                    <td>
                                            @php
                                                $bcashIn = 0; 
                                                $bcashOut = 0;
                                                $payment_method =  $pm->method_type==1?10:2;
                                            @endphp
                                            
                                            @foreach ($pm->journals as $js)
                                               @php
                                                if($js->journal_type == 0 && $js->accounts_id == $payment_method && (strtotime($js->created_at) < strtotime($date))){
                                                       $bcashIn = $bcashIn + $js->amount;
                                                }
                                                @endphp
                                            @endforeach
                                            
                                            @foreach ($pm->journals as $js)
                                            @php
                                                if($js->journal_type == 1 && $js->accounts_id == $payment_method && (strtotime($js->created_at)< strtotime($date))){
                                                       $bcashOut = $bcashOut + $js->amount;
                                                       }
                                            @endphp
                                               
                                            @endforeach
                                            @php
                                            $beginning=$bcashIn-$bcashOut
                                            @endphp
                                            {{$beginning}}
                                    </td>
                                    <td class="text-success">
                                            @php
                                                $tcashIn = 0; 
                                                $tcashOut = 0;
                                                $payment_method =  $pm->method_type==1?10:2;
                                            @endphp
                                            
                                            @foreach ($pm->journals as $js)
                                                @php
                                                if($js->journal_type == 0 && $js->accounts_id == $payment_method && date('Y-m-d',strtotime($js->created_at))==$date){
                                                       $tcashIn = $tcashIn + $js->amount;
                                                }
                                              
                                                @endphp
                                            @endforeach
                                            {{$tcashIn}}
                                        </td>
                                        <td class="text-danger">
                                            @foreach ($pm->journals as $js)
                                            @php
                                                if($js->journal_type == 1 && $js->accounts_id == $payment_method && (date('Y-m-d',strtotime($js->created_at))==$date)){
                                                  
                                                       $tcashOut = $tcashOut + $js->amount;
                                                }
                                            @endphp
                                              
                                            @endforeach
                                            {{$tcashOut}}
                                        </td>
                                        <td class="{{($beginning+$tcashIn-$tcashOut)>1?"text-info":"text-danger"}}"><b>{{$beginning+$tcashIn - $tcashOut}}</b></td>
                                    @empty
                                    <tr>
                                        <td colspan="8">There is no records available</td>
                                    </tr>
                                    @endforelse
                                </tbody>
                            </table>
                            <span class="float-right"> {{$payment_methods->links()}}</span>
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>
    </div>
         <!-- start today -->
    {{-- <div class="col-md-12 d-flex align-items-stretch grid-margin">
        <div class="row flex-grow row_table_responsive">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Today Payment Method Status </h4>
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Title</th>
                                        <th>Type</th>
                                        <th>Beginning Balance</th>
                                        <th>In</th>
                                        <th>Out</th>
                                        <th>Ending Balance</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    @php 
                                        $i =1;
                                    @endphp
                                    @forelse($payment_methods as $pm)
                                    <tr>
                                        <td>{{$i++}}</td>
                                        <td>{{$pm->method_title}}</td>
                                        <td>
                                            {{$pm->method_type==1?'Bank':'Cash'}}
                                        </td>
                                        <td class="text-success">
                                            @php
                                                $bcashIn = 0; 
                                                $bcashOut = 0;
                                                $payment_method =  $pm->method_type==1?10:2;
                                            @endphp
                                            
                                            @foreach ($pm->journals as $js)
                                               @php
                                                if($js->journal_type == 0 && $js->accounts_id == $payment_method && (strtotime($js->created_at) < strtotime($date))){
                                                       $bcashIn = $bcashIn + $js->amount;
                                                }
                                                @endphp
                                            @endforeach
                                            
                                            @foreach ($pm->journals as $js)
                                            @php
                                                if($js->journal_type == 1 && $js->accounts_id == $payment_method && (strtotime($js->created_at)< strtotime($date))){
                                                       $bcashOut = $bcashOut + $js->amount;
                                                       }
                                            @endphp
                                               
                                            @endforeach
                                            @php
                                            $beginning=$bcashIn-$bcashOut
                                            @endphp
                                            {{$beginning}}
                                        </td>
                                        <td class="text-success">
                                            @php
                                                $tcashIn = 0; 
                                                $tcashOut = 0;
                                                $payment_method =  $pm->method_type==1?10:2;
                                            @endphp
                                            
                                            @foreach ($pm->journals as $js)
                                                @php
                                                if($js->journal_type == 0 && $js->accounts_id == $payment_method && date('Y-m-d',strtotime($js->created_at))==$date){
                                                       $tcashIn = $tcashIn + $js->amount;
                                                }
                                              
                                                @endphp
                                            @endforeach
                                            {{$tcashIn}}
                                        </td>
                                        <td class="text-danger">
                                            @foreach ($pm->journals as $js)
                                            @php
                                                if($js->journal_type == 1 && $js->accounts_id == $payment_method && (date('Y-m-d',strtotime($js->created_at))==$date)){
                                                  
                                                       $tcashOut = $tcashOut + $js->amount;
                                                }
                                            @endphp
                                              
                                            @endforeach
                                            {{$tcashOut}}
                                        </td>
                                        <td class="{{($beginning+$tcashIn-$tcashOut)>1?"text-info":"text-danger"}}"><b>{{$beginning+$tcashIn - $tcashOut}}</b></td>
                                        <!-- <td>{{$pm->user->user_type}}</td> -->
                                    </tr>
                                    @empty
                                    <tr>
                                        <td colspan="8">There is no records available</td>
                                    </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                    {{$payment_methods->links()}}
                </div>
            </div>
        </div>
    </div> --}}
        <!-- finish today -->
</div>
{{-- model for update or increase amount --}}
<div class="modal fade" id="myModal">
    <div class="modal-dialog">
		<div class="modal-content">
      
        <!-- Modal Header -->
				<div class="col-12">
                  <div class="card">
                    <div class="card-body">
                      <h4 class="card-title">Balance In or Update<button type="button" class="close" data-dismiss="modal">&times;</button></h4>
                      <p class="card-description">
                      </p>
                      <form class="forms-sample" id="attId" method="POST" action="{{url('add_payment_method_balance')}}">
                      @csrf
                      @if(Gate::allows('isSuperadmin'))
                      <div class="form-group">
                        <label for="branch_id">Branch</label>
                        <select required class="form-control" id="branch_id" name="branch_id">
                            <option value="">Select branch</option>
                            @forelse($branches as $bp)
                            <option value="{{$bp->id}}">{{$bp->name}}</option>
                            @empty
                            <option>{{'There is no branch'}}</option>
                            @endforelse
                        </select>
                    </div>
                    @else
                    <input type="hidden" name="branch_id" id="branch_id" value="{{ Auth::user()->branch_id }}">
                    @endif
        
                    
                    <input type="hidden" name="payment_method_to_be_add_id" id="payment_method_to_be_add_id" value="">
                    <input type="hidden" name="payment_method_to_be_add_type" id="payment_method_to_be_add_type" value="">
                    
                    <div class="form-group">
                        {{-- <label for="transaction_type">Transaction Type</label> --}}
                        <select required class="form-control" id="transaction_type" name="transaction_type" onchange="get_form_element()">
                         <option value="0">Add Beginning Balance</option>
                         <option value="1" selected>Transfer Balance</option>
                       </select>
                    </div>
						
                        <div class="form-group" id="payable_field" style="display:block">
                            <label for="payment_method_id">Payment Method</label>
                            <select class="form-control" id="payment_method_id" name="payment_method_id"
                                onchange="addpaidvalue()">
                                <option value="">Select Payment Method</option>
                                @foreach($methods as $pm)
                                {{--  balace option --}}
                                @php
                                $cashIn = 0; 
                                $cashOut = 0;
                                $payment_method =  $pm->method_type==1?10:2;
                                @endphp
                            
                            @foreach ($pm->journals as $js)
                                @if ($js->journal_type == 0 && $js->accounts_id == $payment_method)
                                   @php
                                       $cashIn = $cashIn + $js->amount;
                                   @endphp
                                @endif
                            @endforeach
                           
                            @foreach ($pm->journals as $js)
                                @if ($js->journal_type == 1 && $js->accounts_id == $payment_method)
                                   @php
                                       $cashOut = $cashOut + $js->amount;
                                   @endphp
                                @endif
                            @endforeach
                           
                            </td>
                                {{-- balace option --}}
                                <option value="{{$pm->id." | ".$pm->method_type }}" @if($pm->method_type==0 && ($cashIn-$cashOut)<=0) {{"disabled"}} class="text-danger" @endif>{{$pm->method_title}} &nbsp;&nbsp;&nbsp;&nbsp; balance:  {{$cashIn-$cashOut}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="paid_amount">Amount</label>
                            <input type="number" class="form-control" name="paid_amount" id="paid_amount" value="{{ old('paid_amount') }}" step=".01">
                              @if ($errors->any())
                                      @foreach($errors->all() as $error)
                                      <span class="alert text-danger">**{{ $error }}
                                      </span>
                                      @endforeach
                                  @endif
                          </div>
						  <button type="submit" class="btn btn-success mr-2">Submit</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                      </form>
                    </div>
						
                  </div>
                </div>
        
        
		</div>
    </div>
  </div>
{{-- model for update or increase amount --}}
@endsection
@push('js')
<script type="text/javascript" src="{{asset('admin/js/select-togglebutton.js')}}"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $('#transaction_type').togglebutton();
            })
        </script>
        <script>
        function get_form_element(){
            var ttype=document.getElementById('transaction_type').value;
            ttype==1?$('#payable_field').css('display','block'):$('#payable_field').css('display','none');
        }
        function addId(id,type){
            $('#payment_method_to_be_add_id').val(id);
            $('#payment_method_to_be_add_type').val(type);
        }
        </script>    
@endpush