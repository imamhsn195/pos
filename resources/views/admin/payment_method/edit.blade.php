@extends('layouts.admin')

@section('title','Payment Method')

@section('content')



<div class="row">
    <div class="col-md-6 d-flex align-items-stretch grid-margin">
        <div class="row flex-grow">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Payment Method Information</h4>
                        <form class="forms-sample" method="post" action="{{route('payment_method.update',$payment_method->id)}}">
                            @csrf
                            @method('put')
                            <div class="form-group">
                                <label for="method_title">Method Title</label>
                                <input type="text" value="{{$payment_method->method_title}}" class="form-control" name="method_title"
                                    id="method_title" placeholder="Enter Payment Method Title">
                            </div>
                            <div class="form-group">
                                <label for="method_type">Method Type</label>
                                <select name="method_type" class="form-control">
                                    <option value="{{$payment_method->method_type}}">{{$payment_method->method_type?'Yes':''}}</option>
                                    <option value="1">Yes</option>
                                    <option value="0">No</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea class="form-control" name="description" id="description" placeholder="Enter Description">{{$payment_method->description}}</textarea>
                            </div>
                            <button type="submit" class="btn btn-success mr-2">Submit</button>
                            <a href="{{route('payment_method.index')}}" class="btn btn-primary mr-2">Back</a>
                            <button class="btn btn-light">Cancel</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection