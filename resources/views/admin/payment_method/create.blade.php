@extends('layouts.admin')

@section('content')

<div class="row">
    <div class="col-md-6 d-flex align-items-stretch grid-margin">
        <div class="row flex-grow">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Add New Payment Method</h4>
                        <form class="forms-sample" method="post" action="{{route('payment_method.store')}}">
                            @csrf
                            <div class="form-group">
                                <label for="method_title">Method Title</label>
                                <input type="text" class="form-control" name="method_title" id="method_title"
                                    placeholder="Enter Payment Method Title">
                            </div>
                            <div class="form-group">
                                <label for="method_type">Method Type</label>
                                <select name="method_type" class="form-control">
                                    <option value="">Select Method Type</option>
                                    <!--0==cash & 1==Bank;-->
                                    <option value="0">Cash</option>
                                    <option value="1">Bank</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea class="form-control" name="description" id="description" placeholder="Enter Description"></textarea>
                            </div>
                            <button type="submit" class="btn btn-success mr-2">Submit</button>
                            <a href="{{route('payment_method.index')}}" class="btn btn-primary mr-2">Back</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection