@extends('layouts.admin')

@push('css')
<style type="text/css">
    .open ul.dropdown-menu {
        display: block;
        color: #000;
    }
</style>
@endpush

@section('content')

<div class="row">
    <div class="col-md-12 d-flex align-items-stretch grid-margin">
        <div class="row flex-grow row_table_responsive">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h2 class="text-center">Bundle Product Information</h2>
                        <div class="row">
                                {{-- <div class="col-md-6"></div> --}}
                                <div class="col-md-12">
                                    <form action="{{route('bundle_product.index')}}" method="GET">
                                      <div class="input-group float-right">
                                          <input class="form-control py-2 border-primary" placeholder="Search here.." name="bundle_name" type="text" id="example-search-input" required="">
                                          <span class="input-group-append">
                                            <button class="btn btn-primary" type="submit">
                                                <i class="mdi mdi-magnify"></i>
                                            </button>
                                            <a class="btn btn-primary" title="Register New Product" href="{{route('bundle_product.create')}}"><i class="mdi mdi-playlist-plus"></i></a>  
                                            @if (request()->query())
                                            <a class="btn btn-primary" title="Show All Product" href="{{route('bundle_product.index')}}"><i class="mdi mdi-format-list-bulleted"></i></a> 
                                            @endif
                    
                                          </span>
                                      </div>
                                    </form>
                                </div>
                              </div>
                     <br>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered" id="table" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Bundle Products</th>
                                        <th>Created By</th>
                                        <th>Status</th>
                                        <th>Discount</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i =1; ?>
                                    @forelse($bundle_products as $bp)
                                    <tr>
                                        <td>{{$i++}}</td>
                                        <td>{{$bp->bundle_name}}</td>
                                        <td>
                                            {{-- <select title="Show Products" class="form-control form-control-sm">
                                                <option>products</option>
                                                
                                            </select> --}}
                                            <b>Products:</b> 
                                            @foreach($bp->bundle_product_details as $bp_detail){{$bp_detail->product->product_sku}}<br>
                                            @endforeach
                                        </td>
                                        <td>{{$bp->user->user_type}}</td>
                                        <td>
                                            <form action="{{route('change_status',$bp->id)}}" method="post">
                                                @csrf
                                                <input type="hidden" name="status" value="{{$bp->status}}">
                                                <button title="Change Status" type="submit" onclick="return confirm('Are you sure to change status??')"
                                                    class="btn btn-link">
                                                    @if($bp->status == 1)
                                                    <span class="badge badge-success">Active</span>
                                                    @else
                                                    <span class="badge badge-danger">Inactive</span>
                                                    @endif
                                                </button>
                                            </form>
                                        <td>
                                            {{$bp->bundle_discount_amount}}
                                        </td>
                                    </tr>
                                    @empty
                                    <tr>
                                        <td colspan="8">There is no records available</td>
                                    </tr>
                                    @endforelse
                                </tbody>
                            </table>
                            <span class="float-right">{{$bundle_products->links()}}</span>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>

</div>
<script>
  
</script>
@endsection