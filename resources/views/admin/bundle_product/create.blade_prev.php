@extends('layouts.admin')

@section('title','Bundle Product')

@push('css')
<link rel="stylesheet" href="{{asset('admin/css/bootstrap-select.css')}}">
@endpush

@section('content')



<div class="row">
    <div class="col-md-6 d-flex align-items-stretch grid-margin">
        <div class="row flex-grow">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Add New Bundle Product</h4>
                        <form class="forms-sample" method="post" action="{{route('bundle_product.store')}}" enctype="multipart/form-data">
                            @csrf

                            <div class="form-group">
                                <label for="bundle_name">Bundle Name</label>
                                <input type="text" class="form-control" name="bundle_name" id="bundle_name" placeholder="Enter Bundle Name">
                            </div>
                            <?php $proo=0; ?>
                            <div class="form-group">
                                <label>Products For Bundle</label>
                                <select name="productSelection" id="productSelection" class="form-control selectpicker show-tick"
                                    data-live-search="true" onchange="showDiv()">
                                    <option value="">Select product for Bundle</option>
                                    @foreach($products as $product)
                                    @if($proo !== $product->product->id)
                                    <option value="{{$product->product->id}}">
                                        <?=str_before($product->product->product_sku," | ");?>
                                    </option>
                                    <?php $proo=$product->product->id; ?>
                                    @endif
                                    @endforeach
                                </select>
                            </div>

                            <div>
                                <?php $proo=0; ?>
                                @foreach($products as $product)
                                <div class="form-group" style="display:none" id="individual{{$product->product_id}}">

                                    @if($proo !== $product->product_id)
                                    <div id="helloboss{{$product->product_id}}">
                                        <div class="form-check form-check-flat">
                                                <input type="hidden" class="" id="product_id{{$product->product_id}}"
                                                    name="product_id[]" value="{{$product->product_id}}">
                                                    <i class="mdi text-success  mdi-cart text-info icon-md"></i>
                                                {{$product->product->product_sku}}<span class="float-right" onclick="hideDiv({{$product->product_id}})"> <i class="mdi text-danger  mdi-close text-info icon-md">
                                                </i></span>
                                                
                                        </div>
                                        <div class="form-group">
                                            <select name="purchase_id[]" id="getPurchasePrice" class="form-control"
                                                onchange="getPurchase_price(this.value,{{$product->product_id}})">
                                                <option value="">Select Invoice No</option>
                                                @foreach ($product->product->purchases as $purchase)
                                                <option value="{{$purchase->id.' | '.$purchase->sell_unit_price}}">{{$purchase->invoice_no}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <input type='number' name='qty[]' id="qty_id{{$product->product_id}}" class='form-control'
                                        placeholder='Enter Quantity' onblur="getMultify('{{$product->product_id}}'),findTotal()">

                                    <br>
                                    <input type="number" name="unt_price" class="form-control" placeholder="Unit Price"
                                        id="unt_price{{$product->product_id}}">


                                    <input type="hidden" name="subtotal_price" id="subtotal_price{{$product->product_id}}"
                                        value=0>
                                    @endif
                                    <?php $proo=$product->product_id; ?>

                                </div>
                                @endforeach
                            </div>

                            <div class="form-group">
                                <label for="customer_group">Select Customer Group</label>
                                <select name="customerGroup[]" id="customer_group" class="form-control selectpicker show-tick"
                                    data-live-search="true" multiple>
                                    @foreach($customer_groups as $cg)
                                    <option value="{{$cg->id}}">{{$cg->customer_group_title}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="customer_group">Select Customer Criteria</label>
                                <select name="customerCriteria[]" id="customer_group" class="form-control selectpicker show-tick"
                                    data-live-search="true" multiple>
                                    @foreach($customer_criterias as $cs)
                                    <option value="{{$cs->id}}">{{$cs->title}}</option>
                                    @endforeach
                                </select>
                            </div>


                            <div class="form-group">
                                <label for="bundle_image">Select Image</label>
                                <input type="file" class="form-control-file" name="bundle_image">
                            </div>
                            <div class="form-group">
                                <label for="bundle_purchase_price">Bundle Purchase Price or Suggested Price</label>
                                <input type="number" name="suggested_price" class="form-control" id="bundle_purchase_price"
                                    placeholder="Amount">
                            </div>




                            <div class="form-group">
                                <label for="bundle_price">Bundle Sale Price</label>
                                <input type="number" class="form-control" name="bundle_price" id="bundle_price">
                            </div>



                            <button type="submit" class="btn btn-success mr-2">Submit</button>
                            <a href="{{route('bundle_product.index')}}" class="btn btn-primary mr-2">Back</a>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<script type="text/javascript">

//display product to make bundle product
    function showDiv() {
        var product_id = document.getElementById('productSelection').value;
        $('#individual' + product_id).css('display', 'block');
    }

    //display sell unit price according to invoice no
    function getPurchase_price(price, id) {
        var purchaseID = 'unt_price' + id;
        var purchasePrice = price.split(" | ");
        $('#unt_price' + id).attr('value', purchasePrice[1]);
    }

    function hideDiv(id){
        $('#subtotal_price'+id).val('');
        $('#individual'+id).css('display','none');
        findTotal();
    }


    //multiply unit price with quantity and keep value in a hidden field
    function getMultify(id) {
        var qcheck = $('#qty_id' + id).attr('type');
        if (qcheck == 'number') {
            var quantity = $('#qty_id' + id).val();
            var u_price = $('#unt_price' + id).val();
            var qmultiply = quantity * u_price;
            $('#subtotal_price' + id).val(qmultiply);
        } else {
            $('#subtotal_price' + id).val(0);
        }
    }

    //sum all subtotal and show a total price for bundle
    function findTotal() {
        var arr = document.getElementsByName('subtotal_price');
        var tot = 0;
        for (var i = 0; i < arr.length; i++) {
            if (parseInt(arr[i].value))
                tot += parseInt(arr[i].value);
        }
        document.getElementById('bundle_purchase_price').value = tot;
    }

  
</script>

@endsection

@push('js')

<script type="text/javascript" src="{{asset('admin/js/bootstrap-select.js')}}"></script>
<script>
    //package for multiple select option
    $('.selectpicker').selectpicker();
</script>

@endpush
