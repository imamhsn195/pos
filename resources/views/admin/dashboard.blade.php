@extends('layouts.admin')

@section('title','Dashboard')

@section('content')

<div class="content-wrapper">
    <div class="row purchace-popup">
            
    </div>
    {{-- <div class="row">
        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
            <div class="card card-statistics">
                <div class="card-body">
                    <div class="clearfix">
                        <div class="float-left">
                            <i class="mdi mdi-cube text-danger icon-lg"></i>
                        </div>
                        <div class="float-right">
                            <p class="mb-0 text-right">Todays Receipts</p>
                            <div class="fluid-container">
                                <h3 class="font-weight-medium text-right mb-0">{{$todaysReceipts}}</h3>
                            </div>
                        </div>
                    </div>
                    <p class="text-muted mt-3 mb-0">
                        <i class="mdi mdi-alert-octagon mr-1" aria-hidden="true"></i>Branch wise Todays Total Received
                    </p>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
            <div class="card card-statistics">
                <div class="card-body">
                    <div class="clearfix">
                        <div class="float-left">
                            <i class="mdi mdi-receipt text-warning icon-lg"></i>
                        </div>
                        <div class="float-right">
                            <p class="mb-0 text-right">Todays Payments</p>
                            <div class="fluid-container">
                                <h3 class="font-weight-medium text-right mb-0">{{$todaysPayments}}</h3>
                            </div>
                        </div>
                    </div>
                    <p class="text-muted mt-3 mb-0">
                        @if( Gate::allows('isSuperadmin')) <a href={{url('purchases')}}>@endif <i class="mdi mdi-bookmark-outline mr-1"
                                aria-hidden="true"></i>
                            Branch wise Todays Total Payments</a>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
            <div class="card card-statistics">
                <div class="card-body">
                    <div class="clearfix">
                        <div class="float-left">
                            <a href={{url('/laravel_google_chart')}}> <i class="mdi mdi-poll-box text-success icon-lg"></i></a>
                        </div>
                        <div class="float-right">
                            <p class="mb-0 text-right">Sales</p>
                            <div class="fluid-container">
                                <h3 class="font-weight-medium text-right mb-0">{{$totalsales}}</h3>
                            </div>
                        </div>
                    </div>
                    <p class="text-muted mt-3 mb-0">
                        @if( Gate::allows('isSuperadmin')) <a href={{url('sales_report')}}>@endif <i class="mdi mdi-calendar mr-1"
                                aria-hidden="true"></i> View
                            Sales</a>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
            <div class="card card-statistics">
                <div class="card-body">
                    <div class="clearfix">
                        <div class="float-left">
                            <span style="cursor: pointer" data-toggle="modal" data-target="#myBrand"> <i class="mdi mdi-account-location text-info icon-lg"></i></span>
                        </div>
                        <div class="float-right">
                            <p class="mb-0 text-right">Employees</p>
                            <div class="fluid-container">
                                <h3 class="font-weight-medium text-right mb-0">{{$totalemployee}}</h3>
                            </div>
                        </div>
                    </div>
                    <p class="text-muted mt-3 mb-0">
                        <i class="mdi mdi-reload mr-1" aria-hidden="true"></i> Our Employees
                    </p>
                </div>
            </div>
        </div>
    </div> --}}
<div class="row">
    @if( Gate::allows('isSuperadmin') || Gate::allows('isAdmin'))
        <div class="col-lg-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Today Balance Status</h4>
                    <div class="table-responsive">
                    <table class="table table-hover table-bordered">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Title</th>
                                        <!-- <th>Type</th> -->
                                        <th>Beginning</th>
                                        <th>In</th>
                                        <th>Out</th>
                                        <th>Ending Balance</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    @php 
                                        $i =1;
                                        $date=date('Y-m-d');
                                    @endphp
                                    @forelse($payment_methods as $pm)
                                    <tr>
                                        <td>{{$i++}}</td>
                                        <td>{{$pm->method_title}}</td>
                                        <!-- <td>
                                            {{$pm->method_type==1?'Bank':'Cash'}}
                                        </td> -->
                                        <td class="text-success">
                                            @php
                                                $bcashIn = 0; 
                                                $bcashOut = 0;
                                                $payment_method =  $pm->method_type==1?10:2;
                                            @endphp
                                            
                                            @foreach ($pm->journals as $js)
                                               @php
                                                if($js->journal_type == 0 && $js->accounts_id == $payment_method && (strtotime($js->created_at) < strtotime($date))){
                                                       $bcashIn = $bcashIn + $js->amount;
                                                }
                                                @endphp
                                            @endforeach
                                            
                                            @foreach ($pm->journals as $js)
                                            @php
                                                if($js->journal_type == 1 && $js->accounts_id == $payment_method && (strtotime($js->created_at)< strtotime($date))){
                                                       $bcashOut = $bcashOut + $js->amount;
                                                       }
                                            @endphp
                                               
                                            @endforeach
                                            @php
                                            $beginning=$bcashIn-$bcashOut
                                            @endphp
                                            {{$beginning}}
                                        </td>
                                        <td class="text-success">
                                            @php
                                                $tcashIn = 0; 
                                                $tcashOut = 0;
                                                $payment_method =  $pm->method_type==1?10:2;
                                            @endphp
                                            
                                            @foreach ($pm->journals as $js)
                                                @php
                                                if($js->journal_type == 0 && $js->accounts_id == $payment_method && date('Y-m-d',strtotime($js->created_at))==$date){
                                                       $tcashIn = $tcashIn + $js->amount;
                                                }
                                              
                                                @endphp
                                            @endforeach
                                            {{$tcashIn}}
                                        </td>
                                        <td class="text-danger">
                                            @foreach ($pm->journals as $js)
                                            @php
                                                if($js->journal_type == 1 && $js->accounts_id == $payment_method && (date('Y-m-d',strtotime($js->created_at))==$date)){
                                                  
                                                       $tcashOut = $tcashOut + $js->amount;
                                                }
                                            @endphp
                                              
                                            @endforeach
                                            {{$tcashOut}}
                                        </td>
                                        <td class="text-info"><b>{{$beginning+$tcashIn - $tcashOut}}</b></td>
                                        <!-- <td>{{$pm->user->user_type}}</td> -->
                                    </tr>
                                    @empty
                                    <tr>
                                        <td colspan="8">There is no records available</td>
                                    </tr>
                                    @endforelse
                                </tbody>
                            </table>
                    </div>
                </div>
            </div>
        </div>
    @endif
</div>
    @if( Gate::allows('isSuperadmin') || Gate::allows('isAdmin'))
    <div class="row">
            <div class="col-md-6"> 
                {{--  {!! $TotalSaleschart->html() !!}  --}}
            </div>
            <div class="col-md-6"> 
                {{--  {!! $donut_chart->html() !!}  --}}
            </div>
    </div>
    <div class="row">
    @endif
    @if( Gate::allows('isSuperadmin')) {{--  this show the branch information  --}}
        <div class="col-lg-6 grid-margin">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Branch Details</h4>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th> #</th>
                                    <th>Branch Name</th>
                                    <th>Branch Type</th>
                                    <th>Details</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i =1; ?>
                                @forelse($branches as $bi)
                                @if($i<8)
                                <tr>
                                    <td class="font-weight-medium">{{$i++}}</td>
                                    <td>{{$bi->name}}</td>
                                    <td>
                                        {{--
                                        <div class="progress">
                                            <div class="progress-bar bg-success progress-bar-striped" role="progressbar"
                                                style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                        --}}
                                        <label class="badge badge-{{$bi->branch_type?'success':'info'}}">
                                            {{$bi->branch_type?'Warehouse':'Showroom'}}
                                        </label>
                                    </td>
                                    <td>
                                        <span class="badge badge-success" data-toggle="modal" data-target="#dashboardBranch"
                                            style="cursor: pointer" onclick="details({{$bi->id}})">Details</span>
                                    </td>
                                    {{-- <td>
                                        {{$bi->stock_quantity}}
                                    </td> --}}
                                </tr>
                                @endif
                                @empty
                              <tr><td>{{'there is no record available'}}</td></tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 grid-margin">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Top Selling Products</h4>
                        <div class="table-responsive">
                            <table class="table table-bordered  table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th> #</th>
                                        <th>Product Name</th>
                                        <th>Qty</th>
                                        <th>Amount</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i =1; ?>
                                    @forelse($qty_ary as $k=>$v)
                                    @if($i<6)
                                    @php
                                        $i++;
                                    @endphp                              
                                    <tr>
                                        <td class="font-weight-medium"><img src="{{asset('')}}{{$topproducts[$k]['product_img']}}" ></td>
                                        <td>{{str_limit($topproducts[$k]['product_sku'],25)}}</td>
                                        <td>
                                            {{$topproducts[$k]['total_sold_qty']}}
                                        </td>
                                        <td>
                                            {{$topproducts[$k]['sold_amount']}}
                                        </td>
                                    </tr>
                                    @endif
                                    @empty
                                        <tr><td>{{'there is no record available'}}</td></tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
    @endif
    </div>
    @if( Gate::allows('isSuperadmin') || Gate::allows('isAdmin'))
        <div class="modal fade" id="myBrand">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                @if(Auth::user()->user_type == 'superadmin')
                                <div class="row">
                                    <div class="col-12 grid-margin">
                                        <div class="card">
                                            <div class="card-body">
                                                <h5 class="card-title mb-4">Users List</h5>
                                                <div class="fluid-container">
                                                    @forelse($users as $user)
                                                    <div class="row ticket-card mt-3 pb-2 border-bottom pb-3 mb-3">
                                                        <div class="col-md-1">
                                                            <img class="img-sm rounded-circle mb-4 mb-md-0" src="{{url('').'/uploads/avatar/'.$user->avatar}}"
                                                                alt="profile image">
                                                        </div>
                                                        <div class="ticket-details col-md-9">
                                                            <div class="d-flex">
                                                                <p class="text-dark font-weight-semibold mr-2 mb-0 no-wrap">{{$user->name}}
                                                                    :</p>
                                                                <p class="text-primary mr-1 mb-0">[#23047{{$user->id}}]</p>
                                                                <p class="mb-0 ellipsis">Designation: <b>{{$user->user_type}}</b></p>
                                                            </div>
                                                            <br>
                                                            <p class="text-gray ellipsis mb-6">Joined On App :
                                                                {{$user->created_at}}
                                                            </p>
                                                            <div class="row text-gray d-md-flex d-none">
                                                                <div class="col-4 d-flex">
                                                                    <small class="mb-0 mr-2 text-muted text-muted">
                                                                        {{$user->branch ? $user->branch->name:"Super Admin"}}
                                                                    </small>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="ticket-actions col-md-2">
                                                            <form action="{{route('change_user_status',$user->id)}}" method="post">
                                                                @csrf
                                                                <input type="hidden" name="status" value="{{$user->status}}">
                                                                <button type="submit" onclick="return confirm('Are you sure to change Status??')"
                                                                    class="btn btn-link">
                                                                    @if($user->status == 1)
                                                                    <span class="badge badge-success">Active</span> @else
                                                                    <span class="badge badge-danger">Inactive</span> @endif
                                                                </button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                    @empty
                                                <span>{{'Trere is no records available for you'}}</span>
                                                    @endforelse
                                                    <button type="reset" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    {{-- branch details modal --}}
    @if( Gate::allows('isSuperadmin') || Gate::allows('isAdmin'))
        <div class="modal fade" id="dashboardBranch">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12 d-flex align-items-stretch grid-margin">
                                        <div class="row flex-grow">
                                            <div class="col-md-12">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <h4 class="card-title">Inventory Information</h4>
                                                        <div class="table-responsive">
                                                            <table class="table table-striped table-bordered">
                                                                <thead>
                                                                    <tr>
                                                                        <th>ID</th>
                                                                        <th>Product Name</th>
                                                                        <th>Invoice No</th>
                                                                        <th>Purchase Price</th>
                                                                        <th>Stock Quantity</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody id="showBranchData">

                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <button type="reset" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    @endsection
    @push('js')
        <script>
            function details(id) {
                if (id) {
                    $('#showBranchData').empty();
                    $.ajax({
                        type: 'GET',
                        url: 'inventory_details/' + id,
                        success: function (branch) {
                            console.log(branch)
                            $('#showBranchData').append(branch);
                        }
                    });
                }

            }
        </script>
    {{--  {!! Charts::scripts() !!}  --}}
    {{--  {!! $TotalSaleschart->script() !!}   --}}
    {{--  {!! $donut_chart->script() !!}   --}}
    @endpush
