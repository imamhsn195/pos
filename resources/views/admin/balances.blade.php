@extends('layouts.admin')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <table class="table  table-bordered">
                <thead>
                    <tr>
                        <th>SL</th>
                        <th>Accounts Name</th>
                        <th>Cash In</th>
                        <th>Cash Out</th>
                        <th>Cash Balances</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>Bkash</td>
                        <td class="text-success">Tk. 5000</td>
                        <td class="text-danger">Tk. 3000</td>
                        <td class="text-info">Tk. 2000</td>
                    </tr>
                    
                    <tr>
                        <td>2</td>
                        <td>DBBL</td>
                        <td class="text-success">Tk. 15000</td>
                        <td class="text-danger">Tk. 13000</td>
                        <td class="text-info">Tk. 2000</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Cash</td>
                        <td class="text-success">Tk. 40000</td>
                        <td class="text-danger">Tk. 25000</td>
                        <td class="text-info">Tk. 15000</td>
                    </tr>

                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection