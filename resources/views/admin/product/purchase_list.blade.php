@extends('layouts.admin')
    @push('css')

    @endpush
@section('content')

<div class="row">
    <div class="col-md-12 d-flex align-items-stretch grid-margin">
        <div class="row flex-grow row_table_responsive">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h2 class="text-center">Product Stock Report</h2>
                        @php
                            $purchase_price = 0;
                            $selling_price = 0;
                        @endphp
                        @forelse ($products as $product)
                            @php
                                $purchase_price  += $product->stock_quantity*$product->purchase->purchase_unit_price;
                                $selling_price += $product->stock_quantity*$product->purchase->sell_unit_price;
                            @endphp
                        @empty
                            
                        @endforelse
                        @if ((request('status') && request('status')=='outofstock')) 
                        {{--  out of stock is calculated from view and purchase and sell price should be shown when out of stock is selected  --}}
                        
                            @php
                                $purchase_price = 0;
                                $selling_price = 0;
                            @endphp
                        @endif
                            <h5 class="text-center">
                                <table style="margin:auto" >
                                    <tr><td style="text-align:right">Total Purchase Price: </td><td> TK. <b>{{number_format($purchase_price,2 )}}</b></td></tr>
                                    <tr><td style="text-align:right">Total Selling Price: </td><td> Tk. <b>{{ number_format($selling_price,2)}}</b></td></tr> 
                                </table>
                            </h5>  
                        
                            {{-- 
                            <h5 class="text-center">
                                <table style="margin:auto" >
                                    <tr><td style="text-align:right">Total Purchase Price: </td><td> TK. <b>{{number_format($total_purchase_price,2 )}}</b></td></tr>
                                    <tr><td style="text-align:right">Total Selling Price: </td><td> Tk. <b>{{ number_format($total_selling_price,2)}}</b></td></tr> 
                                </table>
                            </h5> --}}
                        <div class="row">
                            <form action="{{route('inventories.index')}}" method="get" >
                            <!-- Search Product Form -->
                                <div class="row float-right" >
                                    <div class="form-group col-sm-2">
                                        <select name="supplier_id" class="form-control" onchange="showCategories(this.value)">
                                            <option value="all">All Suppliers</option>
                                            @forelse ($suppliers as $s)
                                                <option {{(isset($_GET['supplier_id']) && $_GET['supplier_id'] == $s->id)?'selected':''}}  value="{{$s->id}}">{{$s->supplier_name}}</option>
                                            @empty
                                                <option value="product">No Supplier available</option>
                                            @endforelse
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-2">
                                            <select name="category_id" class="form-control" onchange="showProductName(this.value)">
                                                <option value="all">All Categories</option>
                                                @forelse ($categories as $c)
                                                <option {{(isset($_GET['category_id']) && $_GET['category_id'] == $c->id)?'selected':''}} value="{{$c->id}}">{{$c->category_name}}</option>
                                                @empty
                                                    <option value="product">No Categories available</option>
                                                @endforelse
                                            </select>
                                    </div>
                                    <div class="form-group col-sm-2">
                                            <select name="status" class="form-control" onchange="showBranchName(this.value)">
                                                <option value="all">All Status</option>
                                                <option {{(isset($_GET['status']) && $_GET['status'] == 'instock')?'selected':''}} value="instock">In Stock</option>
                                                <option {{(isset($_GET['status']) && $_GET['status'] == 'outofstock')?'selected':''}} value="outofstock">Out Of Stock</option>
                                            </select>
                                    </div>
                                    <div class="form-group col-sm-2">
                                            <select name="type" class="form-control" onchange="showBranchName(this.value)">
                                                <option value="all">All Types</option>
                                                <option {{(isset($_GET['type']) && $_GET['type'] == 'warrantiable')?'selected':''}}  value="warrantiable">Warrantiable Types</option>
                                                <option {{(isset($_GET['type']) && $_GET['type'] == 'expirable')?'selected':''}}  value="expirable">Expirable Types</option>
                                            </select>
                                    </div>
                                    <div class="form-group col-sm-2" id="search_value_div"> 
                                        <input type="text" name="product_sku" value="{{(isset($_GET['product_sku']))?$_GET['product_sku']:''}}" class="form-control" id="search_value" placeholder="Search Pruduct Name">
                                    </div>
                                    <div class="form-group col-sm-1">
                                        <button type="submit" class="btn btn-info " id=""> <i class="mdi mdi-magnify"></i></button> 
                                    </div> 
                                    <div class="form-group col-sm-1">
                                        <a href="{{route('inventories.index')}}" class="btn btn-info "><i class="menu-icon mdi mdi-eye"></i>All</a>
                                    </div> 
                                </div>
                            </form>
                        </div> 
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered" id="productlist">
                                <thead>
                                    <tr>
                                        <th>SL</th>
                                        <th>Name</th>
                                        <th>Stock</th>
                                        <th>Reorder Limit</th>
                                        <th>Category</th>
                                        <th>Status</th>
                                        <th>Details</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php $sl =1; $product_id = 0; @endphp
                                    @if (request('status') && request('status')!=='all')
                                        @forelse($products as $p)
                                            @if ($p->product_id !== $product_id)
                                                @php $quantity = 0; @endphp
                                                @foreach ($products as $product_quantity) 
                                                    @if ($product_quantity->product_id == $p->product_id) @php $quantity = $quantity +  $product_quantity->stock_quantity; @endphp 
                                                    @endif 
                                                @endforeach {{--  For quantity calculation   --}}
                                                @if (request('status')=='instock')
                                                    @if ($quantity > 0)
                                                        <tr>
                                                            <td>{{$sl++}}</td>
                                                            <td>{{str_before($p->product->product_sku,'|')}}<br>{{str_after($p->product->product_sku,'|')}}</td>
                                                            <td>{{$quantity}}</td>
                                                            <td>
                                                                {{$p->product->reorder_limit}}
                                                            </td>
                                                            <td>{{$p->product->category->category_name}}</td>
                                                            <td>
                                                                <a href="{{route('products.show',$p->product->id)}}" title="Click to repurchase">
                                                            <span class="badge badge-{{$quantity?($quantity > $p->product->reorder_limit? "success" :"warning"):"danger"}}">{{$quantity?($quantity > $p->product->reorder_limit? "inStock" :"Reorder"):"OutOfStock"}}</span></a>
                                                            </td>
                                                            </td>
                                                            <td class="">
                                                                <a href="{{route('purchase_history',$p->product_id)}}" onclick="purchaseHistory(this)" data-toggle="modal" data-target="#myModal" >
                                                                    <img src="{{asset($p->product->image)}}" style="width:50;height:50;">
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    @endif  {{--Check of stock quentity is greater than zero--}}
                                                @endif {{--Check if request is for instock product--}}
                                                @if (request('status')=='outofstock')
                                                    @if ($quantity == 0)
                                                        <tr>
                                                            <td>{{$sl++}}</td>
                                                            <td>{{str_before($p->product->product_sku,'|')}}<br>{{str_after($p->product->product_sku,'|')}}</td>
                                                            <td>{{$quantity}}</td>
                                                            <td>
                                                                {{$p->product->reorder_limit}}
                                                            </td>
                                                            <td>{{$p->product->category->category_name}}</td>
                                                            <td>
                                                                <a href="{{route('products.show',$p->product->id)}}" title="Click to repurchase">
                                                            <span class="badge badge-{{$quantity?($quantity > $p->product->reorder_limit? "success" :"warning"):"danger"}}">{{$quantity?($quantity > $p->product->reorder_limit? "inStock" :"Reorder"):"OutOfStock"}}</span></a>
                                                            </td>
                                                            </td>
                                                            <td class="">
                                                                <a href="{{route('purchase_history',[$p->product_id,$p->purchase_id])}}" onclick="purchaseHistory(this)" data-toggle="modal" data-target="#myModal" >
                                                                    <img src="{{asset($p->product->image)}}" style="width:50;height:50;">
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    @endif  {{--Check if request is for OutOfStock product--}}
                                                
                                                @endif {{-- Check if request is for out of stock product--}}
                                            @endif
                                                @php $product_id = $p->product_id; @endphp 
                                        @empty
                                            <tr> <td colspan="9">There is no records available</td> </tr>
                                        @endforelse 
                                    @else
                                        @forelse($products as $p)
                                        @if ($p->product->id !== $product_id)
                                            @php $quantity = 0; @endphp
                                            @foreach ($products as $product_quantity) @if ($product_quantity->product_id == $p->product_id) @php $quantity = $quantity +  $product_quantity->stock_quantity; @endphp @endif  @endforeach
                                                <tr>
                                                    <td>{{$sl++}}</td>
                                                    <td>{{str_before($p->product->product_sku,'|')}}<br>{{str_after($p->product->product_sku,'|')}}</td>
                                                    <td>{{$quantity}}</td>
                                                    <td>
                                                        {{$p->product->reorder_limit}}
                                                    </td>
                                                    <td>{{$p->product->category->category_name}}</td>
                                                    <td>
                                                        <a href="{{route('products.show',$p->product->id)}}" title="Click to repurchase">
                                                    <span class="badge badge-{{$quantity?($quantity > $p->product->reorder_limit? "success" :"warning"):"danger"}}">{{$quantity?($quantity > $p->product->reorder_limit? "inStock" :"Reorder"):"OutOfStock"}}</span></a>
                                                    </td>
                                                    </td>
                                                    <td class="">
                                                        <a href="{{route('purchase_history',$p->product_id)}}" onclick="purchaseHistory(this)" data-toggle="modal" data-target="#myModal" >
                                                            <img src="{{asset($p->product->image)}}" style="width:50;height:50;">
                                                        </a>
                                                    </td>
                                                </tr>
                                                @php $product_id = $p->product_id; @endphp 
                                            @endif
                                        @empty
                                            <tr> <td colspan="9">There is no records available</td> </tr>
                                        @endforelse 
                                    @endif 
                                </tbody>
                            </table>
                        </div>
                        {{-- {{$products->appends(request()->query())->links()}} --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
      <div class="modal-dialog modal-lg">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
              <h4 class="modal-title" style="text-align:center" id="purchaseHistoryHeading">Product Purshase History</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
                <div class="row">
                    <div class="col-md-4" id="product_image">
                        
                    </div>
                    <div class="col-md-8">
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered">
                            <thead>
                                <tr>
                                    <td>Date</td>
                                    <td>Invoice No</td>
                                    <td>BatchID</td>
                                    <td>Available</td>
                                    <td>Purchase Price</td>
                                    <td>Selling Price</td>
                                    <td>Supplier Name</td>
                                </tr>
                            </thead>
                            <tbody id="purchaseHistoryTableBody">
                                <img src="" alt="">
                            </tbody>
                        </table>                        
                    </div>
                </div>

              </div>
                
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
        
      </div>
    </div>
       
@endsection
@push('js')
    <script>
        // show product details in modal
        function purchaseHistory(route){
            {{--  alert(route)  --}}
            $.get(route , function( data ) {
                $('#purchaseHistoryTableBody').empty();
                $('#product_image').empty();
                var productImageSrc = "{{url('')}}/"+data[0].product.image;
                $('#product_image').prepend("<img src='"+productImageSrc+"' width='100%'/>");
                {{--  console.log(data);  --}}
               $('#purchaseHistoryHeading').text(data[0].product.product_sku);
                var purchaseHistorytr = "";
                for(i=0; i<data.length;i++){
                    purchaseHistorytr += "<tr>";
                    purchaseHistorytr += "<td>"+data[i].created_at.split(" ")[0]+"</td>";
                    purchaseHistorytr += "<td>"+data[i].purchase.invoice_no+"</td>";
                    purchaseHistorytr += "<td><span class='badge badge-success'>Batch "+data[i].purchase.id+"</span></td>";
                    purchaseHistorytr += "<td><span class='badge badge-info'>"+data[i].stock_quantity+"</span></td>";
                    purchaseHistorytr += "<td><span class='badge badge-info'>Tk. "+data[i].purchase.purchase_unit_price+"</span></td>";
                    purchaseHistorytr += "<td><span class='badge badge-success'>Tk. "+data[i].purchase.sell_unit_price+"</span></td>";
                    purchaseHistorytr += "<td>"+data[i].purchase.supplier.supplier_name+"</td>";
                    purchaseHistorytr +="</tr>";
                }
                $('#purchaseHistoryTableBody').append(purchaseHistorytr);
              });
        }
    </script>
@endpush