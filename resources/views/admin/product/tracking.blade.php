@extends('layouts.admin')
@push('css')
    body{
        <!-- backgorund-color:black; -->
    }
@endpush
@section('content')
<div class="row" style="min-height: 522px;">
    <div class="col-md-10 d-flex align-items-stretch grid-margin class_commented_below">
        <!-- col-md-10 d-flex align-items-stretch grid-margin -->
        <div class="row flex-grow">
            <div class="card">
                <div class="card-body">
                    <form action="{{route('product_tracking')}}" method="get">
                        @csrf
                        <div class="form-group">
                            <label for="product_id">Select Product Name</label>
                            <input type="text" list='productList' autocomplete="off" value="{{request('product_id')}}"
                                name="product_id" class="form-control" id="product_id" placeholder="Product Name"
                                required>
                            <input type="submit" class='btn btn-info' value="Get Track">
                            <datalist id="productList">
                                @forelse ($products as $p)
                                <option value="{{$p->id.'--'.$p->product_sku}}">
                                    @empty
                                    <p>No Product Registered!</p>
                                    @endforelse
                            </datalist>
                        </div>

                    </form>
                </div>
            </div>
            @if(request('product_id'))
            <div class="row">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">&nbsp; Purchase History of {{str_after(request('product_id'),'--')}}</h4>
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Date</th>
                                        <th>invoice no</th>
                                        <th>Qty</th>
                                        <th>Return Qty</th>
                                        <th>Balance Qty</th>
                                        <th>Supplier</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if($productTracks!=null)
                                    @php
                                        $balance_purchase_qty=0;
                                    @endphp
                                    @forelse($productTracks->purchases as $pt)
                                    <tr>
                                        <td>{{$pt->id}}</td>
                                        <td>{{date('Y-m-d',strtotime($pt->created_at))}}</td>
                                        <td>{{$pt->invoice_no}}</td>
                                        <td>{{$pt->quantity}}</td>
                                        <td>
                                            @php
                                            $rqty=0;
                                            @endphp
                                            @forelse($pt->purchase_return as $ptr)
                                            @php
                                            $rqty+=$ptr->purchase_return_quantity;
                                            @endphp
                                            @empty
                                            @endforelse
                                            {{$rqty}}</td>
                                        <th>
                                            @php
                                                $balance_purchase_qty+=($pt->quantity-$rqty)
                                            @endphp
                                            {{ ($pt->quantity-$rqty) }}
                                        </th>
                                        <td>{{$pt->supplier->supplier_name}}</td>
                                    </tr>
                                    @empty
                                    <tr>
                                        <td colspan="8">There is no records available</td>
                                    </tr>
                                    @endforelse
                                    <tr class="table-active">
                                        <th colspan="5" class="text-right">Total Purchase Qty</th>
                                        <th>{{$balance_purchase_qty}}</th>
                                        <td></td>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                @endif
                @if(request('product_id'))
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Sales History of {{str_after(request('product_id'),'--')}}</h4>
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Date</th>
                                        <th>invoice no</th>
                                        <th>Qty</th>
                                        <th>Return Qty</th>
                                        <th>Balance Qty</th>
                                        <th>Customer</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if($productTracks!=null)
                                    @php
                                        $balance_sold_qty=0;
                                    @endphp
                                    @forelse($productTracks->sales_details as $st)
                                    <tr>
                                        <td>{{$st->id}}</td>
                                        <td>{{date('Y-m-d',strtotime($st->created_at))}}</td>
                                        <td>{{$st->sale->invoice_no}}</td>
                                        <td>{{$st->sold_quantity}}</td>
                                        <td>
                                            @php
                                            $rqty=0;
                                            @endphp
                                            @forelse($st->sales_return as $str)
                                            @php
                                            $rqty+=$str->sold_return_quantity;
                                            @endphp
                                            @empty
                                            @endforelse
                                            {{$rqty}}</td>
                                            <th>
                                                @php
                                                 $balance_sold_qty+=($st->sold_quantity - $rqty); 
                                                @endphp
                                            {{ ($st->sold_quantity - $rqty) }}
                                            </th>
                                            <td>{{$st->sale->customer->customer_name}}</td>
                                    </tr>
                                    @empty
                                    <tr>
                                        <td colspan="8">There is no records available</td>
                                    </tr>
                                    @endforelse
                                    <tr class="table-active">
                                        <th colspan="5" class="text-right">Total Sold Qty</th>
                                        <th>{{$balance_sold_qty}}</th>
                                        <td></td>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                @endif
            </div>
        </div>
    </div>
</div>
@endsection
@push('js')
<script>
    function printData(){
        var contentPrint=document.getElementById('barcodeRow').innerHTML;
        var contentOrg=document.body.innerHTML;
        document.body.innerHTML=contentPrint;
        window.print();
        document.body.innerHTML=contentOrg;
    }
</script>
@endpush