@extends('layouts.admin')
@push('css')
    body{
        <!-- backgorund-color:black; -->
    }
@endpush
@section('content')
<div class="row" style="min-height: 522px;">
    <div class="class_commented_below">
    <!-- col-md-10 d-flex align-items-stretch grid-margin -->
      <div class="row flex-grow">
            <div class="card">
              <div class="card-body">
              <form  action="{{route('barcode')}}"  method="post">
              @csrf
                <div class="form-group">
                    <label for="product_name">Select Product Name</label>
                    <input type="text" list='productList' autocomplete="off" value="{{request('barcodeData')}}" name="barcodeData"  class="form-control" id="product_name" placeholder="Product Name" required>
                    <datalist id="productList">
                        @forelse ($product as $p)
                            <option value="{{$p->product_barcode.'--'.$p->product_sku}}">
                        @empty
                            <p>No Product Registered!</p>
                        @endforelse
                    </datalist> 
                </div>
                <div class="form-group">
                <label for="barcode_column">Barcode Per Column</label>
                    <select name="barcode_column" id="barcode_column" class="form-control">
                        <option {{isset($barcode)?$barcode['barcode_column'] == 4?'selected':'':''}} value="4">3</option>
                        <option {{isset($barcode)?$barcode['barcode_column'] == 3?'selected':'':''}} value="3">4</option>
                    </select>
                </div>
                <div class="form-group">
                <label for="barcodeQty">Quantity</label>
                <input type="number" id="barcodeQty" class="form-control" value="{{request('barcodeQty')}}" name="barcodeQty" placeholder="Enter Quentity" required>
                </div>
                <div class="form-group form-check">
                <label class="form-check-label">
                    <input class="form-check-input" type="checkbox" {{ request('product_name') ? 'checked' : '' }}  name="product_name"> Print Product Name.
                </label>
                </div>
                <div class="form-group form-check">
                <label class="form-check-label">
                    <input class="form-check-input" type="checkbox" {{ request('product_attribute') ? 'checked' : '' }} name="product_attribute"> Print Product Attributes.
                </label>
                </div>
                <div class="form-group form-check">
                <label class="form-check-label">
                    <input class="form-check-input" type="checkbox" {{ request('selling_price') ? 'checked' : '' }} name="selling_price">  Print Product Selling Price.
                </label>
                </div>
                <div class="form-group form-check">
                <label class="form-check-label">
                    <input class="form-check-input" type="checkbox" {{ request('barcode_number') ? 'checked' : '' }} name="barcode_number"> Print Barcode Number
                </label>
                </div>
                <button type="submit" class="btn btn-primary  mb-2 mr-sm-2">Generate</button>
                @if (request('barcodeData') !== null && request('barcodeQty') !== null)
                    <input type="button" value="Print" id="btn_print" onclick="printData()" class="btn btn-primary mb-2 mr-sm-2">
                @endif
            </form>
                      <!-- <form action="{{route('barcode')}}"  method="post" class="form-inline"> 
                          @csrf
                              <input type="text" list='productList' autocomplete="off" name="barcodeData" class="form-control mb-2 mr-sm-2 border-success" placeholder="Select Product">
                              <datalist id="productList">
                                @forelse ($product as $p)
                                  <option value="{{$p->product_barcode.'--'.$p->product_sku}}">
                                @empty
                                    <p>No Product Registered!</p>
                                @endforelse
                              </datalist> 
                              <input type="text" class="form-control mb-2 mr-sm-2 border-success" name="barcodeQty" placeholder="Enter Quentity">
                              <input type="submit" value="Generate" class="btn btn-success mb-2 mr-sm-2">
                              @if (request('barcodeData') !== null && request('barcodeQty') !== null)
                                  <input type="button" value="Print" id="btn_print" onclick="printData()" class="btn btn-primary mb-2 mr-sm-2">
                              @endif
                      </form> -->
                  </div>
        </div>
        <div class="row" id="barcodeRow">
                <div class="card">
                  <div class="card-body row">
                      @if (isset($barcode) && $barcode !== null )
                        @for ($i = 0; $i < $barcode['qty']; $i++)
                            <div class="card-deck col-md-{{$barcode['barcode_per_column']}}">
                                <div class="card">
                                  <div class="card-body">
                                  @if($barcode['show_product_name'])
                                    <p class="small mb-0"><b>Name: </b>{{$barcode['productname']}}</p>
                                  @endif
                                  @if($barcode['show_product_attribute'])
                                    <p class="small mb-0"><b>Attributes: </b>{{$barcode['productattribute']}}</p>
                                  @endif
                                  @if($barcode['show_product_price'])
                                    <p class="small mb-0"><b>Price: BDT-</b>{{$barcode['product_price']}}</p>
                                  @endif
                                    <span  style="text-align:justify">
                                        <?php
                                            $generator = new Picqer\Barcode\BarcodeGeneratorPNG();
                                            if($barcode['barcode_type'] == 'TYPE_EAN_8'){
                                                echo '<img style="width:100%" src="data:image/png;base64,' . base64_encode($generator->getBarcode($barcode['productbarcode'], $generator::TYPE_EAN_8)) . '">';
                                            }
                                            elseif($barcode['barcode_type'] == 'TYPE_EAN_13'){
                                                echo '<img style="width:100%" src="data:image/png;base64,' . base64_encode($generator->getBarcode($barcode['productbarcode'], $generator::TYPE_EAN_13)) . '">';
                                            }
                                            elseif($barcode['barcode_type'] == 'TYPE_CODE_128'){
                                                echo '<img style="width:100%" src="data:image/png;base64,' . base64_encode($generator->getBarcode($barcode['productbarcode'], $generator::TYPE_CODE_128)) . '">';
                                            }
                                        ?>
                                    </span>
                                    @if($barcode['show_product_barcode'])
                                        <h5 style="letter-spacing:{{$barcode['barcode_per_column']==4?'2.2em':'1.223em'}};text-align: center;">{{$barcode['productbarcode']}}</h5>
                                    @endif
                                   
                                  </div>
                                </div>
                                <hr>
                            </div>
                        @endfor   
                      @endif
                  </div>
                </div>
        </div>
      </div>
    </div>
</div>
@endsection
@push('js')
<script>
		function printData(){
			var contentPrint=document.getElementById('barcodeRow').innerHTML;
			var contentOrg=document.body.innerHTML;
			document.body.innerHTML=contentPrint;
			window.print();
			document.body.innerHTML=contentOrg;
		}
	
	</script>
@endpush