@extends('layouts.admin')

@section('content')
<div class="row">
<div class="col-md-10 d-flex align-items-stretch grid-margin">
  <div class="row flex-grow">
	<div class="col-md-12">
	  <div class="card">
		<div class="card-body">
            <h4 class="card-title">Product Register form</h4>
            <form class="forms-sample" method="post" action="{{route('products.store')}}" enctype="multipart/form-data">
              @csrf
              <div class="form-group">
                <label for="product_name">Product Name</label>
                <input type="text" class="form-control" name="product_name" id="product_name" placeholder="Enter product Name">
                <small class="form-text text-danger">Brand Name and Cofiguration Will Autometically prefixed and Suffixed cronologically</small>
              </div>
              <div class="form-group">
                <label for="short_description">Short Description</label>
                <input type="text" class="form-control" name="short_description" id="short_description" placeholder="Enter short description of product">
              </div>
              <div class="form-group">
                <label for="long_description">Long Description</label>
                <textarea class="form-control" name="long_description" id="long_description" placeholder="Enter short description of product"></textarea>
              </div> 
              <div class="form-group row">
                <div class="col-md-6">
                <label for="product_img">Product Image </label>
                <div class="input-group">
                  <div class="custom-file">
                <input type="file" class="custom-file-input" name="product_img" id="product_img" aria-describedby="inputGroupFileAddon01">
                <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
              </div>
            </div>
              </div>
              <div class="col-md-6"> 
                <label for="reorder_limit">ReOrder Limit</label>
                <input type="number" class="form-control" name="reorder_limit" id="reorder_limit" value='10'>
              </div> 
              </div>
              
              <div class="row">
              <div class="form-group col-md-6">
                <label for="category_id">Category</label>
                <select  class="form-control" id="category_id" name="category">
                 <option value="">Select a Category</option>
                 @forelse($categories as $cp)
                 @if(!$cp->parent_id)
                    <option value="{{$cp->id}}" style="font-weight:bold">{{$cp->category_name}}</option>
                      @foreach($cp->childCategory as $cop)
                          <option value="{{$cop->id}}" class="text-info">&nbsp;&nbsp;-> {{$cop->category_name}}</option>
                          @foreach($cop->childCategory as $coop)
                          <option value="{{$coop->id}}" class="text-primary">&nbsp;&nbsp;&nbsp;&nbsp; ->> {{$coop->category_name}}</option>
                          @endforeach
                      @endforeach
                @endif
                 @empty
                 <option>{{'There is no category'}}</option>    
                 @endforelse
               </select>
             </div>
             <div class="form-group col-md-6">
              <label for="brand_id">Brand</label>
              <select  class="form-control" id="brand_id" name="brand">
                <option value="">Select Brand</option>
                @forelse($brands as $brand)
                  @if(!$brand->parent_id)
                    <option value="{{$brand->id."|".$brand->brand_name}}" style="font-weight:bold">{{$brand->brand_name}}</option>
                    @foreach($brand->child_brands as $cbr)
                      <option value="{{$cbr->id."|".$cbr->brand_name}}" class="text-info">&nbsp;&nbsp;->{{$cbr->brand_name}}</option>
                    @endforeach
                  @endif
                @empty
                <option>{{'There is no brand'}}</option>
                @endforelse
             </select>
           </div>
              </div>
             <div class="form-group">
              <label for="attribute_set">Attribute Set</label>
              <div class="input-group mb-3">
              <select class="form-control" id="attribute_set" name="attribute_set" onchange="attrSet()" aria-describedby="basic-addon-attribute">
              
               <option value="">Select attribute set</option>
               @forelse($attribute_set as $ap)
               <option value="{{$ap->id}}">{{$ap->attribute_set_name}}</option>
               @empty
               <option>{{'There is no attribute set'}}</option>
               @endforelse
             </select>
             <div class="input-group-append">
             <button type="button" class="btn btn-md btn-warning" onclick="attrSet(0)" title="To Add multiple Cofigurable Product at Once"><i class="mdi mdi-tooltip-outline-plus"></i></button>
            </div>  
            </div>
             </div>
      
           
           <div class="form-group" id="attribue_value">
           
           </div>
           <div id="barcode">
              <div class="form-group">
                  <label>BarCode</label>
                    <input type="number" class="form-control" name="product_barcode[]" id="product_barcode" placeholder="Enter product Barcode">
                    <small class="form-text text-danger">Optional But Make Easy To Search</small>
                </div>
           </div>
           
           
           {{-- <div class="form-group">
              <label for="vat_id">Vat</label>
              <select  class="form-control" id="vat_id" name="vat">
               <option value="">Select Vat</option>
               @forelse($vat as $vtl)
               <option value="{{$vtl->id}}">{{$vtl->vat_title}}</option>
               @empty
               <option>{{'There is no vat'}}</option>
               @endforelse
             </select>
           </div> --}}
           <button type="submit" class="btn btn-success mr-2">Submit</button>
          <a  href="{{url('products.index')}}" class="btn btn-danger">Cancel</a>
         </form>
       </div>
	  </div>
	</div>
  </div>
</div>

</div>


<script>
 function attrSet(it=1){
 var attribute_set_id = document.getElementById("attribute_set").value;
  if(it){
    $('#attribue_value').empty();
    $('#barcode').empty();
  }
      //alert(attribute_set_id);"<?=url()->current()?>"+"/"+id;
        if(attribute_set_id){
            $.ajax({
                type:'GET',
                url:"<?=url('attributes_set_ajaxdata')?>"+"/"+attribute_set_id,
                success:function(html){
                 $('#attribue_value').prepend(html);
                //  $('#barcode').prepend(html.split(" | ")[1]);
                }
            }); 
        }else{
            }
 }
</script>
@endsection