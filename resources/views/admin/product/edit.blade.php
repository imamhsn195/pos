@extends('layouts.admin')

@section('content')
<div class="row">
<div class="col-md-10 d-flex align-items-stretch grid-margin">
  <div class="row flex-grow">
	<div class="col-md-12">
	  <div class="card">
		<div class="card-body">
            <h4 class="card-title">Product Register form</h4>
            <form class="forms-sample" method="post" action="{{route('products.update',$product->id)}}" enctype="multipart/form-data">
              @csrf
              @method('put')
              <div class="form-group">
                <label for="product_name">Product Name</label>
                <input type="text" class="form-control" name="product_name" id="product_name" value="{{$product->product_sku}}" placeholder="Enter product Name">
              </div>
              <div class="form-group">
                <label for="short_description">Short Description</label>
                <input type="text" class="form-control" name="short_description" id="short_description" value="{{$product->short_description}}" placeholder="Enter short description of product">
              </div>
              <div class="form-group">
                <label for="long_description">Long Description</label>
                <input type="text" class="form-control" name="long_description" id="long_description" value="{{$product->long_description}}" placeholder="Enter short description of product">
              </div> 
              <div class="form-group row">
           
                <div class="col-md-7">
                <label for="product_img">Product Image </label>
                <input type="file" class="form-control-file" name="product_img" id="product_img">
                <img src="{{asset($product->image)}}" width="80px">
              </div>
              <div class="col-md-5"> 
                <label for="reorder_limit">ReOrder Limit</label>
                <input type="number" class="form-control" name="reorder_limit" id="reorder_limit" value="{{$product->reorder_limit}}">
              </div> 
              </div>
              
              
              <div class="form-group">
                <label for="category_id">Category</label>
                <select  class="form-control" id="category_id" name="category">
                  @forelse($categories as $cp)
                  @if(!$cp->parent_id)
                     <option value="{{$cp->id}}" {{ $product->category_id==$cp->id ? "selected":'' }} style="font-weight:bold">{{$cp->category_name}}</option>
                       @foreach($cp->childCategory as $cop)
                           <option value="{{$cop->id}}" {{ $product->category_id==$cop->id ? "selected":'' }} class="text-info">&nbsp;&nbsp;-> {{$cop->category_name}}</option>
                           @foreach($cop->childCategory as $coop)
                           <option value="{{$coop->id}}" {{ $product->category_id==$coop->id ? "selected":'' }} class="text-primary">&nbsp;&nbsp;&nbsp;&nbsp; ->> {{$coop->category_name}}</option>
                           @endforeach
                       @endforeach
                 @endif
                  @empty
                  <option>{{'There is no category'}}</option>    
                  @endforelse
                  {{-- ------
                 @forelse($categories as $cp)
                  @if(!$cp->parent_id)
                    <option value="{{$cp->id}}" {{ $product->category_id==$cp->id ? "selected":'' }}>{{$cp->category_name}}</option>
                      @foreach($categories as $cop)
                        @if($cop->parent_id==$cp->id)
                          <option value="{{$cop->id}}" {{ $product->category_id==$cop->id ? "selected":'' }}>{{$cop->category_name}}</option>
                        @endif
                      @endforeach
                  @endif
                 @empty
                 <option>{{'There is no category'}}</option>    
                 @endforelse --}}
               </select>
             </div>
         
             <div class="form-group row">
              <label for="attribute_set">Attribute Set</label>
              <select class="form-control col-sm-11" id="attribute_set" name="attribute_set" onchange="attrSet()">
              
               <option value="">Select attribute set</option>
               @forelse($attribute_set as $ap)
               <option value="{{$ap->id}}"  {{ $product->attribute_set_id==$ap->id ? "selected":'' }}>{{$ap->attribute_set_name}}</option>
               @empty
               <option>{{'There is no attribute set'}}</option>
               @endforelse
             </select>
             {{-- <button type="button" class="btn btn-xs btn-success  form-control col-sm-1" onclick="attrSet(0)">+more</button> --}}
             </div>
           @if($product->attributeset)
           <div class="form-group" id="attribue_value">
              <div class="row">
                  <?php $a=0; ?>
             @foreach($product->attributeset->attribute as $atts)
                  <div class="col-md-4">
                      <label for=''>{{$atts->attribute_name}}</label>
                      <select  class="form-control" name="attribute_submited_value[]">
                        
                        @foreach($attribute as $attval)
                        @if($attval->parent_id==$atts->id)
                          <option value="{{$attval->id}}" {{ $product->submittedattribute[$a]->id==$attval->id ? "selected":'' }}>{{$attval->attribute_name}}</option>
                          @endif
                         
                        @endforeach
                      </select>
                    </div>
                    <?php $a++;?>
              @endforeach
            </div>
           </div>
           @endif
           <div class="form-group">
              <label for="brand_id">Brand</label>
              <select  class="form-control" id="brand_id" name="brand">
                <option value="">Select Brand</option>
              @forelse($brands as $brand)
                @if(!$brand->parent_id)
                  <option value="{{$brand->id."|".$brand->brand_name}}" style="font-weight:bold" {{ $product->brand_id==$brand->id ? "selected":'' }}>{{$brand->brand_name}}</option>
                  @foreach($brand->child_brands as $cbr)
                    <option value="{{$cbr->id."|".$cbr->brand_name}}" class="text-info" {{ $product->brand_id==$cbr->id ? "selected":'' }}>&nbsp;&nbsp;->{{$cbr->brand_name}}</option>
                  @endforeach
                @endif
              @empty
              <option>{{'There is no brand'}}</option>
              @endforelse


                {{-- @forelse($brands as $brand)
                <option value="{{$brand->id}}" {{ $product->brand_id==$brand->id ? "selected":'' }}>{{$brand->brand_name}}</option>
                @empty
                <option>{{'There is no brand'}}</option>
                @endforelse --}}
             </select>
           </div>
           <div class="form-group">
            <label for="product_barcode">BarCode</label>
            <input type="text" class="form-control" name="product_barcode" id="product_barcode" value="{{$product->product_barcode}}" placeholder="Enter Product Barcode">
          </div>
           {{-- <div class="form-group">
              <label for="vat_id">Vat</label>
              <select  class="form-control" id="vat_id" name="vat">
               <option value="">Select Vat</option>
               @forelse($vat as $vtl)
               <option value="{{$vtl->id}}" {{ $product->vat_id==$vtl->id ? "selected":'' }}>{{$vtl->vat_title}}</option>
               @empty
               <option>{{'There is no vat'}}</option>
               @endforelse
             </select>
           </div> --}}
           <button type="submit" class="btn btn-success mr-2">Submit</button>
           <a href="{{route('products.index')}}"  class="btn btn-danger">Cancel</a>
         </form>
       </div>
	  </div>
	</div>
  </div>
</div>

</div>


<script>
 function attrSet(it=1){
 var attribute_set_id = document.getElementById("attribute_set").value;
  if(it){
    $('#attribue_value').empty();
  }
      //alert(attribute_set_id);
        if(attribute_set_id){
          //alert(attribute_set_id);
            $.ajax({
                type:'GET',
                url:"<?=url('attributes_set_ajaxdata')?>"+"/"+attribute_set_id,
                // data:'',
                success:function(html){
                 $('#attribue_value').prepend(html);
                }
            }); 
        }else{
            }
 }
</script>
@endsection