@extends('layouts.admin')

@section('content')
<div class="row">
<div class="col-md-12">{{--  d-flex grid-margin --}}
  <div class="row flex-grow">
	<div class="col-md-12">
    
    <div class="card">
		<div class="card-body">
      <h2 class="text-center">All Products</h2>
        <div class="row">
            {{-- <div class="col-md-6"></div> --}}
            <div class="col-md-12">
                <form action="{{route('products.index')}}" method="GET">
                  <div class="input-group float-right">
                      <input class="form-control py-2 border-primary" placeholder="Search here.." name="searchby" type="text" id="example-search-input" required="">
                      <span class="input-group-append">
                        <button class="btn btn-primary" type="submit">
                            <i class="mdi mdi-magnify"></i>
                        </button>
                        <a class="btn btn-primary" title="Register New Product" href="{{route('products.create')}}"><i class="mdi mdi-playlist-plus"></i></a>  
                        @if (request()->query())
                        <a class="btn btn-primary" title="Show All Product" href="{{route('products.index')}}"><i class="mdi mdi-format-list-bulleted"></i></a> 
                        @endif
                      </span>
                  </div>
                </form>
            </div>
          </div>
      <br>
      <div class="table-responsive">
        <table class="table table-striped table-bordered">
          <thead>
            <tr>
              <th>ID</th>
              <th>Image</th>
              <th>Product Details</th>
              <th>Category</th>
              <th>Created By</th>
              <th class="text-center">Actions</th>
            </tr>
          </thead>
          <tbody>
        
            @forelse($products as $key=>$wr)
            <tr>
              <td>{{(($products->currentPage() - 1) * $products->perPage() + $key+1)}}</td>
              <td><img src="{{$wr->image !== ''?$wr->image:'default/images/default.png'}}" width="120px" class="rounded"></td>
              <td>
                <b>Name:</b> {{str_before($wr->product_sku,"|")}}<br>
                <b>Configuration:</b> 
                {{--  {{str_after($wr->product_sku,"|")}}  --}}
              <?php $confiqr='';
                $a=0;?>
                @forelse($wr->submittedattribute as $confiq)
                  <?php $confiqr.= $confiq->attribute_name.', ';?>
                  @empty
                  <?php $confiqr="";?>
                @endforelse
                {!!$confiqr!!}
              </td>
              <td>{{$wr->category->category_name}}</td>
              <td title="{{$wr->users->name}}">{{$wr->users->user_type}}</td>
        
              <td class="text-center">
                  <span class="input-group-append">
                <form action="{{route('products.destroy',$wr->id)}}" method="post">
                    @csrf
                    @method('delete')
                  {{-- <a class="btn btn-success btn-sm" title="Purchase" href="{{route('products.show',$wr->id)}}">
                    <i class="mdi mdi-cart-plus"></i>
                  </a> --}}
                  <a href="{{route('products.edit',$wr->id)}}" class="btn btn-dark btn-sm">
                    <i class="mdi mdi-table-edit"></i>
                  </a>
                  <button type="submit" onclick="return confirm('Are you sure to delete this?')" class="btn btn-danger btn-sm">
                      <i class="mdi mdi-delete"></i>
                  </button>
                   
                  
                </form>
                <span class="input-group-append">
              </td>
            </tr>
            @empty
              <tr>
                <td colspan="8">There is no records available</td>
              </tr>
            @endforelse
          </tbody>
        
        </table>
        <span class="float-right">{{$products->links()}}</span>
      </div>
		</div>
	  </div>
	</div>
  </div>
</div>

</div>

@endsection