@extends('layouts.admin')

@section('content')

<div class="row">
<div class="col-md-12 d-flex align-items-stretch grid-margin">
  <div class="row flex-grow row_table_responsive">
	<div class="col-md-12">
	  <div class="card">
		<div class="card-body">
      <h2 class="text-center">Purchase Return History</h2>
      @if(Auth::user()->user_type=="superadmin")
                        <h4 class="text-center">Showing All Branches</h4>
                        @else
                        <h4 class="text-center">Showing {{Auth::user()->branch->name}} Branch</h4>
                        @endif
                        <p class="card-description text-center">
                            <form method="get" action="{{route('purchase_returns.index')}}">
                                <div class="input-group col-md-6 float-right">
                                    <label for="example-search-input1">From :</label>
                                    <input class="form-control py-2" @if(isset($_GET['searchbyfrom']))
                                        value="{{$_GET['searchbyfrom']}}" @else value="{{date('Y-m-01')}}" @endif
                                        name="searchbyfrom" type="date" id="example-search-input1">
                                    <label for="example-search-input2">To :</label>
                                    <input class="form-control py-2" placeholder="To.." name="searchbyto" type="date"
                                        id="example-search-input2" @if(isset($_GET['searchbyto']))
                                        value="{{$_GET['searchbyto']}}" @else value="{{date('Y-m-t')}}" @endif>
                                    <span class="input-group-append">
                                        <button class="btn btn-primary" type="submit">
                                            <i class="mdi mdi-magnify"></i>
                                        </button>
                                    </span>

                                </div>
                            </form>
                        </p>
		  <div class="table-responsive">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>ID</th>
                          <th>Date</th>
                          <th>Product Sku</th>
                          <th>Qty</th>
                          <th>unit price</th>
                          <th>SubTotal</th>
                          <th>Supplier</th>
                          <th>Purchased by</th>
                          
                          
                        </tr>
                      </thead>
                      <tbody>
            <?php $i =1;
            $total=0; ?>
					  @forelse($purchase_returns as $pr)
                        <tr>
                          <td>{{$i++}}</td>
                          <?php $time=strtotime($pr->created_at);
                            $date=date("d-M-Y",$time);
                            $timm=date("h:i:s",$time);?>
                            <td title="{{$timm}}">{{$date}}</td>
                          <td>{{$pr->purchase->product->product_sku}}</td>
                          <td>{{$pr->purchase_return_quantity}}</td>
                          <td>{{$pr->purchase_return_unit_price}}</td>
                          <?php
                          $subtotal=$pr->purchase_return_quantity*$pr->purchase_return_unit_price;
                          $total=$total+$subtotal;?>
                          <td>{{number_format($subtotal,2)}}</td>
                          <td>{{$pr->purchase->supplier->supplier_name}}</td>
                          <td title="{{$pr->users->name}}">{{$pr->users->user_type}}</td>
                          <!-- <td>
                            <a href="{{route('attribute_sets.edit',$pr->id)}}" class="btn btn-link">Edit</a>
							            </td> -->
                          
						  
							
                        </tr>
					@empty
						<tr>
							<td colspan="8">There is no records available</td>
						</tr>
					@endforelse
                      </tbody>
                      <tfoot>
                        <th colspan="5" class="text-right">Total Purchased Amount:</th>
                        <th colspan="3" class="text-left">tk. {{$total}}</th>
                        
                      </tfoot>
                    </table>
                  </div>
		</div>
	  </div>
	</div>
  </div>
</div>

</div>

@endsection