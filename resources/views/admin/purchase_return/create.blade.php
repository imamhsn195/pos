@extends('layouts.admin')
@section('content')
<div class="row">
<div class="col-md-12 d-flex align-items-stretch grid-margin">
  <div class="row flex-grow">
	<div class="col-md-7">
	  <div class="card">
		<div class="card-body">
            <h4 class="card-title">Purchase Return from Here</h4>
            <form class="forms-sample" method="post" action="{{route('purchase_returns.store')}}" enctype="multipart/form-data">
              @csrf           
              <div class="form-group">
                <label for="supplier_id">Supplier</label>
                <select  class="form-control" id="supplier_id" name="supplier_id" onchange="get_invoice()">
                <option value="">Select Supplier</option>
               @forelse($suppliers as $sp)
               <option value="{{$sp->id}}">{{$sp->supplier_name}}</option>
               @empty
               <option>{{'There is no supplier'}}</option>
               @endforelse
               </select>
             </div>

            <div class="form-group" id="invoice_no_select">
            </div>
            
            <input type="hidden" class="form-control" name="purchase_id" id="purchase_id" value="">
                <input type="hidden" class="form-control" name="payment_method_id" id="payment_method_id" value="0">
               
                <input type="hidden" class="form-control" name="branch_inv_id" id="branch_inv_id" value="">

                @if(Gate::allows('isSuperadmin'))
                <div class="form-group">
                    <label for="branch_id">Branch</label>
                    <select required class="form-control" id="branch_id" name="branch_id">
                        <option value="">Select branch</option>
                        @forelse($branches as $bp)
                        <option value="{{$bp->id}}">{{$bp->name}}</option>
                        @empty
                        <option>{{'There is no branch'}}</option>
                        @endforelse
                    </select>
                </div>
                @else
                <input type="hidden" name="branch_id" id="branch_id" value="{{ Auth::user()->branch_id }}">
                @endif
             <div class="form-group" id="product_select">
               
                
                
              </div>
              <div class="form-group">
                <label for="returned_qty">Purchased Quantity</label>
                <input type="number" class="form-control" name="returned_qty" id="returned_qty" onchange="getprice()">
              </div>
              <div class="form-group">
                <label for="remain_qty">Available Quantity</label>
                <input readonly type="number" class="form-control" name="remain_qty" id="remain_qty">
              </div>

              <div class="form-group">
                <label for="p_unit_price">Purchase per Unit Price</label>
                <input readonly type="number" class="form-control" name="p_unit_price" id="p_unit_price" onchange="getprice()" step=".01">
              </div>
            
           <div class="form-group">
                <label for="total_price">Amount to be paid</label>
                <input type="number" class="form-control" name="total_price" id="total_price" value="" readonly>
            </div>
            {{-- <div class="form-group">
              <label for="payment_method_id">Payment Method</label>
              <select required class="form-control" id="payment_method_id" name="payment_method_id"  onchange="addpaidvalue()">
              <option value="">Select Payment Method</option>
              <option value="0">Due</option>
               @foreach($payment_methods as $pm)
               <option value="{{$pm->id}}">{{$pm->method_title}}</option>
               @endforeach
             </select>
           </div> --}}
           <!--form group for paid amount-->
            <div class="form-group" id="payment_tk">
               
            </div>
            <!--form group for warenty type-->
            <div class="form-group" id="warrenty_type_input">
               
            </div>
             <!--form group for product sl no-->
            <div id="product_warranty" class="form-group">
               
            </div>
           
          
           <button type="submit" class="btn btn-success mr-2">Submit</button>
           <a class="btn btn-light" href="{{route('purchase_returns.index')}}">Cancel</a>
         </form>
       </div>
	  </div>
	</div>
 
  </div>
</div>

</div>


<script>
//multiply unit price n qty
 function getprice(){
 var p_unit_price = document.getElementById("p_unit_price").value;
 var purchase_qty = document.getElementById("returned_qty").value;
 var total_price =(purchase_qty*p_unit_price);
  
    $('#total_price').val(total_price);
 }

//input field for duration
//  function showduration(durationId){
//   var durationId='warranty_duration'+durationId;
  
//  var checkType= $('#'+durationId).attr('type')
//    if(checkType=='text'){
//     $('#'+durationId).attr('type','hidden');
//    }else{ 
//     $('#'+durationId).attr('type','text');
//    }
//  }


 //check for invoice and supplier basis purchased history.
 function get_purchase_history(){
  
var invoice_no= document.getElementById("invoice_no").value;
var supplier_id = document.getElementById("supplier_id").value;
var product_id = document.getElementById("product_id").value;
var histry=invoice_no+" | "+product_id;
//check for warranty
 if(invoice_no && supplier_id){
          $.ajax({
                type:'GET',
                url:"<?=url('purchase_history_for_return')?>",
                data:"histry="+histry,
                success:function(html){
                    $('#purchase_id').attr('value',html[0].id);
                    $('#returned_qty').attr('value',html[0].quantity);
                    $('#remain_qty').attr('value',html[0].product_status.stock_quantity);
                    $('#p_unit_price').attr('value',html[0].purchase_unit_price);
                    $('#total_price').attr('value',html[0].purchase_unit_price*html[0].quantity);
                    $('#branch_inv_id').attr('value',html[0].product_status.id);
                    //$('#branch_id').attr('value',html[0].product_status.branch_id);
                    
                  }
            });
        }
 }


 //input field for invoice
 function get_invoice(){
  $('#invoice_no_select').empty();
    var supplier_id = document.getElementById("supplier_id").value;
    $.ajax({
            type:'GET',
            url:"<?=url('purchase_history_for_return')?>",
            data:"supplier_id="+supplier_id,
            success:function(html){
              console.log(html.length);
              var showProduct="<label for='invoice_no'>Invoice</label><select name='invoice_no' id='invoice_no' class='form-control' onchange='get_product()'><option value=''>Select Invoice No.</option>";
                    var i=0;
                    var getLen=html.length;
                    while(i<getLen){
                      showProduct+="<option value='"+html[i].invoice_no+"'>"+html[i].invoice_no+"</option>";
                      i++;
                    }
                    showProduct+="</select>";
                    $('#invoice_no_select').append(showProduct);
                    
                }
    });
    }

 function get_product(){

  $('#product_select').empty();
    var invoice_no= document.getElementById("invoice_no").value;
    var supplier_id = document.getElementById("supplier_id").value;
    var histry=invoice_no+" | "+supplier_id;

    $.ajax({
            type:'GET',
            url:"<?=url('purchase_history_for_return')?>",
            data:"condition="+histry,
            success:function(html){
              console.log(html.length);
              var showProduct="<label for='product_id'>Product</label><select name='product_id' id='product_id' class='form-control' onchange='get_purchase_history()'><option value=''>Select Product</option>";
                    var i=0;
                    var getLen=html.length;
                    while(i<getLen){
                      showProduct+="<option value='"+html[i].product_id+"'>"+html[i].product.product_sku+"</option>";
                      i++;
                    }
                    showProduct+="</select>";
                    $('#product_select').append(showProduct);
                }
    });
    }
 
      
</script>
@endsection