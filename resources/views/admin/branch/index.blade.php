@extends('layouts.admin')

@section('content')

<div class="row">
  <div class="col-md-12 d-flex align-items-stretch grid-margin">
    <div class="row flex-grow row_table_responsive">
     <div class="col-md-12">
       <div class="card">
        <div class="card-body">
            <div class="row">   
                <div class="col-md-12">
                    <form action="{{route('branches.index')}}" method="GET">
                      <div class="input-group float-right">
                          <input class="form-control py-2 border-primary" placeholder="Search here.." name="searchby" type="text" id="example-search-input" required="">
                          <span class="input-group-append">
                            <button class="btn btn-primary" type="submit">
                                <i class="mdi mdi-magnify"></i>
                            </button>
                              {{--Only Supper Admin and admin can see the add branch button --}}
                            @if(Gate::allows('isSuperadmin'))
                              <a class="btn btn-primary" title="Register New Branches" href="{{route('branches.create')}}"><i class="mdi mdi-playlist-plus"></i></a>
                            @endif
                            @if (request()->query())
                              <a class="btn btn-primary" title="Show All Branches" href="{{route('branches.index')}}"><i class="mdi mdi-format-list-bulleted"></i></a> 
                            @endif
                          </span>
                      </div>
                    </form>
                </div>
              </div>
           <div class="table-responsive">
            <table class="table table-striped table-bordered border-primary">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Branch Name</th>
                  <th>Branch Type</th>
                  <th>Contact No.</th>
                  <th>Full Address</th>
            {{--Only Supper Admin and admin can see the action button --}}
                @if(Gate::allows('isSuperadmin') || Gate::allows('isAdmin'))
                  <th colspan="2">Actions</th>
                @endif
                </tr>
              </thead>
              <tbody>
               @forelse($addresses as $key => $add)
               <tr>
                <td>{{(($addresses->currentPage() - 1) * $addresses->perPage() + $key+1)}}</td>
                <td>{{$add->name}}</td>

                <td>
                  <label class="badge badge-{{$add->branch_type?'success':'info'}}">{{$add->branch_type?'Warehouse':'Showroom'}}
                </td>
                <td>{{$add->address?$add->address->phone:''}}</td>
                <td>{{$add->address?$add->address->full_address:''}}</td>
                <span class="input-group-append">
              @if(Gate::allows('isSuperadmin') || Gate::allows('isAdmin'))
              <form action="{{route('branches.destroy',$add->id)}}" method="post">
                <td>
                  <a href="{{route('branches.edit',$add->id)}}" class="btn btn-dark btn-sm"><i class="mdi mdi-table-edit"></i></a>
               
              @endif
              @if(Gate::allows('isSuperadmin')) 
                   
                     @csrf
                     @method('delete')
                     <button type="submit" onclick="return confirm('Are you sure to delete this??')" class="btn btn-danger btn-sm"><i class="mdi mdi-delete"></i></button>
                   </form>
                   @endif
                </span>
                </td>
              
           </tr>
           @empty
           <tr>
             <td colspan="8">There is no records available</td>
           </tr>
           @endforelse
         </tbody>
       </table>
     </div>
   </div>
 </div>
</div>
</div>
</div>

</div>

@endsection