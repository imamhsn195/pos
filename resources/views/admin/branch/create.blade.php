@extends('layouts.admin')

@section('content')

<div class="row">
    <div class="col-md-6 d-flex align-items-stretch grid-margin">
        <div class="row flex-grow">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form class="forms-sample" method="post" action="{{route('branches.store')}}">
                            @csrf
                            <div class="form-group">
                                <label for="branch_name">Branch Name</label>
                                <input type="text" class="form-control" name="name" id="branch_name" placeholder="Enter Branch Name">
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Branch Type</label>
                                <div class="col-sm-4">
                                    <div class="form-radio">
                                        <label class="form-check-label">
                                            <input type="radio" class="form-check-input" value="0" name="branch_type"
                                                id="showroom">
                                            <i class="input-helper">Showroom</i></label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-radio">
                                        <label class="form-check-label">
                                            <input type="radio" class="form-check-input" value="1" name="branch_type"
                                                id="warehouse">
                                            <i class="input-helper">Warehouse</i></label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="email">Email Address</label>
                                <input type="email" class="form-control" name="email" id="email" placeholder="input a valid email id">
                            </div>
                            <div class="form-group">
                                <label for="fulladdress">Address Information</label>
                                <textarea class="form-control" name="fulladdress" id="fulladdress" placeholder="Enter fulladdress"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="phone">Contact No</label>
                                <input type="text" class="form-control" name="phone" id="phone" placeholder="e.g. 018XX-XXXXXX">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputCity1">Begining Balance</label>
                                @foreach($fixed_assets as $asset)

                                <hr>
                                <div class="form-check">
                                    <label class="form-check-label"><span class="float-right badge badge-info">{{$asset->type}}</span>
                                        <input type="checkbox" name="asset[]" onclick="showDiv({{$asset->id}})" class="form-check-input"
                                            value="{{$asset->id}}">
                                        {{$asset->account_head}}
                                        <i class="input-helper"></i></label>
                                </div>
                                <div class="form-group" style="display:none" id="{{$asset->id}}">
                                    <label for="exampleInputCity1">Amount</label>
                                    <input type="number" class="form-control" name="begining_balance[]" id="exampleInputCity1"
                                        placeholder="Amount">
                                </div>
                                <hr>
                                @endforeach
                                <div id="addDiv">

                                </div>
                                <button type="button" class="btn btn-xs btn-success  form-control col-sm-3" onclick="newDiv()">+more
                                    assets</button>
                            </div>
                            <button type="submit" class="btn btn-success mr-2">Submit</button>
                            <a href="{{route('branches.index')}}" class="btn btn-danger">Cancel</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function showDiv(id){
        attType = $('#'+id).css('display');
        if(attType=='none'){
            $('#'+id).css('display','block');
        }else{
             $('#'+id).css('display','none');
        }
    }

    function newDiv(){
        var newPre = '<div class="newAsset"><label for="exampleInputCity1">Title</label>';
        newPre += '<span style="cursor:pointer" class="badge badge-danger assetClose float-right" onclick="hideDiv()">remove</span>';
        newPre +='<input type="text" name="account_head[]" class="form-control" placeholder="Title"/>';
        newPre +='<label for="exampleInputCity1">Select Type</label>';
        newPre +='<select name="type[]" class="form-control">';
        newPre +='<option value="Current Assets">Current Assets</option>';
        newPre +='<option value="Fixed Assets">Fixed Assets</option>';
        newPre +='</select>';
        newPre +='<label for="exampleInputCity1">Amount</label>';
        newPre +='<input type="number" class="form-control" name="begining_balance[]"    placeholder="Amount"/><br><hr></div>';
        $('#addDiv').prepend(newPre);
    }
   function hideDiv(){
    $('.assetClose').click(function(){
   $(this).parent().remove();
});
   }

</script>
@endsection
