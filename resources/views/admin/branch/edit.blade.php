@extends('layouts.admin')

@section('content')

<div class="row">
	<div class="col-md-6 d-flex align-items-stretch grid-margin">
		<div class="row flex-grow">
			<div class="col-12">
				<div class="card">
					<div class="card-body">
						<form class="forms-sample" method="post" action="{{route('branches.update',$branch->id)}}">
							@csrf
              				@method('put')
							<div class="form-group">
								<label for="name">Branch Name</label>
								<input type="text" class="form-control" name="name" value="{{$branch->name}}" id="name" placeholder="Enter Branch Name">
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label">Branch Type</label>
								<div class="col-sm-4">
									<div class="form-radio">
										<label class="form-check-label">
											<input type="radio" class="form-check-input" {{$branch->branch_type?"":"checked"}} value="0" name="branch_type" id="showroom">
											<i class="input-helper">Showroom</i></label>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="form-radio">
											<label class="form-check-label">
												<input type="radio" class="form-check-input" {{$branch->branch_type?"checked":""}} value="1" name="branch_type" id="warehouse">
												<i class="input-helper">Warehouse</i></label>
											</div>
										</div>
									</div>
									<div class="form-group">
										<label for="email">Email Address</label>
										<input type="email" class="form-control" name="email" value="{{$branch->address->email}}" id="email" placeholder="input a valid email id">
									</div>
									<div class="form-group">
										<label for="fulladdress">Address Information</label>
										<textarea class="form-control" name="fulladdress" id="fulladdress" placeholder="Enter fulladdress">{{$branch->address->full_address}}</textarea>
									</div>
									<div class="form-group">
										<label for="phone">Contact No</label>
										<input type="text" class="form-control" name="phone" id="phone" value="{{$branch->address->phone}}" placeholder="e.g. 018XX-XXXXXX">
									</div>
									<button type="submit" class="btn btn-success mr-2">Submit</button>
									<a href="{{route('branches.index')}}" class="btn btn-danger">Cancel</a>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		@endsection