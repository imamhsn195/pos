@extends('layouts.admin') 
@section('content')
<div class="content-wrapper">
	<div class="row">
		<div class="col-md-12 grid-margin stretch-card">
			<div class="card" id="print_portion">
				<div class="card-body">
					@php 
				 		$f = new \NumberFormatter("en", \NumberFormatter::SPELLOUT);//write in php.init ;extension=php_intl.dll
					@endphp
					@if($invoice_setting->logo_enable)
					<h2 class="text-{{$invoice_setting->logo_position?$invoice_setting->logo_position:'left'}}"> <img src="{{$invoice_setting->invoice_logo?asset($invoice_setting->invoice_logo):asset($company->company_logo)}}" id="imageView" style="width:250px;"/></h2>	
					@endif
					<h2 class="text-center">{{$company->company_name}}</h2>	
				<h3 class="text-center">{{$sales->branch->name}} Branch</h3>
					<h4 class="text-center">{{$sales->branch->address!=null ? $sales->branch->address->email:"--"}}</h4>
					<h4 class="text-center">{{$sales->branch->address !=null ? $sales->branch->address->full_address:"----"}}</h4>
					<h3 class="text-center">{{$invoice_setting->invoice_title}}</h3>
					<p class="card-description text-center">{{$invoice_setting->invoice_no_title}}: <b>{{$sales->invoice_no}}</b></p>
					<span class="text-left">Date: <b>{{date("d-F-Y",strtotime($sales->created_at))}}</b></span>
					<span class="text-right float-right">{{$invoice_setting->customer_name}}: <b>{{$sales->customer->customer_name}}</b></span>
					<div class="table-responsive">
						<table width='100%' border="1" cellpadding="10">
							<thead>
								<tr >
									<th>{{$invoice_setting->sl}}</th>
									<th>{{$invoice_setting->product_name}}</th>
									<th>Product SL NO.
                                        {{-- {{$invoice_setting->product_name}} --}}</th>
									<th>{{$invoice_setting->qty}}</th>
									<th>{{$invoice_setting->unit_price}}</th>
									{{-- <th>{{$invoice_setting->total}}</th>
									<th>{{$invoice_setting->discount_type}}</th> --}}
									<th>{{$invoice_setting->discount}}</th>
									<th>{{$invoice_setting->subtotal}}</th>
								</tr>
							</thead>
							<tbody>
								@php 
									$discount=0;
									$kisu = 0;
									$bdlName="hello";
								@endphp
								
								@foreach($Sale_detail as $key=>$sale)
								
								
								@if($sale->discount_type=="bundle")
									@if($bdlName!==$sale->bundle->bundle_name)
									<tr>
										<td>{{$key+1}}</td>
										<td><b>{{ $sale->bundle->bundle_name .' ('.$sale->discount_type.')'}}</b></td>
										<td></td>
										{{--  <td>{{$sale->discount_type}}</td>  --}}
										<td>{{"1"}}</td>
										{{--  <td>{{$sale->bundle->bundle_price + $sale->bundle->bundle_discount_amount }}</td>  --}}
										<td>{{$sale->bundle->bundle_price + $sale->bundle->bundle_discount_amount}}</td>
										<td>{{$sale->bundle->bundle_discount_amount }}</td>
										<td>{{$sale->bundle->bundle_price}}</td>
										@php 
										$discount+=$sale->bundle->bundle_discount_amount;
										$kisu =$kisu+($sale->bundle->bundle_price+$sale->bundle->bundle_discount_amount);
										$bdlName=$sale->bundle->bundle_name;
										@endphp
									</tr>
									@endif
								@endif
								@if($sale->discount_type!=="bundle")
								<tr>
									<td>{{$key+1}}</td>
                                    <td><b>{{str_before($sale->product->product_sku,'|')}}</b><br>
                                        {{str_after($sale->product->product_sku,'|')}}
                                    </td>
                                    <td>
										@forelse($sale->serial_numbers as $psl)
											<i># {{$psl->product_sl_no}}</i>
										@empty
										@endforelse
									</td>
									<td>{{$sale->sold_quantity}}</td>
									<td>{{$sale->sold_unit_price}}</td>
									{{-- <td>{{$sale->getSubtotal()}}</td> --}}
									{{-- <td>{{$sale->discount_type}}</td> --}}
									<td>{{$sale->discount_subtotal}}</td>
									<td>{{$sale->getSubtotal()-$sale->discount_subtotal}}</td>
									@php 
									$discount+=$sale->discount_subtotal;
									$kisu = $kisu + $sale->getSubtotal();
									 @endphp
								</tr>
								@endif
								@endforeach
							</tbody>
							<tfoot>
								<tr>
									<th colspan="4">{{$invoice_setting->grand_total}} :</th>
									<th> {{$kisu}}</th>
									{{-- <th></th> --}}
									<th> {{$discount}}</th>
									<th> {{$gt=$kisu-$discount}}</th>
								</tr>
								<tr><th colspan="7" class="text-center">-------------------------------------</th></tr>
								<tr>
									<th colspan="6">{{$invoice_setting->sales_total}} :</th>
									<th>{{$kisu}}</th>
									
								</tr>
								@if($sales->discount_total>0 || $discount>0)
								<tr>
								<th colspan="3">{{$invoice_setting->discount_total}} :</th>
								@if($sales->discount_total>0)
								<th colspan="2">{{$invoice_setting->coupon}} {{'@'}} {{$sales->discount_offer_details?$sales->discount_offer_details->discount_amount:''}}{{' TK'}}</th>
									<th> {{$coupon=$sales->discount_total?$sales->discount_total:0}}</th>
									<th></th>
									@else
									<th colspan="2"></th>
									<th></th>
									<th></th>
									@php $coupon=$sales->discount_total?$sales->discount_total:0;@endphp
								@endif

								</tr>
								<tr>
								<th colspan="3"></th>
								<th colspan="2">{{$invoice_setting->single_discount}} :</th>
								<th style="border-bottom:black solid 1px"> {{$discount}}</th>
								<th style="border-bottom:black solid 1px">({{$grossDiscount=$discount+$coupon}})</th>
								</tr>
								
								<tr >
									<th colspan="3">{{$invoice_setting->discounted_total}} :</th>
									<th colspan="2"></th>
									<th></th>
								<th>{{$kisu-$grossDiscount}}</th>
								</tr>
								@else
								@php
								$coupon=$sales->discount_total?$sales->discount_total:0; 
								$grossDiscount=$discount+$coupon @endphp
								@endif 
								{{-- end both single and coupon discount check --}}
								@if($sales->total_tax>0)
								<tr>
								<th colspan="3"></th>
									<th colspan="2">{{$invoice_setting->tax}} :</th>
									<th></th><th>
								{{-- {{$tax=\Cart::session(Auth::id())->getCondition('VAT') ? \Cart::session(Auth::id())->getCondition('VAT')->getCalculatedValue($kisu-$grossDiscount) : 0}} --}}
								{{$tax=$sales->total_tax}}</th>
								</tr>
								@else
								<?php $tax=$sales->total_tax;?>
								@endif
								@if($sales->regular_discount_total>0)
								<tr>
									<th colspan="3"></th>
									<th colspan="2">{{$invoice_setting->regular_discount}} :</th>
									<th></th>
									<th>({{$regular=$sales->regular_discount_total}})</th>
								</tr>
								@else
								@php $regular=$sales->regular_discount_total; @endphp
								@endif
								<tr >
									<th colspan="3"></th>
									<th colspan="2">{{$invoice_setting->net_sales_amount}}</th>
									<th></th>
									<th>{{$amountToBeInWords=($kisu-$grossDiscount-$regular+$tax)}}</th>
								</tr>
								@if ($sales->transaction !== null)
									@foreach($sales->transaction->journals as $jr)
										@if($jr->accounts_id==2)
										<tr >
											<th colspan="3">{{$invoice_setting->paid_amount}} :</th>
												<th colspan="2">{{$invoice_setting->paid_by}} {{$jr->account->account_head}} by {{$jr->paymentmethod->method_title}}</th>
												<th></th>
												<th>{{$jr->amount}}</th>
											@elseif($jr->accounts_id==10)
											<tr >
											<th colspan="4">{{$invoice_setting->paid_amount}} :</th>
											<th colspan="2">{{$invoice_setting->paid_by}} {{$jr->account->account_head}} by {{$jr->paymentmethod->method_title}}</th>
												<th></th>
												<th>{{$jr->amount}}</th>
										</tr>
										@endif
									@endforeach
									@foreach($sales->transaction->journals as $jr)
										@if($jr->accounts_id==5)
										<tr class="table-danger">
											<th colspan="3">{{$invoice_setting->due_amount}} :</th>
											<th colspan="2"></th>
											<th></th>
											<th>{{$jr->amount}}</th></tr>
										@endif
									@endforeach
								@endif
									<tr>
										<th colspan="3">Amount In Words:</th>
									<td colspan="4" class="text-dark">{{$f->format($amountToBeInWords).' taka only'}}</td>
										
									</tr>
							</tfoot>
						</table>	

					</div>
					<div class="row" style="margin-top:50px">
						@php
							$i=1;
						@endphp
							@forelse ($footer_contents as $fc)
					<div class="form-group col-md-{{(12/$invoice_setting->footer_content_no)}}">
								<hr style="border-top:solid 1px black">
								<h4 class="text-center">
									@php
									$hello=explode('|',$fc->footer_content);
									@endphp
									@foreach ($hello as $item)
										{{$item}}<br>
									@endforeach
									</h4>
								@if($invoice_setting->footer_content_no<=$i++)
										@php
										break;	
										@endphp
										@endif
							</div>
							@empty
							@endforelse
					</div>
							<div class="col-md-12" style="margin-top:50px 0">
						
									<hr style="border-top:solid 1px black">
							<h5 class="text-center">
									@php
									$helloAdd=explode('|',$invoice_setting->footer_address);
									@endphp
									@foreach ($helloAdd as $itemAdd)
										{{$itemAdd}}<br>
									@endforeach
							</h5>
							<hr style="border-bottom:solid 1px black">
							</div>
					</div>
										
					<button type="button" id="btn_print" class="btn btn-block btn-primary" onclick="printData()"><i class="mdi mdi-printer" ></i>Print Report</button>
				</div>
			</div>
		</div>
	</div>
	<script>
		function printData(){
			$('#btn_print').hide();
			var contentPrint=document.getElementById('print_portion').innerHTML;
			var contentOrg=document.body.innerHTML;
			document.body.innerHTML=contentPrint;
			window.print();
			document.body.innerHTML=contentOrg;
			$('#btn_print').show();
		}
	
	</script>
	@endsection