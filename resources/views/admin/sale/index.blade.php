@extends('layouts.admin')

@section('title','Sale')

@push('css')
<link rel="stylesheet"
href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css">
<link rel="stylesheet"
href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
<style type="text/css">
    .main-panel {
      width: 100%;
    }

    /* The flip card container - set the width and height to whatever you want. We have added the border property to demonstrate that the flip itself goes out of the box on hover (remove perspective if you don't want the 3D effect */
  .flip-card {
    background-color: transparent;
    width: 300px;
    height: 200px;
    border: 1px solid #f1f1f1;
    perspective: 1000px; /* Remove this if you don't want the 3D effect */
  }

  /* This container is needed to position the front and back side */
  .flip-card-inner {
    position: relative;
    width: 100%;
    height: 100%;
    text-align: center;
    transition: transform 0.8s;
    transform-style: preserve-3d;
  }

  /* Do an horizontal flip when you move the mouse over the flip box container */
  .flip-card:hover .flip-card-inner {
    transform: rotateY(180deg);
  }

  /* Position the front and back side */
  .flip-card-front, .flip-card-back {
    position: absolute;
    width: 100%;
    height: 100%;
    backface-visibility: hidden;
  }

  /* Style the front side (fallback if image is missing) */
  .flip-card-front {
    background-color: #bbb;
    color: black;
  }

  /* Style the back side */
  .flip-card-back {
    background-color: #205e9be6;
    color: white;
    transform: rotateY(180deg);
  } 
  .modal-body{
    height: 500px;
    overflow-y: auto;
  }
</style>
@endpush
@section('content')
<div class="content-wrapper">
  <div class="row">
    <div class="col-sm-7">
      {{--  <form action="{{route('sales.index')}}" method="get" >
        <div class="col-sm-12">
      <!-- Search Product Form -->
          <div class="row">
            <div class="form-group">
              <select name="search_by" id="search_by" class="form-control" onchange="showSearchValue(this.value)">
                <option value="product">Search By Product</option>
                <option value="category">Search with Category</option>
                <option value="brand">Search with Brand </option>
              </select>
            </div>
            <div class="form-group" id="search_value_div"> 
              <div class="input-group mb-3">
                <input type="text" name="search_value" class="form-control" id="search_value" placeholder="Search Product">
              <div class="input-group-append">
                  <input type="submit" value="Search" class="btn btn-info" id=""/>
              </div>
            </div>
          </div>
          </div>
        </div> 
      </form>
       --}}
      <!-- Display Single Products List --> 
      <div class="row">
      <div class="input-group mb-3">
        <div class="input-group-append col" style="padding:0">
          <button class="btn btn-outline-danger col-2" type="button" id="brand-button-addon" data-toggle="modal" data-target="#productsSearchFilterByBrand" >Brand</button>
          <button class="btn btn-outline-primary col-2" type="button" id="category-button-addon" data-toggle="modal" data-target="#productsSearchFilterByCategory">Category</button>
          <a  href="{{route('sales.index')}}" class="btn btn-outline-primary col-2" type="button" >All</a>

          <input type="text" list="productDataList" autocomplete="off" name="search_value" id="productDataListInput" class="form-control border-success col-6" placeholder="Search Product">
         
          <datalist id="productDataList">
            @php
                $counterdatalist = 0;
            @endphp
            @forelse ($products as $p)
              @if ($counterdatalist !== $p->product_id)
                <option data-productId = "{{$p->product_id}}" data-purchaseId = "{{$p->purchase_id}}"  value="{{$p->product->product_sku . " " .$p->product->product_barcode}}">
              @endif
              @php
                  $counterdatalist = $p->product_id;
              @endphp
            @empty
                <option value="No Product Available">
            @endforelse
            
          </datalist> 
        </div>
      </div>
      <div class="row">
        <div class="col-xl-4 col-lg-4 col-md-4">
          <h2  style="display:inline" class="h2">Single Products</h2><p></p></div>
          <div class="col-xl-8 col-lg-8 col-md-8"><p></p>
          <marquee>{{$discount_offers_for_all ? ($discount_offers_for_all->discount_percent*100)."% Discount On All Products" : "No Discount Offer Availible" }}</marquee>
        </div>
      </div>
<!-- Modal For brand Search -->
@php
  $btn_classes = ['primary','info','danger','warning','success'];
@endphp

<div class="modal fade" id="productsSearchFilterByBrand" tabindex="-1" role="dialog" aria-labelledby="productsSearchFilterByBrand" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">All Brands</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

<form action="{{route('sales.index')}}" method="GET">
      <div class="modal-body">
        <div class="container-fluid" id="brand_row">
          <div class="row">
            @forelse ($brands as $brand)
              <div class="col-md-4">
                  <div class="btn-group-toggle" data-toggle="buttons">
                  <label class="btn btn-{{ $btn_classes[rand(0,4)]}}" style="width: 100%;{{str_word_count($brand->brand_name)>1?'white-space:normal':''}}">
                        <input type="checkbox" name="brands[]" value="{{$brand->id}}">
                          {{$brand->brand_name}}                        
                      </label>
                  </div><br>
              </div>
            @empty
                No Brand registered!
            @endforelse
          </div>

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Search</button>
      </div>
</form>
    </div>
  </div>
</div>
<!-- Modal For Categories Search -->
<div class="modal fade" id="productsSearchFilterByCategory" tabindex="-1" role="dialog" aria-labelledby="productsSearchFilterByCategory" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">All Categories</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
<form action="{{route('sales.index')}}" method="GET">
      <div class="modal-body">
        <div class="container-fluid" id="brand_row">
          <div class="row">
            @forelse ($categories as $category)
              <div class="col-md-4">
                  <div class="btn-group-toggle" data-toggle="buttons">
                      <label class="btn btn-{{ $btn_classes[rand(0,4)]}}" style="width: 100%; {{str_word_count($category->category_name)>1?'white-space:normal':''}}">
                        <input  type="checkbox" name="categories[]" value="{{$category->id}}">
                          {{$category->category_name}}                        
                      </label>
                  </div><br>
              </div>
            @empty
                No Brand registered!
            @endforelse
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Search</button>
      </div>
</form>
    </div>
  </div>
</div>
       
        <?php 
        $counter = 0 ;
        ?>
        @forelse($products as $product)

        @if($counter !== $product->product_id)

        <?php $quentity = 0; ?>
        <?php foreach ($products as $product_quentity): ?>
          <?php if ($product_quentity->product_id == $product->product_id): ?>
            <?php $quentity = $quentity +  $product_quentity->stock_quantity?>
          <?php endif ?>
        <?php endforeach ?>

        <div class="flip-card col-xl-3 col-lg-3 col-md-4 col-sm-4 grid-margin stretch-card">
              <div class="flip-card-inner">
                <div class="flip-card-front">
                  <img src="{{asset('').$product->product->image}}" alt="Product Image" style="width:100%;height:100%;">
                </div>
                <div class="flip-card-back" style="padding-top:15px">
                  <h5 id="{{$product->product->product_sku}}">{{str_before($product->product->product_sku,'|')}}</h5>
                 @if($product->product->discount!=null && $product->product->discount->first())
                 Discount: {{$product->product->discount->first()->discount_percent?($product->product->discount->first()->discount_percent*100).'%':$product->product->discount->first()->discount_amount}}
                 @endif
                  <h6>
                    {{--  <span>Category: {{$product->product->category->category_name}}</span> <br>  --}}
                    <span id="{{$product->purchase->sell_unit_price}}">BDT: {{$product->purchase->sell_unit_price}}</span> |
                    <span  id="{{$product->stock_quantity}}">QTY: {{$quentity}}</span>
                  </h6>
                  <input type="hidden" id="product_qty{{$product->product_id}}" value="{{$quentity}}">
                    @if($product->purchase->warranty_types)
                      <input type="hidden" id="warrenty-type-{{$product->product_id}}" value="1">
                    @endif
                  <h5  class="text-center">
                  <a href="javascript:void(0)" class="text-white h5" style="text-decoration:none" onclick="addToCart({{$product->product_id}},{{$product->purchase_id}})">Add</a> | <a href="#"class="text-white h5" style="text-decoration:none" onclick="view_product_details({{$product->product_id}})" data-toggle="modal" data-target="#myLargeModal">Details</a></h5>
                </div>
              </div>
            </div> 
       
        @endif

        <?php $counter=$product->product_id ?>
        @empty
        <div class="alert alert-danger">
          <h3>No record found!</h3>
        </div>
        @endforelse
      </div>
      <!-- Display Bundle Products List -->
      <h2 class="Bundle Products">Bundle Products</h2>
      <div class="row">
        @forelse($bundle_product as $product)
        <div class="flip-card col-xl-3 col-lg-3 col-md-4 col-sm-4 grid-margin stretch-card">
          <div class="flip-card-inner">
            <div class="flip-card-front">
              <img src="{{asset($product->bundle_image)}}" alt="No image found" class="mdi mdi-cube text-danger icon-lg" style="width:100%;height:100%;">
            </div>
            <div class="flip-card-back" style="padding-top:15px">
              <h5 id="">{{$product->bundle_name}}</h5>
              <h5><span id="{{$product->bundle_discount_amount}}">{{$product->bundle_discount_amount}}</span></h5>
              <h5><span id="{{$product->bundle_price}}">BDT:{{$product->bundle_price}}</span></h5>
              {{-- <input type="hidden" id="product_qty2" value="1"> --}}
              <input type="hidden" id="warrenty-type-2" value="1">
              <h5 class="text-center">
              <a href="javascript:void(0)" class="text-white h5" style="text-decoration:none" onclick="addBundleToCart({{$product->id}})">Add |</a> <a href="#" class="text-white h5" style="text-decoration:none" onclick="view_bundle_product_details({{$product->id}})" data-toggle="modal" data-target="#myLargeModal">Details</a></h5>
            </div>
          </div>
        </div>
      
        @empty
        <div class="alert alert-danger">
          <h3>No record found!</h3>
        </div>
        @endforelse
      </div>
    </div>

    <div class="col-sm-5">
      <!-- Customer Search Form -->
        <div class="col-sm-12">
              <div class="form-group row">
                  <div class="input-group mb-3">
                    <input list="cuntomer_info_list" value="{{session('customer_name')}}{{session('customer_id')==1?session('customer_phone'):'('.session('customer_phone').')'}}" id="customer_info_input" onclick="showCustomerList()" name='customer' class="form-control" placeholder="Customer Search">
                    <datalist id="cuntomer_info_list">
                      
                    </datalist>
                    <div class="input-group-append">
                      <span class="input-group-btn">
                      <a href="{{route('customers.show',session('customer_id'))}}" id="customerDueButton" class="btn btn-danger" type="button" title="Cr. Limit:{{session('customer_credit_limit')?session('customer_credit_limit'):00}}">Due Tk. {{session('customer_due')?number_format(session('customer_due'),2):00}}</a>
                      </span>
                        <button type="button" id="addCustomerBtn" class="btn btn-info" data-toggle="modal" data-target=""><i class="mdi mdi-account-plus"></i></button> 
                    </div>
                    <input type="text" name="coupon_no" placeholder="Coupon" class="form-control" id="coupon_no" value="{{session('coupon_no')?session('coupon_no'):''}}"/>
                    <div class="input-group-append">
                        <button type="button" name="" onclick="applyCoupon()" title="Apply Coupon" class="btn btn-info" id="">
                        <i class="mdi mdi-playlist-plus"></i></button>
                    </div>
                    <input type='hidden' name='cust_group_id' id='cust_group_id' value="{{ session('cust_group_id') }}">
                    <input type='hidden' name='customer_id' value="{{ session('customer_id') }}">
                    <input type='hidden' id='salesPersonFeature' name='sale_person_feature' value="{{ session('sale_person_feature') }}">
              </div> 
              
           
            </div> 
          {{-- Add new customer  --}}
            <div class="row" id="addCustomerRow" style="margin-top: -20px; display: none; z-index:200;">        
                <form class="forms-sample" method="post" action="{{route('customers.store')}}">
                    @csrf     
                <div class="form-group">
                    <div class="input-group mb-3">
                      <input type="hidden" name="addCustomerFromSale" value="requestFromSales">

                      <input type="text"  name="customer_name" class="form-control" placeholder="Name">

                      <input type="email" name="email" class="form-control"  placeholder="Email">

                      <input type="text"   name="phone" class="form-control"  placeholder="Phone">
                      
                      <select name="customer_group_id" class="form-control" >
                        @forelse ($cusgroup as $cg)
                          <option value="{{$cg->id}}">{{$cg->customer_group_title}}</option>
                        @empty
                            <option value="">No Group Registered</option>
                        @endforelse
                      </select>
                      <div class="input-group-append">
                        <span class="input-group-btn">
                          <button type="submit" class="btn btn-info" >Save</button> 
                        </span>
                      </div>
                    </div>
                    
                </div> 
                </form>
            </div>
      </div>
      <div style="position: fixed;top: 178px; right: 15px; width: 550px; z-index:100">
      <p style="min-height:1px"></p>
    
      {{-- here to be paste --}}
     <div id="cartTableData" style="display: block;height: 55vh ;overflow-y: auto;overflow-x: hidden;">
      <table class="table table-bordered"  style="width:100%;">
        <thead style="display: block;">
          <tr>
            <th>#</th>
            <th>SKU</th>
            <th>Qty</th>
            <th>Price</th>
            <th>Cost</th>
            <th>Dis.</th>
            <th>Sub</th>
            <th title="last sold price">LP</th>
          </tr>
        </thead>

        @if(!Cart::session(Auth::id())->isEmpty())
        <tbody style="display: block; height: 400px;overflow-y: auto; overflow-x: hidden;">
            @php 
            $counter = 1;
            $intakePrice=0;
            $discount=0;
            $subTotal=0;
            @endphp
             @foreach(Cart::session(Auth::id())->getContent() as $key =>$cart_items)
               <tr>                                                                                               <td style="cursor:pointer" onclick="removeFromCart({{$cart_items->id}})">X</td> 
                {{-- <td>{{$counter++}}</td> --}}
                <td> {{ str_limit(str_before($cart_items->name,"|"),15)}}</td>
                <td><input type='number' style="max-width: 45px" value="{{$cart_items->quantity}}" id="pqty{{$cart_items->id}}" onblur="update_cart_quentity({{$cart_items->id}},this.value,{{$cart_items->quantity}})" size="1" maxlength="3" minlength="1"/></td>
                {{-- <td>{{ $cart_items->attributes->purchase_id}}</td>
                <td>{{ $cart_items->attributes->type}}</td> --}}
                <td>{{ $cart_items->price }}</td>
                <?php $intakePrice=$intakePrice+Cart::get($cart_items->id)->getPriceSum();?>
                <td>{{Cart::get($cart_items->id)->getPriceSum()}}</td>
                <td>{{Cart::get($cart_items->id)->getPriceSum()-$cart_items->getPriceSumWithConditions()}}</td>
                <?php $discount+= Cart::get($cart_items->id)->getPriceSum()-$cart_items->getPriceSumWithConditions();
                $subTotal+= $cart_items->getPriceSumWithConditions(); ?>
                <td>{{ $cart_items->getPriceSumWithConditions()}}</td>
                <td>{{ $cart_items->attributes->previous_sold_price }}</td>
                
              </tr>
             @endforeach
        </tbody>
        {{-- <tfoot>
          <tr>
              <th colspan='3' rowspan='4'></th>
            <th colspan="2">GrandTotal : </th>
            <th>{{$intakePrice}}</th>
          @php //Cart::session(Auth::id())->getSubTotal() @endphp
            <th>{{$discount}}</th>
            <th>{{$subTotal}}</th>
          </tr>
          <tr>
            <th colspan="2">Discount :( {{Cart::session(Auth::id())->getCondition('coupon@100')?Cart::session(Auth::id())->getCondition('coupon@100')->getType($subTotal):'0'}})</th>
            <th colspan="2">&nbsp;</th>
            @if(!$subTotal==0)
            <th>{{ Cart::session(Auth::id())->getCondition('coupon@100') ? $coupon=Cart::session(Auth::id())->getCondition('coupon@100')->getCalculatedValue($subTotal) : $coupon=0}}</th>
            @endif
          </tr>
           <tr>
            <th colspan="2">TAX @ {{Cart::session(Auth::id())->getCondition('VAT') ? Cart::session(Auth::id())->getCondition('VAT')->getValue():'0'}} :</th>
            <th colspan="2">&nbsp;</th>
            @if(!$subTotal==0)
            <th>{{ Cart::session(Auth::id())->getCondition('VAT') ? $tax= Cart::session(Auth::id())->getCondition('VAT')->getCalculatedValue($subTotal-$coupon):$tax='0'}}</th>
            @endif
          </tr>
          <tr>
            <th colspan="2">Net Total :</th>
            <th colspan="2">&nbsp;</th>
            {{-- <th>{{Cart::session(Auth::id())->getTotal()}}</th> --}}
           {{-- <th>{{$subTotal-$coupon+$tax}}</th>
          </tr>
        </tfoot>--}}
        @endif 
      </table>
    

     </div>
      {{-- end to be paste --}}
      
<!-- payment Modal -->
<!-- Button trigger modal -->
<button type="submit" class="btn btn-info btn-lg btn-block" onclick="payNow()">Pay Now</button>
@if(session('holded_customer_id'))
<a href="{{route('hold_back_customer_cart')}}"  class="btn btn-primary btn-lg btn-block">Hold Back Customer</a>
@else
<a href="{{route('hold_customer_cart')}}"  class="btn btn-danger btn-lg btn-block">Hold Customer</a>
@endif
{{-- data-toggle="modal" data-target="#paymentModal" --}}
</div>
<!-- Modal -->
<div class="modal fade" id="paymentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Payment Details</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form action="{{route('sales.store')}}" method="post" id="sales_form">
              @csrf
       <div class="row" id="cartPaymentTable">
          {{-- <table class="table table-bordered"  style="width:100%">
            <thead>
              <tr>
                <th>#</th>
                <th>SKU</th>
                {{-- <th>purchase ID</th>
                <th>Type</th>
                <th>Qty</th>
                <th>Price</th>
                <th>Total</th>
                <th>Discount</th>
                <th>SubTotal</th>
              </tr>
            </thead>
    
            @if(!Cart::session(Auth::id())->isEmpty())
            <tbody id="cart_table">
                
              @php 
                $counter = 1;
                $intakePrice=0;
                $discount=0;
                $subTotal=0;
              @endphp
              @foreach(Cart::session(Auth::id())->getContent() as $key =>$cart_items)
                <tr>
                   <td>{{$counter++}}</td>
                   <td> {{ str_before($cart_items->name,"|")}}</td>
                  
                   {{-- <td>{{ $cart_items->attributes->purchase_id}}</td> --}} {{-- 
                   <td>{{ $cart_items->attributes->type}}</td>
                   <td>{{ $cart_items->quantity}}</td>
                   <td>{{ $cart_items->price }}</td>
                   @php $intakePrice=$intakePrice+Cart::get($cart_items->id)->getPriceSum();@endphp
                   <td>{{Cart::get($cart_items->id)->getPriceSum()}}</td>
                   <td>{{Cart::get($cart_items->id)->getPriceSum()-$cart_items->getPriceSumWithConditions()}}</td>
                   @php $discount+= Cart::get($cart_items->id)->getPriceSum()-$cart_items->getPriceSumWithConditions();
                   $subTotal+= $cart_items->getPriceSumWithConditions(); @endphp
                   <td>{{ $cart_items->getPriceSumWithConditions()}}</td>
                  
                 </tr>
                @endforeach
            </tbody>
            <tfoot>
              <tr>
                <th colspan="3" rowspan="4">
                    <div class="form-group">
                        {{-- <label for="payment_method_id">Payment Method</label> --}} {{-- 
                        <select required class="form-control" id="payment_method_id" name="payment_method_id"  onchange="addpaidvalue()">
                        <option value="">Select Payment Method</option>
                         @foreach($payment_methods as $pm)
                         <option value="{{$pm->id." | ".$pm->method_type }}">{{$pm->method_title}}</option>
                         @endforeach
                       </select>
                     </div>
                     <!--form group for paid amount-->
                      <div class="form-group" id="payment_tk">
                         
                      </div>
                      <div class="form-group" id="date_field">
                         
                      </div>
                </th>
                <th colspan="2">GrandTotal : </th>
                <th>{{$intakePrice}}</th>
                @php //Cart::session(Auth::id())->getSubTotal() @endphp
                  <th>{{$discount}}</th>
                  <th>{{$subTotal}}</th>
              </tr>
              <tr>
                <th colspan="2">Discount :( {{Cart::session(Auth::id())->getCondition('coupon@100')?Cart::session(Auth::id())->getCondition('coupon@100')->getType($subTotal):'0'}})</th>
                <th colspan="2">&nbsp;</th>
                @if(!$subTotal==0)
                <th>{{ Cart::session(Auth::id())->getCondition('coupon@100') ? $coupon=Cart::session(Auth::id())->getCondition('coupon@100')->getCalculatedValue($subTotal) : $coupon=0}}</th>
                @endif
              </tr>
               <tr>
                <th colspan="2">TAX @ {{Cart::session(Auth::id())->getCondition('VAT') ? Cart::session(Auth::id())->getCondition('VAT')->getValue():'0'}} :</th>
                <th colspan="2">&nbsp;</th>
                @if(!$subTotal==0)
                <th>{{ Cart::session(Auth::id())->getCondition('VAT') ? $tax= Cart::session(Auth::id())->getCondition('VAT')->getCalculatedValue($subTotal-$coupon):'0'}}</th>
                @endif
              </tr>
              <tr>
                <th colspan="2">Net Total :</th>
                <th colspan="2">&nbsp;</th>
                {{-- <th>{{Cart::session(Auth::id())->getTotal()}}</th> --}} {{-- 
                <th>{{$subTotal-$coupon+$tax}}</th>
              </tr>
            </tfoot>
            @endif
          </table> --}}
          
       </div>
    
      </div>
      <div class="modal-footer">
        {{-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> --}}
        <button type="button" onclick="saleDataSubmit()" class="btn btn-primary">Complete Transaction</button>
      </form>
      </div>
    </div>
  </div>
</div>
<!-- payment modal end -->
    </div>
  </div>
  <!-- Button to trigger modal -->
{{-- modal for warentable product --}}

<div class="modal fade" id="warrantyModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
        <form id="warranty_form" action="{{route('sales.store')}}" method="post">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Select warrantable product Sl.</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
            @csrf
            <input type="hidden" name="warranty_form" value="1">
          <div class="row" id="warrantable_product">
          </div>
        
      </div>
      <div class="modal-footer">
          {{-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> --}}
          <a class="btn btn-primary" onclick="formSubmit()">Set Warenty Product No. </a>
        
      </div>
    </form>
    </div>
  </div>
</div>


{{-- modal for warentable product --}}

{{-- modal for sales person --}}

<div class="modal fade" id="salePersonModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
        <form id="sales_person_form" action="{{route('sales.store')}}" method="post">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Select Sales Person</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
            @csrf
            <input type="hidden" name="sales_person_form" value="1">
          <div class="row" id="sales_person_data">
              @if(session('sale_person_feature')==1)
                <div class="form-group col-md-6">
                    <label for="sales_person">Sales Persons</label>
                    <select multiple required class="form-control" id="sales_person" name="sales_persons[]">
                        <option value="">Select Sales Persons</option>
                        @forelse($sales_persons as $sps)
                        <option value="{{$sps->id}}">{{$sps->name}}</option>
                        @empty
                        <option>{{'There is no Sales Person Registered'}}</option>
                        @endforelse
                    </select>
                </div>
              @endif
          </div>
        
      </div>
      <div class="modal-footer">
          {{-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> --}}
          <a class="btn btn-primary" onclick="formSubmitSalesPerson()">Set Sales Persons </a>
        
      </div>
    </form>
    </div>
  </div>
</div>


{{-- modal for sales person --}}


  <!-- Modal -->
  <div class="modal fade"  id="myLargeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title productTitle" id="myModalLabel">Product Name</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
          <div class="row" id="roduct_details_row" style="display: none">
            <div class="col-sm-4">
              <div class="thumbnail">
                <img id="productImageSrc" src="" width="100%">
              </div>
            </div>
            <div class="col-sm-8">
              <div class="caption">
                <h4>Available Quantity</h4>
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th>Branch Name</th>
                      <th>Available Quantity</th>
                    </tr>
                  </thead>
                  <tbody id="branchName">
                  </tbody>
                  <tfoot>
                   <tr>
                    <th>Total Available Quantity </th>
                    <td id="totalqty"></td>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
        <!-- Display Bundle Product List -->
        <div class="row" id="view_bundle_row" style="display: none">
          <div class="col-sm-4">
            <div class="thumbnail">
              <img id="bundleproductImageSrc" src="" width="100%">
            </div>
          </div>
          <div class="col-sm-8">
              <div class="caption">
                <h4 id="packageDetailsHeading"></h4>
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th colspan="2">Product Name</th>
                      <th>Qty X Unit Price</th>
                      <th>Single Price</th>
                    </tr>
                  </thead>
                  <tbody id="bundle_product_table">
                  </tbody>
                  <tfoot>
                   <tr>
                    <th colspan="3" class="text-right
                    ">Discount Amount</th>
                    <td id="bundleDiscount"></td>
                  </tr>
                  <tr>
                    <th colspan="3" class="text-right">Package Price</th>
                    <td id="bundlePrice"></td>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
        <!-- Customer Add Form -->
     {{--  <div class="card row" id="customer_add_row" style="display: none;">  --}}
          {{--  <div class="card-body">
            <br>
            <form class="forms-sample" method="post" action="{{route('customers.store')}}">
              @csrf
              <input type="hidden" name="addCustomerFromSale" id="addCustomerFromSale" value="requestFromSales">
           
              <div class="form-group">
                <label for="customer_name">Customer Name</label>
                <input type="text" class="form-control" name="customer_name" id="customer_name" placeholder="Enter Name">
              </div> 
              <br> 
              <div class="form-group">
                <label for="email">Email</label>
                <input type="text" class="form-control" name="email" id="email" placeholder="Enter Email Address">
              </div>
              <br>
              <div class="form-group">
                <label for="phone">Phone</label>
                <input type="text" class="form-control" name="phone" id="phone" placeholder="Enter Phone Number">
              </div> 
              <br>
              <div class="form-group" id="customer_group_div">
                <label for="customer_group_id">If Any Group Available! <span style="color:red"> Select 'default' if customer belongs to no group.</span></label>
                <select name="customer_group_id" id="customer_group_id" class="form-control">
                  <option value="">select customer group</option>
                  @forelse($cusgroup as $cg)
                  <option value="{{$cg->id}}">{{$cg->customer_group_title}}</option>
                  @empty
                  <option>{{'No group available'}}</option>
                  @endforelse
                </select>
              </div> 
              <br>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" id="addCustomerSubmitBtn" style="display:none" class="btn btn-primary">Save Customer</button>
              </div>
            </form>
          </div>  --}}
        {{--  </div>    --}}
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>
</div>
<!-- content-wrapper ends -->
</div>

@endsection
@push('js')
<script type="text/javascript">
  // Display Customer List
  function payNow() {
    var sales_person_feature=document.getElementById('salesPersonFeature').value;
    var route = "{{url('')}}/payment_model/"+1;
    //applyCoupon();
    $.get(route, function(data) {
      $('#warrantable_product').empty();
      if(data.split("|,|")[1]){
    $('#warrantable_product').html(data.split("|,|")[1]);
    $('#warrantyModal').modal('show');
      }else if(sales_person_feature==1){
        $('#salePersonModel').modal('show');  
    }else{
    $('#cartPaymentTable').empty();
    $('#cartPaymentTable').html(data.split("|,|")[0]);
    $('#paymentModal').modal('show');
      }
    });
  }
  function formSubmit() {
    var sales_person_feature=document.getElementById('salesPersonFeature').value;
    var route = "{{url('')}}/sales";
    $.ajax({
        url: route,
        type: 'post',
        data: $('#warranty_form').serialize(), // Remember that you need to have your csrf token included
        dataType: 'json',
        success: function(data){
          $('#warrantyModal').modal('hide');
          if(sales_person_feature==1){
            $('#salePersonModel').modal('show');  
         }else{
            payNowAfterWarranty();
        }
        },
        error: function(data){
           alert('Some mistake');
        }
    });
// });
    }
  function formSubmitSalesPerson() {
    var route = "{{url('')}}/sales";
    $.ajax({
        url: route,
        type: 'post',
        data: $('#sales_person_form').serialize(), // Remember that you need to have your csrf token included
        dataType: 'json',
        success: function(data){
          $('#salePersonModel').modal('hide');
          payNowAfterWarranty();
        },
        error: function(data){
           alert('Some mistake');
        }
    });
// });
    }
  function payNowAfterWarranty() {
    var route = "{{url('')}}/payment_model/"+1;
   
    $.get(route, function(data) {
      $('#cartPaymentTable').empty();
    $('#cartPaymentTable').html(data.split("|,|")[0]);
    });
    $('#paymentModal').modal('show');
  }

  function saleDataSubmit() {
    if($('#paid_amount').length){
      var salePrice=parseInt($('#netSalePrice').text());
      var paidValue=parseInt($('#paid_amount').val());
      if(paidValue>salePrice){
       var confirmation=confirm("Are You Sure PayMore That Net Sales Total!")
       if(confirmation){
         document.getElementById('sales_form').submit()
       }
      }else{
        document.getElementById('sales_form').submit()
      }
    }else{
      document.getElementById('sales_form').submit()
        }
  }
  function restricSelectOption(qtyNum,id) {
    if ($("#warranty"+id+" :selected").length > qtyNum) {
     document.getElementById("warranty"+id).selectedIndex=-1;
        alert('You can select upto '+qtyNum+' options only');
    }
//});
  }
  
  function showCustomerList() { 
    var route = "{{url('')}}/get_customer";
      $.get(route, function(data) {
          var returnData ="<option value='Walking_customer(NA)'";
          for(i=1; i<data['customers'].length;i++){
            returnData += "<option value='"+data['customers'][i].customer_name +"("+data['customers'][i].address.phone+")"+"'>";
          }
          if(data['customer_due']!== null){
            $('#customerDueButton').text("Due Tk. "+data['customer_due']+".00");
          }
          $('#cuntomer_info_list').html(returnData);
      });
  }
  function setCustomer(selectedCustomer) {
     var  customer_search_input ='';
    if(selectedCustomer.indexOf("(") !== -1){
      customer_search_input =  selectedCustomer.split("(")[1].split(")")[0];
    }
    alert(customer_search_input);
    var route = "{{url('')}}/set_customer_info/"+customer_search_input;
     $.get(route, function(data) {
      location.reload();
        });
      }

  // Set Customer when Selected from dropdown 
  $("#customer_info_input").on('click', function () {
    $(this).select();
  });
  $("#customer_info_input").on('input', function () {
      var selectedCustomer = $('#customer_info_input').val();
      var  customer_search_input = '';
      if(selectedCustomer.indexOf("(") !== -1){
        customer_search_input =   $('#customer_info_input').val().split("(")[1].split(")")[0];
      }
        var route = "{{url('')}}/set_customer_info/"+customer_search_input;
        $.get(route, function(data) {
          location.reload();
        });
  });
  // showCustomerList('');

  // Add Customer Details on model
  
  // Load Customer In Modal
  {{--  function addCustomerInfoToModal(){
    var phone = $('#customer_search_input').val();
    var route = "{{url('')}}/get_customer?customer_search_input="+phone;
    $.get(route, function(data) {
      if (data['customers'].length == 0) {
        $('#myModalLabel').text('Register New Customer');
        $("#phone").val(phone);
        $("#roduct_details_row").hide();
        $("#customer_add_row").show();
        $("#addCustomerSubmitBtn").show();
        $("#customer_name").val('');
        $("#email").val('');
        $("#customer_group_id").val('');
      }else{
        $('#myModalLabel').text('Customer Details');
        $("#customer_name").val(data['customers'][0].customer_name); 
        $("#customet_id").val(data['customers'][0].id); 
        $("#email").val(data['customers'][0].address.email);
        $("#phone").val(data['customers'][0].address.phone);
        $("#customer_group_id").val(data['customers'][0].customer_group_id);
        $("#customer_add_row").show();
        $('#roduct_details_row').hide();
        $('#view_bundle_row').hide();
        //Remove Customer Group Dropdown 
        $('#customer_group_div').empty();
        //Add Customer Group Title of Selected Customer
        var groups = data['groups'];
        var customer_group = data['customers'][0].customer_group_id;
        for(i=0 ; i<groups.length;i++){
         if(customer_group == groups[i].id ){
            $("#customer_group_div").append( "<p>"+groups[i].customer_group_title+"</p>");
            //$("#cust_group_id").val();
         }
        }
      }
    });
  }  --}}
  //Add New Customer form function end
  /* Display Product Details*/
  function view_product_details(product_id) {
   $('#roduct_details_row').show(); 
   $('#customer_add_row').hide();
   var route = "{{url('')}}/view_product_details/"+product_id;
   $.ajax({
    url: route,
    type: 'GET',
  })
   .done(function(done) {
     var returnData = '';
     var totalqty = 0;
     var productName = done[0].product.product_sku;
     var productImageSrc = "{{url('')}}/"+done[0].product.image;
     $('#productImageSrc').attr('src', productImageSrc);
     $('#myModalLabel').text(productName);
     $('#branchName').empty();
     for(i=0;i<done.length;i++){
      totalqty += parseInt(done[i].stock_quantity);
      returnData += '<tr><td>' + done[i].branch.name+'</td><td>' +  done[i].stock_quantity + '</td></tr>';
    }
    $('#branchName').prepend(returnData);
    $('#totalqty').html(totalqty);
    $('#view_bundle_row').hide();    
  });
 }

 // Display Bundle Product Details

 function view_bundle_product_details(bundleId){
  $('#myModalLabel').text('Bundle Products Details');
  var route = "{{url('')}}/view_bundle_product_details/"+bundleId;
  $.get(route, function(data) {
    $('#bundleproductImageSrc').attr("src","{{url('')}}/"+data[0].bundle_image);
    $('#packageDetailsHeading').text(data[0].bundle_name+" Quentities in Details");
    var bundleProductRow = "";
    for(i=0;i<data[0].bundle_product_details.length;i++){
      var sku = data[0].bundle_product_details[i].purchase.product.product_sku;
      sku = sku.split("|");
      bundleProductRow += "<tr><td  colspan='2'>"+sku[0]+"</td>";
      bundleProductRow += "<td>"+data[0].bundle_product_details[i].quantity+" X "+data[0].bundle_product_details[i].purchase.sell_unit_price+"</td>";
      bundleProductRow += "<td>"+data[0].bundle_product_details[i].quantity*data[0].bundle_product_details[i].purchase.sell_unit_price+"</td>";
      bundleProductRow += '</tr>';
    }
  $('#bundle_product_table').empty();
  $('#bundle_product_table').append(bundleProductRow);
  $('#bundleDiscount').text(data[0].bundle_discount_amount);
  $('#bundlePrice').text(data[0].bundle_price);
  });
  $('#view_bundle_row').show();
  $('#roduct_details_row').hide();  
  $('#customer_add_row').hide();  

 }

 // Add product to cart 
 function addToCart(product_id,purchase_id) {

   var cust_group_id=document.getElementById('cust_group_id').value;
  //  var warrentyType=document.getElementById('warrenty-type-'+product_id);
   //if(warrentyType!==null){ var warrenty=warrentyType.value;}
   //alert(warrenty);
   if($('#pqty'+product_id).length>0){
   var cart_qty=document.getElementById('pqty'+product_id).value;
  }else{
    var cart_qty=null;
  }
  if($('#product_qty'+product_id).length>0){
   var product_qty=parseInt(document.getElementById('product_qty'+product_id).value);
  }else{
    var product_qty = 0;
  }
   if(cart_qty<product_qty || cart_qty==null){
   var route = "{{url('')}}/add_to_cart/"+product_id+"/"+purchase_id+"/"+cust_group_id;
   
   $.get(route, function(data) {
     $('#cartTableData').empty();
    $('#cartTableData').html(data.split("|,|")[0]);
    
      //alert(data.split("|,|")[1]);
    
   });
  }else{
    alert("The Product Stock Has finished");
  }
 }

 // Add bundle product to cart 
 function addBundleToCart(bundle_id) {
   var route = "{{url('')}}/add_to_cart_bundle/"+bundle_id;
   $.get(route, function(data) {
     $('#cartTableData').empty();
    $('#cartTableData').html(data.split("|,|")[0]);
   });
 }

 

 // Remove From Cart

 function applyCoupon() {
  var coupon_no=document.getElementById('coupon_no').value;
    var route = "{{url('')}}/update_cart_discount/"+coupon_no;
    $.get(route, function(data) {
      $('#cartTableData').empty();
      $('#cartTableData').html(data.split("|,|")[0]);
      $('#coupon_no').val(coupon_no);
    });
 }

 

 // Remove From Cart

 function removeFromCart(cartItemId) {
    var route = "{{url('')}}/remove_item/"+cartItemId;
    $.get(route, function(data) {
      if(data.split("|,|")[0]==""){
        location.reload();
      }
      $('#cartTableData').empty();
      $('#cartTableData').html(data.split("|,|")[0]);
    });
 }

 // update Cart quentity'update_cart_quentity/{rowid}/{qty}'
 function update_cart_quentity(productId,qty,preQty){
  var product_qty=parseInt(document.getElementById('product_qty'+productId).value);
  qty = parseInt(qty);
   if(qty<product_qty){
   $('#paymentModal').attr('disabled',true);
  var route = "{{url('')}}/update_cart_quentity/"+productId+"/"+qty;
    $.get(route, function(data) {
      $('#cartTableData').empty();
      $('#cartTableData').html(data.split("|,|")[0]);
    });
 }else{
   alert('You Dont have Enoung Product On Stock');
   document.getElementById('pqty'+productId).value=preQty;
 }
}
 
 function addpaidvalue(){
    
    var payment_method_id = document.getElementById("payment_method_id").value;
   
    $('#payment_tk').empty(paid_amount);
    var salePrice=parseInt($('#netSalePrice').text());
    payment_method_id=payment_method_id.split(" | ");
    if(!(payment_method_id[0]==0)){
      //<label for='paid_amount'>Paid Amount</label>
        var paid_amount="";
        paid_amount+="<input type='number' class='form-control' name='paid_amount' id='paid_amount' value='"+salePrice+"' step='.01'><br><input type='number' step='0.01' class='form-control' name='regular_discount_total' id='regular_discount_total' placeholder='Discount Amount (optional)'>";
        if(payment_method_id[1]==1){
          //<label for='check_no'>Check No <i class='text-danger'>( Optional )</i></label>
        paid_amount+="<br><input type='text' class='form-control' name='check_no' id='check_no' onblur='check_disposal_field()' placeholder='Input Cheque No(optional)'>";
        }
        $('#payment_tk').prepend(paid_amount);
    }
 }
 //extra field for cheque disposal date 
function check_disposal_field(){
    var payment_method_id = document.getElementById("payment_method_id").value;
    $('#date_field').empty(paid_amount);
    payment_method_id=payment_method_id.split(" | ");
    if(payment_method_id[1]==1){
        var paid_amount="<p>Cheque Disposal Date:</p>";
        paid_amount+="<input type='date' class='form-control' size='5' placeholder='Cheque Disposal Date' name='cheque_disposal_date' id='cheque_disposal_date'>";
        $('#date_field').prepend(paid_amount);
    }
 }

 
   // function for show hide category search or brand search value
   function showSearchValue(value)
   {     
    $.ajax({
      url: "productSearch/"+value,
    })
    .done(function(data) {
      if(value =='category'){
        var option = "<select style='min-width: 350px;'' name='search_value' id='search_value'class='form-control'><option value=''>Select Category</option>";
          for(i=0;i<data.length;i++){
            option += "<option value='"+data[i].id+"'>"+data[i].category_name+"</option>";
          }
          option +="</select>";
        $('#search_value_div').empty();
        $('#search_value_div').append(option);
      }else if(value =='brand'){
        var option = "<select style='min-width: 350px;' name='search_value' id='search_value'class='form-control'><option  value=''>Select Brand</option>";
          for(i=0;i<data.length;i++){
            option += "<option value='"+data[i].id+"'>"+data[i].brand_name+"</option>";
          }
          option +="</select>";
        $('#search_value_div').empty();
        $('#search_value_div').append(option);
      }else if(value=='product'){
        var option ="<input type='text' style='min-width: 350px;' name='search_value' class='form-control' id='search_value' placeholder='Search Product'>";
        $('#search_value_div').empty();
        $('#search_value_div').append(option);
      }
    });
   }
// Customer Add
$('#addCustomerBtn').click(function(){
  $('#addCustomerRow').toggle(800);
});


// Product datalist selected and added to cart

$("#productDataListInput").on("change keyup", function(event) {
  var productValue = $(this).val();
  {{-- var quantity = productValue.includes("*"); --}}
  {{-- if(quantity == false){ --}}
  var selectedOption = $("#productDataList option[value='" + $(this).val().replace(/\'/g,"\\'") + "']");
  // var selectedOption = $('#productDataList option[value="' + $(this).val() + '"]');
  var productId = parseInt(selectedOption.attr('data-productId'));
  var purchaseId = parseInt(selectedOption.attr('data-purchaseId'));
    if(event.keyCode === 13){
      if(productId && purchaseId){
        addToCart(productId,purchaseId);
        $("#productDataListInput").val('');
      }
    } 
  {{-- } --}}
  {{-- else{
  var productValueSplit = productValue.split('*');
  var productQuantity = parseInt(productValueSplit[1]);
  var productname = productValueSplit[0];
      if(productQuantity > 0){
        // normally add to cart
        var selectedOption = $("#productDataList option[value='" + productname + "']");
        var productId = parseInt(selectedOption.attr('data-productId'));
        var purchaseId = parseInt(selectedOption.attr('data-purchaseId'));
        var preQty = $('#pqty'+productId).val();
          if(event.keyCode === 13){
            if(productId && purchaseId){
              addToCart(productId,purchaseId);
              update_cart_quentity(productId,productQuantity,preQty);
            }
          }
        // then update cart qty
      }else{
        //Enter a quentity less than 0
      }
  } --}}
});


// Change the selector if needed
var $table = $('table'),
    $bodyCells = $table.find('tbody tr:first').children(),
    colWidth;

// Get the tbody columns width array
colWidth = $bodyCells.map(function() {
    return $(this).width();
}).get();

// Set the width of thead columns
$table.find('thead tr').children().each(function(i, v) {
    $(v).width(colWidth[i]);
});  

</script>

@endpush
