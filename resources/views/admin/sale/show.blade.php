@extends('layouts.admin')

@section('title','Sale')

@push('css')
<style>
  .vhr{
  border-right: 1px solid #333;
  height:100%;
}
}
</style>
@endpush

@section('content')
<div class="row">
    <div class="col-lg-6 grid-margin stretch-card">
      <div class="card vhr">
        <div class="card-body">
          <h4 class="card-title">Product Image :</h4>
          <img src="{{asset($product->image)}}" alt="">
        </div>
      </div>
    </div>
    <div class="col-lg-6 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Product Details :</h4>
          <div class="table-responsive">
            <table class="table table-hover">
              
                <tr>
                  <th>Name : </th>
                  <td>{{$product->product_sku}}</td>
                </tr>
                <tr>
                  <th>Created At : </th>
                  <td>{{$product->created_at}}</td>
                </tr>
                <tr>
                  <th>Short Description : </th>
                  <td>{{$product->short_description}}</td>
                </tr>
                <tr>
                  <th>Created By : </th>
                  <td>{{$product->users->name}}</td>
                </tr>
                </tr>
                <tr>
                  <th>Reorder Limit : </th>
                  <td>{{$product->reorder_limit}}</td>
                </tr>
                </tr>
                <tr>
                  <th>Category : </th>
                  <td>{{$product->category->category_name}}</td>
                </tr>
                </tr>
                <tr>
                  <th>Brand : </th>
                  <td>{{$product->brand->brand_name}}</td>
                </tr>
                </tr>
                <tr>
                  <th>Vat : </th>
                  <td>{{$product->vat->vat_title}}</td>
                </tr>
                </tr>
              
            </table>
          </div>

        </div>
      </div>
    </div>
  </div>
@endsection

@push('js')

@endpush