@extends('layouts.admin') 
@section('content')
<div class="content-wrapper">
	<div class="row">
		<div class="col-md-12 grid-margin stretch-card">
			<div class="card" id="print_portion"> 
				<div class="card-body">
						@php 
						$f = new \NumberFormatter("en", \NumberFormatter::SPELLOUT);//write in php.init ;extension=php_intl.dll
						   @endphp
						   @if($invoice_setting->logo_enable)
						   <h2 class="text-{{$invoice_setting->logo_position?$invoice_setting->logo_position:'left'}}"> <img src="{{$invoice_setting->invoice_logo?asset($invoice_setting->invoice_logo):asset($company->company_logo)}}" id="imageView" style="width:250px;"/></h2>
						   @endif	
						   <h2 class="text-center">{{$company->company_name}}</h2>	
					   <h3 class="text-center">{{$sales->branch->name}} Branch</h3>
						   <h4 class="text-center">{{$sales->branch->address !=null?$sales->branch->address->email:"--"}}</h4>
						   <h4 class="text-center">{{$sales->branch->address !=null?$sales->branch->address->full_address:'---'}}</h4>
						   <h3 class="text-center">{{$invoice_setting->invoice_title}}</h3>
						   <p class="card-description text-center">{{$invoice_setting->invoice_no_title}}: <b>{{$sales->invoice_no}}</b></p>
						   <span class="text-left">Date: <b>{{date("d-F-Y",strtotime($sales->created_at))}}</b></span>
						   <span class="text-right float-right">{{$invoice_setting->customer_name}}: <b>{{$sales->customer->customer_name}}</b></span>
					<div class="table-responsive">
						<table class="table table-bordered">
							<thead>
								<tr class="table-active">
									<th>{{$invoice_setting->sl}}</th>
									<th>{{$invoice_setting->product_name}}</th>
									<th>{{$invoice_setting->qty}}</th>
									<th>{{$invoice_setting->unit_price}}</th>
									<th>{{$invoice_setting->total}}</th>
									<th>{{$invoice_setting->discount_type}}</th>
									<th>{{$invoice_setting->discount}}</th>
									<th>{{$invoice_setting->subtotal}}</th>
								</tr>
							</thead>
							<tbody>
								@php 
									$discount=0;
									$kisu = 0;
									$bdlName="hello";
								@endphp
								
								@foreach($Sale_detail as $key=>$sale)
								
								
								@if($sale->discount_type=="bundle")
									@if($bdlName!==$sale->bundle->bundle_name)
									<tr>
										<td>{{$key+1}}</td>
										<td><b>{{ $sale->bundle->bundle_name}}</b></td>
										<td  class="text-right">{{"1"}}</td>
										<td  class="text-right">{{$sale->bundle->bundle_price + $sale->bundle->bundle_discount_amount }}</td>
										<td  class="text-right">{{$sale->bundle->bundle_price + $sale->bundle->bundle_discount_amount}}</td>
										<td>{{$sale->discount_type}}</td>
										<td  class="text-right">{{$sale->bundle->bundle_discount_amount }}</td>
										<td  class="text-right">{{$sale->bundle->bundle_price}}</td>
										@php 
										$discount+=$sale->bundle->bundle_discount_amount;
										$kisu =$kisu+($sale->bundle->bundle_price+$sale->bundle->bundle_discount_amount);
										$bdlName=$sale->bundle->bundle_name;
										@endphp
									</tr>
									@endif
								@endif
								@if($sale->discount_type!=="bundle")
								<tr>
									<td>{{$key+1}}</td>
									<td><b>{{str_before($sale->product->product_sku,'|')}}</b><br>
										@forelse($sale->serial_numbers as $psl)
											<i># {{$psl->product_sl_no}}</i>
										@empty
										@endforelse
									</td>
									<td class="text-right">{{$sale->sold_quantity}}</td>
									<td class="text-right">{{$sale->sold_unit_price}}</td>
									<td class="text-right">{{$sale->getSubtotal()}}</td>
									<td>{{ $sale->discount_type }}</td>
									<td class="text-right">{{$sale->discount_subtotal}}</td>
									<td class="text-right">{{$sale->getSubtotal()-$sale->discount_subtotal}}</td>
									@php 
									$discount+=$sale->discount_subtotal;
									$kisu = $kisu + $sale->getSubtotal();
									 @endphp
								</tr>
								@endif
								@endforeach
							</tbody>
							<tfoot>
								<tr class="table-active">
									<th colspan="4">{{$invoice_setting->grand_total}} :</th>
									<th class="text-right"> {{$kisu}}</th>
									<th></th>
									<th class="text-right"> {{$discount}}</th>
									<th class="text-right"> {{$gt=$kisu-$discount}}</th>
								</tr>
								<tr><th colspan="8" class="text-center">-------------------------------------</th></tr>
								<tr>
									<th colspan="7">{{$invoice_setting->sales_total}} :</th>
									<th class="text-right">{{ number_format($kisu,2) }}</th>
									
								</tr>
								@if($sales->discount_total>0 || $discount>0)
								<tr>
								<th colspan="4">{{$invoice_setting->discount_total}} :</th>
								@if($sales->discount_total>0)
								<th colspan="2">{{$invoice_setting->coupon}} {{'@'}} {{$sales->discount_offer_details?$sales->discount_offer_details->discount_amount:''}}{{' TK'}}</th>
									<th class="text-right"> 
										@php
											$coupon=$sales->discount_total?$sales->discount_total:0;
										@endphp
										{{ number_format($coupon,2)}}</th>
									<th></th>
									@else
									<th colspan="2"></th>
									<th></th>
									<th></th>
									@php $coupon=$sales->discount_total?$sales->discount_total:0;@endphp
								@endif

								</tr>
								<tr>
								<th colspan="4"></th>
								<th colspan="2">{{$invoice_setting->single_discount}} :</th>
								<th style="border-bottom:black solid 1px" class="text-right"> {{ number_format($discount,2)}}</th>
								<th style="border-bottom:black solid 1px" class="text-right">
									@php
										$grossDiscount= ($discount+$coupon);
									@endphp
									({{number_format($grossDiscount,2)}})</th>
								</tr>
								
								<tr class="table-active">
									<th colspan="4">{{$invoice_setting->discounted_total}} :</th>
									<th colspan="2"></th>
									<th></th>
								<th class="text-right">{{number_format(($kisu-$grossDiscount),2)}}</th>
								</tr>
								@else
								@php
								$coupon=$sales->discount_total?$sales->discount_total:0; 
								$grossDiscount=$discount+$coupon @endphp
								@endif 
								{{-- end both single and coupon discount check --}}
								@if($sales->total_tax>0)
								<tr>
								<th colspan="4"></th>
									<th colspan="2">{{$invoice_setting->tax}} :</th>
									<th></th><th class="text-right">
								{{-- {{$tax=\Cart::session(Auth::id())->getCondition('VAT') ? \Cart::session(Auth::id())->getCondition('VAT')->getCalculatedValue($kisu-$grossDiscount) : 0}} --}}
								@php
									$tax=$sales->total_tax;
								@endphp
								{{number_format($sales->total_tax,2)}}</th>
								</tr>
								@else
								<?php $tax=$sales->total_tax;?>
								@endif
								@if($sales->regular_discount_total>0)
								<tr>
									<th colspan="4"></th>
									<th colspan="2">{{$invoice_setting->regular_discount}} :</th>
									<th></th>
									<th>
										@php
											$regular=$sales->regular_discount_total;
										@endphp
										({{ number_format($regular,2) }})</th>
								</tr>
								@else
								@php $regular=$sales->regular_discount_total; @endphp
								@endif
								<tr class="table-active">
									<th colspan="4"></th>
									<th colspan="2">{{$invoice_setting->net_sales_amount}}</th>
									<th></th>
									<th class="text-right">@php
										$amountToBeInWords=($kisu-$grossDiscount-$regular+$tax);
									@endphp
									{{ number_format($amountToBeInWords,2) }}</th>
								</tr>

								@if ($sales->transaction !== null)
									@foreach($sales->transaction->journals as $jr)
										@if($jr->accounts_id==2)
										<tr class="table-active">
											<th colspan="4">{{$invoice_setting->paid_amount}} :</th>
												<th colspan="2">{{$invoice_setting->paid_by}} {{$jr->account->account_head}} by {{$jr->paymentmethod->method_title}}</th>
												<th></th>
												<th class="text-right">{{$jr->amount}}</th>
											@elseif($jr->accounts_id==10)
											<tr class="table-active">
											<th colspan="4">{{$invoice_setting->paid_amount}} :</th>
											<th colspan="2">{{$invoice_setting->paid_by}} {{$jr->account->account_head}} by {{$jr->paymentmethod->method_title}}</th>
												<th></th>
												<th class="text-right">{{$jr->amount}}</th>
										</tr>
										@endif
									@endforeach
									@php
									$currentDue=0;
									@endphp
									@foreach($sales->transaction->journals as $jr)
										@if($jr->accounts_id==5)
										<tr class="table-danger">
											<th colspan="4">{{$invoice_setting->due_amount}} :</th>
											<th colspan="2"></th>
											<th></th>
											<th class="text-right">
												@php
													$currentDue=$jr->amount;
												@endphp
												{{number_format($currentDue,2)}}</th></tr>
										@endif
									@endforeach
									@if($previousBalance>0)
									<tr class="table-warning">
										<th colspan="4"></th>
										<th colspan="2">{{"Previous Due"}}</th>
										<th></th>
										<th class="text-right">{{number_format(($previousBalance),2)}}</th>
										{{-- <th>{{($previousBalance)}}</th> --}}
									</tr>
									<tr class="table-danger">
										<th colspan="4"></th>
										<th colspan="2">{{"Total Due"}}</th>
										<th></th>
										<th class="text-right">{{number_format(($previousBalance+$currentDue),2)}}</th>
									</tr>
									@endif
								@endif
								
									<tr>
										<th colspan="4">Billed Amount In Words:</th>
									<td colspan="4">{{ucwords($f->format($amountToBeInWords).' taka only')}}</td>
										
									</tr>
							</tfoot>
						</table>	

					</div>
					<div class="row" style="margin-top:50px">
							@php
								$i=1;
							@endphp
								@forelse ($footer_contents as $fc)
						<div class="form-group col-md-{{(12/$invoice_setting->footer_content_no)}}">
									<hr style="border-top:solid 1px black">
									<h4 class="text-center">
										@php
										$hello=explode('|',$fc->footer_content);
										@endphp
										@foreach ($hello as $item)
											{{$item}}<br>
										@endforeach
										</h4>
									@if($invoice_setting->footer_content_no<=$i++)
											@php
											break;	
											@endphp
											@endif
								</div>
								@empty
								@endforelse
						</div>
								<div class="col-md-12" style="margin-top:50px 0">
							
										<hr style="border-top:solid 1px black">
								<h5 class="text-center">
										@php
										$helloAdd=explode('|',$invoice_setting->footer_address);
										@endphp
										@foreach ($helloAdd as $itemAdd)
											{{$itemAdd}}<br>
										@endforeach
								</h5>
								<hr style="border-bottom:solid 1px black">
								</div>
						</div>
								
					<form action="{{route('sales.destroy',$sales->id)}}" method="POST">
						@csrf
						@method('delete')
					<button type="submit" id="delete_invoice_btn" onclick="return confirm('Are you sure to delete invoice??\n It can\'t be Retained')" class="btn btn-block btn-danger"><i class="mdi mdi-delete" ></i>Delete Invoice</button>
					<br>
					<button type="button" id="btn_print" class="btn btn-block btn-primary" onclick="printData()"><i class="mdi mdi-printer" ></i>Print Report</button>
				</div>
			</div>
		</div>
	</div>
	<script>
		function printData(){
			$('#btn_print').hide();
			$('#delete_invoice_btn').hide();
			var contentPrint=document.getElementById('print_portion').innerHTML;
			var contentOrg=document.body.innerHTML;
			document.body.innerHTML=contentPrint;
			window.print();
			document.body.innerHTML=contentOrg;
			$('#btn_print').show();
			$('#delete_invoice_btn').show();
		}
	
	</script>
	@endsection