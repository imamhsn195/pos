@extends('layouts.admin') 
@section('content')
@push('name')
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
@endpush

<h1 class="text-center">{{$customer->customer_name}}'s  Invoice Information</h1>
<h3 class="text-center">
	@if (Auth::user()->user_type == 'superadmin')
		 All Branches 
	@else
		{{Auth::user()->branch->name}}
	@endif
</h3>
<h3 class="text-center">
	@php
		$searchbyfrom = date('Y-m-01');
		$searchbyto = date('Y-m-t');
	@endphp
@if(isset($_GET['searchbyfrom']) && $_GET['searchbyfrom']!='')
@php
	$searchbyfrom = $_GET['searchbyfrom'];
	$searchbyto = $_GET['searchbyto'];
@endphp
@endif
	<form method="post" action="{{route('pdfreport')}}" id="downloadpdf">
			@csrf
			<input type="hidden" value="{{$searchbyfrom}}" name="searchbyfrom"  id="example-search-input1">
			<input type="hidden" value="{{$searchbyto}}" name="searchbyto"  id="example-search-input2">
			<input type='hidden' id='format'  name='format'/>
	</form>
</h3>

		<h4 class="text-center">
			{{"Showing Reports From "}}<b>{{$searchbyfrom}}</b>{{" To "}}<b>{{$searchbyto}}</b>
		</h4>


<p class="card-description text-center">

	<form method="get" action="{{url('').'/customer_invoice_show/'.Request::segment(2)}}">
	 <div class="input-group col-md-6 float-right">
				<label for="example-search-input1">From :</label>
				<input class="form-control py-2" value="{{$searchbyfrom}}" name="searchbyfrom" type="date" id="example-search-input1">
				<label for="example-search-input2">To :</label>
				<input class="form-control py-2" value="{{$searchbyto}}" name="searchbyto" type="date" id="example-search-input2">
				<span class="input-group-append">
					<button class="btn btn-primary" type="submit">
						<i class="mdi mdi-magnify"></i>
					</button>
				</span>
			</div>
		 <div class="btn-group float-left" role="group" aria-label="Basic example">
				<button type="button" onclick="downloadReport(this.id)" id="PdfReport" class="btn btn-secondary">PDF</button>
				<button type="button" onclick="downloadReport(this.id)" id="ExcelReport"  class="btn btn-secondary">EXCEL</button>
				<button type="button" onclick="downloadReport(this.id)" id="CSVReport"  class="btn btn-secondary">CSV</button>
			</div>
	</form>
  </p>
<div class="table-responsive" style="margin:0% 2%">
<table class="table table-bordered">
	<thead>
		<tr>
			<th>#SL</th>
			<th>Date</th>
			<th>Invoice ID</th>
			<th>Customer Name</th>
			<th >Total</th>
			<th>Discount</th>
			<th>SubTotal</th>
			<th>Created By</th>
		</tr>
	</thead>
	<tbody>
		<?php $totalSubtotal=0; $discountTotal=0;?>
	@foreach($sales as $key=>$sale)
		<tr>
			<td>{{$key+1}}</td>
			<td title="{{$sale->created_at}}">{{date("d M Y",strtotime($sale->created_at))}}</td>
			<td><a class="link" href="{{url('sales_details_report/'.$sale->id)}}">
				{{$sale->invoice_no}}
			</a> </td>
			<td>{{$sale->customer->customer_name}}</td>
			<td>Tk. {{$sale->total_amount}}</td>
			{{-- <td>{{$sale->sale_details->sum('discount_subtotal')}}</td> --}}
			<td>Tk. {{$discount=$sale->sale_details->sum('discount_subtotal')+$sale->discount_total+$sale->regular_discount_total}}</td>
			<?php $discountTotal+=$discount;
			$totalSubtotal+=$sale->total_amount-$discount;
			?>
			<td>Tk. {{$sale->total_amount-$discount}}</td>
			<td title="{{$sale->user->name}}">{{$sale->user->user_type}}</td>
		</tr>
	@endforeach
	</tbody>
	<tfoot>
		<tr>
			<th colspan="4"><span  class="float-right">Total Amount </th>
			<th>Tk. {{$total}}</th>
			<th>Tk. {{$discountTotal}}</th>
			<th colspan="2">Tk. {{$totalSubtotal}}</th>
		</tr>
	</tfoot>
</table>
</div>	
@endsection
@push('js')
	<script>
		// Download report in multiple format
		function downloadReport(format){
			$('#format').val(format);
			$('#downloadpdf').submit();
		}
	</script>
@endpush