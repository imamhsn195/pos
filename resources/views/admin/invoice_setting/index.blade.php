@extends('layouts.admin')
@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-12 grid-margin stretch-card">

            <div class="card" id="print_portion">
                <div class="card-body">
                    <form method="POST" action="{{route('invoice_setting.store')}}" enctype="multipart/form-data">
                        @csrf
                    <div id="invoice_logo_enable" style="{{$invoice_setting->logo_enable?'display:block':'display:none'}}">
                    <h2 id="applyAlign" class="text-{{$invoice_setting->logo_position?$invoice_setting->logo_position:'left'}}">
                            <img src="{{$invoice_setting->invoice_logo?asset($invoice_setting->invoice_logo):asset($company->company_logo)}}" id="imageView" style="width:250px;"/>
                            <span class="caption"><input disabled type="file" class="form-control deditable" name="invoice_logo" onchange="showImage(this)">
                            <input type="hidden" class="form-control deditable" name="old_invoice_logo" value="{{$invoice_setting->invoice_logo}}"></span>
                        </h2>
						<h2 class="text-center">
                                <div class="form-group">
                                    <p>Logo Alignment</p>
                                        <select class="form-control" id="logo_position" name="logo_position" onchange="get_align()">
                                         <option value="left" {{$invoice_setting->logo_position=='left'?'selected':''}} disabled class="deditable">Left</option>
                                         <option value="center" {{$invoice_setting->logo_position=='center'?'selected':''}} disabled class="deditable">Center</option>
                                         <option value="right" {{$invoice_setting->logo_position=='right'?'selected':''}} disabled class="deditable">Right</option>
                                       </select>
                                </div>
                        </h2>
                    </div>
                        <h2 class="text-center">
                            <div class="form-group">
                                <p><label>Enable Logo &nbsp;&nbsp; </label><input class="deditable" type="checkbox" onclick="enableLogo({{$invoice_setting->logo_enable}})" name="logo_enable" disabled
                                    {{$invoice_setting->logo_enable? 'checked':''}}
                                    value="1"/> </p>
                                   
                            </div>
                    </h2>
                        <h1 class="text-center"><input type="text" class="form-control offset-md-4 col-4 editable"
                                readonly name='invoice_title' value="{{$invoice_setting->invoice_title}}"><input
                                type="button" class="btn btn-primary float-right col-3" value="Edit Title?"
                                onclick="removeAttrReadonly()"></h1>
                        <p class="card-description text-center"><input type="text"
                                class="form-control offset-md-4 col-4 editable" readonly name='invoice_no_title'
                                value="{{$invoice_setting->invoice_no_title}}"> <b>{{'Invoice no-123456879'}}</b></p>
                        <div class="row">
                            <span class="text-left col-4">Date : <b>{{date("d-F-Y")}}</b></span>
                            <div class="text-right float-right col-8">
                                <input type="text" class="form-control editable offset-md-8 col-4" readonly
                                    name='customer_name' value="{{$invoice_setting->customer_name}}">
                                <b>{{"Demo Customer Name"}}</b></span>
							</div>
							

                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr class="table-active">
                                            <th><input type="text" class="form-control editable" readonly name='sl'
                                                    value="{{$invoice_setting->sl}}"></th>
                                            <th class="input-group mb2"><input type="text" class="form-control editable"
                                                    readonly name='product_name'
                                                    value="{{$invoice_setting->product_name}}"
                                                    placeholder="product_name"><input type="text"
                                                    class="form-control editable" readonly name='product_sl_no'
                                                    value="{{$invoice_setting->product_sl_no}}"
                                                    placeholder="product_sl_no"></th>
                                            <th><input type="text" class="form-control editable" readonly name='qty'
                                                    value="{{$invoice_setting->qty}}"></th>
                                            <th><input type="text" class="form-control editable" readonly
                                                    name='unit_price' value="{{$invoice_setting->unit_price}}"></th>
                                            <th><input type="text" class="form-control editable" readonly name='total'
                                                    value="{{$invoice_setting->total}}"></th>
                                            <th><input type="text" class="form-control editable" readonly
                                                    name='discount_type' value="{{$invoice_setting->discount_type}}">
                                            </th>
                                            <th><input type="text" class="form-control editable" readonly
                                                    name='discount' value="{{$invoice_setting->discount}}"></th>
                                            <th><input type="text" class="form-control editable" readonly
                                                    name='subtotal' value="{{$invoice_setting->subtotal}}"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>Product Sku With Sl.</td>
                                            <td>2pcs</td>
                                            <td>xx</td>
                                            <td>xxx</td>
                                            <td>single</td>
                                            <td>xxx</td>
                                            <td>xxx</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Product Sku With Sl.</td>
                                            <td>2pcs</td>
                                            <td>xx</td>
                                            <td>xxx</td>
                                            <td>bundle</td>
                                            <td>xxx</td>
                                            <td>xxx</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Product Sku With Sl.</td>
                                            <td>2pcs</td>
                                            <td>xx</td>
                                            <td>xxx</td>
                                            <td>customer group</td>
                                            <td>xxx</td>
                                            <td>xxx</td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr class="table-active">
                                            <th colspan="4"><input type="text" class="form-control editable" readonly
                                                    name='grand_total' value="{{$invoice_setting->grand_total}}"> </th>
                                            <th> XXX</th>
                                            <th></th>
                                            <th> XXX</th>
                                            <th> XXX</th>
                                        </tr>
                                        <tr>
                                            <th colspan="8" class="text-center">-------------------------------------
                                            </th>
                                        </tr>
                                        <tr>
                                            <th colspan="7"><input type="text" class="form-control col-4 editable"
                                                    readonly name='sales_total'
                                                    value="{{$invoice_setting->sales_total}}"></th>
                                            <th>xx</th>

                                        </tr>
                                        <tr>
                                            <th colspan="4"><input type="text" class="form-control col-4 editable"
                                                    readonly name='discount_total'
                                                    value="{{$invoice_setting->discount_total}}"> </th>
                                            <th colspan="2"><input type="text" class="form-control editable" readonly
                                                    name='coupon' value="{{$invoice_setting->coupon}}"> {{'@'}}
                                                1000{{' TK'}}</th>
                                            <th> xx </th>
                                            <th></th>


                                        </tr>
                                        <tr>
                                            <th colspan="4"></th>
                                            <th colspan="2"><input type="text" class="form-control editable" readonly
                                                    name='single_discount'
                                                    value="{{$invoice_setting->single_discount}}"> </th>
                                            <th style="border-bottomblack solid 1px">XX</th>
                                            <th style="border-bottomblack solid 1px">(XXX)</th>
                                        </tr>

                                        <tr class="table-active">
                                            <th colspan="4"><input type="text" class="form-control editable" readonly
                                                    name='discounted_total'
                                                    value="{{$invoice_setting->discounted_total}}"> </th>
                                            <th colspan="2"></th>
                                            <th></th>
                                            <th>XX</th>
                                        </tr>
                                        <tr>
                                            <th colspan="4"></th>
                                            <th colspan="2"><input type="text" class="form-control editable" readonly
                                                    name='tax' value="{{$invoice_setting->tax}}"> </th>
                                            <th></th>
                                            <th>XX</th>
                                        </tr>
                                        <tr>
                                            <th colspan="4"></th>
                                            <th colspan="2"><input type="text" class="form-control editable" readonly
                                                    name='regular_discount'
                                                    value="{{$invoice_setting->regular_discount}}"> </th>
                                            <th></th>
                                            <th>(XXX)</th>
                                        </tr>

                                        <tr class="table-active">
                                            <th colspan="4"></th>
                                            <th colspan="2"><input type="text" class="form-control editable" readonly
                                                    name='net_sales_amount'
                                                    value="{{$invoice_setting->net_sales_amount}}"></th>
                                            <th></th>
                                            <th>(XXX)</th>
                                        </tr>
                                        <tr class="table-active">
                                            <th colspan="4"><input type="text" class="form-control editable" readonly
                                                    name='paid_amount' value="{{$invoice_setting->paid_amount}}"> </th>
                                            <th colspan="2"><input type="text" class="form-control editable" readonly
                                                    name='paid_by' value="{{$invoice_setting->paid_by}}">Cash/Bank by
                                                Bkash/DBBL</th>
                                            <th></th>
                                            <th>XXX</th>


                                        <tr class="table-danger">
                                            <th colspan="4"><input type="text" class="form-control editable" readonly
                                                    name='due_amount' value="{{$invoice_setting->due_amount}}"> </th>
                                            <th colspan="2"></th>
                                            <th></th>
                                            <th>XXX.xx</th>
                                    </tfoot>
                                </table>
                                {{-- {{asset('admin')}}/vendors/js/vendor.bundle.base.js --}}
                            </div>
                            <button type="submit" id="btn_print" class="btn btn-block btn-primary"
                                style="display:none"><i class="mdi mdi-table-edit"></i>Update</button>
                    </form>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="row">

                        <input type="button" class="btn btn-primary offset-md-9 col-3" value="Edit Footer And Layout?"
                            onclick="removeFooterReadonly()">


                        <form method="GET" action="{{route('invoice_setting.create')}}">
                            @csrf
                            <div class="form-group">
                                <label>Footer Content No</label>
                                <select disabled type="number" id='footer_no' name="footer_content_no"
									onchange="getFooterContent()" class="form-control editable-radio">
									<option {{$invoice_setting->footer_content_no==2?'selected':''}} value="2">2</option>
									<option {{$invoice_setting->footer_content_no==3?'selected':''}} value="3">3</option>
									<option {{$invoice_setting->footer_content_no==4?'selected':''}} value="4">4</option>
								
								</select>
                            </div>

                            <div class="row" id="footerContentNewField">
                                @forelse ($footer_contents as $fc)
							<div class="form-group col-md-{{(12/$invoice_setting->footer_content_no)}}">
										<hr style="border-top:solid 1px black">
                                    <input type="text" readonly class="form-control footer_content"
										name="footer_content[{{$fc->id}}]" value="{{$fc->footer_content}}">
									
                                </div>
                                @empty
                                @endforelse

							</div>
							<div class="row" style="margin:50px 0">
									<hr style="border-top:solid 1px black">
							<input type="text" readonly class="form-control feditable" value="{{$invoice_setting->footer_address}}" name="footer_address" placeholder="footer address">
							<hr style="border-bottom:solid 1px black">
							</div>


                    <div class="form-group row">
                        <div class="option col-md-4">
                            <input class="editable-radio" type="radio" name="layouts" disabled
                                {{$invoice_setting->layouts=='admin.sale.sales_details_report'? 'checked':''}}
                                value="admin.sale.sales_details_report" id="layout1" />
                            <label for="layout1">
                                <img src="{{asset("")}}upload/invoice_layouts/layout2.png" alt=""
                                    class="img-thumbnail" /><span class="caption"> Layout1(DIscounted)</span>
                            </label>
                        </div>
                        <div class="option col-md-4">
                            <input class="editable-radio" type="radio" name="layouts" disabled
                                {{$invoice_setting->layouts=='admin.sale.sales_invoice_layout2'? 'checked':''}}
                                value="admin.sale.sales_invoice_layout2" id="layout2" />
                            <label for="layout2">
                                <img src="{{asset('')}}upload/invoice_layouts/layout1.png" alt=""
                                    class="img-thumbnail" /><span class="caption">layout2(Samsumg)</span>
                            </label>
                        </div>
                    </div>
                    <button type="submit" id="btn_print_footer" class="btn btn-block btn-primary"
                        style="display:none"><i class="mdi mdi-table-edit"></i> Update Layouts And Footer</button>
                    </form>
                </div>

            </div>
        </div>

    </div>
</div>

<script>
    function removeAttrReadonly() {
        $('.editable').attr('readonly', false);
        $('.deditable').attr('disabled',false);
        $('#btn_print').show();
        //$('#btn_print_footer').show();
    }
    function enableLogo(yesLogo) {
        
    $('#invoice_logo_enable').toggle();
      
    }
    function get_align() {
        var align='text-'+$('#logo_position').val();
        //$('.editable-radio').attr('disabled',false);
        $('#applyAlign').attr('class',align);
        //$('#btn_print_footer').show();
    }

    function removeFooterReadonly() {
        $('.feditable').attr('readonly', false);
        $('.footer_content').attr('readonly', false);
        $('.editable-radio').attr('disabled', false);
        //$('#btn_print').show();
        $('#btn_print_footer').show();
    }

    function getFooterContent() {
        var footerNo = $('#footer_no').val();
        var footer_content_old = $('.footer_content').length;
        var i = 0;
        var NewinputField = '';
        while (i < (footerNo - footer_content_old)) {
            NewinputField +=
                '<div class="form-group col-md-4"><label>New Footer Content</label><input type="text" class="form-control footer_content" name="footer_content_new[]"></div>'
            i++;
        }
        $('#footerContentNewField').append(NewinputField);
    }
    function showImage(input) {
        console.log(input)
          if (input.files && input.files[0]) {
              var reader = new FileReader();
              reader.onload = function (event) {
                  $('#imageView').attr('src', event.target.result);
              }
              reader.readAsDataURL(input.files[0]);
          }
    }

    // function printData(){
    // 	$('#btn_print').hide();
    // 	var contentPrint=document.getElementById('print_portion').innerHTML;
    // 	var contentOrg=document.body.innerHTML;
    // 	document.body.innerHTML=contentPrint;
    // 	window.print();
    // 	document.body.innerHTML=contentOrg;
    // 	$('#btn_print').show();
    // }
    

</script>
@endsection
@push('js')
<script type="text/javascript" src="{{asset('admin/js/select-togglebutton.js')}}"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $('#logo_position').togglebutton();
            })
        </script>    
@endpush