@extends('layouts.admin')

@section('content')

<div class="row">
            <div class="col-md-6 d-flex align-items-stretch grid-margin">
              <div class="row flex-grow">
                <div class="col-12">
                  <div class="card">
                    <div class="card-body">
                      <h4 class="card-title">Attribute Set Form</h4>
                      <p class="card-description">
                      </p>
					  
                      <form class="forms-sample" method="post" action="{{route('attribute_sets.update',$atset->id)}}">
					  @csrf
					  @method('put')
                        <div class="form-group">
                          <label for="attribute_name">Attribute Set</label>
                          <input type="text" class="form-control" name="attribute_set_name" id="attribute_set_name" value="{{ $atset->attribute_set_name }}">
								@if ($errors->any())
									@foreach($errors->all() as $error)
                                    <span class="alert text-danger">**{{ $error }}
                                    </span>
									@endforeach
                                @endif
						</div>
						
						<div class="form-group">
                          
						  <label for="description">Description</label>
                          <textarea class="form-control" name="description" id="description" value="" >{{ $atset->description }}</textarea>
						 
						</div>
						
						<div class="form-group">
                          
						  <label for="choose_attribute">ChooseAttribute</label>
                           <select multiple class="form-control" name="choose_attribute[]">
							  @forelse($attributes as $attrs)
								<option <?php foreach($atset->attribute as $chek){if($chek->id==$attrs->id) { echo "selected";}}?> value="{{ $attrs->id }}">{{ $attrs->attribute_name }}</option>
							  @empty
							  <a href="{{route('attributes.create')}}" class="link link-info">Add Atrribute First</a>
							  @endforelse
							</select>
							<small> <i class="alert text-danger">"Ctrl+Click"</i> to Select Multiple Atrribute</small>
													 
						</div>
							 
						 <button type="submit" class="btn btn-success mr-2">Submit</button>
                        <a class="btn btn-light" href="{{route('attribute_sets.index')}}">Cancel</a>
                      </form>
                    </div>
						
                  </div>
                </div>
              </div>
            </div>
            
          </div>

@endsection