@extends('layouts.admin')

@section('content')

<div class="row">
<div class="col-md-12 d-flex align-items-stretch grid-margin">
  <div class="row flex-grow row_table_responsive">
	<div class="col-md-12">
	  <div class="card">
		<div class="card-body">
				<h2 class="text-center">All Attribute Sets</h2>
				<div class="row">
            {{-- <div class="col-md-3"></div> --}}
            <div class="col-md-12">
                <form action="{{route('attribute_sets.index')}}" method="GET">
                  <div class="input-group float-right">
                      <input class="form-control py-2 border-primary" placeholder="Search here.." name="searchby" type="text" id="example-search-input" required="">
                      <span class="input-group-append">
                        <button class="btn btn-primary" type="submit">
                            <i class="mdi mdi-magnify"></i>
                        </button>
                        <a class="btn btn-primary" title="Register New Attribute Set" href="{{route('attribute_sets.create')}}"><i class="mdi mdi-playlist-plus"></i></a>  
                        @if (request()->query())
                        <a class="btn btn-primary" title="Show All Attribute Sets" href="{{route('attribute_sets.index')}}"><i class="mdi mdi-format-list-bulleted"></i></a> 
                        @endif
                      </span>
                  </div>
                </form>
						</div>
						{{-- <div class="col-md-3"></div> --}}
          </div>
      <br>
			

		  <div class="table-responsive">
		      <table class="table table-striped table-bordered border-primary">
		          <thead>
		              <tr>
		                  <th>ID</th>
		                  <th>Attribute Set</th>
		                  <th>Description</th>
		                  <th>Attributes</th>
		                  <th>Created By</th>


		                  <th colspan="2" class="text-center">Actions</th>
		              </tr>
		          </thead>
		          <tbody>
		              <?php $i =1; ?>
		              @forelse($attribute_sets as $cat)
		              <tr>
		                  <td>{{$i++}}</td>
		                  <td>{{$cat->attribute_set_name}}</td>
		                  <td>{{str_limit($cat->description,25)}}</td>

		                  <?php
								$att_val="";

								foreach($cat->attribute as $sattr){
								$att_val.=$sattr->attribute_name.".<br>";
								} ?>
		                  <td title="{{ rtrim($att_val, ' , ')."." }}">

		                      {!!rtrim($att_val, '<br>').""!!}
		                  </td>
		                  <td>{{$cat->users->name}}</td>
		                  <td>
		                      <span class="input-group-append">
		                          <a href="{{route('attribute_sets.edit',$cat->id)}}" class="btn btn-dark btn-sm"><i
		                                  class="mdi mdi-table-edit"></i></a>
		                          <form action="{{route('attribute_sets.destroy',$cat->id)}}" method="post">
		                              @csrf
		                              @method('delete')
		                              <button type="submit" onclick="return confirm('Are you sure to delete this?')"
		                                  class="btn btn-danger btn-sm">
		                                  <i class="mdi mdi-delete"></i>
		                              </button>
		                          </form>
		                      </span>
		                  </td>
		              </tr>
		              @empty
		              <tr>
		                  <td colspan="8">There is no records available</td>
		              </tr>
		              @endforelse
		          </tbody>
		      </table>
		      <span class="float-right">{{$attribute_sets->appends(request()->query())->links()}}</span>
		  </div>
		  </div>
	  </div>
	</div>
  </div>
</div>

</div>

@endsection