@extends('layouts.admin')

@section('content')

<div class="row">
<div class="col-md-12 d-flex align-items-stretch grid-margin">
  <div class="row flex-grow row_table_responsive">
	<div class="col-md-12">
	  <div class="card">
		<div class="card-body">
      <h1 class="text-center">Journal Sheet</h1>
      @if(Auth::user()->user_type=="superadmin")
      <h4 class="text-center">Showing All Branches</h4>
      @else
      <h4 class="text-center">Showing {{Auth::user()->branch->name}} Branch</h4>
      @endif
      <p class="card-description text-center">
          <form method="get" action="{{route('journals.index')}}">
              <div class="input-group col-md-6 float-right">
                  <label for="example-search-input1">From :</label>
                  <input class="form-control py-2" @if(isset($_GET['searchbyfrom'])) value="{{$_GET['searchbyfrom']}}"
                      @else value="{{date('Y-m-01')}}" @endif name="searchbyfrom" type="date"
                      id="example-search-input1">
                  <label for="example-search-input2">To :</label>
                  <input class="form-control py-2" placeholder="To.." name="searchbyto" type="date"
                      id="example-search-input2" @if(isset($_GET['searchbyto'])) value="{{$_GET['searchbyto']}}" @else
                      value="{{date('Y-m-t')}}" @endif>
                  <span class="input-group-append">
                      <button class="btn btn-primary" type="submit">
                          <i class="mdi mdi-magnify"></i>
                      </button>
                  </span>

              </div>
          </form>
      </p>
      
      <div class="table-responsive">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>Transaction ID</th>
                          <th>Date</th>
                          <th>Account Title</th>
                          <th>description</th>
                          <th>Debit</th>
                          <th>Credit</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php
                        $t_id=0;
                        $j_date="";
                        $j_des="";
                        $amount=0;
                        //$tspan=2;
                        ?>
					  @forelse($journals as $jr)
                        <tr>
                        @if($t_id!==$jr->transaction_id)
                            <td>{{$jr->transaction_id}}</td>
                           <?php $time=strtotime($jr->created_at);
                            $date=date("d-M-Y",$time);?>
                            <td>{{$date}}</td>
                            <td>{{$jr->account->account_head}}</td>
                            <td>{{$jr->description}}</td>                             
                          @else
                         <td></td>
                         <td></td>
                            @if($jr->journal_type==1)
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$jr->account->account_head}}</td>
                            @else
                            <td>{{$jr->account->account_head}}</td>
                            @endif
                          <td></td>
                          @endif                          
                         
                         
                          @if($jr->journal_type==0)
                          <td>{{number_format($jr->amount,2)}}</td>
                         <?php $amount=$amount+$jr->amount;?>
                          @else
                          <td></td>
                          @endif
                          @if($jr->journal_type==1)
                          <td>{{number_format($jr->amount,2)}}</td>
                          @else
                          <td></td>
                          @endif
                          <?php
                        $t_id=$jr->transaction_id;
                        $j_date=$jr->created_at;
                        $j_des=$jr->description;
                        //$tspan=0;
                       ?>
                        </tr>
					@empty
						<tr>
							<td colspan="8">There is no records available</td>
						</tr>
					@endforelse
                      </tbody>
                      <tfoot>
                        <th colspan="4" class="text-right">Total Transaction :</th>
                        <th>{{ number_format($amount,2) }}</th>
                        <th>{{ number_format($amount,2) }}</th>
                      </tfoot>
                    </table>
                   <center>{{ $journals->appends(request()->query())->links()}}</center>
                  </div>
		</div>
	  </div>
	</div>
  </div>
</div>

</div>

@endsection