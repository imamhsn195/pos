@extends('layouts.admin')

@section('content')

<div class="row">
    <div class="col-md-10 d-flex align-items-stretch grid-margin">
        <div class="row flex-grow">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h1 class="text-center">{{$supplier->supplier_name}}'s Invoice Information</h1>
<h3 class="text-center"> 
	@if (Auth::user()->user_type == 'superadmin')
		 All Branches 
	@else
		{{Auth::user()->branch->name}}
	@endif
</h3>
<h3 class="text-center">
	@php
		$searchbyfrom = date('Y-m-01');
		$searchbyto = date('Y-m-t');
	@endphp
@if(isset($_GET['searchbyfrom']) && $_GET['searchbyfrom']!='')
@php
	$searchbyfrom = $_GET['searchbyfrom'];
	$searchbyto = $_GET['searchbyto'];
@endphp
@endif
	{{-- <form method="post" action="{{route('pdfreport')}}" id="downloadpdf">
			@csrf
			<input type="hidden" value="{{$searchbyfrom}}" name="searchbyfrom"  id="example-search-input1">
			<input type="hidden" value="{{$searchbyto}}" name="searchbyto"  id="example-search-input2">
			<input type='hidden' id='format'  name='format'/>
	</form>
</h3>

		<h4 class="text-center">
			{{"Showing Reports From "}}<b>{{$searchbyfrom}}</b>{{" To "}}<b>{{$searchbyto}}</b>
		</h4> --}}


                            <form method="get" action="{{url('').'/show_invoice/'.Request::segment(2) }}">
                                <div class="input-group col-md-6 float-right">
                                    <label for="example-search-input1">From :</label>
                                    <input class="form-control py-2" value="{{$searchbyfrom}}" name="searchbyfrom" type="date" id="example-search-input1">
                                    <label for="example-search-input2">To :</label>
                                    <input class="form-control py-2" value="{{$searchbyto}}" name="searchbyto" type="date" id="example-search-input2">
                                    <span class="input-group-append">
                                        <button class="btn btn-primary" type="submit">
                                            <i class="mdi mdi-magnify"></i>
                                        </button>
                                    </span>
                                </div>
                                  </form>
                        </p>
                        <div class="table-responsive">
                            <table class="table table-bordered" id="accordion" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>InvoiceNo</th>
                                        <th>Date</th>
                                        <th>Total Purchase Price</th>
                                        <th>Total Sale Price</th>
                                        <th>PurchaseBy</th>
                                       
                                    </tr>
                                </thead>
                                <tbody>
                                   
                        @forelse($supplier_invoice as $key=>$supplier_status)
                                <tr class="collapsed" data-toggle="collapse" href="#collapseOne{{$key}}" aria-expanded="true" aria-controls="collapseOne">
                            <th>{{$key}}</th>
                            @php $time=strtotime($supplier_invoice[$key][0]->created_at);
                            $date=date("d-M-Y",$time); @endphp
                                        <td>{{$date}}</td>
                                        @php
            
                                        $saleAmt=0;
                                        $purchasedAmt=0;
                                        //$tspan=2;
                                        @endphp
                                    @foreach($supplier_status as $jr)

                                        @php $purchasedAmt=$purchasedAmt+($jr->quantity*$jr->purchase_unit_price);
                                        $saleAmt=$saleAmt+($jr->quantity*$jr->sell_unit_price);
                                        @endphp           
                                    @endforeach
                                    <td>{{number_format($purchasedAmt,2)}}</td>
                                    <td>{{number_format($saleAmt,2)}}</td>
                                    <td>{{$supplier_invoice[$key][0]->users->name}}</td>
                                   
                        </tr>
                    <tbody id="collapseOne{{$key}}" class="table table-success collapse" data-parent="#accordion" style="border:black 2px solid">
                            <tr class="table-info">
                            <th colspan="5" class="text-center">{{$key}}</th>
                            </tr>
                                <tr class="table-success">
                                <th>SL</th>
                                <th>SKU</th>
                                <th>Purchase Unit Price</th>
                                <th>Qty</th>
                                <th>SubTotal</th>
                                </tr>
                                @php 
                                $subTotal=0;
                                @endphp 
                                    @foreach($supplier_status as $subkey=>$jr)

                                             
                               <tr>
                                <td>{{$subkey+1}}</td>
                                <td>{{str_limit($jr->product->product_sku,15)}}</td>
                                <td>{{$jr->purchase_unit_price}}</td>
                                <td>{{$jr->quantity}}</td>
                                @php $subTotal=$subTotal+($jr->quantity*$jr->purchase_unit_price) @endphp
                                <td>{{($jr->quantity*$jr->purchase_unit_price)}}</td>
                               </tr>
                                @endforeach
                           
                                <tr class="table-info">
                                <th colspan="4" class="text-right">Invoice Total</th>
                            <th>{{$subTotal}}</th>
                                </tr>
                            
                    </tbody>
                                    @empty
                                    <tr>
                                        <td colspan="8">There is no records available</td>
                                    </tr>
                                    @endforelse
                                </tbody>
                                {{-- <tfoot>
                                        @php
                                        $got=0;
                                        $payable=0;
                                        @endphp
                                        <tr class="table-default">
                                        <th colspan="5" class="text-right">Total Transaction :</th>
                                        <th>{{$getable=number_format($damount,2)}}</th>
                                        <th>{{$payable=number_format($amount,2)}}</th>
                                        </tr>
                                        @php
                                        $got=$damount;
                                        $payable=$amount;
                                        @endphp
                                        @if($got<$payable)
                                       
                                        <tr class="table-danger">
                                        <th colspan="5" class="text-right">Amount To Be Paid :</th>
                                        <th colspan="2" class="text-right">{{number_format(($amount-$damount),2)}}</th>
                                        </tr>
                                        @endif
                                        @if($got>$payable)
                                        <tr class="table-success">
                                        <th colspan="5" class="text-right">Amount To Be Received :</th>
                                        <th colspan="2" class="text-right">{{number_format(($damount-$amount),2)}}</th>
                                        </tr>
                                        @endif
                                       
                                      </tfoot> --}}
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection
