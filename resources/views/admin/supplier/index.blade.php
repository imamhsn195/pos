@extends('layouts.admin')
    @push('css')
        <style>
            .table td.address{
                vertical-align: middle;
                font-size: 13px;
                line-height: 1.5; 
                white-space: normal;
               
            }
        </style>
    @endpush
@section('content')

<div class="row">
    <div class="col-md-12 d-flex align-items-stretch grid-margin">
        <div class="row flex-grow row_table_responsive">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                            <h2 class="text-center">All Suppliers</h2>
                            <div class="row">
                                {{-- <div class="col-md-6"></div> --}}
                                <div class="col-md-12">
                                    <form action="{{route('suppliers.index')}}" method="GET">
                                      <div class="input-group float-right">
                                          <input class="form-control py-2 border-primary" placeholder="Search here.." name="searchby" type="text" id="example-search-input" required="">
                                          <span class="input-group-append">
                                            <button class="btn btn-primary" type="submit">
                                                <i class="mdi mdi-magnify"></i>
                                            </button>
                                            <a class="btn btn-primary" title="Register New Supplier" href="{{route('suppliers.create')}}"><i class="mdi mdi-playlist-plus"></i></a>  
                                            @if (request()->query())
                                            <a class="btn btn-primary" title="Show All Suppliers" href="{{route('suppliers.index')}}"><i class="mdi mdi-format-list-bulleted"></i></a> 
                                            @endif
                                          </span>
                                      </div>
                                    </form>
                                </div>
                              </div>
                          <br>
                      
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered border-primary">
                                <thead>
                                    <tr>
                                        <th>SL</th>
                                        <th>Supplier Name</th>
                                        <th>Email</th>
                                        <th>Contact No.</th>
                                        <th class="address">Full Address</th>
                                        <th class="text-center">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse($addresses as $key => $add)
                                    <tr>
                                        <td>{{(($addresses->currentPage() - 1) * $addresses->perPage() + $key+1)}}</td>

                                        <td>{{$add->supplier_name}}</td>
                                        <td>{{$add->address->email}}</td>
                                        <td>{{$add->address->phone}}</td>
                                        <td class="address">{{str_limit($add->address->full_address,20)}}</td>
                                        <td>
                                        <span class="input-group-append">
                                            {{-- <form action="{{route('suppliers.destroy',$add->id)}}" method="post">  --}}
                                            <a href="{{route('suppliers.show',$add->id)}}" class="btn btn-success btn-sm" title="Account Status">
                                                <i class="mdi mdi-book-open"></i>
                                            </a>
                                            <a href="{{route('show_invoice',$add->id)}}" class="btn btn-info btn-sm" title="Invoice Status"> <i class="mdi mdi-newspaper"></i>
                                            </a>
                                            <a href="{{route('suppliers.edit',$add->id)}}" class="btn btn-dark btn-sm" title="Edit Information">
                                                <i class="mdi mdi-table-edit"></i>
                                            </a>
                                                {{-- @csrf --}}
                                                {{-- @method('delete') --}}
                                                {{-- <button type="submit" onclick="return confirm('Are you sure to delete this??')" --}}
                                                    {{-- class="btn btn-danger btn-sm"><i class="mdi mdi-delete"></i></button> --}}
                                                <button type="button" class="btn btn-primary btn-sm"  data-toggle="modal" data-target="#myModal" onclick='addId({{$add->id}})' title="Add Previous Supplier Amount"><i class="mdi mdi-bank"></i></button>
                                            {{-- </form> --}}
                                        </span>
                                        </td>
                                    </tr>
                                    @empty
                                    <tr>
                                        <td colspan="8">There is no records available</td>
                                    </tr>
                                    @endforelse
                                </tbody>
                            </table>
                            <span class="float-right">{{$addresses->appends(request()->query())->links()}}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
{{-- model for update or increase amount --}}
<div class="modal fade" id="myModal">
    <div class="modal-dialog">
		<div class="modal-content">
      
        <!-- Modal Header -->
				<div class="col-12">
                  <div class="card">
                    <div class="card-body">
                      <h4 class="card-title">Add Beginning Balance<button type="button" class="close" data-dismiss="modal">&times;</button></h4>
                      <p class="card-description">
                      </p>
                      <form class="forms-sample" method="POST" action="{{url('supplier_beginning_balance')}}">
                      @csrf
                      @if(Gate::allows('isSuperadmin'))
                      <div class="form-group">
                        <label for="branch_id">Branch</label>
                        <select required class="form-control" id="branch_id" name="branch_id">
                            <option value="">Select branch</option>
                            @forelse($branches as $bp)
                            <option value="{{$bp->id}}">{{$bp->name}}</option>
                            @empty
                            <option>{{'There is no branch'}}</option>
                            @endforelse
                        </select>
                    </div>
                    @else
                    <input type="hidden" name="branch_id" id="branch_id" value="{{ Auth::user()->branch_id }}">
                    @endif
        
                    
                    <input type="hidden" name="supplier_id" id="supplier_id" value="">
						
                        
                        <div class="form-group">
                            <label for="paid_amount">Amount</label>
                            <input type="number" class="form-control" name="paid_amount" id="paid_amount" value="{{ old('paid_amount') }}" step=".01">
                              @if ($errors->any())
                                      @foreach($errors->all() as $error)
                                      <span class="alert text-danger">**{{ $error }}
                                      </span>
                                      @endforeach
                                  @endif
                          </div>
						  <button type="submit" class="btn btn-success mr-2">Submit</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                      </form>
                    </div>
						
                  </div>
                </div>
        
        
		</div>
    </div>
  </div>
{{-- model for update or increase amount --}}
@endsection
@push('js')
        <script>
        function addId(id){
            $('#supplier_id').val(id);
        }
        </script>    
@endpush
