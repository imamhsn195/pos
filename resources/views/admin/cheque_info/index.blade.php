@extends('layouts.admin')

@section('content')

<div class="row">
<div class="col-md-12 d-flex align-items-stretch grid-margin">
  <div class="row flex-grow row_table_responsive">
	<div class="col-md-12">
	  <div class="card">
		<div class="card-body">
		
      <h2 class="text-center">Cheque Information</h2>
      @if(Auth::user()->user_type=="superadmin")
      <h4 class="text-center">Showing All Branches</h4>
      @else
      <h4 class="text-center">Showing {{Auth::user()->branch->name}} Branch</h4>
      @endif
      <form method="get" action="{{route('cheque_infos.index')}}">
        <div class="input-group col-md-4 float-right">
            <input class="form-control py-2" placeholder="Search here.." name="searchby" type="text" id="example-search-input">
            <span class="input-group-append">
              <button class="btn btn-primary" type="submit">
                  <i class="mdi mdi-magnify"></i>
              </button>
            </span>
           
      </div>
      </form>
		 
		  <div class="table-responsive">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>ID</th>
                          <th>Created at</th>
                          <th>Cheque Sl.</th>
                          <th>RelatedParty</th>
						              <th>Disposal Date</th>
                          <th>Status</th>
                          <th>Amount</th>
                          @if(Auth::user()->user_type=="superadmin")
                          <th>Branch</th>
                          @endif
                        </tr>
                      </thead>
                      <tbody>
					  
						
                        <?php $i =1; ?>
					  @forelse($cheque_index as $cheque)
                        <tr>
                          <td>{{$i++}}</td>  
                          <td>{{date('d-M-Y',strtotime($cheque->created_at))}}</td>
                          <td>{{str_limit($cheque->cheque_sl_no,25)}}</td>
                          <td title="{{$cheque->related_party_type}}">
                          @if($cheque->related_party_type == 'supplier')
                          {{$cheque->supplier->supplier_name}}
                          @else
                          {{$cheque->customer->customer_name}}
                          @endif

                          </td>
                          <td>{{$cheque->cheque_disposal_date}}</td>
                        <td>
                          
                            @if($cheque->cheque_disposal_date && $cheque->status==1)
                              @if(Gate::allows('isSuperadmin'))
                                <span class="badge badge-warning">Pending</span>
                              @else
                                <form action="{{route('transactions.update',$cheque->id)}}" method="post">
                                    @csrf
                                    @method('put')
                                    <input type="hidden" name="status" value="{{$cheque->status}}">
                                    <button title="Change Status" type="submit" onclick="return confirm('Are you sure to make a Pending Transaction Complete??')" class="badge badge-warning" style="cursor:pointer">
                                        <span>Pending</span>
                                    </button>
                                </form>
                              @endif
						              @elseif($cheque->related_party_type == 'supplier')
                          <span class="badge badge-danger">Paid</span>
                          @else
                        <span class="badge badge-success">Received</span>
                        @endif
                          </td>
                          
                          <td>{{$cheque->amount}}</td>
                          @if(Auth::user()->user_type=="superadmin")
                          <td>{{$cheque->branch->name}}</td>
                          @endif
                        </tr>
					@empty
						<tr>
							<td colspan="8">There is no records available</td>
						</tr>
					@endforelse
                      </tbody>
                    </table>
                  <span>{{$cheque_index->links()}}</span>
                  </div>
		</div>
	  </div>
	</div>
  </div>
</div>

</div>

@endsection