@extends('layouts.admin')

@section('content')

<div class="row">
<div class="col-md-12 d-flex align-items-stretch grid-margin">
  <div class="row flex-grow row_table_responsive">
	<div class="col-md-12">
	  <div class="card">
		<div class="card-body">
        <h2 class="text-center">All Attributes With Values</h2>
        <div class="row">
            {{-- <div class="col-md-8"></div> --}}
            <div class="col-md-12">
                <form action="{{route('attributes.index')}}" method="GET">
                  <div class="input-group float-right">
                      <input class="form-control py-2 border-primary" placeholder="Search here.." name="searchby" type="text" id="example-search-input" required="">
                      <span class="input-group-append">
                        <button class="btn btn-primary" type="submit">
                            <i class="mdi mdi-magnify"></i>
                        </button>
                        <button type="button" class="btn btn-primary" title="Register New Attribute" onclick="createAttr()" data-toggle="modal" data-target="#myModal"><i class="mdi mdi-playlist-plus"></i></button>  
                        @if (request()->query())
                        <a class="btn btn-primary" title="Show All Attribute" href="{{route('attributes.index')}}"><i class="mdi mdi-format-list-bulleted"></i></a> 
                        @endif

                      </span>
                  </div>
                </form>
            </div>
          </div>
      <br>
    
		  <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>ID</th>
                          <span class="text-danger"><small>*Double Click Item To Edit</small></span>
                          <th>Attribute Name</th>
                          <th>values</th>
                          <th>Created By</th>
                          <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
					  @forelse($attributes as $key=>$cat)
                        <tr>
                          <td>{{(($attributes->currentPage() - 1) * $attributes->perPage() + $key+1)}}</td>

                          <td><span title="DoubleClick to Edit" data-toggle="modal" onclick="hello({{$cat->id}})" data-target="#myModal">{{$cat->attribute_name}}</span></td>
                        <td style="text-align:left;max-width=100px;overflow:scroll;">
							<?php $att_val="";
						foreach($cat->attributeVal as $sattr){
								$att_val.="<span class='badge badge-info' title='DoubleClick to Edit' data-toggle=modal onclick=hello($sattr->id,1) data-target=#myModal>".$sattr->attribute_name."</span> | ";
							}
						$ttt=rtrim($att_val, ' | ').".";
						
						  echo $ttt;
						?>  </td>
              <td title="{{$cat->users->name}}">{{$cat->users->user_type}}</td>
            
                          
							<td><form action="{{route('attributes.destroy',$cat->id)}}" method="post">
							@csrf
							@method('delete')
								<button type="submit" onclick="return confirm('Are you sure to delete this??')" class="btn btn-danger btn-sm"><i class="mdi mdi-delete"></i></button>
							</form>
							
                          </td>
                        </tr>
					@empty
						<tr>
							<td colspan="8">There is no records available</td>
						</tr>
					@endforelse
                      </tbody>
                    </table>
                    <span class="float-right">{{$attributes->appends(request()->query())->links()}}</span>
                  </div>
		</div>
	  </div>
	</div>
  </div>
</div>

</div>
<!-- The Modal -->
  <div class="modal fade" id="myModal">
      <div class="modal-dialog">
          <div class="modal-content">

              <!-- Modal Header -->
              <div class="col-12">
                  <div class="card">
                      <div class="card-body">
                          <h4 class="card-title">Attribute Form<button type="button" class="close"
                                  data-dismiss="modal">&times;</button></h4>
                          <p class="card-description">
                          </p>
                          <form class="forms-sample" id="attId" method="POST" action="{{route('attributes.create')}}">
                              @csrf
                              <input type="hidden" id="mtd" name="_method" value="put">

                              <div class="form-group">
                                  <label for="attribute_name">Attribute</label>
                                  <input type="text" class="form-control" name="attribute_name" id="attribute_name"
                                      value="{{ old('attribute_name') }}" placeholder="Enter attribute Name">
                                  @if ($errors->any())
                                  @foreach($errors->all() as $error)
                                  <span class="alert text-danger">**{{ $error }}
                                  </span>
                                  @endforeach
                                  @endif
                              </div>

                              <div class="form-group" id="idshow">
                                  <label for="attribute_values">Attribute Values</label>
                                  <p id="attribute_values_previous"></p>
                                  <textarea class="form-control" name="attribute_values" id="attribute_values"
                                      value="{{ old('attribute_values') }}"
                                      placeholder="Enter attribute Value with ,(comma) separated"></textarea>
                                  <small>Use<i class="alert text-danger">" ',' (comma)"</i> to add multiple
                                      values</small>
                              </div>
                              @if ($errors->any())
                              @foreach($errors->all() as $error)
                              <span class="alert text-danger">**{{ $error }}
                              </span>
                              @endforeach
                              @endif
                              <button type="submit" class="btn btn-success mr-2">Submit</button>
                              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                          </form>
                      </div>

                  </div>
              </div>


          </div>
      </div>
  </div>
  <script>
  function hello(id,attVal=0){
	  var attID = id;
        if(attID){
            $.ajax({
                type:'GET',
                url:'attributes/'+attID+'/edit',
                data:'attID='+attID,
                success:function(html){
                  //console.log(html.attribute_val[0].attribute_name);
					    attVal ? $("#idshow").hide() : $("#idshow").show();
                    $('#attribute_name').attr('value',html.attribute_name);
                    var attrValtd="";
                    if(html.attribute_val.length>0){
                      for(a=0; a<html.attribute_val.length; a++){
                        attrValtd += html.attribute_val[a].attribute_name +" | ";
                      }

                    }
                    //console.log(attrValtd);
                   
                    $('#attribute_values_previous').text(attrValtd);
          var route="<?=url()->current()?>"+"/"+id;
                    $('#attId').attr('action',route);
                   if(!$('#mtd').length ){
					            $('#attId').prepend('<input type="hidden" id="mtd" name="_method" value="put">');
					        } 
                     
                }
            }); 
        }else{
            }
  }
  function createAttr(){
	  var route="attributes"
	  $("#mtd").remove();
	  $("#attribute_name").val("");
	  $("#attribute_values_previous").text("");
	  $('#attId').attr('action',route);
	  $("#idshow").show();
	  
  }
  
  </script>

@endsection