@extends('layouts.admin')

@section('content')

<div class="row">
    <div class="col-md-6 d-flex align-items-stretch grid-margin">
        <div class="row flex-grow">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Attribute Form</h4>
                        <p class="card-description">
                        </p>
                        <form class="forms-sample" method="post" action="{{route('attributes.store')}}">
                            @csrf
                            <div class="form-group">
                                <label for="attribute_name">Attribute</label>
                                <input type="text" class="form-control" name="attribute_name" id="attribute_name" value="{{ old('attribute_name') }}"
                                    placeholder="Enter attribute Name">
                                @if ($errors->any())
                                @foreach($errors->all() as $error)
                                <span class="alert text-danger">**{{ $error }}
                                </span>
                                @endforeach
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="attribute_values">Attribute Values</label>
                                <textarea class="form-control" name="attribute_values" id="attribute_values" value="{{ old('attribute_values') }}"
                                    placeholder="Enter attribute Value with ,(comma) separated"></textarea>
                                <small>Use<i class="alert text-danger">" ',' (comma)"</i> to add multiple values</small>
                            </div>
                            @if ($errors->any())
                            @foreach($errors->all() as $error)
                            <span class="alert text-danger">**{{ $error }}
                            </span>
                            @endforeach
                            @endif
                            <button type="submit" class="btn btn-success mr-2">Submit</button>
                            <a class="btn btn-light" href="{{route('attributes.index')}}">Cancel</a>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>

</div>

@endsection
