@extends('layouts.admin')

@section('content')

<div class="row">
            <div class="col-md-6 d-flex align-items-stretch grid-margin">
              <div class="row flex-grow">
                <div class="col-12">
                  <div class="card">
                    <div class="card-body">
                      <h4 class="card-title">Warranty Type form</h4>
                      <p class="card-description">
                        Warranty Type Name6520056
                      </p>
					 
                      <form class="forms-sample" method="post" action="{{route('categories.update',$editid)}}">
					  @csrf
					  @method('put')
                        <div class="form-group">
                          <label for="category_name">category</label>
                          <input type="text" class="form-control" name="category_name" id="category_name" value=" {{$ecat->category_name}}" placeholder="Enter category Name">
                        </div> 
						 <div class="form-group">
                         
                          <input type="checkbox" class="form-control" value="1" name="is_expirable" id="is_expirable" {{$ecat->is_expirable?'checked':''}}>
						   <label for="is_expirable">Is Expirable???</label>
                        </div> 
						<div class="form-group">
                          <label for="category_parent_id">Root Category</label>
                          <select  class="form-control" id="category_parent_id" name="parent_id">
							<option value="">Select Root Category</option>
							@forelse($categories as $cp)
								<option {{$ecat->parent_id==$cp->id ?'selected':''}} value="{{$cp->id}}">{{$cp->category_name}}</option>
							@empty
								<option>{{'There is no category'}}</option>
							@endforelse
						  </select>
                        </div>
                        <div class="form-group">
                          <label for="parent_warranty_type">Warranty Type</label>
                          <select  class="form-control" id="parent_warranty_type" name="warranty_type_id">
							<option value="">Select Root Warranty</option>
							@forelse($warranty_type as $wp)
								<option {{$ecat->parent_id==$wp->id ?'selected':''}} value="{{$wp->id}}">{{$wp->warranty_name}}</option>
							@empty
								<option>{{'There is no warranty type'}}</option>
							@endforelse
						  </select>
                        </div>
                        <button type="submit" class="btn btn-success mr-2">Submit</button>
                        <button class="btn btn-light">Cancel</button>
                      </form> 
					
					  
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
          </div>

@endsection