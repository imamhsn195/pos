@extends('layouts.admin')

@section('content')

<div class="row">
  <div class="col-md-6 d-flex align-items-stretch grid-margin">
    <div class="row flex-grow">
      <div class="col-12">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">Category edit form</h4>
            <form class="forms-sample" method="post" action="{{route('categories.update',$category->id)}}">
             @csrf
             @method('put')
             <div class="form-group">
              <label for="category_name">Category</label>
              <input type="text" class="form-control" name="category_name" id="category_name" value=" {{$category->category_name}}" placeholder="Enter category Name">
            </div> 
            <div class="form-group">
              <div class="form-check form-check-flat">
                <label class="form-check-label">
                      <input type="checkbox" class="form-check-input" value="1" name="is_expirable" id="is_expirable"  {{$category->is_expirable?'checked':''}}>Is Expirable?
                    <i class="input-helper"></i></label>
              </div>
            </div>
            <div class="form-group">
              <div class="form-check form-check-flat">
                <label class="form-check-label">
                      <input type="checkbox" class="form-check-input" value="1" name="is_warrantable" id="is_warrantable"  {{$category->is_warrantable?'checked':''}}>Is Warrantiable?
                    <i class="input-helper"></i></label>
              </div>
            </div>
          <div class="form-group">
              <label for="category_parent_id">Root Category</label>
              <select  class="form-control" id="category_parent_id" name="parent_id">
               <option value="">Root Category</option>
               @forelse($categories as $cp)
               <option {{$category->parent_id == $cp->id ?"selected":""}} value="{{$cp->id}}">{{$cp->category_name}}</option>
               @empty
               <option>{{'There is no category'}}</option>
               @endforelse
             </select>
           </div>

         <button type="submit" class="btn btn-success mr-2">Submit</button>
         <a href="{{route('categories.index')}}" class="btn btn-danger">Cancel</a>
       </form> 
     </div>
   </div>
 </div>
</div>
</div>
</div>

@endsection
