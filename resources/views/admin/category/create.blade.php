@extends('layouts.admin') 
@section('content')

<div class="row">
  <div class="col-md-6 d-flex align-items-stretch grid-margin">
    <div class="row flex-grow">
      <div class="col-12">
        <div class="card">
          <div class="card-body">
          <form action="{{route('categories.store')}}" method="post">
            @csrf
            <div class="form-group">
              <label for="category_parent_id">Category Name</label>
               <input type="text" class="form-control" name="category_name" id="category_name" placeholder="Enter category Name">
            </div>
          <div class="form-group">
            <div class="form-check form-check-flat">
              <label class="form-check-label">
                    <input type="checkbox" class="form-check-input" value="1" name="is_expirable" id="is_expirable">Is Expirable?
                  <i class="input-helper"></i></label>
            </div>
          </div>
          <div class="form-group">
            <div class="form-check form-check-flat">
              <label class="form-check-label">
                    <input type="checkbox" class="form-check-input" value="1" name="is_warrantable" id="is_warrantable">Is Warrantiable?
                  <i class="input-helper"></i></label>
            </div>
          </div>
          <div class="form-group">
            <label for="category_parent_id">Root Category</label>
              <select class="form-control" id="category_parent_id" name="parent_id">
                 <option value="">Select Root Category</option>
                 @forelse($categories as $cp)
                 <option value="{{$cp->id}}">{{$cp->category_name}}</option>
                 @empty
                 <option value="">{{'There is no category'}}</option>
                 @endforelse
               </select>
          </div>
          <button type="submit" class="btn btn-success mr-2">Submit</button>
          <a type="submit" href="{{route('categories.index')}}" class="btn btn-danger">Cancel</a>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
</div>

@endsection