@extends('layouts.admin')
@push('css')

@section('content')

<div class="row">
  <div class="col-md-12 d-flex align-items-stretch grid-margin">
    <div class="row flex-grow row_table_responsive">
      <div class="col-md-12">
        <div class="card">
          <div class="card-body">
              <h2 class="text-center">All Categories</h2>
          <div class="row">
          {{-- <div class="col-md-8"></div> --}}
          <div class="col-md-12">
              <form action="{{route('categories.index')}}" method="GET">
                <div class="input-group float-right">
                    <input class="form-control py-2 border-primary" placeholder="Search here.." name="searchby" type="text" id="example-search-input" required>
                    <span class="input-group-append">
                      <button class="btn btn-primary" type="submit">
                          <i class="mdi mdi-magnify"></i>
                      </button>
                      <button type="button" id="createCategoryBtn" class="btn btn-primary"><i class="mdi mdi-playlist-plus"></i></button>
                        @if (request()->query())
                          <a class="btn btn-primary" href="{{route('categories.index')}}"><i class="mdi mdi-format-list-bulleted"></i></a>
                        @endif
                      {{--  <a class="btn btn-primary" href="{{route('categories.create')}}">+</a>  --}}
                    </span>
                </div>
              </form>
          </div>
          </div>
          <br>
          <div class="table-responsive">
          <table id="createCategoriesTable" class="table table-striped table-bordered border-primary" style="width:100%; display:none" >
              <thead>
                <tr>
                  <th colspan="2">Create New Category</th>
                  <th>Expirable ?</th>
                  <th>Warrantiable ?</th>
                  <th>Created By</th>
                  <th>Status ?</th>
                  <th class="text-center">Action</th>
                </tr>
              </thead>
              <tbody>
                  <form action="{{route('categories.store')}}" method="post">
                      @csrf
                  <tr>
                      <td colspan="2">
                          <div class="input-group float-right">
                              <input class="form-control py-2 col-md-6 border-primary" placeholder="Category Name.." name="category_name" type="text" id="category_name" required>
                              <select class="form-control col-md-6 border-primary" id="category_parent_id" name="parent_id">
                                  <option value="">Select Root Category</option>
                                  @forelse($categories as $cp)
                                  <option value="{{$cp->id}}">{{$cp->category_name}}</option>
                                  @empty
                                  <option value="">{{'There is no category'}}</option>
                                  @endforelse
                              </select>
                          </div>
                      </td>
                      <td>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="is_expirable" id="is_expirable" value="1">
                            <label class="custom-control-label" for="is_expirable">Yes</label>
                          </div>
                      </td>
                      <td>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="is_warrantable" id="is_warrantable" value="1">
                            <label class="custom-control-label" for="is_warrantable">Yes</label>
                          </div>
                      </td>
                      <td>{{Auth::User()->name}}</td>
                      <td>
                          <div class="custom-control custom-checkbox">
                              <input type="checkbox" class="custom-control-input" id="is_status_active">
                              <label class="custom-control-label" for="is_status_active">Active</label>
                            </div>
                      
                      <td class="text-center">
                          <button href="" class="btn btn-dark btn-sm">
                              <i class="mdi mdi-library-plus"></i>
                          </button>                     
                          <a type="submit" href="{{route('categories.index')}}" class="btn btn-danger btn-sm text-white">
                           <i class="mdi mdi-cancel"></i>
                          </a>
                      </td>
                 </tr>
                </form>
                </tbody>
            </table>
                <table id="categories" class="table table-striped table-bordered border-primary" style="width:100%" >
                  <thead>
                      <tr>
                        <th>ID</th>
                        <th>Category Name</th>
                        <th>Expirable</th>
                        <th>Warrantiable</th>
                        <th>Created By</th>
                        <th class="text-center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
               <?php $i =1; ?>
               @forelse($all_cat_users as $key => $cat)
               <tr>
                <td>{{(($all_cat_users->currentPage() - 1) * $all_cat_users->perPage() + $key+1)}}</td>
                <td>{{$cat->category_name}}</td>
                <td><label class="badge badge-{{$cat->is_expirable?'success':'danger'}}">{{$cat->is_expirable?'Yes':'Not'}}</label></td>
                <td><label class="badge badge-{{$cat->is_warrantable?'success':'danger'}}">{{$cat->is_warrantable?'Yes':'Not'}}</label></td>
                <td>{{$cat->user->user_type}}</td>
                <td>
                  <span class="input-group-append">
                  <form action="{{route('categories.update',$cat->id)}}" method="post" style="float:left; padding-right:2px">
                    @method('put')
                    @csrf
                    <input type="hidden"  name="status" value="{{$cat->status}}">
                    <button title="Click to {{$cat->status?'Inactive':'Active'}}" type="submit" class="btn btn-{{$cat->status?'success':'danger'}} btn-sm" onclick="return confirm('Are you sure to {{$cat->status?'Inctive':'Active'}}')">{!!$cat->status?'<i class="mdi mdi-thumb-up"></i>':'<i class="mdi mdi-thumb-down"></i>'!!}</button>
                  </form>
                  <form action="{{route('categories.destroy',$cat->id)}}" method="post">
                    <a href="{{route('categories.edit',$cat->id)}}" class="btn btn-dark btn-sm" title="Edit">
                        <i class="mdi mdi-table-edit"></i>  {{--  <i class="mdi mdi-table-edit"></i>  --}}
                      </a>
                     {{--  @csrf
                     @method('delete')
                     <a type="submit" value="Delete" onclick="return confirm('Are you sure to delete this??')" class="btn btn-danger btn-sm text-white">
                     <i class="mdi mdi-thumb-down-outline"></i></a>  --}}
                  </form>
                  </span>
                </td>
           </tr>
           @empty
           <tr>
             <td colspan="8">There is no records available</td>
           </tr>
           @endforelse
         </tbody>
       </table>
       <span class="float-right">{{$all_cat_users->appends(request()->query())->links()}}</span>
     </div>
   </div>
 </div>
</div>
</div>
</div>

</div>

@endsection
@push('js')
    <script>
      $('#createCategoryBtn').click(function(){
        $('#createCategoriesTable').toggle(800);
      });
    </script>
@endpush