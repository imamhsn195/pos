@extends('layouts.admin')

@section('title','Brand')

@push('css')

@endpush()

@section('content')

<div class="row">
    <div class="col-md-12 d-flex align-items-stretch grid-margin">
        <div class="row flex-grow row_table_responsive">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                            <h2 class="text-center">Brand Information</h2>
                            <div class="row">
                                {{-- <div class="col-md-6"></div> --}}
                                <div class="col-md-12">
                                    <form action="{{route('brand.index')}}" method="GET">
                                      <div class="input-group float-right">
                                          <input class="form-control py-2 border-primary" placeholder="Search here.." name="brand_name" type="text" id="example-search-input" required="">
                                          <span class="input-group-append">
                                            <button class="btn btn-primary" type="submit">
                                                <i class="mdi mdi-magnify"></i>
                                            </button>
                                            <button class="btn btn-primary" title="Register New Brand" data-toggle="modal" data-target="#myBrand" onclick="addBrand()"><i class="mdi mdi-playlist-plus"></i></button>  
                                            @if (request()->query())
                                            <a class="btn btn-primary" title="Show All Product" href="{{route('brand.index')}}"><i class="mdi mdi-format-list-bulleted"></i></a> 
                                            @endif
                                          </span>
                                      </div>
                                    </form>
                                </div>
                              </div>
                          <br>

                        <div class="table-responsive">
                            <table class="table table-striped table-bordered border-primary">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <span class="text-danger"><small>*Double click on Brand to edit</small></span>
                                        <th>Brand</th>
                                        <th>Sub-Brand</th>
                                        <th>Status</th>
                                        <th>Created By</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse($parent_brands as $key => $brand)
                                    <tr>
                                        <td>{{(($parent_brands->currentPage() - 1) * $parent_brands->perPage() + $key+1)}}</td>

                                        <td><span style="cursor: pointer;" data-toggle="modal" data-target="#myBrand" onclick="brandEdit({{$brand->id}})">{{$brand->brand_name}}</span>
                                        </td>

                                        <td>
                                            <?php $brnd = null;  ?>
                                            @forelse($brand->child_brands as $cb)
                                            <?php $brnd .="<span  style='cursor: pointer;' class='badge badge-info' data-toggle='modal'  data-target='#myBrand' onclick=brandEdit($cb->id,1)>".$cb->brand_name."</span> | ";  ?>

                                            @empty
                                            {{"There is No Sub Brand"}}

                                            @endforelse
                                            <?php $bname=rtrim($brnd, " | ");?>
                                            {!! $bname !!}

                                        </td>
                                        <td>
                                            <form action="{{route('brand.update',$brand->id)}}" method="post">
                                                @csrf
                                                @method('put')
                                                <input type="hidden" name="status" value="{{$brand->status}}">
                                                <button type="submit" onclick="return confirm('Are you sure to change status??')"
                                                    class="btn btn-link">
                                                    @if($brand->status == 1)
                                                    <span class="badge badge-success">Active</span>
                                                    @else
                                                    <span class="badge badge-danger">Inactive</span>
                                                    @endif
                                                </button>
                                            </form>
                                        </td>
                                        <td>{{$brand->user->user_type}}</td>


                                    </tr>
                                    @empty
                                    <tr>
                                        <td colspan="8">There is no records available</td>
                                    </tr>
                                    @endforelse
                                </tbody>
                            </table>
                            <span class="float-right">{{$parent_brands->links()}}</span>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myBrand">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Add New Brand</h4>
                        <form id="brndId" class="forms-sample" method="post" action="">
                            <input type="hidden" id="putmethod" name="_method" value="put">
                            @csrf

                            <div class="form-group">
                                <label for="brand_name">Brand Name</label>
                                <input type="text" class="form-control" name="brand_name" id="brand_name" placeholder="Enter Brand Name">
                            </div>
                            <div class="form-group" id="idshow">
                                <label for="brand_parent_id">Sub Brand (optional)</label>
                                <p id="subBrandData"></p>
                                <input type="text" class="form-control" name="brand_child" id="brand_parent_id"
                                    placeholder="Enter Brand Name">
                                <small><strong class="text-danger">**use ',' </strong>for addding multiple brands</small>
                            </div>
                            <button type="submit" class="btn btn-success mr-2">Submit</button>
                            <button type="reset" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                        </form>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>
<script>

// brandEdit for editing brand
    function brandEdit(id, attVal = 0) {
        var brandID= id;

        if (brandID) {
            $.ajax({
                type: 'GET',
                url: 'brand/' + brandID + '/edit',
                data: 'brandID=' + brandID,
                success: function (html) {
                    //check attVal is true or false to hide a div when edit sub brand
                    attVal ? $("#idshow").hide() : $("#idshow").show();

                    $('#brand_name').attr('value', html.brand_name);
                    var attrValtd="";
                    if(html.child_brands.length>0){
                      for(a=0; a<html.child_brands.length; a++){
                        attrValtd += html.child_brands[a].brand_name +" | ";
                      }

                    }

                    $('#subBrandData').text(attrValtd);
                    
                    //update route store in var route and set it to form action
                   var route = "<?=url()->current()?>"+"/"+id;
                   
                    $('#brndId').attr('action', route);

                    //check hidden field for put method
                    if (!$('#putmethod').length) {
                        $('#brndId').prepend(
                            '<input type="hidden" id="putmethod" name="_method" value="put">');
                    }
                }
            });
        } else {
        }
    }

//addBrand for adding new brand
    function addBrand() {
        //store route store in var route
        var route = "brand";


        //remove put method when store brand
        $("#idshow").show();
        $('#putmethod').remove();
        $('#brndId').attr('action', route);
        $('#brand_name').attr('value', "");
    }
</script>


@endsection