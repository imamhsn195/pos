@extends('layouts.admin')

@section('content')


<div class="row">
    <div class="col-md-6 d-flex align-items-stretch grid-margin">
        <div class="row flex-grow">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Brand Information</h4>
                        <form class="forms-sample" method="post" action="{{route('brand.store')}}">
                            @csrf
                            <div class="form-group">
                                <label for="brand_name">Brand Name</label>
                                <input type="text" value="{{$brand->brand_name}}" class="form-control" name="brand_name"
                                    id="brand_name" placeholder="Enter Brand Name">
                            </div>
                            <div class="form-group">
                                <label for="brand_parent_id">Root Brand</label>
                                <select class="form-control" id="brand_parent_id" name="parent_id">
                                    <option value="">Select Root Brand</option>
                                    @forelse($brands as $brand)
                                    <option value="{{$brand->id}}">{{$brand->brand_name}}</option>
                                    @empty
                                    <option>{{'There is no brand'}}</option>
                                    @endforelse
                                </select>
                            </div>
                            <button type="submit" class="btn btn-success mr-2">Submit</button>
                            <a href="{{route('brand.index')}}" class="btn btn-primary mr-2">Back</a>
                            <button type="reset" class="btn btn-light">Cancel</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>



@endsection