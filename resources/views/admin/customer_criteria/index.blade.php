@extends('layouts.admin')

@section('content')

<div class="row">
    <div class="col-md-12 d-flex align-items-stretch grid-margin">
        <div class="row flex-grow row_table_responsive">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="text-center">Criteria Data</h4>
                        <a class="btn btn-info pull-right" href="{{route('customer_criterias.create')}}">New Offer</a>

                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Title</th>
                                        <th>Type</th>
                                        <th>Value</th>
                                        <th>Condition</th>
                                        <th>Status</th>
                                        <th colspan="2">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i =1; ?>
                                    @forelse($criteria_index as $criteria)
                                    <tr>
                                        <td>{{$i++}}</td>
                                        <td>{{$criteria->title}}</td>
                                        <td>@if($criteria->criteria_type==1)
                                            Membership Duration
                                            @elseif($criteria->criteria_type==2)
                                            Age
                                            @else
                                            Total Purchase Amount
                                            @endif
                                        </td>
                                        <td>{{$criteria->criteria_value}}</td>
                                        <td>@if($criteria->condition==0)
                                            Greater than criteria value
                                            @else
                                            Less than criteria value
                                            @endif
                                        </td>


                                        <td>
                                            
                                            {{--active ,inactive--}}

                                            <form action="{{route('change_criteria_status',$criteria->id)}}" method="post">
                                                @csrf
                                                <input type="hidden" name="status" value="{{$criteria->status}}">
                                                <button type="submit" onclick="return confirm('Are you sure to change Status??')"
                                                    class="btn btn-link">
                                                    @if($criteria->status == 1)
                                                    <span class="badge badge-success">Active</span>
                                                    @else
                                                    <span class="badge badge-danger">Inactive</span>
                                                    @endif
                                                </button>
                                            </form>
                                        </td>

                                        <td>


                                            <form action="{{route('customer_criterias.destroy',$criteria->id)}}" method="post">
                                                @csrf
                                                @method('delete')
                                                <input type="submit" value="Delete" onclick="return confirm('Are you sure to delete this??')"
                                                    class="btn btn-link" />
                                            </form>

                                        </td>
                                    </tr>
                                    @empty
                                    <tr>
                                        <td colspan="8">There is no records available</td>
                                    </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection
