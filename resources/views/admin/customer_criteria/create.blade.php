@extends('layouts.admin')

@section('content')

<div class="row">
    <div class="col-md-6 d-flex align-items-stretch grid-margin">
        <div class="row flex-grow">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title" align="center">Customer Criteria Create Form</h4>
                        <br>
                        <form class="forms-sample" method="post" action="{{route('customer_criterias.store')}}">
                            @csrf
                            <div class="form-group">
                                <label for="customer_criteria_title">Customer Criteria Name</label>
                                <input type="text" class="form-control" name="customer_criteria_title" id="customer_criteria_title"
                                    placeholder="Enter Criteria Name">
                            </div>

                            <br>

                            <!--
                                get_type() has been used in "script" below 
                            -->

                            <div class="form-group">
                                <label for="criteria_type">Criteria Type</label>
                                <select name="criteria_type" id="criteria_type" class="form-control" onchange="get_type()">
                                    <option value="">Select Criteria Type</option>
                                    <option value="1">Membership Duration</option>
                                    <option value="2">Age</option>
                                    <option value="3">Total Purchase Amount</option>
                                </select>
                            </div>

                            <br>

                            <div class="form-group">
                                <label for="criteria_value">Criteria Value</label>

                                <input type="date" class="form-control" name="criteria_value" id="criteria_value">

                            </div>




                            <br>

                            <div class="form-group">
                                <label for="condition">Condition</label>
                                <select name="condition" class="form-control">
                                    <option value="">Select Condition Type</option>
                                    <option value="0">Greater than criteria value</option>
                                    <option value="1">Less than criteria value</option>
                                </select>
                            </div>
                            <br>

                            <button type="submit" class="btn btn-success mr-2">Submit</button>
                            <a href="{{route('customer_criterias.index')}}" class="btn btn-danger">Cancel</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<script>

    /**
    *for changing the input field type in create form
    */
    function get_type() {
        var a = $('#criteria_type').val();
        var crit_type = $('#criteria_value').attr('type');
        if (a == 3) {
            $('#criteria_value').attr('type', 'text');
        }
        if (a == 1) {
            $('#criteria_value').attr('type', 'date');
        }
        if (a == 2) {
            $('#criteria_value').attr('type', 'number');
        }

    }

</script>
@endsection
