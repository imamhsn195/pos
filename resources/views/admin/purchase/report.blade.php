@extends('layouts.admin')

@section('content')

<div class="row">
    <div class="col-md-12 d-flex align-items-stretch grid-margin">
        <div class="row flex-grow row_table_responsive">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h2 class="text-center">Purchase History</h2>
                        @if(Auth::user()->user_type=="superadmin")
                        <h4 class="text-center">Showing All Branches</h4>
                        @else
                        <h4 class="text-center">Showing {{Auth::user()->branch->name}} Branch</h4>
                        @endif
                        <p class="card-description text-center">
                            <form method="get" action="{{route('purchases.index')}}">
                                <div class="input-group col-md-6 float-right">
                                    <label for="example-search-input1">From :</label>
                                    <input class="form-control py-2" @if(isset($_GET['searchbyfrom']))
                                        value="{{$_GET['searchbyfrom']}}" @else value="{{date('Y-m-01')}}" @endif
                                        name="searchbyfrom" type="date" id="example-search-input1">
                                    <label for="example-search-input2">To :</label>
                                    <input class="form-control py-2" placeholder="To.." name="searchbyto" type="date"
                                        id="example-search-input2" @if(isset($_GET['searchbyto']))
                                        value="{{$_GET['searchbyto']}}" @else value="{{date('Y-m-t')}}" @endif>
                                    <span class="input-group-append">
                                        <button class="btn btn-primary" type="submit">
                                            <i class="mdi mdi-magnify"></i>
                                        </button>
                                    </span>

                                </div>
                            </form>
                        </p>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Invoice No:</th>
                                        <th>Total Qty</th>
                                        <th>SubTotal</th>
                                        <th>Supplier</th>
                                        <th>Purchased by</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        $i =1;
                                        $total=0;
                                    ?>
                                    @forelse ($purchases as $key => $purchase)
                                    <tr>
                                        <?php 
                                            $time=strtotime($purchase->first()->created_at);
                                            $date=date("d-M-Y",$time);
                                            $timm=date("h:i:s",$time);
                                        ?>
                                        <td title="{{$timm}}">{{$date}}</td>
                                        <td>
                                            <input type="button" value="{{$key}}"
                                                onclick="showInvoiceDetails(this.value)" data-toggle="modal"
                                                data-target="#exampleModalCenter">
                                        </td>
                                        <td>
                                            {{$purchase->sum('quantity')}}
                                        </td>
                                        <td>
                                            @php
                                            $totalPurchaseAmount=0;
                                                foreach($purchase as $purchaseData){
                                                    $totalPurchaseAmount=$totalPurchaseAmount+($purchaseData->quantity*$purchaseData->purchase_unit_price);
                                                }
                                            @endphp
                                            {{$totalPurchaseAmount}}
                                        </td>
                                        <td>{{$purchase->first()->supplier->supplier_name}}</td>
                                        <td title="{{$purchase->first()->users->name}}">{{$purchase[0]->users->user_type}}
                                        </td>
                                        <td>
                                                @php
                                                $showing=true;
                                                @endphp
                                            @foreach($purchase as $purchaseData)
                                            @if(count($purchaseData->sales_details)>0)
                                                @php
                                                    $showing=false;
                                                @endphp
                                                @endif
                                            @endforeach
                                                @if($showing)
                                            <a class="btn btn-info" href="{{route('show_invoice_details',$key)}}?iseditable=true"><i class="mdi mdi-pencil"></i></a>
                                                @else
                                                <button class="btn btn-info"  type="button" disabled="true"><i class="mdi mdi-pencil"></i></button>
                                                @endif
                                        </td>
                                    </tr>
                                    @empty
                                    <tr>
                                        <td colspan="8">There is no records available</td>
                                    </tr>
                                    @endforelse
                                </tbody>
                                <tfoot>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="exampleModalBody">
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Product Name</th>
                            <th>Qty</th>
                            <th>Purchase Price</th>
                            <th>Selling Price</th>
                        </tr>
                    </thead>
                    <tbody id="exampleModalTable">
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
 {{--Model-For-Edit-Invoice  --}}
 <div class="modal fade" id="editInvoiceModal" tabindex="-1" role="dialog" aria-labelledby="editInvoiceModalTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editModalTableTitle">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="exampleModalBody">
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Product Name</th>
                            <th>Qty</th>
                            <th>Purchase Price</th>
                            <th>Selling Price</th>
                        </tr>
                    </thead>
                    <tbody id="editModalTable">
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection
@push('js')
    <script>
      // show Invoice details
      function showInvoiceDetails(invoice_no){
        var route = "{{url('')}}/show_invoice_details/"+invoice_no;
        $('#exampleModalLongTitle').text("Invoice No: "+invoice_no);
        $.get(route,function(data){
          console.log(data);
          $('#exampleModalTable').empty();
          var tr = "<tr>";
          for(i=0;i<data.length;i++){
            tr += "<td>"+data[i].product.product_sku.split("|")[0]+"</td>";
            tr += "<td>"+data[i].quantity+"</td>";
            tr += "<td>"+data[i].purchase_unit_price+"</td>";
            tr += "<td>"+data[i].sell_unit_price+"</td>";
            tr += "</tr>";
          }
          $('#exampleModalTable').html(tr);
        });
      }

      // show Invoice Details For Edit
    //   function showInvoiceDetailsForEdit(invoice_no){
    //     var route = "{{url('')}}/show_invoice_details/"+invoice_no;
    //     var actionurl = "{{url('')}}/show_invoice_details/"+invoice_no;
    //     $('#editModalTableTitle').text("Invoice No: "+invoice_no);
    //     $.get(route,function(data){
    //       console.log(data);
    //       $('#editModalTable').empty();
    //       var tr = "<tr><form method='post' action='"+actionurl+"'>";
    //       for(i=0;i<data.length;i++){
    //         tr += "<td>"+data[i].product.product_sku.split("|")[0]+"</td>";
    //         tr += "<td><input type='number' size='3' name='new_qty[]' class='form-control-sm' value='"+data[i].quantity+"'><input type='hidden'  name='old_qty[]'  value='"+data[i].quantity+"'></td>";
    //         tr += "<td><input type='number' size='3' name='new_purchase_unit_price[]' class='form-control-sm' value='"+data[i].purchase_unit_price+"'><input type='hidden'  name='old_purchase_unit_price[]' value='"+data[i].purchase_unit_price+"'></td>";
    //         tr += "<td><input type='number' size='3' name='new_sell_unit_price[]' class='form-control-sm' value='"+data[i].sell_unit_price+"'><input type='hidden'  name='old_sell_unit_price[]' value='"+data[i].sell_unit_price+"'></td>";
    //         tr += "</form></tr>";
    //       }
    //       $('#editModalTable').html(tr);
    //     });
    //   }
    </script>
@endpush