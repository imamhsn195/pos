@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-md-12 d-flex align-items-stretch grid-margin">
        <div class="row flex-grow">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Purchase Product from Here</h4>
                        <form class="forms-sample" method="post" action="{{route('purchases.store')}}" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="product_id" id="product_id" value="{{$product->id}}">
                            <div class="form-group">
                                <label for="invoice_no">Invoice</label>
                            <input type="text" class="form-control" name="invoice_no" id="invoice_no" value="{{old('invoice_no')}}">
                            </div>
                            <div class="form-group">
                                    <label for="supplier_id">Supplier</label>
                                    <select class="form-control" id="supplier_id" name="supplier_id" onchange="invoiceCheck()">
                                        <option value="">Select Supplier</option>
                                        @forelse($suppliers as $sp)
                                        <option value="{{$sp->id}}">{{$sp->supplier_name}}</option>
                                        @empty
                                        <option>{{'There is no supplier'}}</option>
                                        @endforelse
                                    </select>
                                </div>
                                @if(Gate::allows('isSuperadmin'))
                                <div class="form-group">
                                    <label for="branch_id">Branch</label>
                                    <select required class="form-control" id="branch_id" name="branch_id">
                                        <option value="">Select branch</option>
                                        @forelse($branches as $bp)
                                        <option value="{{$bp->id}}">{{$bp->name}}</option>
                                        @empty
                                        <option>{{'There is no branch'}}</option>
                                        @endforelse
                                    </select>
                                </div>
                                @else 
                                <input type="hidden" name="branch_id" id="branch_id" value="{{ Auth::user()->branch_id }}">
                                @endif
                               
                            
                            <div class="form-group">
                                <label for="purchase_qty">Purchase Quantity</label>
                                <input type="number" required class="form-control" name="purchase_qty" id="purchase_qty"
                                    onblur="getProductInfo()" max="99" min="1">
                            </div>
                            <div class="form-group">
                                <label for="p_unit_price">Purchase per Unit Price</label>
                                <input type="number" class="form-control" required name="p_unit_price" id="p_unit_price"
                                    onkeyup="getprice()" step=".01">
                            </div>

                            <div class="form-group">
                                <label for="sales_unit_price">Sells per Unit Price</label>
                                <input type="number" required class="form-control" name="sales_unit_price" id="sales_unit_price"  step=".01">
                            </div>
                            <!--form group forexpireDate-->
                            <div id="expireDate" class="form-group">

                            </div>
                            <div class="form-group">
                                <label for="total_price">Amount to be paid</label>
                                <input type="number" class="form-control" name="total_price" id="total_price" value=""
                                    readonly>
                            </div>


                           
                            <div class="form-group">
                                <label for="payment_method_id">Payment Method</label>
                                <select required class="form-control" id="payment_method_id" name="payment_method_id"
                                    onchange="addpaidvalue()">
                                    <option value="">Select Payment Method</option>
                                    <option value="0 | 0" style="font-weight:bold"><b>Due</b></option>
                                    @foreach($payment_methods as $pm)
                                    {{--  balace option --}}
                                    @php
                                    $cashIn = 0; 
                                    $cashOut = 0;
                                    $payment_method =  $pm->method_type==1?10:2;
                                    @endphp
                                
                                @foreach ($pm->journals as $js)
                                    @if ($js->journal_type == 0 && $js->accounts_id == $payment_method)
                                       @php
                                           $cashIn = $cashIn + $js->amount;
                                       @endphp
                                    @endif
                                @endforeach
                               
                                @foreach ($pm->journals as $js)
                                    @if ($js->journal_type == 1 && $js->accounts_id == $payment_method)
                                       @php
                                           $cashOut = $cashOut + $js->amount;
                                       @endphp
                                    @endif
                                @endforeach
                               
                            </td>
                                    {{-- balace option --}}
                                    <option value="{{$pm->id.' | '.$pm->method_type }}" @if($pm->method_type==0 && ($cashIn-$cashOut)<=0) {{"disabled"}} class="text-danger" @endif>{{$pm->method_title}} &nbsp;&nbsp;&nbsp;&nbsp; balance:  {{$cashIn-$cashOut}}</option>
                                    @endforeach
                                </select>
                            </div>

                            {{-- payment method with balance --}}
                           
                            
                          
                            <!--form group for paid amount-->
                            <div class="form-group" id="payment_tk">

                            </div>
                            <div class="form-group" id="date_field">

                            </div>
                            <!--form group for warenty type-->
                             
                            <div class="form-group" id="warrenty_type_input">

                            </div>
                            <!--form group for product sl no-->
                            <div id="product_warranty" class="form-group">

                            </div>

                            <button type="submit" class="btn btn-success mr-2">Submit</button>
                            <a href="{{route('products.index')}}" class="btn btn-danger">Cancel</a>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">{{$product->product_sku}}</h4>
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <tr>
                                    <th>Barcode :</th>
                                    <td>{{$product->product_barcode?$product->product_barcode:'Not set'}}</td>
                                </tr>
                                <tr>
                                    <th>Image :</th>
                                    <td><img src="{{url($product->image)}}" width="100px"></td>
                                </tr>
                                <tr>
                                    <th>Product Name :</th>
                                    <td>{{$product->product_sku}}</td>
                                </tr>
                                <tr>
                                    <th>Category :</th>
                                    <td>{{$product->category->category_name}}</td>
                                </tr>
                                <tr>
                                    <th>Configuration :</th>
                                    <td>
                                        <?php $confiqr='';
                          $a=0;?>
                                        @if($product->attributeset)
                                        @foreach($product->attributeset->attribute as $confiq)
                                        <?php $confiqr.= $confiq->attribute_name.' : '.$product->submittedattribute[$a++]->attribute_name."<br>"?>
                                        @endforeach
                                        @endif
                                        {!!$confiqr!!}</td>
                                </tr>

                                <tr>
                                    <th>Expirable ?</th>
                                    <td>{{$product->category->is_expirable ? 'Yes' : 'No'}}</td>
                                </tr>
                                <input type="hidden" id="is_expirable" value="{{$product->category->is_expirable}}">
                                <input type="hidden" id="is_warrantable" value="{{$product->category->is_warrantable}}">    
                                <tr>
                                    <th>warrantable ?</th>
                                    <td>{{$product->category->is_warrantable ? 'Yes' : 'No'}}</td>
                                </tr>
                                <tr class="table-active">
                                    <th>Last Sold unit Price:</th>
                                    <td>{{$suggested_price?$suggested_price->sell_unit_price:"Yet not purchased"}}
                                    </td>
                                </tr>
                                <tr class="table-active">
                                    <th>Last Purchased unit Price:</th>
                                    <td>{{$suggested_price?$suggested_price->purchase_unit_price:"Yet not purchased"}}
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body" id="pro_details">
                    </div></div>
            </div>
        </div>
    </div>

</div>


<script>
     //for has invoice any purchase?
     function invoiceCheck() {

var supplier_id = document.getElementById("supplier_id").value;
var invoice_no = document.getElementById("invoice_no").value;
var sentData=supplier_id+"|"+invoice_no;
var route = "{{url('')}}/invoice_details/"+sentData;
$.get(route, function(data) {
    //console.log(data);
    if(data){
    alert('Invoice Number Has Already Registered');
    $('#pro_details').empty();
    $('#pro_details').html(data);
}else{
    $('#pro_details').empty();
}
});
}
    //multiply unit price n qty
    function getprice() {
        var p_unit_price = document.getElementById("p_unit_price").value;
        var purchase_qty = document.getElementById("purchase_qty").value;
        var total_price = (purchase_qty * p_unit_price);

        $('#total_price').val(total_price);
    }

    //input field for duration
    function showduration(durationId) {
        var d_id=durationId;
        var spanTag="<span class='input-group-text text-danger' id='basic-addon"+d_id+"'>month</span>";
        var durationId = 'warranty_duration' + durationId;
        var checkType = $('#' + durationId).attr('type')
        if (checkType == 'text') {
            $('#' + durationId).attr('type', 'hidden');
            $('#add_on_haha' + d_id).empty();
        } else {
            $('#' + durationId).attr('type', 'text');
            $('#add_on_haha' + d_id).prepend(spanTag);
        }
    }


    //check for warranty and is_expirable
    function getProductInfo() {
        $('#warrenty_type_input').empty();
        $('#product_warranty').empty();
        $('#expireDate').empty();
        var product_id = document.getElementById("product_id").value;
        var purchase_qty = document.getElementById("purchase_qty").value;
        var warrenty = document.getElementById("is_warrantable").value;
        var expirable = document.getElementById("is_expirable").value;
        //alert(expirable);

        //check for warranty
        if (warrenty == 1) {
            $.ajax({
                type: 'GET',
                url: "<?=url('warranty_type_data')?>",
                // data:'',
                success: function (html) {
                    $('#warrenty_type_input').prepend(html);
                    var productwarentyfield = "<label>Warranty Product Serial</label>";

                    for (var i = 0; i < purchase_qty; i++) {
                        productwarentyfield +=
                            "<input type='text' name='product_sl_no[]' class='form-control border-primary' required>";
                    }
                    $('#product_warranty').prepend(productwarentyfield);
                }
            });
        }

        //check for is_expirable
        if (expirable == 1) {
            expirablefield =
                "<label>Expire Date</label><input type='date' name='expire_date' class='form-control' required>";

            $('#expireDate').prepend(expirablefield);

        }
    }


    function addpaidvalue() {

        var payment_method_id = document.getElementById("payment_method_id").value;

        $('#payment_tk').empty(paid_amount);
        payment_method_id = payment_method_id.split(" | ");
        console.log(payment_method_id[0]);
        if (payment_method_id[0]>0) {
            var paid_amount = "<label for='paid_amount'>Paid Amount</label>";
            paid_amount += "<input type='number' class='form-control' name='paid_amount' id='paid_amount' step='.01'>";
            if (payment_method_id[1] == 1) {
                paid_amount +=
                    "<label for='check_no'>Check No <i class='text-danger'>( Optional )</i></label><input type='text' class='form-control' name='check_no' id='check_no' onblur='check_disposal_field()'>";
            }
            $('#payment_tk').prepend(paid_amount);
        }
    }
    //extra field for cheque disposal date 
    function check_disposal_field() {
        var payment_method_id = document.getElementById("payment_method_id").value;
        $('#date_field').empty(paid_amount);
        payment_method_id = payment_method_id.split(" | ");
        if (payment_method_id[1] == 1) {
            var paid_amount =
                "<label for='cheque_disposal_date'>Cheque Disposal Date <i class='text-danger'>( Set if not Today )</i></label>";
            paid_amount +=
                "<input type='date' class='form-control' name='cheque_disposal_date' id='cheque_disposal_date'>";
            $('#date_field').prepend(paid_amount);
        }
    }
</script>
@endsection
