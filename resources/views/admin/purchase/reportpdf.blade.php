
<!-- inject:css -->
<link rel="stylesheet" href="{{asset('admin')}}/css/style.css">

<div class="row">
<div class="col-md-12 d-flex align-items-stretch grid-margin">
  <div class="row flex-grow row_table_responsive">
	<div class="col-md-12">
	  <div class="card">
		<div class="card-body">
		  <h4 class="text-center">Purchase History</h4>
		  <div class="table-responsive">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>ID</th>
                          <th>Date</th>
                          <th>Product Sku</th>
                          <th>Qty</th>
                          <th>unit price</th>
                          <th>SubTotal</th>
                          <th>Supplier</th>
                          <th>Purchased by</th>
                        </tr>
                      </thead>
                      <tbody>
            <?php $i =1;
            $total=0; ?>
					  @forelse($purchases as $cat)
                        <tr>
                          <td>{{$i++}}</td>
                          <?php $time=strtotime($cat->created_at);
                            $date=date("d-M-Y",$time);
                            $timm=date("h:i:s",$time);?>
                            <td title="{{$timm}}">{{$date}}</td>
                          <td>{{str_before($cat->product->product_sku,"|")}}<br>{{str_after($cat->product->product_sku,"|")}}</td>
                          <td>{{$cat->quantity}}</td>
                          <td>{{$cat->purchase_unit_price}}</td>
                          <?php
                          $subtotal=$cat->quantity*$cat->purchase_unit_price;
                          $total=$total+$subtotal;?>
                          <td>tk. {{$subtotal}}</td>
                          <td>{{$cat->supplier->supplier_name}}</td>
                          <td title="{{$cat->users->name}}">{{$cat->users->user_type}}</td>
                          <!-- <td>
                            <a href="{{route('attribute_sets.edit',$cat->id)}}" class="btn btn-link">Edit</a>
							            </td> -->
                        </tr>
					@empty
						<tr>
							<td colspan="8">There is no records available</td>
						</tr>
					@endforelse
                      </tbody>
                      <tfoot>
                        <th colspan="5" class="text-right">Total Purchased Amount:</th>
                        <th colspan="3" class="text-left">tk. {{$total}}</th>
                        
                      </tfoot>
                    </table>
                  </div>
		</div>
	  </div>
	</div>
  </div>
</div>

</div>
