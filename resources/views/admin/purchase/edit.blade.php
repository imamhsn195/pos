@extends('layouts.admin')

@section('content')

<div class="row">
    <div class="col-md-6 d-flex align-items-stretch grid-margin">
        <div class="row flex-grow">
            <div class="col-12">
                <form class="forms-sample" method="post"
                action="{{route('purchases.update',$purchase->first()->id)}}">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Purchase Edit Form</h4>
                            @csrf
                            @method('put')
                            {{--Model-For-Edit-Invoice  --}}
                    <input type="hidden" name="invoice_no" value="{{$purchase->first()->invoice_no}}">
                            <div class="modal-body" id="exampleModalBody">
                                <table class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Product Name</th>
                                            <th>Qty</th>
                                            <th>Purchase Price</th>
                                            <th>Selling Price</th>
                                        </tr>
                                    </thead>
                                    <tbody id="editModalTable">
                                        @forelse ($purchase as $data)
                                        <tr>
                                            <input type="hidden" name="purchase_id[]" value="{{$data->id}}">
                                            <th>{{str_before($data->product->product_sku,'|')}}</th>
                                            <th><input type="number" name="quantity[]" class="form-control-sm"
                                                    value="{{$data->quantity}}"><input type="hidden" name="old_quantity[]" class="form-control-sm"
                                                    value="{{$data->quantity}}"></th>
                                            <th><input type="number" name="purchase_unit_price[]"
                                                    class="form-control-sm" value="{{$data->purchase_unit_price}}"><input type="hidden" name="old_purchase_unit_price[]"
                                                    class="form-control-sm" value="{{$data->purchase_unit_price}}"></th>
                                            <th><input type="number" name="sell_unit_price[]" class="form-control-sm"
                                                    value="{{$data->sell_unit_price}}"><input type="hidden" name="old_sell_unit_price[]" class="form-control-sm"
                                                    value="{{$data->sell_unit_price}}"></th>
                                        </tr>
                                        @empty

                                        @endforelse

                                    </tbody>
                                </table>
                            </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-success mr-2">Submit</button>
            
            {{-- <a href="{{route('categories.index')}}" class="btn btn-danger">Cancel</a> --}}
            </form>
            </div>
            {{--Model-For-Edit-Invoice  --}}

            
        </div>
    </div>
</div>


@endsection
