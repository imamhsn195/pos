@extends('layouts.admin')
@section('content')
<div class="row">
        <div class="col-md-12 d-flex align-items-stretch grid-margin">
          <div class="row flex-grow row_table_responsive">
            <div class="col-md-12"><br>
                <div class="table-responsive">
                    <table class="datatableclass table table-striped table-hover" id="datatable">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Invoice No</th>
                                <th>Supplier</th>
                                <th>Qty</th>
                                <th>Created By</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
          </div>
        </div>
    </div>
@endsection
@push('js')
<script>
        $(function () {
            $('#datatable').DataTable({
                serverSide: true,
                processing: true,
                ajax: 'datatabledata',
                columns: [
                    {data: 'created_at'},
                    {data: 'invoice_no'},
                    {data: 'supplier.supplier_name'},
                    {data: 'Total'},
                    {data: 'users.name'},
                ]
            });
        });
    </script>
@endpush