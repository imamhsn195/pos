@extends('layouts.admin')

@push( 'js' )
    <script src="{{url( 'vendor/jquery.min.js' )}}"></script>

    <script src="{{url( 'vendor/Chart.min.js' )}}"></script>

    <script src="{{url( 'vendor/create-charts.js' )}}"></script>
@endpush
@section( 'content' )
    <br>
    <div class="row">
        <!-- Area Chart Example-->
        <div class="card col-md-12">
            <div class ="card-header">Sales</div>
            <div class="card-body">
                <div id="myAreaChart" width="100%" height="60"></div>
            </div>
            <div class="card-footer small text-muted">Updated at @php  echo date('F j, Y', time() ) @endphp</div>
        </div>
    </div>

    
 @endsection
